package fr.n7.game.xtext.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.n7.game.xtext.services.GameGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalGameParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_BOOLEAN", "RULE_COMPARATEUR", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Objets'", "':'", "'Transformations'", "'Connaissances'", "'Explorateur'", "'Personnes'", "'Territoire'", "'-'", "'taille'", "'visible'", "'descriptions'", "'condition'", "'objets_in'", "'objets_out'", "'connaissances'", "'objets'", "'Lieux'", "'Chemins'", "'deposable'", "'depart'", "'fin'", "'personnes'", "'lieu_in'", "'lieu_out'", "'ouvert'", "'obligatoire'", "'objets_recus'", "'objets_conso'", "'interactions'", "'actions'", "'fin_interaction'", "'texte'", "'||'", "'&&'", "'!'"
    };
    public static final int RULE_BOOLEAN=7;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int RULE_COMPARATEUR=8;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalGameParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalGameParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalGameParser.tokenNames; }
    public String getGrammarFileName() { return "InternalGame.g"; }


    	private GameGrammarAccess grammarAccess;

    	public void setGrammarAccess(GameGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleJeu"
    // InternalGame.g:53:1: entryRuleJeu : ruleJeu EOF ;
    public final void entryRuleJeu() throws RecognitionException {
        try {
            // InternalGame.g:54:1: ( ruleJeu EOF )
            // InternalGame.g:55:1: ruleJeu EOF
            {
             before(grammarAccess.getJeuRule()); 
            pushFollow(FOLLOW_1);
            ruleJeu();

            state._fsp--;

             after(grammarAccess.getJeuRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleJeu"


    // $ANTLR start "ruleJeu"
    // InternalGame.g:62:1: ruleJeu : ( ( rule__Jeu__Group__0 ) ) ;
    public final void ruleJeu() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:66:2: ( ( ( rule__Jeu__Group__0 ) ) )
            // InternalGame.g:67:2: ( ( rule__Jeu__Group__0 ) )
            {
            // InternalGame.g:67:2: ( ( rule__Jeu__Group__0 ) )
            // InternalGame.g:68:3: ( rule__Jeu__Group__0 )
            {
             before(grammarAccess.getJeuAccess().getGroup()); 
            // InternalGame.g:69:3: ( rule__Jeu__Group__0 )
            // InternalGame.g:69:4: rule__Jeu__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Jeu__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getJeuAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleJeu"


    // $ANTLR start "entryRuleObjet"
    // InternalGame.g:78:1: entryRuleObjet : ruleObjet EOF ;
    public final void entryRuleObjet() throws RecognitionException {
        try {
            // InternalGame.g:79:1: ( ruleObjet EOF )
            // InternalGame.g:80:1: ruleObjet EOF
            {
             before(grammarAccess.getObjetRule()); 
            pushFollow(FOLLOW_1);
            ruleObjet();

            state._fsp--;

             after(grammarAccess.getObjetRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleObjet"


    // $ANTLR start "ruleObjet"
    // InternalGame.g:87:1: ruleObjet : ( ( rule__Objet__Group__0 ) ) ;
    public final void ruleObjet() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:91:2: ( ( ( rule__Objet__Group__0 ) ) )
            // InternalGame.g:92:2: ( ( rule__Objet__Group__0 ) )
            {
            // InternalGame.g:92:2: ( ( rule__Objet__Group__0 ) )
            // InternalGame.g:93:3: ( rule__Objet__Group__0 )
            {
             before(grammarAccess.getObjetAccess().getGroup()); 
            // InternalGame.g:94:3: ( rule__Objet__Group__0 )
            // InternalGame.g:94:4: rule__Objet__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Objet__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getObjetAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleObjet"


    // $ANTLR start "entryRuleTransformation"
    // InternalGame.g:103:1: entryRuleTransformation : ruleTransformation EOF ;
    public final void entryRuleTransformation() throws RecognitionException {
        try {
            // InternalGame.g:104:1: ( ruleTransformation EOF )
            // InternalGame.g:105:1: ruleTransformation EOF
            {
             before(grammarAccess.getTransformationRule()); 
            pushFollow(FOLLOW_1);
            ruleTransformation();

            state._fsp--;

             after(grammarAccess.getTransformationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTransformation"


    // $ANTLR start "ruleTransformation"
    // InternalGame.g:112:1: ruleTransformation : ( ( rule__Transformation__Group__0 ) ) ;
    public final void ruleTransformation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:116:2: ( ( ( rule__Transformation__Group__0 ) ) )
            // InternalGame.g:117:2: ( ( rule__Transformation__Group__0 ) )
            {
            // InternalGame.g:117:2: ( ( rule__Transformation__Group__0 ) )
            // InternalGame.g:118:3: ( rule__Transformation__Group__0 )
            {
             before(grammarAccess.getTransformationAccess().getGroup()); 
            // InternalGame.g:119:3: ( rule__Transformation__Group__0 )
            // InternalGame.g:119:4: rule__Transformation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Transformation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTransformationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTransformation"


    // $ANTLR start "entryRuleConnaissance"
    // InternalGame.g:128:1: entryRuleConnaissance : ruleConnaissance EOF ;
    public final void entryRuleConnaissance() throws RecognitionException {
        try {
            // InternalGame.g:129:1: ( ruleConnaissance EOF )
            // InternalGame.g:130:1: ruleConnaissance EOF
            {
             before(grammarAccess.getConnaissanceRule()); 
            pushFollow(FOLLOW_1);
            ruleConnaissance();

            state._fsp--;

             after(grammarAccess.getConnaissanceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConnaissance"


    // $ANTLR start "ruleConnaissance"
    // InternalGame.g:137:1: ruleConnaissance : ( ( rule__Connaissance__Group__0 ) ) ;
    public final void ruleConnaissance() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:141:2: ( ( ( rule__Connaissance__Group__0 ) ) )
            // InternalGame.g:142:2: ( ( rule__Connaissance__Group__0 ) )
            {
            // InternalGame.g:142:2: ( ( rule__Connaissance__Group__0 ) )
            // InternalGame.g:143:3: ( rule__Connaissance__Group__0 )
            {
             before(grammarAccess.getConnaissanceAccess().getGroup()); 
            // InternalGame.g:144:3: ( rule__Connaissance__Group__0 )
            // InternalGame.g:144:4: rule__Connaissance__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Connaissance__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConnaissanceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConnaissance"


    // $ANTLR start "entryRuleExplorateur"
    // InternalGame.g:153:1: entryRuleExplorateur : ruleExplorateur EOF ;
    public final void entryRuleExplorateur() throws RecognitionException {
        try {
            // InternalGame.g:154:1: ( ruleExplorateur EOF )
            // InternalGame.g:155:1: ruleExplorateur EOF
            {
             before(grammarAccess.getExplorateurRule()); 
            pushFollow(FOLLOW_1);
            ruleExplorateur();

            state._fsp--;

             after(grammarAccess.getExplorateurRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExplorateur"


    // $ANTLR start "ruleExplorateur"
    // InternalGame.g:162:1: ruleExplorateur : ( ( rule__Explorateur__Group__0 ) ) ;
    public final void ruleExplorateur() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:166:2: ( ( ( rule__Explorateur__Group__0 ) ) )
            // InternalGame.g:167:2: ( ( rule__Explorateur__Group__0 ) )
            {
            // InternalGame.g:167:2: ( ( rule__Explorateur__Group__0 ) )
            // InternalGame.g:168:3: ( rule__Explorateur__Group__0 )
            {
             before(grammarAccess.getExplorateurAccess().getGroup()); 
            // InternalGame.g:169:3: ( rule__Explorateur__Group__0 )
            // InternalGame.g:169:4: rule__Explorateur__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Explorateur__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExplorateurAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExplorateur"


    // $ANTLR start "entryRuleTerritoire"
    // InternalGame.g:178:1: entryRuleTerritoire : ruleTerritoire EOF ;
    public final void entryRuleTerritoire() throws RecognitionException {
        try {
            // InternalGame.g:179:1: ( ruleTerritoire EOF )
            // InternalGame.g:180:1: ruleTerritoire EOF
            {
             before(grammarAccess.getTerritoireRule()); 
            pushFollow(FOLLOW_1);
            ruleTerritoire();

            state._fsp--;

             after(grammarAccess.getTerritoireRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTerritoire"


    // $ANTLR start "ruleTerritoire"
    // InternalGame.g:187:1: ruleTerritoire : ( ( rule__Territoire__Group__0 ) ) ;
    public final void ruleTerritoire() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:191:2: ( ( ( rule__Territoire__Group__0 ) ) )
            // InternalGame.g:192:2: ( ( rule__Territoire__Group__0 ) )
            {
            // InternalGame.g:192:2: ( ( rule__Territoire__Group__0 ) )
            // InternalGame.g:193:3: ( rule__Territoire__Group__0 )
            {
             before(grammarAccess.getTerritoireAccess().getGroup()); 
            // InternalGame.g:194:3: ( rule__Territoire__Group__0 )
            // InternalGame.g:194:4: rule__Territoire__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Territoire__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTerritoireAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTerritoire"


    // $ANTLR start "entryRuleLieu"
    // InternalGame.g:203:1: entryRuleLieu : ruleLieu EOF ;
    public final void entryRuleLieu() throws RecognitionException {
        try {
            // InternalGame.g:204:1: ( ruleLieu EOF )
            // InternalGame.g:205:1: ruleLieu EOF
            {
             before(grammarAccess.getLieuRule()); 
            pushFollow(FOLLOW_1);
            ruleLieu();

            state._fsp--;

             after(grammarAccess.getLieuRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLieu"


    // $ANTLR start "ruleLieu"
    // InternalGame.g:212:1: ruleLieu : ( ( rule__Lieu__Group__0 ) ) ;
    public final void ruleLieu() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:216:2: ( ( ( rule__Lieu__Group__0 ) ) )
            // InternalGame.g:217:2: ( ( rule__Lieu__Group__0 ) )
            {
            // InternalGame.g:217:2: ( ( rule__Lieu__Group__0 ) )
            // InternalGame.g:218:3: ( rule__Lieu__Group__0 )
            {
             before(grammarAccess.getLieuAccess().getGroup()); 
            // InternalGame.g:219:3: ( rule__Lieu__Group__0 )
            // InternalGame.g:219:4: rule__Lieu__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Lieu__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLieuAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLieu"


    // $ANTLR start "entryRuleChemin"
    // InternalGame.g:228:1: entryRuleChemin : ruleChemin EOF ;
    public final void entryRuleChemin() throws RecognitionException {
        try {
            // InternalGame.g:229:1: ( ruleChemin EOF )
            // InternalGame.g:230:1: ruleChemin EOF
            {
             before(grammarAccess.getCheminRule()); 
            pushFollow(FOLLOW_1);
            ruleChemin();

            state._fsp--;

             after(grammarAccess.getCheminRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleChemin"


    // $ANTLR start "ruleChemin"
    // InternalGame.g:237:1: ruleChemin : ( ( rule__Chemin__Group__0 ) ) ;
    public final void ruleChemin() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:241:2: ( ( ( rule__Chemin__Group__0 ) ) )
            // InternalGame.g:242:2: ( ( rule__Chemin__Group__0 ) )
            {
            // InternalGame.g:242:2: ( ( rule__Chemin__Group__0 ) )
            // InternalGame.g:243:3: ( rule__Chemin__Group__0 )
            {
             before(grammarAccess.getCheminAccess().getGroup()); 
            // InternalGame.g:244:3: ( rule__Chemin__Group__0 )
            // InternalGame.g:244:4: rule__Chemin__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCheminAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleChemin"


    // $ANTLR start "entryRulePersonne"
    // InternalGame.g:253:1: entryRulePersonne : rulePersonne EOF ;
    public final void entryRulePersonne() throws RecognitionException {
        try {
            // InternalGame.g:254:1: ( rulePersonne EOF )
            // InternalGame.g:255:1: rulePersonne EOF
            {
             before(grammarAccess.getPersonneRule()); 
            pushFollow(FOLLOW_1);
            rulePersonne();

            state._fsp--;

             after(grammarAccess.getPersonneRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePersonne"


    // $ANTLR start "rulePersonne"
    // InternalGame.g:262:1: rulePersonne : ( ( rule__Personne__Group__0 ) ) ;
    public final void rulePersonne() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:266:2: ( ( ( rule__Personne__Group__0 ) ) )
            // InternalGame.g:267:2: ( ( rule__Personne__Group__0 ) )
            {
            // InternalGame.g:267:2: ( ( rule__Personne__Group__0 ) )
            // InternalGame.g:268:3: ( rule__Personne__Group__0 )
            {
             before(grammarAccess.getPersonneAccess().getGroup()); 
            // InternalGame.g:269:3: ( rule__Personne__Group__0 )
            // InternalGame.g:269:4: rule__Personne__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Personne__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPersonneAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePersonne"


    // $ANTLR start "entryRuleInteraction"
    // InternalGame.g:278:1: entryRuleInteraction : ruleInteraction EOF ;
    public final void entryRuleInteraction() throws RecognitionException {
        try {
            // InternalGame.g:279:1: ( ruleInteraction EOF )
            // InternalGame.g:280:1: ruleInteraction EOF
            {
             before(grammarAccess.getInteractionRule()); 
            pushFollow(FOLLOW_1);
            ruleInteraction();

            state._fsp--;

             after(grammarAccess.getInteractionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInteraction"


    // $ANTLR start "ruleInteraction"
    // InternalGame.g:287:1: ruleInteraction : ( ( rule__Interaction__Group__0 ) ) ;
    public final void ruleInteraction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:291:2: ( ( ( rule__Interaction__Group__0 ) ) )
            // InternalGame.g:292:2: ( ( rule__Interaction__Group__0 ) )
            {
            // InternalGame.g:292:2: ( ( rule__Interaction__Group__0 ) )
            // InternalGame.g:293:3: ( rule__Interaction__Group__0 )
            {
             before(grammarAccess.getInteractionAccess().getGroup()); 
            // InternalGame.g:294:3: ( rule__Interaction__Group__0 )
            // InternalGame.g:294:4: rule__Interaction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Interaction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInteractionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInteraction"


    // $ANTLR start "entryRuleAction"
    // InternalGame.g:303:1: entryRuleAction : ruleAction EOF ;
    public final void entryRuleAction() throws RecognitionException {
        try {
            // InternalGame.g:304:1: ( ruleAction EOF )
            // InternalGame.g:305:1: ruleAction EOF
            {
             before(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            ruleAction();

            state._fsp--;

             after(grammarAccess.getActionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalGame.g:312:1: ruleAction : ( ( rule__Action__Group__0 ) ) ;
    public final void ruleAction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:316:2: ( ( ( rule__Action__Group__0 ) ) )
            // InternalGame.g:317:2: ( ( rule__Action__Group__0 ) )
            {
            // InternalGame.g:317:2: ( ( rule__Action__Group__0 ) )
            // InternalGame.g:318:3: ( rule__Action__Group__0 )
            {
             before(grammarAccess.getActionAccess().getGroup()); 
            // InternalGame.g:319:3: ( rule__Action__Group__0 )
            // InternalGame.g:319:4: rule__Action__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Action__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleDescription"
    // InternalGame.g:328:1: entryRuleDescription : ruleDescription EOF ;
    public final void entryRuleDescription() throws RecognitionException {
        try {
            // InternalGame.g:329:1: ( ruleDescription EOF )
            // InternalGame.g:330:1: ruleDescription EOF
            {
             before(grammarAccess.getDescriptionRule()); 
            pushFollow(FOLLOW_1);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getDescriptionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDescription"


    // $ANTLR start "ruleDescription"
    // InternalGame.g:337:1: ruleDescription : ( ( rule__Description__Group__0 ) ) ;
    public final void ruleDescription() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:341:2: ( ( ( rule__Description__Group__0 ) ) )
            // InternalGame.g:342:2: ( ( rule__Description__Group__0 ) )
            {
            // InternalGame.g:342:2: ( ( rule__Description__Group__0 ) )
            // InternalGame.g:343:3: ( rule__Description__Group__0 )
            {
             before(grammarAccess.getDescriptionAccess().getGroup()); 
            // InternalGame.g:344:3: ( rule__Description__Group__0 )
            // InternalGame.g:344:4: rule__Description__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Description__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDescriptionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDescription"


    // $ANTLR start "entryRuleCondition"
    // InternalGame.g:353:1: entryRuleCondition : ruleCondition EOF ;
    public final void entryRuleCondition() throws RecognitionException {
        try {
            // InternalGame.g:354:1: ( ruleCondition EOF )
            // InternalGame.g:355:1: ruleCondition EOF
            {
             before(grammarAccess.getConditionRule()); 
            pushFollow(FOLLOW_1);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getConditionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCondition"


    // $ANTLR start "ruleCondition"
    // InternalGame.g:362:1: ruleCondition : ( ( rule__Condition__Group__0 ) ) ;
    public final void ruleCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:366:2: ( ( ( rule__Condition__Group__0 ) ) )
            // InternalGame.g:367:2: ( ( rule__Condition__Group__0 ) )
            {
            // InternalGame.g:367:2: ( ( rule__Condition__Group__0 ) )
            // InternalGame.g:368:3: ( rule__Condition__Group__0 )
            {
             before(grammarAccess.getConditionAccess().getGroup()); 
            // InternalGame.g:369:3: ( rule__Condition__Group__0 )
            // InternalGame.g:369:4: rule__Condition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Condition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCondition"


    // $ANTLR start "entryRuleConditionEt"
    // InternalGame.g:378:1: entryRuleConditionEt : ruleConditionEt EOF ;
    public final void entryRuleConditionEt() throws RecognitionException {
        try {
            // InternalGame.g:379:1: ( ruleConditionEt EOF )
            // InternalGame.g:380:1: ruleConditionEt EOF
            {
             before(grammarAccess.getConditionEtRule()); 
            pushFollow(FOLLOW_1);
            ruleConditionEt();

            state._fsp--;

             after(grammarAccess.getConditionEtRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConditionEt"


    // $ANTLR start "ruleConditionEt"
    // InternalGame.g:387:1: ruleConditionEt : ( ( rule__ConditionEt__Group__0 ) ) ;
    public final void ruleConditionEt() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:391:2: ( ( ( rule__ConditionEt__Group__0 ) ) )
            // InternalGame.g:392:2: ( ( rule__ConditionEt__Group__0 ) )
            {
            // InternalGame.g:392:2: ( ( rule__ConditionEt__Group__0 ) )
            // InternalGame.g:393:3: ( rule__ConditionEt__Group__0 )
            {
             before(grammarAccess.getConditionEtAccess().getGroup()); 
            // InternalGame.g:394:3: ( rule__ConditionEt__Group__0 )
            // InternalGame.g:394:4: rule__ConditionEt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ConditionEt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConditionEtAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConditionEt"


    // $ANTLR start "entryRuleConditionTest"
    // InternalGame.g:403:1: entryRuleConditionTest : ruleConditionTest EOF ;
    public final void entryRuleConditionTest() throws RecognitionException {
        try {
            // InternalGame.g:404:1: ( ruleConditionTest EOF )
            // InternalGame.g:405:1: ruleConditionTest EOF
            {
             before(grammarAccess.getConditionTestRule()); 
            pushFollow(FOLLOW_1);
            ruleConditionTest();

            state._fsp--;

             after(grammarAccess.getConditionTestRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConditionTest"


    // $ANTLR start "ruleConditionTest"
    // InternalGame.g:412:1: ruleConditionTest : ( ( rule__ConditionTest__Alternatives ) ) ;
    public final void ruleConditionTest() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:416:2: ( ( ( rule__ConditionTest__Alternatives ) ) )
            // InternalGame.g:417:2: ( ( rule__ConditionTest__Alternatives ) )
            {
            // InternalGame.g:417:2: ( ( rule__ConditionTest__Alternatives ) )
            // InternalGame.g:418:3: ( rule__ConditionTest__Alternatives )
            {
             before(grammarAccess.getConditionTestAccess().getAlternatives()); 
            // InternalGame.g:419:3: ( rule__ConditionTest__Alternatives )
            // InternalGame.g:419:4: rule__ConditionTest__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ConditionTest__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getConditionTestAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConditionTest"


    // $ANTLR start "entryRuleConditionBoolean"
    // InternalGame.g:428:1: entryRuleConditionBoolean : ruleConditionBoolean EOF ;
    public final void entryRuleConditionBoolean() throws RecognitionException {
        try {
            // InternalGame.g:429:1: ( ruleConditionBoolean EOF )
            // InternalGame.g:430:1: ruleConditionBoolean EOF
            {
             before(grammarAccess.getConditionBooleanRule()); 
            pushFollow(FOLLOW_1);
            ruleConditionBoolean();

            state._fsp--;

             after(grammarAccess.getConditionBooleanRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConditionBoolean"


    // $ANTLR start "ruleConditionBoolean"
    // InternalGame.g:437:1: ruleConditionBoolean : ( ( rule__ConditionBoolean__ValeurAssignment ) ) ;
    public final void ruleConditionBoolean() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:441:2: ( ( ( rule__ConditionBoolean__ValeurAssignment ) ) )
            // InternalGame.g:442:2: ( ( rule__ConditionBoolean__ValeurAssignment ) )
            {
            // InternalGame.g:442:2: ( ( rule__ConditionBoolean__ValeurAssignment ) )
            // InternalGame.g:443:3: ( rule__ConditionBoolean__ValeurAssignment )
            {
             before(grammarAccess.getConditionBooleanAccess().getValeurAssignment()); 
            // InternalGame.g:444:3: ( rule__ConditionBoolean__ValeurAssignment )
            // InternalGame.g:444:4: rule__ConditionBoolean__ValeurAssignment
            {
            pushFollow(FOLLOW_2);
            rule__ConditionBoolean__ValeurAssignment();

            state._fsp--;


            }

             after(grammarAccess.getConditionBooleanAccess().getValeurAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConditionBoolean"


    // $ANTLR start "entryRuleConditionConnaissance"
    // InternalGame.g:453:1: entryRuleConditionConnaissance : ruleConditionConnaissance EOF ;
    public final void entryRuleConditionConnaissance() throws RecognitionException {
        try {
            // InternalGame.g:454:1: ( ruleConditionConnaissance EOF )
            // InternalGame.g:455:1: ruleConditionConnaissance EOF
            {
             before(grammarAccess.getConditionConnaissanceRule()); 
            pushFollow(FOLLOW_1);
            ruleConditionConnaissance();

            state._fsp--;

             after(grammarAccess.getConditionConnaissanceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConditionConnaissance"


    // $ANTLR start "ruleConditionConnaissance"
    // InternalGame.g:462:1: ruleConditionConnaissance : ( ( rule__ConditionConnaissance__Group__0 ) ) ;
    public final void ruleConditionConnaissance() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:466:2: ( ( ( rule__ConditionConnaissance__Group__0 ) ) )
            // InternalGame.g:467:2: ( ( rule__ConditionConnaissance__Group__0 ) )
            {
            // InternalGame.g:467:2: ( ( rule__ConditionConnaissance__Group__0 ) )
            // InternalGame.g:468:3: ( rule__ConditionConnaissance__Group__0 )
            {
             before(grammarAccess.getConditionConnaissanceAccess().getGroup()); 
            // InternalGame.g:469:3: ( rule__ConditionConnaissance__Group__0 )
            // InternalGame.g:469:4: rule__ConditionConnaissance__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ConditionConnaissance__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConditionConnaissanceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConditionConnaissance"


    // $ANTLR start "entryRuleConditionObjet"
    // InternalGame.g:478:1: entryRuleConditionObjet : ruleConditionObjet EOF ;
    public final void entryRuleConditionObjet() throws RecognitionException {
        try {
            // InternalGame.g:479:1: ( ruleConditionObjet EOF )
            // InternalGame.g:480:1: ruleConditionObjet EOF
            {
             before(grammarAccess.getConditionObjetRule()); 
            pushFollow(FOLLOW_1);
            ruleConditionObjet();

            state._fsp--;

             after(grammarAccess.getConditionObjetRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConditionObjet"


    // $ANTLR start "ruleConditionObjet"
    // InternalGame.g:487:1: ruleConditionObjet : ( ( rule__ConditionObjet__Group__0 ) ) ;
    public final void ruleConditionObjet() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:491:2: ( ( ( rule__ConditionObjet__Group__0 ) ) )
            // InternalGame.g:492:2: ( ( rule__ConditionObjet__Group__0 ) )
            {
            // InternalGame.g:492:2: ( ( rule__ConditionObjet__Group__0 ) )
            // InternalGame.g:493:3: ( rule__ConditionObjet__Group__0 )
            {
             before(grammarAccess.getConditionObjetAccess().getGroup()); 
            // InternalGame.g:494:3: ( rule__ConditionObjet__Group__0 )
            // InternalGame.g:494:4: rule__ConditionObjet__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ConditionObjet__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConditionObjetAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConditionObjet"


    // $ANTLR start "rule__ConditionTest__Alternatives"
    // InternalGame.g:502:1: rule__ConditionTest__Alternatives : ( ( ruleConditionBoolean ) | ( ruleConditionConnaissance ) | ( ruleConditionObjet ) );
    public final void rule__ConditionTest__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:506:1: ( ( ruleConditionBoolean ) | ( ruleConditionConnaissance ) | ( ruleConditionObjet ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case RULE_BOOLEAN:
                {
                alt1=1;
                }
                break;
            case 47:
                {
                alt1=2;
                }
                break;
            case RULE_ID:
                {
                int LA1_3 = input.LA(2);

                if ( (LA1_3==EOF||LA1_3==15||LA1_3==17||(LA1_3>=19 && LA1_3<=20)||(LA1_3>=22 && LA1_3<=23)||LA1_3==25||(LA1_3>=27 && LA1_3<=28)||(LA1_3>=32 && LA1_3<=34)||LA1_3==38||LA1_3==41||LA1_3==43||(LA1_3>=45 && LA1_3<=46)) ) {
                    alt1=2;
                }
                else if ( (LA1_3==RULE_COMPARATEUR) ) {
                    alt1=3;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 3, input);

                    throw nvae;
                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalGame.g:507:2: ( ruleConditionBoolean )
                    {
                    // InternalGame.g:507:2: ( ruleConditionBoolean )
                    // InternalGame.g:508:3: ruleConditionBoolean
                    {
                     before(grammarAccess.getConditionTestAccess().getConditionBooleanParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleConditionBoolean();

                    state._fsp--;

                     after(grammarAccess.getConditionTestAccess().getConditionBooleanParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGame.g:513:2: ( ruleConditionConnaissance )
                    {
                    // InternalGame.g:513:2: ( ruleConditionConnaissance )
                    // InternalGame.g:514:3: ruleConditionConnaissance
                    {
                     before(grammarAccess.getConditionTestAccess().getConditionConnaissanceParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleConditionConnaissance();

                    state._fsp--;

                     after(grammarAccess.getConditionTestAccess().getConditionConnaissanceParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGame.g:519:2: ( ruleConditionObjet )
                    {
                    // InternalGame.g:519:2: ( ruleConditionObjet )
                    // InternalGame.g:520:3: ruleConditionObjet
                    {
                     before(grammarAccess.getConditionTestAccess().getConditionObjetParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleConditionObjet();

                    state._fsp--;

                     after(grammarAccess.getConditionTestAccess().getConditionObjetParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionTest__Alternatives"


    // $ANTLR start "rule__Jeu__Group__0"
    // InternalGame.g:529:1: rule__Jeu__Group__0 : rule__Jeu__Group__0__Impl rule__Jeu__Group__1 ;
    public final void rule__Jeu__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:533:1: ( rule__Jeu__Group__0__Impl rule__Jeu__Group__1 )
            // InternalGame.g:534:2: rule__Jeu__Group__0__Impl rule__Jeu__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Jeu__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__0"


    // $ANTLR start "rule__Jeu__Group__0__Impl"
    // InternalGame.g:541:1: rule__Jeu__Group__0__Impl : ( 'Objets' ) ;
    public final void rule__Jeu__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:545:1: ( ( 'Objets' ) )
            // InternalGame.g:546:1: ( 'Objets' )
            {
            // InternalGame.g:546:1: ( 'Objets' )
            // InternalGame.g:547:2: 'Objets'
            {
             before(grammarAccess.getJeuAccess().getObjetsKeyword_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getObjetsKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__0__Impl"


    // $ANTLR start "rule__Jeu__Group__1"
    // InternalGame.g:556:1: rule__Jeu__Group__1 : rule__Jeu__Group__1__Impl rule__Jeu__Group__2 ;
    public final void rule__Jeu__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:560:1: ( rule__Jeu__Group__1__Impl rule__Jeu__Group__2 )
            // InternalGame.g:561:2: rule__Jeu__Group__1__Impl rule__Jeu__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Jeu__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__1"


    // $ANTLR start "rule__Jeu__Group__1__Impl"
    // InternalGame.g:568:1: rule__Jeu__Group__1__Impl : ( ':' ) ;
    public final void rule__Jeu__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:572:1: ( ( ':' ) )
            // InternalGame.g:573:1: ( ':' )
            {
            // InternalGame.g:573:1: ( ':' )
            // InternalGame.g:574:2: ':'
            {
             before(grammarAccess.getJeuAccess().getColonKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__1__Impl"


    // $ANTLR start "rule__Jeu__Group__2"
    // InternalGame.g:583:1: rule__Jeu__Group__2 : rule__Jeu__Group__2__Impl rule__Jeu__Group__3 ;
    public final void rule__Jeu__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:587:1: ( rule__Jeu__Group__2__Impl rule__Jeu__Group__3 )
            // InternalGame.g:588:2: rule__Jeu__Group__2__Impl rule__Jeu__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__Jeu__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__2"


    // $ANTLR start "rule__Jeu__Group__2__Impl"
    // InternalGame.g:595:1: rule__Jeu__Group__2__Impl : ( ( rule__Jeu__Group_2__0 )* ) ;
    public final void rule__Jeu__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:599:1: ( ( ( rule__Jeu__Group_2__0 )* ) )
            // InternalGame.g:600:1: ( ( rule__Jeu__Group_2__0 )* )
            {
            // InternalGame.g:600:1: ( ( rule__Jeu__Group_2__0 )* )
            // InternalGame.g:601:2: ( rule__Jeu__Group_2__0 )*
            {
             before(grammarAccess.getJeuAccess().getGroup_2()); 
            // InternalGame.g:602:2: ( rule__Jeu__Group_2__0 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==20) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalGame.g:602:3: rule__Jeu__Group_2__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Jeu__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getJeuAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__2__Impl"


    // $ANTLR start "rule__Jeu__Group__3"
    // InternalGame.g:610:1: rule__Jeu__Group__3 : rule__Jeu__Group__3__Impl rule__Jeu__Group__4 ;
    public final void rule__Jeu__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:614:1: ( rule__Jeu__Group__3__Impl rule__Jeu__Group__4 )
            // InternalGame.g:615:2: rule__Jeu__Group__3__Impl rule__Jeu__Group__4
            {
            pushFollow(FOLLOW_3);
            rule__Jeu__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__3"


    // $ANTLR start "rule__Jeu__Group__3__Impl"
    // InternalGame.g:622:1: rule__Jeu__Group__3__Impl : ( 'Transformations' ) ;
    public final void rule__Jeu__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:626:1: ( ( 'Transformations' ) )
            // InternalGame.g:627:1: ( 'Transformations' )
            {
            // InternalGame.g:627:1: ( 'Transformations' )
            // InternalGame.g:628:2: 'Transformations'
            {
             before(grammarAccess.getJeuAccess().getTransformationsKeyword_3()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getTransformationsKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__3__Impl"


    // $ANTLR start "rule__Jeu__Group__4"
    // InternalGame.g:637:1: rule__Jeu__Group__4 : rule__Jeu__Group__4__Impl rule__Jeu__Group__5 ;
    public final void rule__Jeu__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:641:1: ( rule__Jeu__Group__4__Impl rule__Jeu__Group__5 )
            // InternalGame.g:642:2: rule__Jeu__Group__4__Impl rule__Jeu__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__Jeu__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__4"


    // $ANTLR start "rule__Jeu__Group__4__Impl"
    // InternalGame.g:649:1: rule__Jeu__Group__4__Impl : ( ':' ) ;
    public final void rule__Jeu__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:653:1: ( ( ':' ) )
            // InternalGame.g:654:1: ( ':' )
            {
            // InternalGame.g:654:1: ( ':' )
            // InternalGame.g:655:2: ':'
            {
             before(grammarAccess.getJeuAccess().getColonKeyword_4()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getColonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__4__Impl"


    // $ANTLR start "rule__Jeu__Group__5"
    // InternalGame.g:664:1: rule__Jeu__Group__5 : rule__Jeu__Group__5__Impl rule__Jeu__Group__6 ;
    public final void rule__Jeu__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:668:1: ( rule__Jeu__Group__5__Impl rule__Jeu__Group__6 )
            // InternalGame.g:669:2: rule__Jeu__Group__5__Impl rule__Jeu__Group__6
            {
            pushFollow(FOLLOW_6);
            rule__Jeu__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__5"


    // $ANTLR start "rule__Jeu__Group__5__Impl"
    // InternalGame.g:676:1: rule__Jeu__Group__5__Impl : ( ( rule__Jeu__Group_5__0 )* ) ;
    public final void rule__Jeu__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:680:1: ( ( ( rule__Jeu__Group_5__0 )* ) )
            // InternalGame.g:681:1: ( ( rule__Jeu__Group_5__0 )* )
            {
            // InternalGame.g:681:1: ( ( rule__Jeu__Group_5__0 )* )
            // InternalGame.g:682:2: ( rule__Jeu__Group_5__0 )*
            {
             before(grammarAccess.getJeuAccess().getGroup_5()); 
            // InternalGame.g:683:2: ( rule__Jeu__Group_5__0 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==20) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalGame.g:683:3: rule__Jeu__Group_5__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Jeu__Group_5__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getJeuAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__5__Impl"


    // $ANTLR start "rule__Jeu__Group__6"
    // InternalGame.g:691:1: rule__Jeu__Group__6 : rule__Jeu__Group__6__Impl rule__Jeu__Group__7 ;
    public final void rule__Jeu__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:695:1: ( rule__Jeu__Group__6__Impl rule__Jeu__Group__7 )
            // InternalGame.g:696:2: rule__Jeu__Group__6__Impl rule__Jeu__Group__7
            {
            pushFollow(FOLLOW_3);
            rule__Jeu__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__6"


    // $ANTLR start "rule__Jeu__Group__6__Impl"
    // InternalGame.g:703:1: rule__Jeu__Group__6__Impl : ( 'Connaissances' ) ;
    public final void rule__Jeu__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:707:1: ( ( 'Connaissances' ) )
            // InternalGame.g:708:1: ( 'Connaissances' )
            {
            // InternalGame.g:708:1: ( 'Connaissances' )
            // InternalGame.g:709:2: 'Connaissances'
            {
             before(grammarAccess.getJeuAccess().getConnaissancesKeyword_6()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getConnaissancesKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__6__Impl"


    // $ANTLR start "rule__Jeu__Group__7"
    // InternalGame.g:718:1: rule__Jeu__Group__7 : rule__Jeu__Group__7__Impl rule__Jeu__Group__8 ;
    public final void rule__Jeu__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:722:1: ( rule__Jeu__Group__7__Impl rule__Jeu__Group__8 )
            // InternalGame.g:723:2: rule__Jeu__Group__7__Impl rule__Jeu__Group__8
            {
            pushFollow(FOLLOW_7);
            rule__Jeu__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__7"


    // $ANTLR start "rule__Jeu__Group__7__Impl"
    // InternalGame.g:730:1: rule__Jeu__Group__7__Impl : ( ':' ) ;
    public final void rule__Jeu__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:734:1: ( ( ':' ) )
            // InternalGame.g:735:1: ( ':' )
            {
            // InternalGame.g:735:1: ( ':' )
            // InternalGame.g:736:2: ':'
            {
             before(grammarAccess.getJeuAccess().getColonKeyword_7()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getColonKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__7__Impl"


    // $ANTLR start "rule__Jeu__Group__8"
    // InternalGame.g:745:1: rule__Jeu__Group__8 : rule__Jeu__Group__8__Impl rule__Jeu__Group__9 ;
    public final void rule__Jeu__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:749:1: ( rule__Jeu__Group__8__Impl rule__Jeu__Group__9 )
            // InternalGame.g:750:2: rule__Jeu__Group__8__Impl rule__Jeu__Group__9
            {
            pushFollow(FOLLOW_7);
            rule__Jeu__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__8"


    // $ANTLR start "rule__Jeu__Group__8__Impl"
    // InternalGame.g:757:1: rule__Jeu__Group__8__Impl : ( ( rule__Jeu__Group_8__0 )* ) ;
    public final void rule__Jeu__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:761:1: ( ( ( rule__Jeu__Group_8__0 )* ) )
            // InternalGame.g:762:1: ( ( rule__Jeu__Group_8__0 )* )
            {
            // InternalGame.g:762:1: ( ( rule__Jeu__Group_8__0 )* )
            // InternalGame.g:763:2: ( rule__Jeu__Group_8__0 )*
            {
             before(grammarAccess.getJeuAccess().getGroup_8()); 
            // InternalGame.g:764:2: ( rule__Jeu__Group_8__0 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==20) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalGame.g:764:3: rule__Jeu__Group_8__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Jeu__Group_8__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getJeuAccess().getGroup_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__8__Impl"


    // $ANTLR start "rule__Jeu__Group__9"
    // InternalGame.g:772:1: rule__Jeu__Group__9 : rule__Jeu__Group__9__Impl rule__Jeu__Group__10 ;
    public final void rule__Jeu__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:776:1: ( rule__Jeu__Group__9__Impl rule__Jeu__Group__10 )
            // InternalGame.g:777:2: rule__Jeu__Group__9__Impl rule__Jeu__Group__10
            {
            pushFollow(FOLLOW_3);
            rule__Jeu__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__9"


    // $ANTLR start "rule__Jeu__Group__9__Impl"
    // InternalGame.g:784:1: rule__Jeu__Group__9__Impl : ( 'Explorateur' ) ;
    public final void rule__Jeu__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:788:1: ( ( 'Explorateur' ) )
            // InternalGame.g:789:1: ( 'Explorateur' )
            {
            // InternalGame.g:789:1: ( 'Explorateur' )
            // InternalGame.g:790:2: 'Explorateur'
            {
             before(grammarAccess.getJeuAccess().getExplorateurKeyword_9()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getExplorateurKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__9__Impl"


    // $ANTLR start "rule__Jeu__Group__10"
    // InternalGame.g:799:1: rule__Jeu__Group__10 : rule__Jeu__Group__10__Impl rule__Jeu__Group__11 ;
    public final void rule__Jeu__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:803:1: ( rule__Jeu__Group__10__Impl rule__Jeu__Group__11 )
            // InternalGame.g:804:2: rule__Jeu__Group__10__Impl rule__Jeu__Group__11
            {
            pushFollow(FOLLOW_8);
            rule__Jeu__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__10"


    // $ANTLR start "rule__Jeu__Group__10__Impl"
    // InternalGame.g:811:1: rule__Jeu__Group__10__Impl : ( ':' ) ;
    public final void rule__Jeu__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:815:1: ( ( ':' ) )
            // InternalGame.g:816:1: ( ':' )
            {
            // InternalGame.g:816:1: ( ':' )
            // InternalGame.g:817:2: ':'
            {
             before(grammarAccess.getJeuAccess().getColonKeyword_10()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getColonKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__10__Impl"


    // $ANTLR start "rule__Jeu__Group__11"
    // InternalGame.g:826:1: rule__Jeu__Group__11 : rule__Jeu__Group__11__Impl rule__Jeu__Group__12 ;
    public final void rule__Jeu__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:830:1: ( rule__Jeu__Group__11__Impl rule__Jeu__Group__12 )
            // InternalGame.g:831:2: rule__Jeu__Group__11__Impl rule__Jeu__Group__12
            {
            pushFollow(FOLLOW_9);
            rule__Jeu__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__11"


    // $ANTLR start "rule__Jeu__Group__11__Impl"
    // InternalGame.g:838:1: rule__Jeu__Group__11__Impl : ( ( rule__Jeu__ExplorateurAssignment_11 ) ) ;
    public final void rule__Jeu__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:842:1: ( ( ( rule__Jeu__ExplorateurAssignment_11 ) ) )
            // InternalGame.g:843:1: ( ( rule__Jeu__ExplorateurAssignment_11 ) )
            {
            // InternalGame.g:843:1: ( ( rule__Jeu__ExplorateurAssignment_11 ) )
            // InternalGame.g:844:2: ( rule__Jeu__ExplorateurAssignment_11 )
            {
             before(grammarAccess.getJeuAccess().getExplorateurAssignment_11()); 
            // InternalGame.g:845:2: ( rule__Jeu__ExplorateurAssignment_11 )
            // InternalGame.g:845:3: rule__Jeu__ExplorateurAssignment_11
            {
            pushFollow(FOLLOW_2);
            rule__Jeu__ExplorateurAssignment_11();

            state._fsp--;


            }

             after(grammarAccess.getJeuAccess().getExplorateurAssignment_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__11__Impl"


    // $ANTLR start "rule__Jeu__Group__12"
    // InternalGame.g:853:1: rule__Jeu__Group__12 : rule__Jeu__Group__12__Impl rule__Jeu__Group__13 ;
    public final void rule__Jeu__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:857:1: ( rule__Jeu__Group__12__Impl rule__Jeu__Group__13 )
            // InternalGame.g:858:2: rule__Jeu__Group__12__Impl rule__Jeu__Group__13
            {
            pushFollow(FOLLOW_3);
            rule__Jeu__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__12"


    // $ANTLR start "rule__Jeu__Group__12__Impl"
    // InternalGame.g:865:1: rule__Jeu__Group__12__Impl : ( 'Personnes' ) ;
    public final void rule__Jeu__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:869:1: ( ( 'Personnes' ) )
            // InternalGame.g:870:1: ( 'Personnes' )
            {
            // InternalGame.g:870:1: ( 'Personnes' )
            // InternalGame.g:871:2: 'Personnes'
            {
             before(grammarAccess.getJeuAccess().getPersonnesKeyword_12()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getPersonnesKeyword_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__12__Impl"


    // $ANTLR start "rule__Jeu__Group__13"
    // InternalGame.g:880:1: rule__Jeu__Group__13 : rule__Jeu__Group__13__Impl rule__Jeu__Group__14 ;
    public final void rule__Jeu__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:884:1: ( rule__Jeu__Group__13__Impl rule__Jeu__Group__14 )
            // InternalGame.g:885:2: rule__Jeu__Group__13__Impl rule__Jeu__Group__14
            {
            pushFollow(FOLLOW_10);
            rule__Jeu__Group__13__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__13"


    // $ANTLR start "rule__Jeu__Group__13__Impl"
    // InternalGame.g:892:1: rule__Jeu__Group__13__Impl : ( ':' ) ;
    public final void rule__Jeu__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:896:1: ( ( ':' ) )
            // InternalGame.g:897:1: ( ':' )
            {
            // InternalGame.g:897:1: ( ':' )
            // InternalGame.g:898:2: ':'
            {
             before(grammarAccess.getJeuAccess().getColonKeyword_13()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getColonKeyword_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__13__Impl"


    // $ANTLR start "rule__Jeu__Group__14"
    // InternalGame.g:907:1: rule__Jeu__Group__14 : rule__Jeu__Group__14__Impl rule__Jeu__Group__15 ;
    public final void rule__Jeu__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:911:1: ( rule__Jeu__Group__14__Impl rule__Jeu__Group__15 )
            // InternalGame.g:912:2: rule__Jeu__Group__14__Impl rule__Jeu__Group__15
            {
            pushFollow(FOLLOW_10);
            rule__Jeu__Group__14__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__14"


    // $ANTLR start "rule__Jeu__Group__14__Impl"
    // InternalGame.g:919:1: rule__Jeu__Group__14__Impl : ( ( rule__Jeu__Group_14__0 )* ) ;
    public final void rule__Jeu__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:923:1: ( ( ( rule__Jeu__Group_14__0 )* ) )
            // InternalGame.g:924:1: ( ( rule__Jeu__Group_14__0 )* )
            {
            // InternalGame.g:924:1: ( ( rule__Jeu__Group_14__0 )* )
            // InternalGame.g:925:2: ( rule__Jeu__Group_14__0 )*
            {
             before(grammarAccess.getJeuAccess().getGroup_14()); 
            // InternalGame.g:926:2: ( rule__Jeu__Group_14__0 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==20) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalGame.g:926:3: rule__Jeu__Group_14__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Jeu__Group_14__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getJeuAccess().getGroup_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__14__Impl"


    // $ANTLR start "rule__Jeu__Group__15"
    // InternalGame.g:934:1: rule__Jeu__Group__15 : rule__Jeu__Group__15__Impl rule__Jeu__Group__16 ;
    public final void rule__Jeu__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:938:1: ( rule__Jeu__Group__15__Impl rule__Jeu__Group__16 )
            // InternalGame.g:939:2: rule__Jeu__Group__15__Impl rule__Jeu__Group__16
            {
            pushFollow(FOLLOW_3);
            rule__Jeu__Group__15__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__16();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__15"


    // $ANTLR start "rule__Jeu__Group__15__Impl"
    // InternalGame.g:946:1: rule__Jeu__Group__15__Impl : ( 'Territoire' ) ;
    public final void rule__Jeu__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:950:1: ( ( 'Territoire' ) )
            // InternalGame.g:951:1: ( 'Territoire' )
            {
            // InternalGame.g:951:1: ( 'Territoire' )
            // InternalGame.g:952:2: 'Territoire'
            {
             before(grammarAccess.getJeuAccess().getTerritoireKeyword_15()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getTerritoireKeyword_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__15__Impl"


    // $ANTLR start "rule__Jeu__Group__16"
    // InternalGame.g:961:1: rule__Jeu__Group__16 : rule__Jeu__Group__16__Impl rule__Jeu__Group__17 ;
    public final void rule__Jeu__Group__16() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:965:1: ( rule__Jeu__Group__16__Impl rule__Jeu__Group__17 )
            // InternalGame.g:966:2: rule__Jeu__Group__16__Impl rule__Jeu__Group__17
            {
            pushFollow(FOLLOW_11);
            rule__Jeu__Group__16__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group__17();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__16"


    // $ANTLR start "rule__Jeu__Group__16__Impl"
    // InternalGame.g:973:1: rule__Jeu__Group__16__Impl : ( ':' ) ;
    public final void rule__Jeu__Group__16__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:977:1: ( ( ':' ) )
            // InternalGame.g:978:1: ( ':' )
            {
            // InternalGame.g:978:1: ( ':' )
            // InternalGame.g:979:2: ':'
            {
             before(grammarAccess.getJeuAccess().getColonKeyword_16()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getColonKeyword_16()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__16__Impl"


    // $ANTLR start "rule__Jeu__Group__17"
    // InternalGame.g:988:1: rule__Jeu__Group__17 : rule__Jeu__Group__17__Impl ;
    public final void rule__Jeu__Group__17() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:992:1: ( rule__Jeu__Group__17__Impl )
            // InternalGame.g:993:2: rule__Jeu__Group__17__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Jeu__Group__17__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__17"


    // $ANTLR start "rule__Jeu__Group__17__Impl"
    // InternalGame.g:999:1: rule__Jeu__Group__17__Impl : ( ( rule__Jeu__TerritoireAssignment_17 ) ) ;
    public final void rule__Jeu__Group__17__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1003:1: ( ( ( rule__Jeu__TerritoireAssignment_17 ) ) )
            // InternalGame.g:1004:1: ( ( rule__Jeu__TerritoireAssignment_17 ) )
            {
            // InternalGame.g:1004:1: ( ( rule__Jeu__TerritoireAssignment_17 ) )
            // InternalGame.g:1005:2: ( rule__Jeu__TerritoireAssignment_17 )
            {
             before(grammarAccess.getJeuAccess().getTerritoireAssignment_17()); 
            // InternalGame.g:1006:2: ( rule__Jeu__TerritoireAssignment_17 )
            // InternalGame.g:1006:3: rule__Jeu__TerritoireAssignment_17
            {
            pushFollow(FOLLOW_2);
            rule__Jeu__TerritoireAssignment_17();

            state._fsp--;


            }

             after(grammarAccess.getJeuAccess().getTerritoireAssignment_17()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group__17__Impl"


    // $ANTLR start "rule__Jeu__Group_2__0"
    // InternalGame.g:1015:1: rule__Jeu__Group_2__0 : rule__Jeu__Group_2__0__Impl rule__Jeu__Group_2__1 ;
    public final void rule__Jeu__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1019:1: ( rule__Jeu__Group_2__0__Impl rule__Jeu__Group_2__1 )
            // InternalGame.g:1020:2: rule__Jeu__Group_2__0__Impl rule__Jeu__Group_2__1
            {
            pushFollow(FOLLOW_12);
            rule__Jeu__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_2__0"


    // $ANTLR start "rule__Jeu__Group_2__0__Impl"
    // InternalGame.g:1027:1: rule__Jeu__Group_2__0__Impl : ( '-' ) ;
    public final void rule__Jeu__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1031:1: ( ( '-' ) )
            // InternalGame.g:1032:1: ( '-' )
            {
            // InternalGame.g:1032:1: ( '-' )
            // InternalGame.g:1033:2: '-'
            {
             before(grammarAccess.getJeuAccess().getHyphenMinusKeyword_2_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getHyphenMinusKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_2__0__Impl"


    // $ANTLR start "rule__Jeu__Group_2__1"
    // InternalGame.g:1042:1: rule__Jeu__Group_2__1 : rule__Jeu__Group_2__1__Impl ;
    public final void rule__Jeu__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1046:1: ( rule__Jeu__Group_2__1__Impl )
            // InternalGame.g:1047:2: rule__Jeu__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Jeu__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_2__1"


    // $ANTLR start "rule__Jeu__Group_2__1__Impl"
    // InternalGame.g:1053:1: rule__Jeu__Group_2__1__Impl : ( ( rule__Jeu__ObjetsAssignment_2_1 ) ) ;
    public final void rule__Jeu__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1057:1: ( ( ( rule__Jeu__ObjetsAssignment_2_1 ) ) )
            // InternalGame.g:1058:1: ( ( rule__Jeu__ObjetsAssignment_2_1 ) )
            {
            // InternalGame.g:1058:1: ( ( rule__Jeu__ObjetsAssignment_2_1 ) )
            // InternalGame.g:1059:2: ( rule__Jeu__ObjetsAssignment_2_1 )
            {
             before(grammarAccess.getJeuAccess().getObjetsAssignment_2_1()); 
            // InternalGame.g:1060:2: ( rule__Jeu__ObjetsAssignment_2_1 )
            // InternalGame.g:1060:3: rule__Jeu__ObjetsAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Jeu__ObjetsAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getJeuAccess().getObjetsAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_2__1__Impl"


    // $ANTLR start "rule__Jeu__Group_5__0"
    // InternalGame.g:1069:1: rule__Jeu__Group_5__0 : rule__Jeu__Group_5__0__Impl rule__Jeu__Group_5__1 ;
    public final void rule__Jeu__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1073:1: ( rule__Jeu__Group_5__0__Impl rule__Jeu__Group_5__1 )
            // InternalGame.g:1074:2: rule__Jeu__Group_5__0__Impl rule__Jeu__Group_5__1
            {
            pushFollow(FOLLOW_12);
            rule__Jeu__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_5__0"


    // $ANTLR start "rule__Jeu__Group_5__0__Impl"
    // InternalGame.g:1081:1: rule__Jeu__Group_5__0__Impl : ( '-' ) ;
    public final void rule__Jeu__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1085:1: ( ( '-' ) )
            // InternalGame.g:1086:1: ( '-' )
            {
            // InternalGame.g:1086:1: ( '-' )
            // InternalGame.g:1087:2: '-'
            {
             before(grammarAccess.getJeuAccess().getHyphenMinusKeyword_5_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getHyphenMinusKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_5__0__Impl"


    // $ANTLR start "rule__Jeu__Group_5__1"
    // InternalGame.g:1096:1: rule__Jeu__Group_5__1 : rule__Jeu__Group_5__1__Impl ;
    public final void rule__Jeu__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1100:1: ( rule__Jeu__Group_5__1__Impl )
            // InternalGame.g:1101:2: rule__Jeu__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Jeu__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_5__1"


    // $ANTLR start "rule__Jeu__Group_5__1__Impl"
    // InternalGame.g:1107:1: rule__Jeu__Group_5__1__Impl : ( ( rule__Jeu__TransformationsAssignment_5_1 ) ) ;
    public final void rule__Jeu__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1111:1: ( ( ( rule__Jeu__TransformationsAssignment_5_1 ) ) )
            // InternalGame.g:1112:1: ( ( rule__Jeu__TransformationsAssignment_5_1 ) )
            {
            // InternalGame.g:1112:1: ( ( rule__Jeu__TransformationsAssignment_5_1 ) )
            // InternalGame.g:1113:2: ( rule__Jeu__TransformationsAssignment_5_1 )
            {
             before(grammarAccess.getJeuAccess().getTransformationsAssignment_5_1()); 
            // InternalGame.g:1114:2: ( rule__Jeu__TransformationsAssignment_5_1 )
            // InternalGame.g:1114:3: rule__Jeu__TransformationsAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Jeu__TransformationsAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getJeuAccess().getTransformationsAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_5__1__Impl"


    // $ANTLR start "rule__Jeu__Group_8__0"
    // InternalGame.g:1123:1: rule__Jeu__Group_8__0 : rule__Jeu__Group_8__0__Impl rule__Jeu__Group_8__1 ;
    public final void rule__Jeu__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1127:1: ( rule__Jeu__Group_8__0__Impl rule__Jeu__Group_8__1 )
            // InternalGame.g:1128:2: rule__Jeu__Group_8__0__Impl rule__Jeu__Group_8__1
            {
            pushFollow(FOLLOW_12);
            rule__Jeu__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_8__0"


    // $ANTLR start "rule__Jeu__Group_8__0__Impl"
    // InternalGame.g:1135:1: rule__Jeu__Group_8__0__Impl : ( '-' ) ;
    public final void rule__Jeu__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1139:1: ( ( '-' ) )
            // InternalGame.g:1140:1: ( '-' )
            {
            // InternalGame.g:1140:1: ( '-' )
            // InternalGame.g:1141:2: '-'
            {
             before(grammarAccess.getJeuAccess().getHyphenMinusKeyword_8_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getHyphenMinusKeyword_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_8__0__Impl"


    // $ANTLR start "rule__Jeu__Group_8__1"
    // InternalGame.g:1150:1: rule__Jeu__Group_8__1 : rule__Jeu__Group_8__1__Impl ;
    public final void rule__Jeu__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1154:1: ( rule__Jeu__Group_8__1__Impl )
            // InternalGame.g:1155:2: rule__Jeu__Group_8__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Jeu__Group_8__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_8__1"


    // $ANTLR start "rule__Jeu__Group_8__1__Impl"
    // InternalGame.g:1161:1: rule__Jeu__Group_8__1__Impl : ( ( rule__Jeu__ConnaissancesAssignment_8_1 ) ) ;
    public final void rule__Jeu__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1165:1: ( ( ( rule__Jeu__ConnaissancesAssignment_8_1 ) ) )
            // InternalGame.g:1166:1: ( ( rule__Jeu__ConnaissancesAssignment_8_1 ) )
            {
            // InternalGame.g:1166:1: ( ( rule__Jeu__ConnaissancesAssignment_8_1 ) )
            // InternalGame.g:1167:2: ( rule__Jeu__ConnaissancesAssignment_8_1 )
            {
             before(grammarAccess.getJeuAccess().getConnaissancesAssignment_8_1()); 
            // InternalGame.g:1168:2: ( rule__Jeu__ConnaissancesAssignment_8_1 )
            // InternalGame.g:1168:3: rule__Jeu__ConnaissancesAssignment_8_1
            {
            pushFollow(FOLLOW_2);
            rule__Jeu__ConnaissancesAssignment_8_1();

            state._fsp--;


            }

             after(grammarAccess.getJeuAccess().getConnaissancesAssignment_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_8__1__Impl"


    // $ANTLR start "rule__Jeu__Group_14__0"
    // InternalGame.g:1177:1: rule__Jeu__Group_14__0 : rule__Jeu__Group_14__0__Impl rule__Jeu__Group_14__1 ;
    public final void rule__Jeu__Group_14__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1181:1: ( rule__Jeu__Group_14__0__Impl rule__Jeu__Group_14__1 )
            // InternalGame.g:1182:2: rule__Jeu__Group_14__0__Impl rule__Jeu__Group_14__1
            {
            pushFollow(FOLLOW_12);
            rule__Jeu__Group_14__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Jeu__Group_14__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_14__0"


    // $ANTLR start "rule__Jeu__Group_14__0__Impl"
    // InternalGame.g:1189:1: rule__Jeu__Group_14__0__Impl : ( '-' ) ;
    public final void rule__Jeu__Group_14__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1193:1: ( ( '-' ) )
            // InternalGame.g:1194:1: ( '-' )
            {
            // InternalGame.g:1194:1: ( '-' )
            // InternalGame.g:1195:2: '-'
            {
             before(grammarAccess.getJeuAccess().getHyphenMinusKeyword_14_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getJeuAccess().getHyphenMinusKeyword_14_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_14__0__Impl"


    // $ANTLR start "rule__Jeu__Group_14__1"
    // InternalGame.g:1204:1: rule__Jeu__Group_14__1 : rule__Jeu__Group_14__1__Impl ;
    public final void rule__Jeu__Group_14__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1208:1: ( rule__Jeu__Group_14__1__Impl )
            // InternalGame.g:1209:2: rule__Jeu__Group_14__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Jeu__Group_14__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_14__1"


    // $ANTLR start "rule__Jeu__Group_14__1__Impl"
    // InternalGame.g:1215:1: rule__Jeu__Group_14__1__Impl : ( ( rule__Jeu__PersonnesAssignment_14_1 ) ) ;
    public final void rule__Jeu__Group_14__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1219:1: ( ( ( rule__Jeu__PersonnesAssignment_14_1 ) ) )
            // InternalGame.g:1220:1: ( ( rule__Jeu__PersonnesAssignment_14_1 ) )
            {
            // InternalGame.g:1220:1: ( ( rule__Jeu__PersonnesAssignment_14_1 ) )
            // InternalGame.g:1221:2: ( rule__Jeu__PersonnesAssignment_14_1 )
            {
             before(grammarAccess.getJeuAccess().getPersonnesAssignment_14_1()); 
            // InternalGame.g:1222:2: ( rule__Jeu__PersonnesAssignment_14_1 )
            // InternalGame.g:1222:3: rule__Jeu__PersonnesAssignment_14_1
            {
            pushFollow(FOLLOW_2);
            rule__Jeu__PersonnesAssignment_14_1();

            state._fsp--;


            }

             after(grammarAccess.getJeuAccess().getPersonnesAssignment_14_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__Group_14__1__Impl"


    // $ANTLR start "rule__Objet__Group__0"
    // InternalGame.g:1231:1: rule__Objet__Group__0 : rule__Objet__Group__0__Impl rule__Objet__Group__1 ;
    public final void rule__Objet__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1235:1: ( rule__Objet__Group__0__Impl rule__Objet__Group__1 )
            // InternalGame.g:1236:2: rule__Objet__Group__0__Impl rule__Objet__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Objet__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Objet__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__0"


    // $ANTLR start "rule__Objet__Group__0__Impl"
    // InternalGame.g:1243:1: rule__Objet__Group__0__Impl : ( ( rule__Objet__NameAssignment_0 ) ) ;
    public final void rule__Objet__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1247:1: ( ( ( rule__Objet__NameAssignment_0 ) ) )
            // InternalGame.g:1248:1: ( ( rule__Objet__NameAssignment_0 ) )
            {
            // InternalGame.g:1248:1: ( ( rule__Objet__NameAssignment_0 ) )
            // InternalGame.g:1249:2: ( rule__Objet__NameAssignment_0 )
            {
             before(grammarAccess.getObjetAccess().getNameAssignment_0()); 
            // InternalGame.g:1250:2: ( rule__Objet__NameAssignment_0 )
            // InternalGame.g:1250:3: rule__Objet__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Objet__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getObjetAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__0__Impl"


    // $ANTLR start "rule__Objet__Group__1"
    // InternalGame.g:1258:1: rule__Objet__Group__1 : rule__Objet__Group__1__Impl rule__Objet__Group__2 ;
    public final void rule__Objet__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1262:1: ( rule__Objet__Group__1__Impl rule__Objet__Group__2 )
            // InternalGame.g:1263:2: rule__Objet__Group__1__Impl rule__Objet__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__Objet__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Objet__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__1"


    // $ANTLR start "rule__Objet__Group__1__Impl"
    // InternalGame.g:1270:1: rule__Objet__Group__1__Impl : ( ':' ) ;
    public final void rule__Objet__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1274:1: ( ( ':' ) )
            // InternalGame.g:1275:1: ( ':' )
            {
            // InternalGame.g:1275:1: ( ':' )
            // InternalGame.g:1276:2: ':'
            {
             before(grammarAccess.getObjetAccess().getColonKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getObjetAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__1__Impl"


    // $ANTLR start "rule__Objet__Group__2"
    // InternalGame.g:1285:1: rule__Objet__Group__2 : rule__Objet__Group__2__Impl rule__Objet__Group__3 ;
    public final void rule__Objet__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1289:1: ( rule__Objet__Group__2__Impl rule__Objet__Group__3 )
            // InternalGame.g:1290:2: rule__Objet__Group__2__Impl rule__Objet__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__Objet__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Objet__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__2"


    // $ANTLR start "rule__Objet__Group__2__Impl"
    // InternalGame.g:1297:1: rule__Objet__Group__2__Impl : ( 'taille' ) ;
    public final void rule__Objet__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1301:1: ( ( 'taille' ) )
            // InternalGame.g:1302:1: ( 'taille' )
            {
            // InternalGame.g:1302:1: ( 'taille' )
            // InternalGame.g:1303:2: 'taille'
            {
             before(grammarAccess.getObjetAccess().getTailleKeyword_2()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getObjetAccess().getTailleKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__2__Impl"


    // $ANTLR start "rule__Objet__Group__3"
    // InternalGame.g:1312:1: rule__Objet__Group__3 : rule__Objet__Group__3__Impl rule__Objet__Group__4 ;
    public final void rule__Objet__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1316:1: ( rule__Objet__Group__3__Impl rule__Objet__Group__4 )
            // InternalGame.g:1317:2: rule__Objet__Group__3__Impl rule__Objet__Group__4
            {
            pushFollow(FOLLOW_13);
            rule__Objet__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Objet__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__3"


    // $ANTLR start "rule__Objet__Group__3__Impl"
    // InternalGame.g:1324:1: rule__Objet__Group__3__Impl : ( ':' ) ;
    public final void rule__Objet__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1328:1: ( ( ':' ) )
            // InternalGame.g:1329:1: ( ':' )
            {
            // InternalGame.g:1329:1: ( ':' )
            // InternalGame.g:1330:2: ':'
            {
             before(grammarAccess.getObjetAccess().getColonKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getObjetAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__3__Impl"


    // $ANTLR start "rule__Objet__Group__4"
    // InternalGame.g:1339:1: rule__Objet__Group__4 : rule__Objet__Group__4__Impl rule__Objet__Group__5 ;
    public final void rule__Objet__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1343:1: ( rule__Objet__Group__4__Impl rule__Objet__Group__5 )
            // InternalGame.g:1344:2: rule__Objet__Group__4__Impl rule__Objet__Group__5
            {
            pushFollow(FOLLOW_14);
            rule__Objet__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Objet__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__4"


    // $ANTLR start "rule__Objet__Group__4__Impl"
    // InternalGame.g:1351:1: rule__Objet__Group__4__Impl : ( ( rule__Objet__TailleAssignment_4 ) ) ;
    public final void rule__Objet__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1355:1: ( ( ( rule__Objet__TailleAssignment_4 ) ) )
            // InternalGame.g:1356:1: ( ( rule__Objet__TailleAssignment_4 ) )
            {
            // InternalGame.g:1356:1: ( ( rule__Objet__TailleAssignment_4 ) )
            // InternalGame.g:1357:2: ( rule__Objet__TailleAssignment_4 )
            {
             before(grammarAccess.getObjetAccess().getTailleAssignment_4()); 
            // InternalGame.g:1358:2: ( rule__Objet__TailleAssignment_4 )
            // InternalGame.g:1358:3: rule__Objet__TailleAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Objet__TailleAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getObjetAccess().getTailleAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__4__Impl"


    // $ANTLR start "rule__Objet__Group__5"
    // InternalGame.g:1366:1: rule__Objet__Group__5 : rule__Objet__Group__5__Impl rule__Objet__Group__6 ;
    public final void rule__Objet__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1370:1: ( rule__Objet__Group__5__Impl rule__Objet__Group__6 )
            // InternalGame.g:1371:2: rule__Objet__Group__5__Impl rule__Objet__Group__6
            {
            pushFollow(FOLLOW_3);
            rule__Objet__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Objet__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__5"


    // $ANTLR start "rule__Objet__Group__5__Impl"
    // InternalGame.g:1378:1: rule__Objet__Group__5__Impl : ( 'visible' ) ;
    public final void rule__Objet__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1382:1: ( ( 'visible' ) )
            // InternalGame.g:1383:1: ( 'visible' )
            {
            // InternalGame.g:1383:1: ( 'visible' )
            // InternalGame.g:1384:2: 'visible'
            {
             before(grammarAccess.getObjetAccess().getVisibleKeyword_5()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getObjetAccess().getVisibleKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__5__Impl"


    // $ANTLR start "rule__Objet__Group__6"
    // InternalGame.g:1393:1: rule__Objet__Group__6 : rule__Objet__Group__6__Impl rule__Objet__Group__7 ;
    public final void rule__Objet__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1397:1: ( rule__Objet__Group__6__Impl rule__Objet__Group__7 )
            // InternalGame.g:1398:2: rule__Objet__Group__6__Impl rule__Objet__Group__7
            {
            pushFollow(FOLLOW_15);
            rule__Objet__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Objet__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__6"


    // $ANTLR start "rule__Objet__Group__6__Impl"
    // InternalGame.g:1405:1: rule__Objet__Group__6__Impl : ( ':' ) ;
    public final void rule__Objet__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1409:1: ( ( ':' ) )
            // InternalGame.g:1410:1: ( ':' )
            {
            // InternalGame.g:1410:1: ( ':' )
            // InternalGame.g:1411:2: ':'
            {
             before(grammarAccess.getObjetAccess().getColonKeyword_6()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getObjetAccess().getColonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__6__Impl"


    // $ANTLR start "rule__Objet__Group__7"
    // InternalGame.g:1420:1: rule__Objet__Group__7 : rule__Objet__Group__7__Impl rule__Objet__Group__8 ;
    public final void rule__Objet__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1424:1: ( rule__Objet__Group__7__Impl rule__Objet__Group__8 )
            // InternalGame.g:1425:2: rule__Objet__Group__7__Impl rule__Objet__Group__8
            {
            pushFollow(FOLLOW_16);
            rule__Objet__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Objet__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__7"


    // $ANTLR start "rule__Objet__Group__7__Impl"
    // InternalGame.g:1432:1: rule__Objet__Group__7__Impl : ( ( rule__Objet__VisibleAssignment_7 ) ) ;
    public final void rule__Objet__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1436:1: ( ( ( rule__Objet__VisibleAssignment_7 ) ) )
            // InternalGame.g:1437:1: ( ( rule__Objet__VisibleAssignment_7 ) )
            {
            // InternalGame.g:1437:1: ( ( rule__Objet__VisibleAssignment_7 ) )
            // InternalGame.g:1438:2: ( rule__Objet__VisibleAssignment_7 )
            {
             before(grammarAccess.getObjetAccess().getVisibleAssignment_7()); 
            // InternalGame.g:1439:2: ( rule__Objet__VisibleAssignment_7 )
            // InternalGame.g:1439:3: rule__Objet__VisibleAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__Objet__VisibleAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getObjetAccess().getVisibleAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__7__Impl"


    // $ANTLR start "rule__Objet__Group__8"
    // InternalGame.g:1447:1: rule__Objet__Group__8 : rule__Objet__Group__8__Impl rule__Objet__Group__9 ;
    public final void rule__Objet__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1451:1: ( rule__Objet__Group__8__Impl rule__Objet__Group__9 )
            // InternalGame.g:1452:2: rule__Objet__Group__8__Impl rule__Objet__Group__9
            {
            pushFollow(FOLLOW_3);
            rule__Objet__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Objet__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__8"


    // $ANTLR start "rule__Objet__Group__8__Impl"
    // InternalGame.g:1459:1: rule__Objet__Group__8__Impl : ( 'descriptions' ) ;
    public final void rule__Objet__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1463:1: ( ( 'descriptions' ) )
            // InternalGame.g:1464:1: ( 'descriptions' )
            {
            // InternalGame.g:1464:1: ( 'descriptions' )
            // InternalGame.g:1465:2: 'descriptions'
            {
             before(grammarAccess.getObjetAccess().getDescriptionsKeyword_8()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getObjetAccess().getDescriptionsKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__8__Impl"


    // $ANTLR start "rule__Objet__Group__9"
    // InternalGame.g:1474:1: rule__Objet__Group__9 : rule__Objet__Group__9__Impl rule__Objet__Group__10 ;
    public final void rule__Objet__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1478:1: ( rule__Objet__Group__9__Impl rule__Objet__Group__10 )
            // InternalGame.g:1479:2: rule__Objet__Group__9__Impl rule__Objet__Group__10
            {
            pushFollow(FOLLOW_17);
            rule__Objet__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Objet__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__9"


    // $ANTLR start "rule__Objet__Group__9__Impl"
    // InternalGame.g:1486:1: rule__Objet__Group__9__Impl : ( ':' ) ;
    public final void rule__Objet__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1490:1: ( ( ':' ) )
            // InternalGame.g:1491:1: ( ':' )
            {
            // InternalGame.g:1491:1: ( ':' )
            // InternalGame.g:1492:2: ':'
            {
             before(grammarAccess.getObjetAccess().getColonKeyword_9()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getObjetAccess().getColonKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__9__Impl"


    // $ANTLR start "rule__Objet__Group__10"
    // InternalGame.g:1501:1: rule__Objet__Group__10 : rule__Objet__Group__10__Impl ;
    public final void rule__Objet__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1505:1: ( rule__Objet__Group__10__Impl )
            // InternalGame.g:1506:2: rule__Objet__Group__10__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Objet__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__10"


    // $ANTLR start "rule__Objet__Group__10__Impl"
    // InternalGame.g:1512:1: rule__Objet__Group__10__Impl : ( ( ( rule__Objet__Group_10__0 ) ) ( ( rule__Objet__Group_10__0 )* ) ) ;
    public final void rule__Objet__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1516:1: ( ( ( ( rule__Objet__Group_10__0 ) ) ( ( rule__Objet__Group_10__0 )* ) ) )
            // InternalGame.g:1517:1: ( ( ( rule__Objet__Group_10__0 ) ) ( ( rule__Objet__Group_10__0 )* ) )
            {
            // InternalGame.g:1517:1: ( ( ( rule__Objet__Group_10__0 ) ) ( ( rule__Objet__Group_10__0 )* ) )
            // InternalGame.g:1518:2: ( ( rule__Objet__Group_10__0 ) ) ( ( rule__Objet__Group_10__0 )* )
            {
            // InternalGame.g:1518:2: ( ( rule__Objet__Group_10__0 ) )
            // InternalGame.g:1519:3: ( rule__Objet__Group_10__0 )
            {
             before(grammarAccess.getObjetAccess().getGroup_10()); 
            // InternalGame.g:1520:3: ( rule__Objet__Group_10__0 )
            // InternalGame.g:1520:4: rule__Objet__Group_10__0
            {
            pushFollow(FOLLOW_5);
            rule__Objet__Group_10__0();

            state._fsp--;


            }

             after(grammarAccess.getObjetAccess().getGroup_10()); 

            }

            // InternalGame.g:1523:2: ( ( rule__Objet__Group_10__0 )* )
            // InternalGame.g:1524:3: ( rule__Objet__Group_10__0 )*
            {
             before(grammarAccess.getObjetAccess().getGroup_10()); 
            // InternalGame.g:1525:3: ( rule__Objet__Group_10__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==20) ) {
                    int LA6_2 = input.LA(2);

                    if ( (LA6_2==RULE_ID) ) {
                        int LA6_3 = input.LA(3);

                        if ( (LA6_3==14) ) {
                            int LA6_4 = input.LA(4);

                            if ( (LA6_4==44) ) {
                                alt6=1;
                            }


                        }


                    }


                }


                switch (alt6) {
            	case 1 :
            	    // InternalGame.g:1525:4: rule__Objet__Group_10__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Objet__Group_10__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getObjetAccess().getGroup_10()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group__10__Impl"


    // $ANTLR start "rule__Objet__Group_10__0"
    // InternalGame.g:1535:1: rule__Objet__Group_10__0 : rule__Objet__Group_10__0__Impl rule__Objet__Group_10__1 ;
    public final void rule__Objet__Group_10__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1539:1: ( rule__Objet__Group_10__0__Impl rule__Objet__Group_10__1 )
            // InternalGame.g:1540:2: rule__Objet__Group_10__0__Impl rule__Objet__Group_10__1
            {
            pushFollow(FOLLOW_12);
            rule__Objet__Group_10__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Objet__Group_10__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group_10__0"


    // $ANTLR start "rule__Objet__Group_10__0__Impl"
    // InternalGame.g:1547:1: rule__Objet__Group_10__0__Impl : ( '-' ) ;
    public final void rule__Objet__Group_10__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1551:1: ( ( '-' ) )
            // InternalGame.g:1552:1: ( '-' )
            {
            // InternalGame.g:1552:1: ( '-' )
            // InternalGame.g:1553:2: '-'
            {
             before(grammarAccess.getObjetAccess().getHyphenMinusKeyword_10_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getObjetAccess().getHyphenMinusKeyword_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group_10__0__Impl"


    // $ANTLR start "rule__Objet__Group_10__1"
    // InternalGame.g:1562:1: rule__Objet__Group_10__1 : rule__Objet__Group_10__1__Impl ;
    public final void rule__Objet__Group_10__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1566:1: ( rule__Objet__Group_10__1__Impl )
            // InternalGame.g:1567:2: rule__Objet__Group_10__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Objet__Group_10__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group_10__1"


    // $ANTLR start "rule__Objet__Group_10__1__Impl"
    // InternalGame.g:1573:1: rule__Objet__Group_10__1__Impl : ( ( rule__Objet__DescriptionsAssignment_10_1 ) ) ;
    public final void rule__Objet__Group_10__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1577:1: ( ( ( rule__Objet__DescriptionsAssignment_10_1 ) ) )
            // InternalGame.g:1578:1: ( ( rule__Objet__DescriptionsAssignment_10_1 ) )
            {
            // InternalGame.g:1578:1: ( ( rule__Objet__DescriptionsAssignment_10_1 ) )
            // InternalGame.g:1579:2: ( rule__Objet__DescriptionsAssignment_10_1 )
            {
             before(grammarAccess.getObjetAccess().getDescriptionsAssignment_10_1()); 
            // InternalGame.g:1580:2: ( rule__Objet__DescriptionsAssignment_10_1 )
            // InternalGame.g:1580:3: rule__Objet__DescriptionsAssignment_10_1
            {
            pushFollow(FOLLOW_2);
            rule__Objet__DescriptionsAssignment_10_1();

            state._fsp--;


            }

             after(grammarAccess.getObjetAccess().getDescriptionsAssignment_10_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__Group_10__1__Impl"


    // $ANTLR start "rule__Transformation__Group__0"
    // InternalGame.g:1589:1: rule__Transformation__Group__0 : rule__Transformation__Group__0__Impl rule__Transformation__Group__1 ;
    public final void rule__Transformation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1593:1: ( rule__Transformation__Group__0__Impl rule__Transformation__Group__1 )
            // InternalGame.g:1594:2: rule__Transformation__Group__0__Impl rule__Transformation__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Transformation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transformation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__0"


    // $ANTLR start "rule__Transformation__Group__0__Impl"
    // InternalGame.g:1601:1: rule__Transformation__Group__0__Impl : ( ( rule__Transformation__NameAssignment_0 ) ) ;
    public final void rule__Transformation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1605:1: ( ( ( rule__Transformation__NameAssignment_0 ) ) )
            // InternalGame.g:1606:1: ( ( rule__Transformation__NameAssignment_0 ) )
            {
            // InternalGame.g:1606:1: ( ( rule__Transformation__NameAssignment_0 ) )
            // InternalGame.g:1607:2: ( rule__Transformation__NameAssignment_0 )
            {
             before(grammarAccess.getTransformationAccess().getNameAssignment_0()); 
            // InternalGame.g:1608:2: ( rule__Transformation__NameAssignment_0 )
            // InternalGame.g:1608:3: rule__Transformation__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Transformation__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getTransformationAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__0__Impl"


    // $ANTLR start "rule__Transformation__Group__1"
    // InternalGame.g:1616:1: rule__Transformation__Group__1 : rule__Transformation__Group__1__Impl rule__Transformation__Group__2 ;
    public final void rule__Transformation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1620:1: ( rule__Transformation__Group__1__Impl rule__Transformation__Group__2 )
            // InternalGame.g:1621:2: rule__Transformation__Group__1__Impl rule__Transformation__Group__2
            {
            pushFollow(FOLLOW_18);
            rule__Transformation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transformation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__1"


    // $ANTLR start "rule__Transformation__Group__1__Impl"
    // InternalGame.g:1628:1: rule__Transformation__Group__1__Impl : ( ':' ) ;
    public final void rule__Transformation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1632:1: ( ( ':' ) )
            // InternalGame.g:1633:1: ( ':' )
            {
            // InternalGame.g:1633:1: ( ':' )
            // InternalGame.g:1634:2: ':'
            {
             before(grammarAccess.getTransformationAccess().getColonKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getTransformationAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__1__Impl"


    // $ANTLR start "rule__Transformation__Group__2"
    // InternalGame.g:1643:1: rule__Transformation__Group__2 : rule__Transformation__Group__2__Impl rule__Transformation__Group__3 ;
    public final void rule__Transformation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1647:1: ( rule__Transformation__Group__2__Impl rule__Transformation__Group__3 )
            // InternalGame.g:1648:2: rule__Transformation__Group__2__Impl rule__Transformation__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__Transformation__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transformation__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__2"


    // $ANTLR start "rule__Transformation__Group__2__Impl"
    // InternalGame.g:1655:1: rule__Transformation__Group__2__Impl : ( 'condition' ) ;
    public final void rule__Transformation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1659:1: ( ( 'condition' ) )
            // InternalGame.g:1660:1: ( 'condition' )
            {
            // InternalGame.g:1660:1: ( 'condition' )
            // InternalGame.g:1661:2: 'condition'
            {
             before(grammarAccess.getTransformationAccess().getConditionKeyword_2()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getTransformationAccess().getConditionKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__2__Impl"


    // $ANTLR start "rule__Transformation__Group__3"
    // InternalGame.g:1670:1: rule__Transformation__Group__3 : rule__Transformation__Group__3__Impl rule__Transformation__Group__4 ;
    public final void rule__Transformation__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1674:1: ( rule__Transformation__Group__3__Impl rule__Transformation__Group__4 )
            // InternalGame.g:1675:2: rule__Transformation__Group__3__Impl rule__Transformation__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__Transformation__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transformation__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__3"


    // $ANTLR start "rule__Transformation__Group__3__Impl"
    // InternalGame.g:1682:1: rule__Transformation__Group__3__Impl : ( ':' ) ;
    public final void rule__Transformation__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1686:1: ( ( ':' ) )
            // InternalGame.g:1687:1: ( ':' )
            {
            // InternalGame.g:1687:1: ( ':' )
            // InternalGame.g:1688:2: ':'
            {
             before(grammarAccess.getTransformationAccess().getColonKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getTransformationAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__3__Impl"


    // $ANTLR start "rule__Transformation__Group__4"
    // InternalGame.g:1697:1: rule__Transformation__Group__4 : rule__Transformation__Group__4__Impl rule__Transformation__Group__5 ;
    public final void rule__Transformation__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1701:1: ( rule__Transformation__Group__4__Impl rule__Transformation__Group__5 )
            // InternalGame.g:1702:2: rule__Transformation__Group__4__Impl rule__Transformation__Group__5
            {
            pushFollow(FOLLOW_19);
            rule__Transformation__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transformation__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__4"


    // $ANTLR start "rule__Transformation__Group__4__Impl"
    // InternalGame.g:1709:1: rule__Transformation__Group__4__Impl : ( ( rule__Transformation__ConditionAssignment_4 ) ) ;
    public final void rule__Transformation__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1713:1: ( ( ( rule__Transformation__ConditionAssignment_4 ) ) )
            // InternalGame.g:1714:1: ( ( rule__Transformation__ConditionAssignment_4 ) )
            {
            // InternalGame.g:1714:1: ( ( rule__Transformation__ConditionAssignment_4 ) )
            // InternalGame.g:1715:2: ( rule__Transformation__ConditionAssignment_4 )
            {
             before(grammarAccess.getTransformationAccess().getConditionAssignment_4()); 
            // InternalGame.g:1716:2: ( rule__Transformation__ConditionAssignment_4 )
            // InternalGame.g:1716:3: rule__Transformation__ConditionAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Transformation__ConditionAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getTransformationAccess().getConditionAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__4__Impl"


    // $ANTLR start "rule__Transformation__Group__5"
    // InternalGame.g:1724:1: rule__Transformation__Group__5 : rule__Transformation__Group__5__Impl rule__Transformation__Group__6 ;
    public final void rule__Transformation__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1728:1: ( rule__Transformation__Group__5__Impl rule__Transformation__Group__6 )
            // InternalGame.g:1729:2: rule__Transformation__Group__5__Impl rule__Transformation__Group__6
            {
            pushFollow(FOLLOW_3);
            rule__Transformation__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transformation__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__5"


    // $ANTLR start "rule__Transformation__Group__5__Impl"
    // InternalGame.g:1736:1: rule__Transformation__Group__5__Impl : ( 'objets_in' ) ;
    public final void rule__Transformation__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1740:1: ( ( 'objets_in' ) )
            // InternalGame.g:1741:1: ( 'objets_in' )
            {
            // InternalGame.g:1741:1: ( 'objets_in' )
            // InternalGame.g:1742:2: 'objets_in'
            {
             before(grammarAccess.getTransformationAccess().getObjets_inKeyword_5()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getTransformationAccess().getObjets_inKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__5__Impl"


    // $ANTLR start "rule__Transformation__Group__6"
    // InternalGame.g:1751:1: rule__Transformation__Group__6 : rule__Transformation__Group__6__Impl rule__Transformation__Group__7 ;
    public final void rule__Transformation__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1755:1: ( rule__Transformation__Group__6__Impl rule__Transformation__Group__7 )
            // InternalGame.g:1756:2: rule__Transformation__Group__6__Impl rule__Transformation__Group__7
            {
            pushFollow(FOLLOW_20);
            rule__Transformation__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transformation__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__6"


    // $ANTLR start "rule__Transformation__Group__6__Impl"
    // InternalGame.g:1763:1: rule__Transformation__Group__6__Impl : ( ':' ) ;
    public final void rule__Transformation__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1767:1: ( ( ':' ) )
            // InternalGame.g:1768:1: ( ':' )
            {
            // InternalGame.g:1768:1: ( ':' )
            // InternalGame.g:1769:2: ':'
            {
             before(grammarAccess.getTransformationAccess().getColonKeyword_6()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getTransformationAccess().getColonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__6__Impl"


    // $ANTLR start "rule__Transformation__Group__7"
    // InternalGame.g:1778:1: rule__Transformation__Group__7 : rule__Transformation__Group__7__Impl rule__Transformation__Group__8 ;
    public final void rule__Transformation__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1782:1: ( rule__Transformation__Group__7__Impl rule__Transformation__Group__8 )
            // InternalGame.g:1783:2: rule__Transformation__Group__7__Impl rule__Transformation__Group__8
            {
            pushFollow(FOLLOW_20);
            rule__Transformation__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transformation__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__7"


    // $ANTLR start "rule__Transformation__Group__7__Impl"
    // InternalGame.g:1790:1: rule__Transformation__Group__7__Impl : ( ( rule__Transformation__Group_7__0 )* ) ;
    public final void rule__Transformation__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1794:1: ( ( ( rule__Transformation__Group_7__0 )* ) )
            // InternalGame.g:1795:1: ( ( rule__Transformation__Group_7__0 )* )
            {
            // InternalGame.g:1795:1: ( ( rule__Transformation__Group_7__0 )* )
            // InternalGame.g:1796:2: ( rule__Transformation__Group_7__0 )*
            {
             before(grammarAccess.getTransformationAccess().getGroup_7()); 
            // InternalGame.g:1797:2: ( rule__Transformation__Group_7__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==20) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalGame.g:1797:3: rule__Transformation__Group_7__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Transformation__Group_7__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getTransformationAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__7__Impl"


    // $ANTLR start "rule__Transformation__Group__8"
    // InternalGame.g:1805:1: rule__Transformation__Group__8 : rule__Transformation__Group__8__Impl rule__Transformation__Group__9 ;
    public final void rule__Transformation__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1809:1: ( rule__Transformation__Group__8__Impl rule__Transformation__Group__9 )
            // InternalGame.g:1810:2: rule__Transformation__Group__8__Impl rule__Transformation__Group__9
            {
            pushFollow(FOLLOW_3);
            rule__Transformation__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transformation__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__8"


    // $ANTLR start "rule__Transformation__Group__8__Impl"
    // InternalGame.g:1817:1: rule__Transformation__Group__8__Impl : ( 'objets_out' ) ;
    public final void rule__Transformation__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1821:1: ( ( 'objets_out' ) )
            // InternalGame.g:1822:1: ( 'objets_out' )
            {
            // InternalGame.g:1822:1: ( 'objets_out' )
            // InternalGame.g:1823:2: 'objets_out'
            {
             before(grammarAccess.getTransformationAccess().getObjets_outKeyword_8()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getTransformationAccess().getObjets_outKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__8__Impl"


    // $ANTLR start "rule__Transformation__Group__9"
    // InternalGame.g:1832:1: rule__Transformation__Group__9 : rule__Transformation__Group__9__Impl rule__Transformation__Group__10 ;
    public final void rule__Transformation__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1836:1: ( rule__Transformation__Group__9__Impl rule__Transformation__Group__10 )
            // InternalGame.g:1837:2: rule__Transformation__Group__9__Impl rule__Transformation__Group__10
            {
            pushFollow(FOLLOW_17);
            rule__Transformation__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transformation__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__9"


    // $ANTLR start "rule__Transformation__Group__9__Impl"
    // InternalGame.g:1844:1: rule__Transformation__Group__9__Impl : ( ':' ) ;
    public final void rule__Transformation__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1848:1: ( ( ':' ) )
            // InternalGame.g:1849:1: ( ':' )
            {
            // InternalGame.g:1849:1: ( ':' )
            // InternalGame.g:1850:2: ':'
            {
             before(grammarAccess.getTransformationAccess().getColonKeyword_9()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getTransformationAccess().getColonKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__9__Impl"


    // $ANTLR start "rule__Transformation__Group__10"
    // InternalGame.g:1859:1: rule__Transformation__Group__10 : rule__Transformation__Group__10__Impl ;
    public final void rule__Transformation__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1863:1: ( rule__Transformation__Group__10__Impl )
            // InternalGame.g:1864:2: rule__Transformation__Group__10__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transformation__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__10"


    // $ANTLR start "rule__Transformation__Group__10__Impl"
    // InternalGame.g:1870:1: rule__Transformation__Group__10__Impl : ( ( rule__Transformation__Group_10__0 )* ) ;
    public final void rule__Transformation__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1874:1: ( ( ( rule__Transformation__Group_10__0 )* ) )
            // InternalGame.g:1875:1: ( ( rule__Transformation__Group_10__0 )* )
            {
            // InternalGame.g:1875:1: ( ( rule__Transformation__Group_10__0 )* )
            // InternalGame.g:1876:2: ( rule__Transformation__Group_10__0 )*
            {
             before(grammarAccess.getTransformationAccess().getGroup_10()); 
            // InternalGame.g:1877:2: ( rule__Transformation__Group_10__0 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==20) ) {
                    int LA8_2 = input.LA(2);

                    if ( (LA8_2==RULE_ID) ) {
                        int LA8_3 = input.LA(3);

                        if ( (LA8_3==EOF||LA8_3==16||LA8_3==20) ) {
                            alt8=1;
                        }


                    }


                }


                switch (alt8) {
            	case 1 :
            	    // InternalGame.g:1877:3: rule__Transformation__Group_10__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Transformation__Group_10__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getTransformationAccess().getGroup_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group__10__Impl"


    // $ANTLR start "rule__Transformation__Group_7__0"
    // InternalGame.g:1886:1: rule__Transformation__Group_7__0 : rule__Transformation__Group_7__0__Impl rule__Transformation__Group_7__1 ;
    public final void rule__Transformation__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1890:1: ( rule__Transformation__Group_7__0__Impl rule__Transformation__Group_7__1 )
            // InternalGame.g:1891:2: rule__Transformation__Group_7__0__Impl rule__Transformation__Group_7__1
            {
            pushFollow(FOLLOW_12);
            rule__Transformation__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transformation__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group_7__0"


    // $ANTLR start "rule__Transformation__Group_7__0__Impl"
    // InternalGame.g:1898:1: rule__Transformation__Group_7__0__Impl : ( '-' ) ;
    public final void rule__Transformation__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1902:1: ( ( '-' ) )
            // InternalGame.g:1903:1: ( '-' )
            {
            // InternalGame.g:1903:1: ( '-' )
            // InternalGame.g:1904:2: '-'
            {
             before(grammarAccess.getTransformationAccess().getHyphenMinusKeyword_7_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getTransformationAccess().getHyphenMinusKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group_7__0__Impl"


    // $ANTLR start "rule__Transformation__Group_7__1"
    // InternalGame.g:1913:1: rule__Transformation__Group_7__1 : rule__Transformation__Group_7__1__Impl ;
    public final void rule__Transformation__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1917:1: ( rule__Transformation__Group_7__1__Impl )
            // InternalGame.g:1918:2: rule__Transformation__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transformation__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group_7__1"


    // $ANTLR start "rule__Transformation__Group_7__1__Impl"
    // InternalGame.g:1924:1: rule__Transformation__Group_7__1__Impl : ( ( rule__Transformation__ObjetsInAssignment_7_1 ) ) ;
    public final void rule__Transformation__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1928:1: ( ( ( rule__Transformation__ObjetsInAssignment_7_1 ) ) )
            // InternalGame.g:1929:1: ( ( rule__Transformation__ObjetsInAssignment_7_1 ) )
            {
            // InternalGame.g:1929:1: ( ( rule__Transformation__ObjetsInAssignment_7_1 ) )
            // InternalGame.g:1930:2: ( rule__Transformation__ObjetsInAssignment_7_1 )
            {
             before(grammarAccess.getTransformationAccess().getObjetsInAssignment_7_1()); 
            // InternalGame.g:1931:2: ( rule__Transformation__ObjetsInAssignment_7_1 )
            // InternalGame.g:1931:3: rule__Transformation__ObjetsInAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__Transformation__ObjetsInAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getTransformationAccess().getObjetsInAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group_7__1__Impl"


    // $ANTLR start "rule__Transformation__Group_10__0"
    // InternalGame.g:1940:1: rule__Transformation__Group_10__0 : rule__Transformation__Group_10__0__Impl rule__Transformation__Group_10__1 ;
    public final void rule__Transformation__Group_10__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1944:1: ( rule__Transformation__Group_10__0__Impl rule__Transformation__Group_10__1 )
            // InternalGame.g:1945:2: rule__Transformation__Group_10__0__Impl rule__Transformation__Group_10__1
            {
            pushFollow(FOLLOW_12);
            rule__Transformation__Group_10__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transformation__Group_10__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group_10__0"


    // $ANTLR start "rule__Transformation__Group_10__0__Impl"
    // InternalGame.g:1952:1: rule__Transformation__Group_10__0__Impl : ( '-' ) ;
    public final void rule__Transformation__Group_10__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1956:1: ( ( '-' ) )
            // InternalGame.g:1957:1: ( '-' )
            {
            // InternalGame.g:1957:1: ( '-' )
            // InternalGame.g:1958:2: '-'
            {
             before(grammarAccess.getTransformationAccess().getHyphenMinusKeyword_10_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getTransformationAccess().getHyphenMinusKeyword_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group_10__0__Impl"


    // $ANTLR start "rule__Transformation__Group_10__1"
    // InternalGame.g:1967:1: rule__Transformation__Group_10__1 : rule__Transformation__Group_10__1__Impl ;
    public final void rule__Transformation__Group_10__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1971:1: ( rule__Transformation__Group_10__1__Impl )
            // InternalGame.g:1972:2: rule__Transformation__Group_10__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transformation__Group_10__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group_10__1"


    // $ANTLR start "rule__Transformation__Group_10__1__Impl"
    // InternalGame.g:1978:1: rule__Transformation__Group_10__1__Impl : ( ( rule__Transformation__ObjetsOutAssignment_10_1 ) ) ;
    public final void rule__Transformation__Group_10__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1982:1: ( ( ( rule__Transformation__ObjetsOutAssignment_10_1 ) ) )
            // InternalGame.g:1983:1: ( ( rule__Transformation__ObjetsOutAssignment_10_1 ) )
            {
            // InternalGame.g:1983:1: ( ( rule__Transformation__ObjetsOutAssignment_10_1 ) )
            // InternalGame.g:1984:2: ( rule__Transformation__ObjetsOutAssignment_10_1 )
            {
             before(grammarAccess.getTransformationAccess().getObjetsOutAssignment_10_1()); 
            // InternalGame.g:1985:2: ( rule__Transformation__ObjetsOutAssignment_10_1 )
            // InternalGame.g:1985:3: rule__Transformation__ObjetsOutAssignment_10_1
            {
            pushFollow(FOLLOW_2);
            rule__Transformation__ObjetsOutAssignment_10_1();

            state._fsp--;


            }

             after(grammarAccess.getTransformationAccess().getObjetsOutAssignment_10_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__Group_10__1__Impl"


    // $ANTLR start "rule__Connaissance__Group__0"
    // InternalGame.g:1994:1: rule__Connaissance__Group__0 : rule__Connaissance__Group__0__Impl rule__Connaissance__Group__1 ;
    public final void rule__Connaissance__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:1998:1: ( rule__Connaissance__Group__0__Impl rule__Connaissance__Group__1 )
            // InternalGame.g:1999:2: rule__Connaissance__Group__0__Impl rule__Connaissance__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Connaissance__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Connaissance__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__0"


    // $ANTLR start "rule__Connaissance__Group__0__Impl"
    // InternalGame.g:2006:1: rule__Connaissance__Group__0__Impl : ( ( rule__Connaissance__NameAssignment_0 ) ) ;
    public final void rule__Connaissance__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2010:1: ( ( ( rule__Connaissance__NameAssignment_0 ) ) )
            // InternalGame.g:2011:1: ( ( rule__Connaissance__NameAssignment_0 ) )
            {
            // InternalGame.g:2011:1: ( ( rule__Connaissance__NameAssignment_0 ) )
            // InternalGame.g:2012:2: ( rule__Connaissance__NameAssignment_0 )
            {
             before(grammarAccess.getConnaissanceAccess().getNameAssignment_0()); 
            // InternalGame.g:2013:2: ( rule__Connaissance__NameAssignment_0 )
            // InternalGame.g:2013:3: rule__Connaissance__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Connaissance__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getConnaissanceAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__0__Impl"


    // $ANTLR start "rule__Connaissance__Group__1"
    // InternalGame.g:2021:1: rule__Connaissance__Group__1 : rule__Connaissance__Group__1__Impl rule__Connaissance__Group__2 ;
    public final void rule__Connaissance__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2025:1: ( rule__Connaissance__Group__1__Impl rule__Connaissance__Group__2 )
            // InternalGame.g:2026:2: rule__Connaissance__Group__1__Impl rule__Connaissance__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Connaissance__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Connaissance__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__1"


    // $ANTLR start "rule__Connaissance__Group__1__Impl"
    // InternalGame.g:2033:1: rule__Connaissance__Group__1__Impl : ( ':' ) ;
    public final void rule__Connaissance__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2037:1: ( ( ':' ) )
            // InternalGame.g:2038:1: ( ':' )
            {
            // InternalGame.g:2038:1: ( ':' )
            // InternalGame.g:2039:2: ':'
            {
             before(grammarAccess.getConnaissanceAccess().getColonKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getConnaissanceAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__1__Impl"


    // $ANTLR start "rule__Connaissance__Group__2"
    // InternalGame.g:2048:1: rule__Connaissance__Group__2 : rule__Connaissance__Group__2__Impl rule__Connaissance__Group__3 ;
    public final void rule__Connaissance__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2052:1: ( rule__Connaissance__Group__2__Impl rule__Connaissance__Group__3 )
            // InternalGame.g:2053:2: rule__Connaissance__Group__2__Impl rule__Connaissance__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__Connaissance__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Connaissance__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__2"


    // $ANTLR start "rule__Connaissance__Group__2__Impl"
    // InternalGame.g:2060:1: rule__Connaissance__Group__2__Impl : ( 'visible' ) ;
    public final void rule__Connaissance__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2064:1: ( ( 'visible' ) )
            // InternalGame.g:2065:1: ( 'visible' )
            {
            // InternalGame.g:2065:1: ( 'visible' )
            // InternalGame.g:2066:2: 'visible'
            {
             before(grammarAccess.getConnaissanceAccess().getVisibleKeyword_2()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getConnaissanceAccess().getVisibleKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__2__Impl"


    // $ANTLR start "rule__Connaissance__Group__3"
    // InternalGame.g:2075:1: rule__Connaissance__Group__3 : rule__Connaissance__Group__3__Impl rule__Connaissance__Group__4 ;
    public final void rule__Connaissance__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2079:1: ( rule__Connaissance__Group__3__Impl rule__Connaissance__Group__4 )
            // InternalGame.g:2080:2: rule__Connaissance__Group__3__Impl rule__Connaissance__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__Connaissance__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Connaissance__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__3"


    // $ANTLR start "rule__Connaissance__Group__3__Impl"
    // InternalGame.g:2087:1: rule__Connaissance__Group__3__Impl : ( ':' ) ;
    public final void rule__Connaissance__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2091:1: ( ( ':' ) )
            // InternalGame.g:2092:1: ( ':' )
            {
            // InternalGame.g:2092:1: ( ':' )
            // InternalGame.g:2093:2: ':'
            {
             before(grammarAccess.getConnaissanceAccess().getColonKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getConnaissanceAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__3__Impl"


    // $ANTLR start "rule__Connaissance__Group__4"
    // InternalGame.g:2102:1: rule__Connaissance__Group__4 : rule__Connaissance__Group__4__Impl rule__Connaissance__Group__5 ;
    public final void rule__Connaissance__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2106:1: ( rule__Connaissance__Group__4__Impl rule__Connaissance__Group__5 )
            // InternalGame.g:2107:2: rule__Connaissance__Group__4__Impl rule__Connaissance__Group__5
            {
            pushFollow(FOLLOW_16);
            rule__Connaissance__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Connaissance__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__4"


    // $ANTLR start "rule__Connaissance__Group__4__Impl"
    // InternalGame.g:2114:1: rule__Connaissance__Group__4__Impl : ( ( rule__Connaissance__VisibleAssignment_4 ) ) ;
    public final void rule__Connaissance__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2118:1: ( ( ( rule__Connaissance__VisibleAssignment_4 ) ) )
            // InternalGame.g:2119:1: ( ( rule__Connaissance__VisibleAssignment_4 ) )
            {
            // InternalGame.g:2119:1: ( ( rule__Connaissance__VisibleAssignment_4 ) )
            // InternalGame.g:2120:2: ( rule__Connaissance__VisibleAssignment_4 )
            {
             before(grammarAccess.getConnaissanceAccess().getVisibleAssignment_4()); 
            // InternalGame.g:2121:2: ( rule__Connaissance__VisibleAssignment_4 )
            // InternalGame.g:2121:3: rule__Connaissance__VisibleAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Connaissance__VisibleAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getConnaissanceAccess().getVisibleAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__4__Impl"


    // $ANTLR start "rule__Connaissance__Group__5"
    // InternalGame.g:2129:1: rule__Connaissance__Group__5 : rule__Connaissance__Group__5__Impl rule__Connaissance__Group__6 ;
    public final void rule__Connaissance__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2133:1: ( rule__Connaissance__Group__5__Impl rule__Connaissance__Group__6 )
            // InternalGame.g:2134:2: rule__Connaissance__Group__5__Impl rule__Connaissance__Group__6
            {
            pushFollow(FOLLOW_3);
            rule__Connaissance__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Connaissance__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__5"


    // $ANTLR start "rule__Connaissance__Group__5__Impl"
    // InternalGame.g:2141:1: rule__Connaissance__Group__5__Impl : ( 'descriptions' ) ;
    public final void rule__Connaissance__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2145:1: ( ( 'descriptions' ) )
            // InternalGame.g:2146:1: ( 'descriptions' )
            {
            // InternalGame.g:2146:1: ( 'descriptions' )
            // InternalGame.g:2147:2: 'descriptions'
            {
             before(grammarAccess.getConnaissanceAccess().getDescriptionsKeyword_5()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getConnaissanceAccess().getDescriptionsKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__5__Impl"


    // $ANTLR start "rule__Connaissance__Group__6"
    // InternalGame.g:2156:1: rule__Connaissance__Group__6 : rule__Connaissance__Group__6__Impl rule__Connaissance__Group__7 ;
    public final void rule__Connaissance__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2160:1: ( rule__Connaissance__Group__6__Impl rule__Connaissance__Group__7 )
            // InternalGame.g:2161:2: rule__Connaissance__Group__6__Impl rule__Connaissance__Group__7
            {
            pushFollow(FOLLOW_17);
            rule__Connaissance__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Connaissance__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__6"


    // $ANTLR start "rule__Connaissance__Group__6__Impl"
    // InternalGame.g:2168:1: rule__Connaissance__Group__6__Impl : ( ':' ) ;
    public final void rule__Connaissance__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2172:1: ( ( ':' ) )
            // InternalGame.g:2173:1: ( ':' )
            {
            // InternalGame.g:2173:1: ( ':' )
            // InternalGame.g:2174:2: ':'
            {
             before(grammarAccess.getConnaissanceAccess().getColonKeyword_6()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getConnaissanceAccess().getColonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__6__Impl"


    // $ANTLR start "rule__Connaissance__Group__7"
    // InternalGame.g:2183:1: rule__Connaissance__Group__7 : rule__Connaissance__Group__7__Impl ;
    public final void rule__Connaissance__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2187:1: ( rule__Connaissance__Group__7__Impl )
            // InternalGame.g:2188:2: rule__Connaissance__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Connaissance__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__7"


    // $ANTLR start "rule__Connaissance__Group__7__Impl"
    // InternalGame.g:2194:1: rule__Connaissance__Group__7__Impl : ( ( ( rule__Connaissance__Group_7__0 ) ) ( ( rule__Connaissance__Group_7__0 )* ) ) ;
    public final void rule__Connaissance__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2198:1: ( ( ( ( rule__Connaissance__Group_7__0 ) ) ( ( rule__Connaissance__Group_7__0 )* ) ) )
            // InternalGame.g:2199:1: ( ( ( rule__Connaissance__Group_7__0 ) ) ( ( rule__Connaissance__Group_7__0 )* ) )
            {
            // InternalGame.g:2199:1: ( ( ( rule__Connaissance__Group_7__0 ) ) ( ( rule__Connaissance__Group_7__0 )* ) )
            // InternalGame.g:2200:2: ( ( rule__Connaissance__Group_7__0 ) ) ( ( rule__Connaissance__Group_7__0 )* )
            {
            // InternalGame.g:2200:2: ( ( rule__Connaissance__Group_7__0 ) )
            // InternalGame.g:2201:3: ( rule__Connaissance__Group_7__0 )
            {
             before(grammarAccess.getConnaissanceAccess().getGroup_7()); 
            // InternalGame.g:2202:3: ( rule__Connaissance__Group_7__0 )
            // InternalGame.g:2202:4: rule__Connaissance__Group_7__0
            {
            pushFollow(FOLLOW_5);
            rule__Connaissance__Group_7__0();

            state._fsp--;


            }

             after(grammarAccess.getConnaissanceAccess().getGroup_7()); 

            }

            // InternalGame.g:2205:2: ( ( rule__Connaissance__Group_7__0 )* )
            // InternalGame.g:2206:3: ( rule__Connaissance__Group_7__0 )*
            {
             before(grammarAccess.getConnaissanceAccess().getGroup_7()); 
            // InternalGame.g:2207:3: ( rule__Connaissance__Group_7__0 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==20) ) {
                    int LA9_2 = input.LA(2);

                    if ( (LA9_2==RULE_ID) ) {
                        int LA9_3 = input.LA(3);

                        if ( (LA9_3==14) ) {
                            int LA9_4 = input.LA(4);

                            if ( (LA9_4==44) ) {
                                alt9=1;
                            }


                        }


                    }


                }


                switch (alt9) {
            	case 1 :
            	    // InternalGame.g:2207:4: rule__Connaissance__Group_7__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Connaissance__Group_7__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getConnaissanceAccess().getGroup_7()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group__7__Impl"


    // $ANTLR start "rule__Connaissance__Group_7__0"
    // InternalGame.g:2217:1: rule__Connaissance__Group_7__0 : rule__Connaissance__Group_7__0__Impl rule__Connaissance__Group_7__1 ;
    public final void rule__Connaissance__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2221:1: ( rule__Connaissance__Group_7__0__Impl rule__Connaissance__Group_7__1 )
            // InternalGame.g:2222:2: rule__Connaissance__Group_7__0__Impl rule__Connaissance__Group_7__1
            {
            pushFollow(FOLLOW_12);
            rule__Connaissance__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Connaissance__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group_7__0"


    // $ANTLR start "rule__Connaissance__Group_7__0__Impl"
    // InternalGame.g:2229:1: rule__Connaissance__Group_7__0__Impl : ( '-' ) ;
    public final void rule__Connaissance__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2233:1: ( ( '-' ) )
            // InternalGame.g:2234:1: ( '-' )
            {
            // InternalGame.g:2234:1: ( '-' )
            // InternalGame.g:2235:2: '-'
            {
             before(grammarAccess.getConnaissanceAccess().getHyphenMinusKeyword_7_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getConnaissanceAccess().getHyphenMinusKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group_7__0__Impl"


    // $ANTLR start "rule__Connaissance__Group_7__1"
    // InternalGame.g:2244:1: rule__Connaissance__Group_7__1 : rule__Connaissance__Group_7__1__Impl ;
    public final void rule__Connaissance__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2248:1: ( rule__Connaissance__Group_7__1__Impl )
            // InternalGame.g:2249:2: rule__Connaissance__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Connaissance__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group_7__1"


    // $ANTLR start "rule__Connaissance__Group_7__1__Impl"
    // InternalGame.g:2255:1: rule__Connaissance__Group_7__1__Impl : ( ( rule__Connaissance__DescriptionsAssignment_7_1 ) ) ;
    public final void rule__Connaissance__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2259:1: ( ( ( rule__Connaissance__DescriptionsAssignment_7_1 ) ) )
            // InternalGame.g:2260:1: ( ( rule__Connaissance__DescriptionsAssignment_7_1 ) )
            {
            // InternalGame.g:2260:1: ( ( rule__Connaissance__DescriptionsAssignment_7_1 ) )
            // InternalGame.g:2261:2: ( rule__Connaissance__DescriptionsAssignment_7_1 )
            {
             before(grammarAccess.getConnaissanceAccess().getDescriptionsAssignment_7_1()); 
            // InternalGame.g:2262:2: ( rule__Connaissance__DescriptionsAssignment_7_1 )
            // InternalGame.g:2262:3: rule__Connaissance__DescriptionsAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__Connaissance__DescriptionsAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getConnaissanceAccess().getDescriptionsAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__Group_7__1__Impl"


    // $ANTLR start "rule__Explorateur__Group__0"
    // InternalGame.g:2271:1: rule__Explorateur__Group__0 : rule__Explorateur__Group__0__Impl rule__Explorateur__Group__1 ;
    public final void rule__Explorateur__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2275:1: ( rule__Explorateur__Group__0__Impl rule__Explorateur__Group__1 )
            // InternalGame.g:2276:2: rule__Explorateur__Group__0__Impl rule__Explorateur__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Explorateur__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Explorateur__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__0"


    // $ANTLR start "rule__Explorateur__Group__0__Impl"
    // InternalGame.g:2283:1: rule__Explorateur__Group__0__Impl : ( 'taille' ) ;
    public final void rule__Explorateur__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2287:1: ( ( 'taille' ) )
            // InternalGame.g:2288:1: ( 'taille' )
            {
            // InternalGame.g:2288:1: ( 'taille' )
            // InternalGame.g:2289:2: 'taille'
            {
             before(grammarAccess.getExplorateurAccess().getTailleKeyword_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getExplorateurAccess().getTailleKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__0__Impl"


    // $ANTLR start "rule__Explorateur__Group__1"
    // InternalGame.g:2298:1: rule__Explorateur__Group__1 : rule__Explorateur__Group__1__Impl rule__Explorateur__Group__2 ;
    public final void rule__Explorateur__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2302:1: ( rule__Explorateur__Group__1__Impl rule__Explorateur__Group__2 )
            // InternalGame.g:2303:2: rule__Explorateur__Group__1__Impl rule__Explorateur__Group__2
            {
            pushFollow(FOLLOW_13);
            rule__Explorateur__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Explorateur__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__1"


    // $ANTLR start "rule__Explorateur__Group__1__Impl"
    // InternalGame.g:2310:1: rule__Explorateur__Group__1__Impl : ( ':' ) ;
    public final void rule__Explorateur__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2314:1: ( ( ':' ) )
            // InternalGame.g:2315:1: ( ':' )
            {
            // InternalGame.g:2315:1: ( ':' )
            // InternalGame.g:2316:2: ':'
            {
             before(grammarAccess.getExplorateurAccess().getColonKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getExplorateurAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__1__Impl"


    // $ANTLR start "rule__Explorateur__Group__2"
    // InternalGame.g:2325:1: rule__Explorateur__Group__2 : rule__Explorateur__Group__2__Impl rule__Explorateur__Group__3 ;
    public final void rule__Explorateur__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2329:1: ( rule__Explorateur__Group__2__Impl rule__Explorateur__Group__3 )
            // InternalGame.g:2330:2: rule__Explorateur__Group__2__Impl rule__Explorateur__Group__3
            {
            pushFollow(FOLLOW_21);
            rule__Explorateur__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Explorateur__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__2"


    // $ANTLR start "rule__Explorateur__Group__2__Impl"
    // InternalGame.g:2337:1: rule__Explorateur__Group__2__Impl : ( ( rule__Explorateur__TailleInventaireAssignment_2 ) ) ;
    public final void rule__Explorateur__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2341:1: ( ( ( rule__Explorateur__TailleInventaireAssignment_2 ) ) )
            // InternalGame.g:2342:1: ( ( rule__Explorateur__TailleInventaireAssignment_2 ) )
            {
            // InternalGame.g:2342:1: ( ( rule__Explorateur__TailleInventaireAssignment_2 ) )
            // InternalGame.g:2343:2: ( rule__Explorateur__TailleInventaireAssignment_2 )
            {
             before(grammarAccess.getExplorateurAccess().getTailleInventaireAssignment_2()); 
            // InternalGame.g:2344:2: ( rule__Explorateur__TailleInventaireAssignment_2 )
            // InternalGame.g:2344:3: rule__Explorateur__TailleInventaireAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Explorateur__TailleInventaireAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getExplorateurAccess().getTailleInventaireAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__2__Impl"


    // $ANTLR start "rule__Explorateur__Group__3"
    // InternalGame.g:2352:1: rule__Explorateur__Group__3 : rule__Explorateur__Group__3__Impl rule__Explorateur__Group__4 ;
    public final void rule__Explorateur__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2356:1: ( rule__Explorateur__Group__3__Impl rule__Explorateur__Group__4 )
            // InternalGame.g:2357:2: rule__Explorateur__Group__3__Impl rule__Explorateur__Group__4
            {
            pushFollow(FOLLOW_3);
            rule__Explorateur__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Explorateur__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__3"


    // $ANTLR start "rule__Explorateur__Group__3__Impl"
    // InternalGame.g:2364:1: rule__Explorateur__Group__3__Impl : ( 'connaissances' ) ;
    public final void rule__Explorateur__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2368:1: ( ( 'connaissances' ) )
            // InternalGame.g:2369:1: ( 'connaissances' )
            {
            // InternalGame.g:2369:1: ( 'connaissances' )
            // InternalGame.g:2370:2: 'connaissances'
            {
             before(grammarAccess.getExplorateurAccess().getConnaissancesKeyword_3()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getExplorateurAccess().getConnaissancesKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__3__Impl"


    // $ANTLR start "rule__Explorateur__Group__4"
    // InternalGame.g:2379:1: rule__Explorateur__Group__4 : rule__Explorateur__Group__4__Impl rule__Explorateur__Group__5 ;
    public final void rule__Explorateur__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2383:1: ( rule__Explorateur__Group__4__Impl rule__Explorateur__Group__5 )
            // InternalGame.g:2384:2: rule__Explorateur__Group__4__Impl rule__Explorateur__Group__5
            {
            pushFollow(FOLLOW_22);
            rule__Explorateur__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Explorateur__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__4"


    // $ANTLR start "rule__Explorateur__Group__4__Impl"
    // InternalGame.g:2391:1: rule__Explorateur__Group__4__Impl : ( ':' ) ;
    public final void rule__Explorateur__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2395:1: ( ( ':' ) )
            // InternalGame.g:2396:1: ( ':' )
            {
            // InternalGame.g:2396:1: ( ':' )
            // InternalGame.g:2397:2: ':'
            {
             before(grammarAccess.getExplorateurAccess().getColonKeyword_4()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getExplorateurAccess().getColonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__4__Impl"


    // $ANTLR start "rule__Explorateur__Group__5"
    // InternalGame.g:2406:1: rule__Explorateur__Group__5 : rule__Explorateur__Group__5__Impl rule__Explorateur__Group__6 ;
    public final void rule__Explorateur__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2410:1: ( rule__Explorateur__Group__5__Impl rule__Explorateur__Group__6 )
            // InternalGame.g:2411:2: rule__Explorateur__Group__5__Impl rule__Explorateur__Group__6
            {
            pushFollow(FOLLOW_22);
            rule__Explorateur__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Explorateur__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__5"


    // $ANTLR start "rule__Explorateur__Group__5__Impl"
    // InternalGame.g:2418:1: rule__Explorateur__Group__5__Impl : ( ( rule__Explorateur__Group_5__0 )* ) ;
    public final void rule__Explorateur__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2422:1: ( ( ( rule__Explorateur__Group_5__0 )* ) )
            // InternalGame.g:2423:1: ( ( rule__Explorateur__Group_5__0 )* )
            {
            // InternalGame.g:2423:1: ( ( rule__Explorateur__Group_5__0 )* )
            // InternalGame.g:2424:2: ( rule__Explorateur__Group_5__0 )*
            {
             before(grammarAccess.getExplorateurAccess().getGroup_5()); 
            // InternalGame.g:2425:2: ( rule__Explorateur__Group_5__0 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==20) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalGame.g:2425:3: rule__Explorateur__Group_5__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Explorateur__Group_5__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getExplorateurAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__5__Impl"


    // $ANTLR start "rule__Explorateur__Group__6"
    // InternalGame.g:2433:1: rule__Explorateur__Group__6 : rule__Explorateur__Group__6__Impl rule__Explorateur__Group__7 ;
    public final void rule__Explorateur__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2437:1: ( rule__Explorateur__Group__6__Impl rule__Explorateur__Group__7 )
            // InternalGame.g:2438:2: rule__Explorateur__Group__6__Impl rule__Explorateur__Group__7
            {
            pushFollow(FOLLOW_3);
            rule__Explorateur__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Explorateur__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__6"


    // $ANTLR start "rule__Explorateur__Group__6__Impl"
    // InternalGame.g:2445:1: rule__Explorateur__Group__6__Impl : ( 'objets' ) ;
    public final void rule__Explorateur__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2449:1: ( ( 'objets' ) )
            // InternalGame.g:2450:1: ( 'objets' )
            {
            // InternalGame.g:2450:1: ( 'objets' )
            // InternalGame.g:2451:2: 'objets'
            {
             before(grammarAccess.getExplorateurAccess().getObjetsKeyword_6()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getExplorateurAccess().getObjetsKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__6__Impl"


    // $ANTLR start "rule__Explorateur__Group__7"
    // InternalGame.g:2460:1: rule__Explorateur__Group__7 : rule__Explorateur__Group__7__Impl rule__Explorateur__Group__8 ;
    public final void rule__Explorateur__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2464:1: ( rule__Explorateur__Group__7__Impl rule__Explorateur__Group__8 )
            // InternalGame.g:2465:2: rule__Explorateur__Group__7__Impl rule__Explorateur__Group__8
            {
            pushFollow(FOLLOW_17);
            rule__Explorateur__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Explorateur__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__7"


    // $ANTLR start "rule__Explorateur__Group__7__Impl"
    // InternalGame.g:2472:1: rule__Explorateur__Group__7__Impl : ( ':' ) ;
    public final void rule__Explorateur__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2476:1: ( ( ':' ) )
            // InternalGame.g:2477:1: ( ':' )
            {
            // InternalGame.g:2477:1: ( ':' )
            // InternalGame.g:2478:2: ':'
            {
             before(grammarAccess.getExplorateurAccess().getColonKeyword_7()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getExplorateurAccess().getColonKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__7__Impl"


    // $ANTLR start "rule__Explorateur__Group__8"
    // InternalGame.g:2487:1: rule__Explorateur__Group__8 : rule__Explorateur__Group__8__Impl ;
    public final void rule__Explorateur__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2491:1: ( rule__Explorateur__Group__8__Impl )
            // InternalGame.g:2492:2: rule__Explorateur__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Explorateur__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__8"


    // $ANTLR start "rule__Explorateur__Group__8__Impl"
    // InternalGame.g:2498:1: rule__Explorateur__Group__8__Impl : ( ( rule__Explorateur__Group_8__0 )* ) ;
    public final void rule__Explorateur__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2502:1: ( ( ( rule__Explorateur__Group_8__0 )* ) )
            // InternalGame.g:2503:1: ( ( rule__Explorateur__Group_8__0 )* )
            {
            // InternalGame.g:2503:1: ( ( rule__Explorateur__Group_8__0 )* )
            // InternalGame.g:2504:2: ( rule__Explorateur__Group_8__0 )*
            {
             before(grammarAccess.getExplorateurAccess().getGroup_8()); 
            // InternalGame.g:2505:2: ( rule__Explorateur__Group_8__0 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==20) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalGame.g:2505:3: rule__Explorateur__Group_8__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Explorateur__Group_8__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getExplorateurAccess().getGroup_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group__8__Impl"


    // $ANTLR start "rule__Explorateur__Group_5__0"
    // InternalGame.g:2514:1: rule__Explorateur__Group_5__0 : rule__Explorateur__Group_5__0__Impl rule__Explorateur__Group_5__1 ;
    public final void rule__Explorateur__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2518:1: ( rule__Explorateur__Group_5__0__Impl rule__Explorateur__Group_5__1 )
            // InternalGame.g:2519:2: rule__Explorateur__Group_5__0__Impl rule__Explorateur__Group_5__1
            {
            pushFollow(FOLLOW_12);
            rule__Explorateur__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Explorateur__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group_5__0"


    // $ANTLR start "rule__Explorateur__Group_5__0__Impl"
    // InternalGame.g:2526:1: rule__Explorateur__Group_5__0__Impl : ( '-' ) ;
    public final void rule__Explorateur__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2530:1: ( ( '-' ) )
            // InternalGame.g:2531:1: ( '-' )
            {
            // InternalGame.g:2531:1: ( '-' )
            // InternalGame.g:2532:2: '-'
            {
             before(grammarAccess.getExplorateurAccess().getHyphenMinusKeyword_5_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getExplorateurAccess().getHyphenMinusKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group_5__0__Impl"


    // $ANTLR start "rule__Explorateur__Group_5__1"
    // InternalGame.g:2541:1: rule__Explorateur__Group_5__1 : rule__Explorateur__Group_5__1__Impl ;
    public final void rule__Explorateur__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2545:1: ( rule__Explorateur__Group_5__1__Impl )
            // InternalGame.g:2546:2: rule__Explorateur__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Explorateur__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group_5__1"


    // $ANTLR start "rule__Explorateur__Group_5__1__Impl"
    // InternalGame.g:2552:1: rule__Explorateur__Group_5__1__Impl : ( ( rule__Explorateur__ConnaissancesAssignment_5_1 ) ) ;
    public final void rule__Explorateur__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2556:1: ( ( ( rule__Explorateur__ConnaissancesAssignment_5_1 ) ) )
            // InternalGame.g:2557:1: ( ( rule__Explorateur__ConnaissancesAssignment_5_1 ) )
            {
            // InternalGame.g:2557:1: ( ( rule__Explorateur__ConnaissancesAssignment_5_1 ) )
            // InternalGame.g:2558:2: ( rule__Explorateur__ConnaissancesAssignment_5_1 )
            {
             before(grammarAccess.getExplorateurAccess().getConnaissancesAssignment_5_1()); 
            // InternalGame.g:2559:2: ( rule__Explorateur__ConnaissancesAssignment_5_1 )
            // InternalGame.g:2559:3: rule__Explorateur__ConnaissancesAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Explorateur__ConnaissancesAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getExplorateurAccess().getConnaissancesAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group_5__1__Impl"


    // $ANTLR start "rule__Explorateur__Group_8__0"
    // InternalGame.g:2568:1: rule__Explorateur__Group_8__0 : rule__Explorateur__Group_8__0__Impl rule__Explorateur__Group_8__1 ;
    public final void rule__Explorateur__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2572:1: ( rule__Explorateur__Group_8__0__Impl rule__Explorateur__Group_8__1 )
            // InternalGame.g:2573:2: rule__Explorateur__Group_8__0__Impl rule__Explorateur__Group_8__1
            {
            pushFollow(FOLLOW_12);
            rule__Explorateur__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Explorateur__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group_8__0"


    // $ANTLR start "rule__Explorateur__Group_8__0__Impl"
    // InternalGame.g:2580:1: rule__Explorateur__Group_8__0__Impl : ( '-' ) ;
    public final void rule__Explorateur__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2584:1: ( ( '-' ) )
            // InternalGame.g:2585:1: ( '-' )
            {
            // InternalGame.g:2585:1: ( '-' )
            // InternalGame.g:2586:2: '-'
            {
             before(grammarAccess.getExplorateurAccess().getHyphenMinusKeyword_8_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getExplorateurAccess().getHyphenMinusKeyword_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group_8__0__Impl"


    // $ANTLR start "rule__Explorateur__Group_8__1"
    // InternalGame.g:2595:1: rule__Explorateur__Group_8__1 : rule__Explorateur__Group_8__1__Impl ;
    public final void rule__Explorateur__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2599:1: ( rule__Explorateur__Group_8__1__Impl )
            // InternalGame.g:2600:2: rule__Explorateur__Group_8__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Explorateur__Group_8__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group_8__1"


    // $ANTLR start "rule__Explorateur__Group_8__1__Impl"
    // InternalGame.g:2606:1: rule__Explorateur__Group_8__1__Impl : ( ( rule__Explorateur__ObjetsAssignment_8_1 ) ) ;
    public final void rule__Explorateur__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2610:1: ( ( ( rule__Explorateur__ObjetsAssignment_8_1 ) ) )
            // InternalGame.g:2611:1: ( ( rule__Explorateur__ObjetsAssignment_8_1 ) )
            {
            // InternalGame.g:2611:1: ( ( rule__Explorateur__ObjetsAssignment_8_1 ) )
            // InternalGame.g:2612:2: ( rule__Explorateur__ObjetsAssignment_8_1 )
            {
             before(grammarAccess.getExplorateurAccess().getObjetsAssignment_8_1()); 
            // InternalGame.g:2613:2: ( rule__Explorateur__ObjetsAssignment_8_1 )
            // InternalGame.g:2613:3: rule__Explorateur__ObjetsAssignment_8_1
            {
            pushFollow(FOLLOW_2);
            rule__Explorateur__ObjetsAssignment_8_1();

            state._fsp--;


            }

             after(grammarAccess.getExplorateurAccess().getObjetsAssignment_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__Group_8__1__Impl"


    // $ANTLR start "rule__Territoire__Group__0"
    // InternalGame.g:2622:1: rule__Territoire__Group__0 : rule__Territoire__Group__0__Impl rule__Territoire__Group__1 ;
    public final void rule__Territoire__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2626:1: ( rule__Territoire__Group__0__Impl rule__Territoire__Group__1 )
            // InternalGame.g:2627:2: rule__Territoire__Group__0__Impl rule__Territoire__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Territoire__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Territoire__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group__0"


    // $ANTLR start "rule__Territoire__Group__0__Impl"
    // InternalGame.g:2634:1: rule__Territoire__Group__0__Impl : ( () ) ;
    public final void rule__Territoire__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2638:1: ( ( () ) )
            // InternalGame.g:2639:1: ( () )
            {
            // InternalGame.g:2639:1: ( () )
            // InternalGame.g:2640:2: ()
            {
             before(grammarAccess.getTerritoireAccess().getTerritoireAction_0()); 
            // InternalGame.g:2641:2: ()
            // InternalGame.g:2641:3: 
            {
            }

             after(grammarAccess.getTerritoireAccess().getTerritoireAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group__0__Impl"


    // $ANTLR start "rule__Territoire__Group__1"
    // InternalGame.g:2649:1: rule__Territoire__Group__1 : rule__Territoire__Group__1__Impl rule__Territoire__Group__2 ;
    public final void rule__Territoire__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2653:1: ( rule__Territoire__Group__1__Impl rule__Territoire__Group__2 )
            // InternalGame.g:2654:2: rule__Territoire__Group__1__Impl rule__Territoire__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Territoire__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Territoire__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group__1"


    // $ANTLR start "rule__Territoire__Group__1__Impl"
    // InternalGame.g:2661:1: rule__Territoire__Group__1__Impl : ( 'Lieux' ) ;
    public final void rule__Territoire__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2665:1: ( ( 'Lieux' ) )
            // InternalGame.g:2666:1: ( 'Lieux' )
            {
            // InternalGame.g:2666:1: ( 'Lieux' )
            // InternalGame.g:2667:2: 'Lieux'
            {
             before(grammarAccess.getTerritoireAccess().getLieuxKeyword_1()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getTerritoireAccess().getLieuxKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group__1__Impl"


    // $ANTLR start "rule__Territoire__Group__2"
    // InternalGame.g:2676:1: rule__Territoire__Group__2 : rule__Territoire__Group__2__Impl rule__Territoire__Group__3 ;
    public final void rule__Territoire__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2680:1: ( rule__Territoire__Group__2__Impl rule__Territoire__Group__3 )
            // InternalGame.g:2681:2: rule__Territoire__Group__2__Impl rule__Territoire__Group__3
            {
            pushFollow(FOLLOW_23);
            rule__Territoire__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Territoire__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group__2"


    // $ANTLR start "rule__Territoire__Group__2__Impl"
    // InternalGame.g:2688:1: rule__Territoire__Group__2__Impl : ( ':' ) ;
    public final void rule__Territoire__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2692:1: ( ( ':' ) )
            // InternalGame.g:2693:1: ( ':' )
            {
            // InternalGame.g:2693:1: ( ':' )
            // InternalGame.g:2694:2: ':'
            {
             before(grammarAccess.getTerritoireAccess().getColonKeyword_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getTerritoireAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group__2__Impl"


    // $ANTLR start "rule__Territoire__Group__3"
    // InternalGame.g:2703:1: rule__Territoire__Group__3 : rule__Territoire__Group__3__Impl rule__Territoire__Group__4 ;
    public final void rule__Territoire__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2707:1: ( rule__Territoire__Group__3__Impl rule__Territoire__Group__4 )
            // InternalGame.g:2708:2: rule__Territoire__Group__3__Impl rule__Territoire__Group__4
            {
            pushFollow(FOLLOW_23);
            rule__Territoire__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Territoire__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group__3"


    // $ANTLR start "rule__Territoire__Group__3__Impl"
    // InternalGame.g:2715:1: rule__Territoire__Group__3__Impl : ( ( rule__Territoire__Group_3__0 )* ) ;
    public final void rule__Territoire__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2719:1: ( ( ( rule__Territoire__Group_3__0 )* ) )
            // InternalGame.g:2720:1: ( ( rule__Territoire__Group_3__0 )* )
            {
            // InternalGame.g:2720:1: ( ( rule__Territoire__Group_3__0 )* )
            // InternalGame.g:2721:2: ( rule__Territoire__Group_3__0 )*
            {
             before(grammarAccess.getTerritoireAccess().getGroup_3()); 
            // InternalGame.g:2722:2: ( rule__Territoire__Group_3__0 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==20) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalGame.g:2722:3: rule__Territoire__Group_3__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Territoire__Group_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getTerritoireAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group__3__Impl"


    // $ANTLR start "rule__Territoire__Group__4"
    // InternalGame.g:2730:1: rule__Territoire__Group__4 : rule__Territoire__Group__4__Impl rule__Territoire__Group__5 ;
    public final void rule__Territoire__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2734:1: ( rule__Territoire__Group__4__Impl rule__Territoire__Group__5 )
            // InternalGame.g:2735:2: rule__Territoire__Group__4__Impl rule__Territoire__Group__5
            {
            pushFollow(FOLLOW_3);
            rule__Territoire__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Territoire__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group__4"


    // $ANTLR start "rule__Territoire__Group__4__Impl"
    // InternalGame.g:2742:1: rule__Territoire__Group__4__Impl : ( 'Chemins' ) ;
    public final void rule__Territoire__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2746:1: ( ( 'Chemins' ) )
            // InternalGame.g:2747:1: ( 'Chemins' )
            {
            // InternalGame.g:2747:1: ( 'Chemins' )
            // InternalGame.g:2748:2: 'Chemins'
            {
             before(grammarAccess.getTerritoireAccess().getCheminsKeyword_4()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getTerritoireAccess().getCheminsKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group__4__Impl"


    // $ANTLR start "rule__Territoire__Group__5"
    // InternalGame.g:2757:1: rule__Territoire__Group__5 : rule__Territoire__Group__5__Impl rule__Territoire__Group__6 ;
    public final void rule__Territoire__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2761:1: ( rule__Territoire__Group__5__Impl rule__Territoire__Group__6 )
            // InternalGame.g:2762:2: rule__Territoire__Group__5__Impl rule__Territoire__Group__6
            {
            pushFollow(FOLLOW_17);
            rule__Territoire__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Territoire__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group__5"


    // $ANTLR start "rule__Territoire__Group__5__Impl"
    // InternalGame.g:2769:1: rule__Territoire__Group__5__Impl : ( ':' ) ;
    public final void rule__Territoire__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2773:1: ( ( ':' ) )
            // InternalGame.g:2774:1: ( ':' )
            {
            // InternalGame.g:2774:1: ( ':' )
            // InternalGame.g:2775:2: ':'
            {
             before(grammarAccess.getTerritoireAccess().getColonKeyword_5()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getTerritoireAccess().getColonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group__5__Impl"


    // $ANTLR start "rule__Territoire__Group__6"
    // InternalGame.g:2784:1: rule__Territoire__Group__6 : rule__Territoire__Group__6__Impl ;
    public final void rule__Territoire__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2788:1: ( rule__Territoire__Group__6__Impl )
            // InternalGame.g:2789:2: rule__Territoire__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Territoire__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group__6"


    // $ANTLR start "rule__Territoire__Group__6__Impl"
    // InternalGame.g:2795:1: rule__Territoire__Group__6__Impl : ( ( rule__Territoire__Group_6__0 )* ) ;
    public final void rule__Territoire__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2799:1: ( ( ( rule__Territoire__Group_6__0 )* ) )
            // InternalGame.g:2800:1: ( ( rule__Territoire__Group_6__0 )* )
            {
            // InternalGame.g:2800:1: ( ( rule__Territoire__Group_6__0 )* )
            // InternalGame.g:2801:2: ( rule__Territoire__Group_6__0 )*
            {
             before(grammarAccess.getTerritoireAccess().getGroup_6()); 
            // InternalGame.g:2802:2: ( rule__Territoire__Group_6__0 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==20) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalGame.g:2802:3: rule__Territoire__Group_6__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Territoire__Group_6__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getTerritoireAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group__6__Impl"


    // $ANTLR start "rule__Territoire__Group_3__0"
    // InternalGame.g:2811:1: rule__Territoire__Group_3__0 : rule__Territoire__Group_3__0__Impl rule__Territoire__Group_3__1 ;
    public final void rule__Territoire__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2815:1: ( rule__Territoire__Group_3__0__Impl rule__Territoire__Group_3__1 )
            // InternalGame.g:2816:2: rule__Territoire__Group_3__0__Impl rule__Territoire__Group_3__1
            {
            pushFollow(FOLLOW_12);
            rule__Territoire__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Territoire__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group_3__0"


    // $ANTLR start "rule__Territoire__Group_3__0__Impl"
    // InternalGame.g:2823:1: rule__Territoire__Group_3__0__Impl : ( '-' ) ;
    public final void rule__Territoire__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2827:1: ( ( '-' ) )
            // InternalGame.g:2828:1: ( '-' )
            {
            // InternalGame.g:2828:1: ( '-' )
            // InternalGame.g:2829:2: '-'
            {
             before(grammarAccess.getTerritoireAccess().getHyphenMinusKeyword_3_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getTerritoireAccess().getHyphenMinusKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group_3__0__Impl"


    // $ANTLR start "rule__Territoire__Group_3__1"
    // InternalGame.g:2838:1: rule__Territoire__Group_3__1 : rule__Territoire__Group_3__1__Impl ;
    public final void rule__Territoire__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2842:1: ( rule__Territoire__Group_3__1__Impl )
            // InternalGame.g:2843:2: rule__Territoire__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Territoire__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group_3__1"


    // $ANTLR start "rule__Territoire__Group_3__1__Impl"
    // InternalGame.g:2849:1: rule__Territoire__Group_3__1__Impl : ( ( rule__Territoire__LieuxAssignment_3_1 ) ) ;
    public final void rule__Territoire__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2853:1: ( ( ( rule__Territoire__LieuxAssignment_3_1 ) ) )
            // InternalGame.g:2854:1: ( ( rule__Territoire__LieuxAssignment_3_1 ) )
            {
            // InternalGame.g:2854:1: ( ( rule__Territoire__LieuxAssignment_3_1 ) )
            // InternalGame.g:2855:2: ( rule__Territoire__LieuxAssignment_3_1 )
            {
             before(grammarAccess.getTerritoireAccess().getLieuxAssignment_3_1()); 
            // InternalGame.g:2856:2: ( rule__Territoire__LieuxAssignment_3_1 )
            // InternalGame.g:2856:3: rule__Territoire__LieuxAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Territoire__LieuxAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getTerritoireAccess().getLieuxAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group_3__1__Impl"


    // $ANTLR start "rule__Territoire__Group_6__0"
    // InternalGame.g:2865:1: rule__Territoire__Group_6__0 : rule__Territoire__Group_6__0__Impl rule__Territoire__Group_6__1 ;
    public final void rule__Territoire__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2869:1: ( rule__Territoire__Group_6__0__Impl rule__Territoire__Group_6__1 )
            // InternalGame.g:2870:2: rule__Territoire__Group_6__0__Impl rule__Territoire__Group_6__1
            {
            pushFollow(FOLLOW_12);
            rule__Territoire__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Territoire__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group_6__0"


    // $ANTLR start "rule__Territoire__Group_6__0__Impl"
    // InternalGame.g:2877:1: rule__Territoire__Group_6__0__Impl : ( '-' ) ;
    public final void rule__Territoire__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2881:1: ( ( '-' ) )
            // InternalGame.g:2882:1: ( '-' )
            {
            // InternalGame.g:2882:1: ( '-' )
            // InternalGame.g:2883:2: '-'
            {
             before(grammarAccess.getTerritoireAccess().getHyphenMinusKeyword_6_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getTerritoireAccess().getHyphenMinusKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group_6__0__Impl"


    // $ANTLR start "rule__Territoire__Group_6__1"
    // InternalGame.g:2892:1: rule__Territoire__Group_6__1 : rule__Territoire__Group_6__1__Impl ;
    public final void rule__Territoire__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2896:1: ( rule__Territoire__Group_6__1__Impl )
            // InternalGame.g:2897:2: rule__Territoire__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Territoire__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group_6__1"


    // $ANTLR start "rule__Territoire__Group_6__1__Impl"
    // InternalGame.g:2903:1: rule__Territoire__Group_6__1__Impl : ( ( rule__Territoire__CheminsAssignment_6_1 ) ) ;
    public final void rule__Territoire__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2907:1: ( ( ( rule__Territoire__CheminsAssignment_6_1 ) ) )
            // InternalGame.g:2908:1: ( ( rule__Territoire__CheminsAssignment_6_1 ) )
            {
            // InternalGame.g:2908:1: ( ( rule__Territoire__CheminsAssignment_6_1 ) )
            // InternalGame.g:2909:2: ( rule__Territoire__CheminsAssignment_6_1 )
            {
             before(grammarAccess.getTerritoireAccess().getCheminsAssignment_6_1()); 
            // InternalGame.g:2910:2: ( rule__Territoire__CheminsAssignment_6_1 )
            // InternalGame.g:2910:3: rule__Territoire__CheminsAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__Territoire__CheminsAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getTerritoireAccess().getCheminsAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__Group_6__1__Impl"


    // $ANTLR start "rule__Lieu__Group__0"
    // InternalGame.g:2919:1: rule__Lieu__Group__0 : rule__Lieu__Group__0__Impl rule__Lieu__Group__1 ;
    public final void rule__Lieu__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2923:1: ( rule__Lieu__Group__0__Impl rule__Lieu__Group__1 )
            // InternalGame.g:2924:2: rule__Lieu__Group__0__Impl rule__Lieu__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Lieu__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__0"


    // $ANTLR start "rule__Lieu__Group__0__Impl"
    // InternalGame.g:2931:1: rule__Lieu__Group__0__Impl : ( ( rule__Lieu__NameAssignment_0 ) ) ;
    public final void rule__Lieu__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2935:1: ( ( ( rule__Lieu__NameAssignment_0 ) ) )
            // InternalGame.g:2936:1: ( ( rule__Lieu__NameAssignment_0 ) )
            {
            // InternalGame.g:2936:1: ( ( rule__Lieu__NameAssignment_0 ) )
            // InternalGame.g:2937:2: ( rule__Lieu__NameAssignment_0 )
            {
             before(grammarAccess.getLieuAccess().getNameAssignment_0()); 
            // InternalGame.g:2938:2: ( rule__Lieu__NameAssignment_0 )
            // InternalGame.g:2938:3: rule__Lieu__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Lieu__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getLieuAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__0__Impl"


    // $ANTLR start "rule__Lieu__Group__1"
    // InternalGame.g:2946:1: rule__Lieu__Group__1 : rule__Lieu__Group__1__Impl rule__Lieu__Group__2 ;
    public final void rule__Lieu__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2950:1: ( rule__Lieu__Group__1__Impl rule__Lieu__Group__2 )
            // InternalGame.g:2951:2: rule__Lieu__Group__1__Impl rule__Lieu__Group__2
            {
            pushFollow(FOLLOW_24);
            rule__Lieu__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__1"


    // $ANTLR start "rule__Lieu__Group__1__Impl"
    // InternalGame.g:2958:1: rule__Lieu__Group__1__Impl : ( ':' ) ;
    public final void rule__Lieu__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2962:1: ( ( ':' ) )
            // InternalGame.g:2963:1: ( ':' )
            {
            // InternalGame.g:2963:1: ( ':' )
            // InternalGame.g:2964:2: ':'
            {
             before(grammarAccess.getLieuAccess().getColonKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__1__Impl"


    // $ANTLR start "rule__Lieu__Group__2"
    // InternalGame.g:2973:1: rule__Lieu__Group__2 : rule__Lieu__Group__2__Impl rule__Lieu__Group__3 ;
    public final void rule__Lieu__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2977:1: ( rule__Lieu__Group__2__Impl rule__Lieu__Group__3 )
            // InternalGame.g:2978:2: rule__Lieu__Group__2__Impl rule__Lieu__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__Lieu__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__2"


    // $ANTLR start "rule__Lieu__Group__2__Impl"
    // InternalGame.g:2985:1: rule__Lieu__Group__2__Impl : ( 'deposable' ) ;
    public final void rule__Lieu__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:2989:1: ( ( 'deposable' ) )
            // InternalGame.g:2990:1: ( 'deposable' )
            {
            // InternalGame.g:2990:1: ( 'deposable' )
            // InternalGame.g:2991:2: 'deposable'
            {
             before(grammarAccess.getLieuAccess().getDeposableKeyword_2()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getDeposableKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__2__Impl"


    // $ANTLR start "rule__Lieu__Group__3"
    // InternalGame.g:3000:1: rule__Lieu__Group__3 : rule__Lieu__Group__3__Impl rule__Lieu__Group__4 ;
    public final void rule__Lieu__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3004:1: ( rule__Lieu__Group__3__Impl rule__Lieu__Group__4 )
            // InternalGame.g:3005:2: rule__Lieu__Group__3__Impl rule__Lieu__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__Lieu__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__3"


    // $ANTLR start "rule__Lieu__Group__3__Impl"
    // InternalGame.g:3012:1: rule__Lieu__Group__3__Impl : ( ':' ) ;
    public final void rule__Lieu__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3016:1: ( ( ':' ) )
            // InternalGame.g:3017:1: ( ':' )
            {
            // InternalGame.g:3017:1: ( ':' )
            // InternalGame.g:3018:2: ':'
            {
             before(grammarAccess.getLieuAccess().getColonKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__3__Impl"


    // $ANTLR start "rule__Lieu__Group__4"
    // InternalGame.g:3027:1: rule__Lieu__Group__4 : rule__Lieu__Group__4__Impl rule__Lieu__Group__5 ;
    public final void rule__Lieu__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3031:1: ( rule__Lieu__Group__4__Impl rule__Lieu__Group__5 )
            // InternalGame.g:3032:2: rule__Lieu__Group__4__Impl rule__Lieu__Group__5
            {
            pushFollow(FOLLOW_25);
            rule__Lieu__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__4"


    // $ANTLR start "rule__Lieu__Group__4__Impl"
    // InternalGame.g:3039:1: rule__Lieu__Group__4__Impl : ( ( rule__Lieu__DeposableAssignment_4 ) ) ;
    public final void rule__Lieu__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3043:1: ( ( ( rule__Lieu__DeposableAssignment_4 ) ) )
            // InternalGame.g:3044:1: ( ( rule__Lieu__DeposableAssignment_4 ) )
            {
            // InternalGame.g:3044:1: ( ( rule__Lieu__DeposableAssignment_4 ) )
            // InternalGame.g:3045:2: ( rule__Lieu__DeposableAssignment_4 )
            {
             before(grammarAccess.getLieuAccess().getDeposableAssignment_4()); 
            // InternalGame.g:3046:2: ( rule__Lieu__DeposableAssignment_4 )
            // InternalGame.g:3046:3: rule__Lieu__DeposableAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Lieu__DeposableAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getLieuAccess().getDeposableAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__4__Impl"


    // $ANTLR start "rule__Lieu__Group__5"
    // InternalGame.g:3054:1: rule__Lieu__Group__5 : rule__Lieu__Group__5__Impl rule__Lieu__Group__6 ;
    public final void rule__Lieu__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3058:1: ( rule__Lieu__Group__5__Impl rule__Lieu__Group__6 )
            // InternalGame.g:3059:2: rule__Lieu__Group__5__Impl rule__Lieu__Group__6
            {
            pushFollow(FOLLOW_3);
            rule__Lieu__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__5"


    // $ANTLR start "rule__Lieu__Group__5__Impl"
    // InternalGame.g:3066:1: rule__Lieu__Group__5__Impl : ( 'depart' ) ;
    public final void rule__Lieu__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3070:1: ( ( 'depart' ) )
            // InternalGame.g:3071:1: ( 'depart' )
            {
            // InternalGame.g:3071:1: ( 'depart' )
            // InternalGame.g:3072:2: 'depart'
            {
             before(grammarAccess.getLieuAccess().getDepartKeyword_5()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getDepartKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__5__Impl"


    // $ANTLR start "rule__Lieu__Group__6"
    // InternalGame.g:3081:1: rule__Lieu__Group__6 : rule__Lieu__Group__6__Impl rule__Lieu__Group__7 ;
    public final void rule__Lieu__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3085:1: ( rule__Lieu__Group__6__Impl rule__Lieu__Group__7 )
            // InternalGame.g:3086:2: rule__Lieu__Group__6__Impl rule__Lieu__Group__7
            {
            pushFollow(FOLLOW_15);
            rule__Lieu__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__6"


    // $ANTLR start "rule__Lieu__Group__6__Impl"
    // InternalGame.g:3093:1: rule__Lieu__Group__6__Impl : ( ':' ) ;
    public final void rule__Lieu__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3097:1: ( ( ':' ) )
            // InternalGame.g:3098:1: ( ':' )
            {
            // InternalGame.g:3098:1: ( ':' )
            // InternalGame.g:3099:2: ':'
            {
             before(grammarAccess.getLieuAccess().getColonKeyword_6()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getColonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__6__Impl"


    // $ANTLR start "rule__Lieu__Group__7"
    // InternalGame.g:3108:1: rule__Lieu__Group__7 : rule__Lieu__Group__7__Impl rule__Lieu__Group__8 ;
    public final void rule__Lieu__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3112:1: ( rule__Lieu__Group__7__Impl rule__Lieu__Group__8 )
            // InternalGame.g:3113:2: rule__Lieu__Group__7__Impl rule__Lieu__Group__8
            {
            pushFollow(FOLLOW_26);
            rule__Lieu__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__7"


    // $ANTLR start "rule__Lieu__Group__7__Impl"
    // InternalGame.g:3120:1: rule__Lieu__Group__7__Impl : ( ( rule__Lieu__DepartAssignment_7 ) ) ;
    public final void rule__Lieu__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3124:1: ( ( ( rule__Lieu__DepartAssignment_7 ) ) )
            // InternalGame.g:3125:1: ( ( rule__Lieu__DepartAssignment_7 ) )
            {
            // InternalGame.g:3125:1: ( ( rule__Lieu__DepartAssignment_7 ) )
            // InternalGame.g:3126:2: ( rule__Lieu__DepartAssignment_7 )
            {
             before(grammarAccess.getLieuAccess().getDepartAssignment_7()); 
            // InternalGame.g:3127:2: ( rule__Lieu__DepartAssignment_7 )
            // InternalGame.g:3127:3: rule__Lieu__DepartAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__Lieu__DepartAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getLieuAccess().getDepartAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__7__Impl"


    // $ANTLR start "rule__Lieu__Group__8"
    // InternalGame.g:3135:1: rule__Lieu__Group__8 : rule__Lieu__Group__8__Impl rule__Lieu__Group__9 ;
    public final void rule__Lieu__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3139:1: ( rule__Lieu__Group__8__Impl rule__Lieu__Group__9 )
            // InternalGame.g:3140:2: rule__Lieu__Group__8__Impl rule__Lieu__Group__9
            {
            pushFollow(FOLLOW_3);
            rule__Lieu__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__8"


    // $ANTLR start "rule__Lieu__Group__8__Impl"
    // InternalGame.g:3147:1: rule__Lieu__Group__8__Impl : ( 'fin' ) ;
    public final void rule__Lieu__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3151:1: ( ( 'fin' ) )
            // InternalGame.g:3152:1: ( 'fin' )
            {
            // InternalGame.g:3152:1: ( 'fin' )
            // InternalGame.g:3153:2: 'fin'
            {
             before(grammarAccess.getLieuAccess().getFinKeyword_8()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getFinKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__8__Impl"


    // $ANTLR start "rule__Lieu__Group__9"
    // InternalGame.g:3162:1: rule__Lieu__Group__9 : rule__Lieu__Group__9__Impl rule__Lieu__Group__10 ;
    public final void rule__Lieu__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3166:1: ( rule__Lieu__Group__9__Impl rule__Lieu__Group__10 )
            // InternalGame.g:3167:2: rule__Lieu__Group__9__Impl rule__Lieu__Group__10
            {
            pushFollow(FOLLOW_15);
            rule__Lieu__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__9"


    // $ANTLR start "rule__Lieu__Group__9__Impl"
    // InternalGame.g:3174:1: rule__Lieu__Group__9__Impl : ( ':' ) ;
    public final void rule__Lieu__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3178:1: ( ( ':' ) )
            // InternalGame.g:3179:1: ( ':' )
            {
            // InternalGame.g:3179:1: ( ':' )
            // InternalGame.g:3180:2: ':'
            {
             before(grammarAccess.getLieuAccess().getColonKeyword_9()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getColonKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__9__Impl"


    // $ANTLR start "rule__Lieu__Group__10"
    // InternalGame.g:3189:1: rule__Lieu__Group__10 : rule__Lieu__Group__10__Impl rule__Lieu__Group__11 ;
    public final void rule__Lieu__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3193:1: ( rule__Lieu__Group__10__Impl rule__Lieu__Group__11 )
            // InternalGame.g:3194:2: rule__Lieu__Group__10__Impl rule__Lieu__Group__11
            {
            pushFollow(FOLLOW_27);
            rule__Lieu__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__10"


    // $ANTLR start "rule__Lieu__Group__10__Impl"
    // InternalGame.g:3201:1: rule__Lieu__Group__10__Impl : ( ( rule__Lieu__FinAssignment_10 ) ) ;
    public final void rule__Lieu__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3205:1: ( ( ( rule__Lieu__FinAssignment_10 ) ) )
            // InternalGame.g:3206:1: ( ( rule__Lieu__FinAssignment_10 ) )
            {
            // InternalGame.g:3206:1: ( ( rule__Lieu__FinAssignment_10 ) )
            // InternalGame.g:3207:2: ( rule__Lieu__FinAssignment_10 )
            {
             before(grammarAccess.getLieuAccess().getFinAssignment_10()); 
            // InternalGame.g:3208:2: ( rule__Lieu__FinAssignment_10 )
            // InternalGame.g:3208:3: rule__Lieu__FinAssignment_10
            {
            pushFollow(FOLLOW_2);
            rule__Lieu__FinAssignment_10();

            state._fsp--;


            }

             after(grammarAccess.getLieuAccess().getFinAssignment_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__10__Impl"


    // $ANTLR start "rule__Lieu__Group__11"
    // InternalGame.g:3216:1: rule__Lieu__Group__11 : rule__Lieu__Group__11__Impl rule__Lieu__Group__12 ;
    public final void rule__Lieu__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3220:1: ( rule__Lieu__Group__11__Impl rule__Lieu__Group__12 )
            // InternalGame.g:3221:2: rule__Lieu__Group__11__Impl rule__Lieu__Group__12
            {
            pushFollow(FOLLOW_3);
            rule__Lieu__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__11"


    // $ANTLR start "rule__Lieu__Group__11__Impl"
    // InternalGame.g:3228:1: rule__Lieu__Group__11__Impl : ( 'personnes' ) ;
    public final void rule__Lieu__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3232:1: ( ( 'personnes' ) )
            // InternalGame.g:3233:1: ( 'personnes' )
            {
            // InternalGame.g:3233:1: ( 'personnes' )
            // InternalGame.g:3234:2: 'personnes'
            {
             before(grammarAccess.getLieuAccess().getPersonnesKeyword_11()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getPersonnesKeyword_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__11__Impl"


    // $ANTLR start "rule__Lieu__Group__12"
    // InternalGame.g:3243:1: rule__Lieu__Group__12 : rule__Lieu__Group__12__Impl rule__Lieu__Group__13 ;
    public final void rule__Lieu__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3247:1: ( rule__Lieu__Group__12__Impl rule__Lieu__Group__13 )
            // InternalGame.g:3248:2: rule__Lieu__Group__12__Impl rule__Lieu__Group__13
            {
            pushFollow(FOLLOW_28);
            rule__Lieu__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__12"


    // $ANTLR start "rule__Lieu__Group__12__Impl"
    // InternalGame.g:3255:1: rule__Lieu__Group__12__Impl : ( ':' ) ;
    public final void rule__Lieu__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3259:1: ( ( ':' ) )
            // InternalGame.g:3260:1: ( ':' )
            {
            // InternalGame.g:3260:1: ( ':' )
            // InternalGame.g:3261:2: ':'
            {
             before(grammarAccess.getLieuAccess().getColonKeyword_12()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getColonKeyword_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__12__Impl"


    // $ANTLR start "rule__Lieu__Group__13"
    // InternalGame.g:3270:1: rule__Lieu__Group__13 : rule__Lieu__Group__13__Impl rule__Lieu__Group__14 ;
    public final void rule__Lieu__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3274:1: ( rule__Lieu__Group__13__Impl rule__Lieu__Group__14 )
            // InternalGame.g:3275:2: rule__Lieu__Group__13__Impl rule__Lieu__Group__14
            {
            pushFollow(FOLLOW_28);
            rule__Lieu__Group__13__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__13"


    // $ANTLR start "rule__Lieu__Group__13__Impl"
    // InternalGame.g:3282:1: rule__Lieu__Group__13__Impl : ( ( rule__Lieu__Group_13__0 )* ) ;
    public final void rule__Lieu__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3286:1: ( ( ( rule__Lieu__Group_13__0 )* ) )
            // InternalGame.g:3287:1: ( ( rule__Lieu__Group_13__0 )* )
            {
            // InternalGame.g:3287:1: ( ( rule__Lieu__Group_13__0 )* )
            // InternalGame.g:3288:2: ( rule__Lieu__Group_13__0 )*
            {
             before(grammarAccess.getLieuAccess().getGroup_13()); 
            // InternalGame.g:3289:2: ( rule__Lieu__Group_13__0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==20) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalGame.g:3289:3: rule__Lieu__Group_13__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Lieu__Group_13__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getLieuAccess().getGroup_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__13__Impl"


    // $ANTLR start "rule__Lieu__Group__14"
    // InternalGame.g:3297:1: rule__Lieu__Group__14 : rule__Lieu__Group__14__Impl rule__Lieu__Group__15 ;
    public final void rule__Lieu__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3301:1: ( rule__Lieu__Group__14__Impl rule__Lieu__Group__15 )
            // InternalGame.g:3302:2: rule__Lieu__Group__14__Impl rule__Lieu__Group__15
            {
            pushFollow(FOLLOW_3);
            rule__Lieu__Group__14__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__14"


    // $ANTLR start "rule__Lieu__Group__14__Impl"
    // InternalGame.g:3309:1: rule__Lieu__Group__14__Impl : ( 'descriptions' ) ;
    public final void rule__Lieu__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3313:1: ( ( 'descriptions' ) )
            // InternalGame.g:3314:1: ( 'descriptions' )
            {
            // InternalGame.g:3314:1: ( 'descriptions' )
            // InternalGame.g:3315:2: 'descriptions'
            {
             before(grammarAccess.getLieuAccess().getDescriptionsKeyword_14()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getDescriptionsKeyword_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__14__Impl"


    // $ANTLR start "rule__Lieu__Group__15"
    // InternalGame.g:3324:1: rule__Lieu__Group__15 : rule__Lieu__Group__15__Impl rule__Lieu__Group__16 ;
    public final void rule__Lieu__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3328:1: ( rule__Lieu__Group__15__Impl rule__Lieu__Group__16 )
            // InternalGame.g:3329:2: rule__Lieu__Group__15__Impl rule__Lieu__Group__16
            {
            pushFollow(FOLLOW_17);
            rule__Lieu__Group__15__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__16();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__15"


    // $ANTLR start "rule__Lieu__Group__15__Impl"
    // InternalGame.g:3336:1: rule__Lieu__Group__15__Impl : ( ':' ) ;
    public final void rule__Lieu__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3340:1: ( ( ':' ) )
            // InternalGame.g:3341:1: ( ':' )
            {
            // InternalGame.g:3341:1: ( ':' )
            // InternalGame.g:3342:2: ':'
            {
             before(grammarAccess.getLieuAccess().getColonKeyword_15()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getColonKeyword_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__15__Impl"


    // $ANTLR start "rule__Lieu__Group__16"
    // InternalGame.g:3351:1: rule__Lieu__Group__16 : rule__Lieu__Group__16__Impl rule__Lieu__Group__17 ;
    public final void rule__Lieu__Group__16() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3355:1: ( rule__Lieu__Group__16__Impl rule__Lieu__Group__17 )
            // InternalGame.g:3356:2: rule__Lieu__Group__16__Impl rule__Lieu__Group__17
            {
            pushFollow(FOLLOW_29);
            rule__Lieu__Group__16__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__17();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__16"


    // $ANTLR start "rule__Lieu__Group__16__Impl"
    // InternalGame.g:3363:1: rule__Lieu__Group__16__Impl : ( ( ( rule__Lieu__Group_16__0 ) ) ( ( rule__Lieu__Group_16__0 )* ) ) ;
    public final void rule__Lieu__Group__16__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3367:1: ( ( ( ( rule__Lieu__Group_16__0 ) ) ( ( rule__Lieu__Group_16__0 )* ) ) )
            // InternalGame.g:3368:1: ( ( ( rule__Lieu__Group_16__0 ) ) ( ( rule__Lieu__Group_16__0 )* ) )
            {
            // InternalGame.g:3368:1: ( ( ( rule__Lieu__Group_16__0 ) ) ( ( rule__Lieu__Group_16__0 )* ) )
            // InternalGame.g:3369:2: ( ( rule__Lieu__Group_16__0 ) ) ( ( rule__Lieu__Group_16__0 )* )
            {
            // InternalGame.g:3369:2: ( ( rule__Lieu__Group_16__0 ) )
            // InternalGame.g:3370:3: ( rule__Lieu__Group_16__0 )
            {
             before(grammarAccess.getLieuAccess().getGroup_16()); 
            // InternalGame.g:3371:3: ( rule__Lieu__Group_16__0 )
            // InternalGame.g:3371:4: rule__Lieu__Group_16__0
            {
            pushFollow(FOLLOW_5);
            rule__Lieu__Group_16__0();

            state._fsp--;


            }

             after(grammarAccess.getLieuAccess().getGroup_16()); 

            }

            // InternalGame.g:3374:2: ( ( rule__Lieu__Group_16__0 )* )
            // InternalGame.g:3375:3: ( rule__Lieu__Group_16__0 )*
            {
             before(grammarAccess.getLieuAccess().getGroup_16()); 
            // InternalGame.g:3376:3: ( rule__Lieu__Group_16__0 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==20) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalGame.g:3376:4: rule__Lieu__Group_16__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Lieu__Group_16__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getLieuAccess().getGroup_16()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__16__Impl"


    // $ANTLR start "rule__Lieu__Group__17"
    // InternalGame.g:3385:1: rule__Lieu__Group__17 : rule__Lieu__Group__17__Impl rule__Lieu__Group__18 ;
    public final void rule__Lieu__Group__17() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3389:1: ( rule__Lieu__Group__17__Impl rule__Lieu__Group__18 )
            // InternalGame.g:3390:2: rule__Lieu__Group__17__Impl rule__Lieu__Group__18
            {
            pushFollow(FOLLOW_3);
            rule__Lieu__Group__17__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__18();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__17"


    // $ANTLR start "rule__Lieu__Group__17__Impl"
    // InternalGame.g:3397:1: rule__Lieu__Group__17__Impl : ( 'objets' ) ;
    public final void rule__Lieu__Group__17__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3401:1: ( ( 'objets' ) )
            // InternalGame.g:3402:1: ( 'objets' )
            {
            // InternalGame.g:3402:1: ( 'objets' )
            // InternalGame.g:3403:2: 'objets'
            {
             before(grammarAccess.getLieuAccess().getObjetsKeyword_17()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getObjetsKeyword_17()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__17__Impl"


    // $ANTLR start "rule__Lieu__Group__18"
    // InternalGame.g:3412:1: rule__Lieu__Group__18 : rule__Lieu__Group__18__Impl rule__Lieu__Group__19 ;
    public final void rule__Lieu__Group__18() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3416:1: ( rule__Lieu__Group__18__Impl rule__Lieu__Group__19 )
            // InternalGame.g:3417:2: rule__Lieu__Group__18__Impl rule__Lieu__Group__19
            {
            pushFollow(FOLLOW_30);
            rule__Lieu__Group__18__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__19();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__18"


    // $ANTLR start "rule__Lieu__Group__18__Impl"
    // InternalGame.g:3424:1: rule__Lieu__Group__18__Impl : ( ':' ) ;
    public final void rule__Lieu__Group__18__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3428:1: ( ( ':' ) )
            // InternalGame.g:3429:1: ( ':' )
            {
            // InternalGame.g:3429:1: ( ':' )
            // InternalGame.g:3430:2: ':'
            {
             before(grammarAccess.getLieuAccess().getColonKeyword_18()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getColonKeyword_18()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__18__Impl"


    // $ANTLR start "rule__Lieu__Group__19"
    // InternalGame.g:3439:1: rule__Lieu__Group__19 : rule__Lieu__Group__19__Impl rule__Lieu__Group__20 ;
    public final void rule__Lieu__Group__19() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3443:1: ( rule__Lieu__Group__19__Impl rule__Lieu__Group__20 )
            // InternalGame.g:3444:2: rule__Lieu__Group__19__Impl rule__Lieu__Group__20
            {
            pushFollow(FOLLOW_30);
            rule__Lieu__Group__19__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__20();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__19"


    // $ANTLR start "rule__Lieu__Group__19__Impl"
    // InternalGame.g:3451:1: rule__Lieu__Group__19__Impl : ( ( rule__Lieu__Group_19__0 )* ) ;
    public final void rule__Lieu__Group__19__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3455:1: ( ( ( rule__Lieu__Group_19__0 )* ) )
            // InternalGame.g:3456:1: ( ( rule__Lieu__Group_19__0 )* )
            {
            // InternalGame.g:3456:1: ( ( rule__Lieu__Group_19__0 )* )
            // InternalGame.g:3457:2: ( rule__Lieu__Group_19__0 )*
            {
             before(grammarAccess.getLieuAccess().getGroup_19()); 
            // InternalGame.g:3458:2: ( rule__Lieu__Group_19__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==20) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalGame.g:3458:3: rule__Lieu__Group_19__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Lieu__Group_19__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getLieuAccess().getGroup_19()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__19__Impl"


    // $ANTLR start "rule__Lieu__Group__20"
    // InternalGame.g:3466:1: rule__Lieu__Group__20 : rule__Lieu__Group__20__Impl rule__Lieu__Group__21 ;
    public final void rule__Lieu__Group__20() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3470:1: ( rule__Lieu__Group__20__Impl rule__Lieu__Group__21 )
            // InternalGame.g:3471:2: rule__Lieu__Group__20__Impl rule__Lieu__Group__21
            {
            pushFollow(FOLLOW_3);
            rule__Lieu__Group__20__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__21();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__20"


    // $ANTLR start "rule__Lieu__Group__20__Impl"
    // InternalGame.g:3478:1: rule__Lieu__Group__20__Impl : ( 'connaissances' ) ;
    public final void rule__Lieu__Group__20__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3482:1: ( ( 'connaissances' ) )
            // InternalGame.g:3483:1: ( 'connaissances' )
            {
            // InternalGame.g:3483:1: ( 'connaissances' )
            // InternalGame.g:3484:2: 'connaissances'
            {
             before(grammarAccess.getLieuAccess().getConnaissancesKeyword_20()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getConnaissancesKeyword_20()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__20__Impl"


    // $ANTLR start "rule__Lieu__Group__21"
    // InternalGame.g:3493:1: rule__Lieu__Group__21 : rule__Lieu__Group__21__Impl rule__Lieu__Group__22 ;
    public final void rule__Lieu__Group__21() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3497:1: ( rule__Lieu__Group__21__Impl rule__Lieu__Group__22 )
            // InternalGame.g:3498:2: rule__Lieu__Group__21__Impl rule__Lieu__Group__22
            {
            pushFollow(FOLLOW_17);
            rule__Lieu__Group__21__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group__22();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__21"


    // $ANTLR start "rule__Lieu__Group__21__Impl"
    // InternalGame.g:3505:1: rule__Lieu__Group__21__Impl : ( ':' ) ;
    public final void rule__Lieu__Group__21__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3509:1: ( ( ':' ) )
            // InternalGame.g:3510:1: ( ':' )
            {
            // InternalGame.g:3510:1: ( ':' )
            // InternalGame.g:3511:2: ':'
            {
             before(grammarAccess.getLieuAccess().getColonKeyword_21()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getColonKeyword_21()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__21__Impl"


    // $ANTLR start "rule__Lieu__Group__22"
    // InternalGame.g:3520:1: rule__Lieu__Group__22 : rule__Lieu__Group__22__Impl ;
    public final void rule__Lieu__Group__22() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3524:1: ( rule__Lieu__Group__22__Impl )
            // InternalGame.g:3525:2: rule__Lieu__Group__22__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Lieu__Group__22__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__22"


    // $ANTLR start "rule__Lieu__Group__22__Impl"
    // InternalGame.g:3531:1: rule__Lieu__Group__22__Impl : ( ( rule__Lieu__Group_22__0 )* ) ;
    public final void rule__Lieu__Group__22__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3535:1: ( ( ( rule__Lieu__Group_22__0 )* ) )
            // InternalGame.g:3536:1: ( ( rule__Lieu__Group_22__0 )* )
            {
            // InternalGame.g:3536:1: ( ( rule__Lieu__Group_22__0 )* )
            // InternalGame.g:3537:2: ( rule__Lieu__Group_22__0 )*
            {
             before(grammarAccess.getLieuAccess().getGroup_22()); 
            // InternalGame.g:3538:2: ( rule__Lieu__Group_22__0 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==20) ) {
                    int LA17_2 = input.LA(2);

                    if ( (LA17_2==RULE_ID) ) {
                        int LA17_3 = input.LA(3);

                        if ( (LA17_3==EOF||LA17_3==20||LA17_3==30) ) {
                            alt17=1;
                        }


                    }


                }


                switch (alt17) {
            	case 1 :
            	    // InternalGame.g:3538:3: rule__Lieu__Group_22__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Lieu__Group_22__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getLieuAccess().getGroup_22()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group__22__Impl"


    // $ANTLR start "rule__Lieu__Group_13__0"
    // InternalGame.g:3547:1: rule__Lieu__Group_13__0 : rule__Lieu__Group_13__0__Impl rule__Lieu__Group_13__1 ;
    public final void rule__Lieu__Group_13__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3551:1: ( rule__Lieu__Group_13__0__Impl rule__Lieu__Group_13__1 )
            // InternalGame.g:3552:2: rule__Lieu__Group_13__0__Impl rule__Lieu__Group_13__1
            {
            pushFollow(FOLLOW_12);
            rule__Lieu__Group_13__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group_13__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_13__0"


    // $ANTLR start "rule__Lieu__Group_13__0__Impl"
    // InternalGame.g:3559:1: rule__Lieu__Group_13__0__Impl : ( '-' ) ;
    public final void rule__Lieu__Group_13__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3563:1: ( ( '-' ) )
            // InternalGame.g:3564:1: ( '-' )
            {
            // InternalGame.g:3564:1: ( '-' )
            // InternalGame.g:3565:2: '-'
            {
             before(grammarAccess.getLieuAccess().getHyphenMinusKeyword_13_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getHyphenMinusKeyword_13_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_13__0__Impl"


    // $ANTLR start "rule__Lieu__Group_13__1"
    // InternalGame.g:3574:1: rule__Lieu__Group_13__1 : rule__Lieu__Group_13__1__Impl ;
    public final void rule__Lieu__Group_13__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3578:1: ( rule__Lieu__Group_13__1__Impl )
            // InternalGame.g:3579:2: rule__Lieu__Group_13__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Lieu__Group_13__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_13__1"


    // $ANTLR start "rule__Lieu__Group_13__1__Impl"
    // InternalGame.g:3585:1: rule__Lieu__Group_13__1__Impl : ( ( rule__Lieu__PersonnesAssignment_13_1 ) ) ;
    public final void rule__Lieu__Group_13__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3589:1: ( ( ( rule__Lieu__PersonnesAssignment_13_1 ) ) )
            // InternalGame.g:3590:1: ( ( rule__Lieu__PersonnesAssignment_13_1 ) )
            {
            // InternalGame.g:3590:1: ( ( rule__Lieu__PersonnesAssignment_13_1 ) )
            // InternalGame.g:3591:2: ( rule__Lieu__PersonnesAssignment_13_1 )
            {
             before(grammarAccess.getLieuAccess().getPersonnesAssignment_13_1()); 
            // InternalGame.g:3592:2: ( rule__Lieu__PersonnesAssignment_13_1 )
            // InternalGame.g:3592:3: rule__Lieu__PersonnesAssignment_13_1
            {
            pushFollow(FOLLOW_2);
            rule__Lieu__PersonnesAssignment_13_1();

            state._fsp--;


            }

             after(grammarAccess.getLieuAccess().getPersonnesAssignment_13_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_13__1__Impl"


    // $ANTLR start "rule__Lieu__Group_16__0"
    // InternalGame.g:3601:1: rule__Lieu__Group_16__0 : rule__Lieu__Group_16__0__Impl rule__Lieu__Group_16__1 ;
    public final void rule__Lieu__Group_16__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3605:1: ( rule__Lieu__Group_16__0__Impl rule__Lieu__Group_16__1 )
            // InternalGame.g:3606:2: rule__Lieu__Group_16__0__Impl rule__Lieu__Group_16__1
            {
            pushFollow(FOLLOW_12);
            rule__Lieu__Group_16__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group_16__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_16__0"


    // $ANTLR start "rule__Lieu__Group_16__0__Impl"
    // InternalGame.g:3613:1: rule__Lieu__Group_16__0__Impl : ( '-' ) ;
    public final void rule__Lieu__Group_16__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3617:1: ( ( '-' ) )
            // InternalGame.g:3618:1: ( '-' )
            {
            // InternalGame.g:3618:1: ( '-' )
            // InternalGame.g:3619:2: '-'
            {
             before(grammarAccess.getLieuAccess().getHyphenMinusKeyword_16_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getHyphenMinusKeyword_16_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_16__0__Impl"


    // $ANTLR start "rule__Lieu__Group_16__1"
    // InternalGame.g:3628:1: rule__Lieu__Group_16__1 : rule__Lieu__Group_16__1__Impl ;
    public final void rule__Lieu__Group_16__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3632:1: ( rule__Lieu__Group_16__1__Impl )
            // InternalGame.g:3633:2: rule__Lieu__Group_16__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Lieu__Group_16__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_16__1"


    // $ANTLR start "rule__Lieu__Group_16__1__Impl"
    // InternalGame.g:3639:1: rule__Lieu__Group_16__1__Impl : ( ( rule__Lieu__DescriptionsAssignment_16_1 ) ) ;
    public final void rule__Lieu__Group_16__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3643:1: ( ( ( rule__Lieu__DescriptionsAssignment_16_1 ) ) )
            // InternalGame.g:3644:1: ( ( rule__Lieu__DescriptionsAssignment_16_1 ) )
            {
            // InternalGame.g:3644:1: ( ( rule__Lieu__DescriptionsAssignment_16_1 ) )
            // InternalGame.g:3645:2: ( rule__Lieu__DescriptionsAssignment_16_1 )
            {
             before(grammarAccess.getLieuAccess().getDescriptionsAssignment_16_1()); 
            // InternalGame.g:3646:2: ( rule__Lieu__DescriptionsAssignment_16_1 )
            // InternalGame.g:3646:3: rule__Lieu__DescriptionsAssignment_16_1
            {
            pushFollow(FOLLOW_2);
            rule__Lieu__DescriptionsAssignment_16_1();

            state._fsp--;


            }

             after(grammarAccess.getLieuAccess().getDescriptionsAssignment_16_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_16__1__Impl"


    // $ANTLR start "rule__Lieu__Group_19__0"
    // InternalGame.g:3655:1: rule__Lieu__Group_19__0 : rule__Lieu__Group_19__0__Impl rule__Lieu__Group_19__1 ;
    public final void rule__Lieu__Group_19__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3659:1: ( rule__Lieu__Group_19__0__Impl rule__Lieu__Group_19__1 )
            // InternalGame.g:3660:2: rule__Lieu__Group_19__0__Impl rule__Lieu__Group_19__1
            {
            pushFollow(FOLLOW_12);
            rule__Lieu__Group_19__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group_19__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_19__0"


    // $ANTLR start "rule__Lieu__Group_19__0__Impl"
    // InternalGame.g:3667:1: rule__Lieu__Group_19__0__Impl : ( '-' ) ;
    public final void rule__Lieu__Group_19__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3671:1: ( ( '-' ) )
            // InternalGame.g:3672:1: ( '-' )
            {
            // InternalGame.g:3672:1: ( '-' )
            // InternalGame.g:3673:2: '-'
            {
             before(grammarAccess.getLieuAccess().getHyphenMinusKeyword_19_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getHyphenMinusKeyword_19_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_19__0__Impl"


    // $ANTLR start "rule__Lieu__Group_19__1"
    // InternalGame.g:3682:1: rule__Lieu__Group_19__1 : rule__Lieu__Group_19__1__Impl ;
    public final void rule__Lieu__Group_19__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3686:1: ( rule__Lieu__Group_19__1__Impl )
            // InternalGame.g:3687:2: rule__Lieu__Group_19__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Lieu__Group_19__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_19__1"


    // $ANTLR start "rule__Lieu__Group_19__1__Impl"
    // InternalGame.g:3693:1: rule__Lieu__Group_19__1__Impl : ( ( rule__Lieu__ObjetsAssignment_19_1 ) ) ;
    public final void rule__Lieu__Group_19__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3697:1: ( ( ( rule__Lieu__ObjetsAssignment_19_1 ) ) )
            // InternalGame.g:3698:1: ( ( rule__Lieu__ObjetsAssignment_19_1 ) )
            {
            // InternalGame.g:3698:1: ( ( rule__Lieu__ObjetsAssignment_19_1 ) )
            // InternalGame.g:3699:2: ( rule__Lieu__ObjetsAssignment_19_1 )
            {
             before(grammarAccess.getLieuAccess().getObjetsAssignment_19_1()); 
            // InternalGame.g:3700:2: ( rule__Lieu__ObjetsAssignment_19_1 )
            // InternalGame.g:3700:3: rule__Lieu__ObjetsAssignment_19_1
            {
            pushFollow(FOLLOW_2);
            rule__Lieu__ObjetsAssignment_19_1();

            state._fsp--;


            }

             after(grammarAccess.getLieuAccess().getObjetsAssignment_19_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_19__1__Impl"


    // $ANTLR start "rule__Lieu__Group_22__0"
    // InternalGame.g:3709:1: rule__Lieu__Group_22__0 : rule__Lieu__Group_22__0__Impl rule__Lieu__Group_22__1 ;
    public final void rule__Lieu__Group_22__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3713:1: ( rule__Lieu__Group_22__0__Impl rule__Lieu__Group_22__1 )
            // InternalGame.g:3714:2: rule__Lieu__Group_22__0__Impl rule__Lieu__Group_22__1
            {
            pushFollow(FOLLOW_12);
            rule__Lieu__Group_22__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lieu__Group_22__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_22__0"


    // $ANTLR start "rule__Lieu__Group_22__0__Impl"
    // InternalGame.g:3721:1: rule__Lieu__Group_22__0__Impl : ( '-' ) ;
    public final void rule__Lieu__Group_22__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3725:1: ( ( '-' ) )
            // InternalGame.g:3726:1: ( '-' )
            {
            // InternalGame.g:3726:1: ( '-' )
            // InternalGame.g:3727:2: '-'
            {
             before(grammarAccess.getLieuAccess().getHyphenMinusKeyword_22_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getHyphenMinusKeyword_22_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_22__0__Impl"


    // $ANTLR start "rule__Lieu__Group_22__1"
    // InternalGame.g:3736:1: rule__Lieu__Group_22__1 : rule__Lieu__Group_22__1__Impl ;
    public final void rule__Lieu__Group_22__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3740:1: ( rule__Lieu__Group_22__1__Impl )
            // InternalGame.g:3741:2: rule__Lieu__Group_22__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Lieu__Group_22__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_22__1"


    // $ANTLR start "rule__Lieu__Group_22__1__Impl"
    // InternalGame.g:3747:1: rule__Lieu__Group_22__1__Impl : ( ( rule__Lieu__ConnaissancesAssignment_22_1 ) ) ;
    public final void rule__Lieu__Group_22__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3751:1: ( ( ( rule__Lieu__ConnaissancesAssignment_22_1 ) ) )
            // InternalGame.g:3752:1: ( ( rule__Lieu__ConnaissancesAssignment_22_1 ) )
            {
            // InternalGame.g:3752:1: ( ( rule__Lieu__ConnaissancesAssignment_22_1 ) )
            // InternalGame.g:3753:2: ( rule__Lieu__ConnaissancesAssignment_22_1 )
            {
             before(grammarAccess.getLieuAccess().getConnaissancesAssignment_22_1()); 
            // InternalGame.g:3754:2: ( rule__Lieu__ConnaissancesAssignment_22_1 )
            // InternalGame.g:3754:3: rule__Lieu__ConnaissancesAssignment_22_1
            {
            pushFollow(FOLLOW_2);
            rule__Lieu__ConnaissancesAssignment_22_1();

            state._fsp--;


            }

             after(grammarAccess.getLieuAccess().getConnaissancesAssignment_22_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__Group_22__1__Impl"


    // $ANTLR start "rule__Chemin__Group__0"
    // InternalGame.g:3763:1: rule__Chemin__Group__0 : rule__Chemin__Group__0__Impl rule__Chemin__Group__1 ;
    public final void rule__Chemin__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3767:1: ( rule__Chemin__Group__0__Impl rule__Chemin__Group__1 )
            // InternalGame.g:3768:2: rule__Chemin__Group__0__Impl rule__Chemin__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Chemin__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__0"


    // $ANTLR start "rule__Chemin__Group__0__Impl"
    // InternalGame.g:3775:1: rule__Chemin__Group__0__Impl : ( ( rule__Chemin__NameAssignment_0 ) ) ;
    public final void rule__Chemin__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3779:1: ( ( ( rule__Chemin__NameAssignment_0 ) ) )
            // InternalGame.g:3780:1: ( ( rule__Chemin__NameAssignment_0 ) )
            {
            // InternalGame.g:3780:1: ( ( rule__Chemin__NameAssignment_0 ) )
            // InternalGame.g:3781:2: ( rule__Chemin__NameAssignment_0 )
            {
             before(grammarAccess.getCheminAccess().getNameAssignment_0()); 
            // InternalGame.g:3782:2: ( rule__Chemin__NameAssignment_0 )
            // InternalGame.g:3782:3: rule__Chemin__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getCheminAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__0__Impl"


    // $ANTLR start "rule__Chemin__Group__1"
    // InternalGame.g:3790:1: rule__Chemin__Group__1 : rule__Chemin__Group__1__Impl rule__Chemin__Group__2 ;
    public final void rule__Chemin__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3794:1: ( rule__Chemin__Group__1__Impl rule__Chemin__Group__2 )
            // InternalGame.g:3795:2: rule__Chemin__Group__1__Impl rule__Chemin__Group__2
            {
            pushFollow(FOLLOW_31);
            rule__Chemin__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__1"


    // $ANTLR start "rule__Chemin__Group__1__Impl"
    // InternalGame.g:3802:1: rule__Chemin__Group__1__Impl : ( ':' ) ;
    public final void rule__Chemin__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3806:1: ( ( ':' ) )
            // InternalGame.g:3807:1: ( ':' )
            {
            // InternalGame.g:3807:1: ( ':' )
            // InternalGame.g:3808:2: ':'
            {
             before(grammarAccess.getCheminAccess().getColonKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__1__Impl"


    // $ANTLR start "rule__Chemin__Group__2"
    // InternalGame.g:3817:1: rule__Chemin__Group__2 : rule__Chemin__Group__2__Impl rule__Chemin__Group__3 ;
    public final void rule__Chemin__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3821:1: ( rule__Chemin__Group__2__Impl rule__Chemin__Group__3 )
            // InternalGame.g:3822:2: rule__Chemin__Group__2__Impl rule__Chemin__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__Chemin__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__2"


    // $ANTLR start "rule__Chemin__Group__2__Impl"
    // InternalGame.g:3829:1: rule__Chemin__Group__2__Impl : ( 'lieu_in' ) ;
    public final void rule__Chemin__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3833:1: ( ( 'lieu_in' ) )
            // InternalGame.g:3834:1: ( 'lieu_in' )
            {
            // InternalGame.g:3834:1: ( 'lieu_in' )
            // InternalGame.g:3835:2: 'lieu_in'
            {
             before(grammarAccess.getCheminAccess().getLieu_inKeyword_2()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getLieu_inKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__2__Impl"


    // $ANTLR start "rule__Chemin__Group__3"
    // InternalGame.g:3844:1: rule__Chemin__Group__3 : rule__Chemin__Group__3__Impl rule__Chemin__Group__4 ;
    public final void rule__Chemin__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3848:1: ( rule__Chemin__Group__3__Impl rule__Chemin__Group__4 )
            // InternalGame.g:3849:2: rule__Chemin__Group__3__Impl rule__Chemin__Group__4
            {
            pushFollow(FOLLOW_12);
            rule__Chemin__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__3"


    // $ANTLR start "rule__Chemin__Group__3__Impl"
    // InternalGame.g:3856:1: rule__Chemin__Group__3__Impl : ( ':' ) ;
    public final void rule__Chemin__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3860:1: ( ( ':' ) )
            // InternalGame.g:3861:1: ( ':' )
            {
            // InternalGame.g:3861:1: ( ':' )
            // InternalGame.g:3862:2: ':'
            {
             before(grammarAccess.getCheminAccess().getColonKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__3__Impl"


    // $ANTLR start "rule__Chemin__Group__4"
    // InternalGame.g:3871:1: rule__Chemin__Group__4 : rule__Chemin__Group__4__Impl rule__Chemin__Group__5 ;
    public final void rule__Chemin__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3875:1: ( rule__Chemin__Group__4__Impl rule__Chemin__Group__5 )
            // InternalGame.g:3876:2: rule__Chemin__Group__4__Impl rule__Chemin__Group__5
            {
            pushFollow(FOLLOW_32);
            rule__Chemin__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__4"


    // $ANTLR start "rule__Chemin__Group__4__Impl"
    // InternalGame.g:3883:1: rule__Chemin__Group__4__Impl : ( ( rule__Chemin__LieuInAssignment_4 ) ) ;
    public final void rule__Chemin__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3887:1: ( ( ( rule__Chemin__LieuInAssignment_4 ) ) )
            // InternalGame.g:3888:1: ( ( rule__Chemin__LieuInAssignment_4 ) )
            {
            // InternalGame.g:3888:1: ( ( rule__Chemin__LieuInAssignment_4 ) )
            // InternalGame.g:3889:2: ( rule__Chemin__LieuInAssignment_4 )
            {
             before(grammarAccess.getCheminAccess().getLieuInAssignment_4()); 
            // InternalGame.g:3890:2: ( rule__Chemin__LieuInAssignment_4 )
            // InternalGame.g:3890:3: rule__Chemin__LieuInAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__LieuInAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getCheminAccess().getLieuInAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__4__Impl"


    // $ANTLR start "rule__Chemin__Group__5"
    // InternalGame.g:3898:1: rule__Chemin__Group__5 : rule__Chemin__Group__5__Impl rule__Chemin__Group__6 ;
    public final void rule__Chemin__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3902:1: ( rule__Chemin__Group__5__Impl rule__Chemin__Group__6 )
            // InternalGame.g:3903:2: rule__Chemin__Group__5__Impl rule__Chemin__Group__6
            {
            pushFollow(FOLLOW_3);
            rule__Chemin__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__5"


    // $ANTLR start "rule__Chemin__Group__5__Impl"
    // InternalGame.g:3910:1: rule__Chemin__Group__5__Impl : ( 'lieu_out' ) ;
    public final void rule__Chemin__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3914:1: ( ( 'lieu_out' ) )
            // InternalGame.g:3915:1: ( 'lieu_out' )
            {
            // InternalGame.g:3915:1: ( 'lieu_out' )
            // InternalGame.g:3916:2: 'lieu_out'
            {
             before(grammarAccess.getCheminAccess().getLieu_outKeyword_5()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getLieu_outKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__5__Impl"


    // $ANTLR start "rule__Chemin__Group__6"
    // InternalGame.g:3925:1: rule__Chemin__Group__6 : rule__Chemin__Group__6__Impl rule__Chemin__Group__7 ;
    public final void rule__Chemin__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3929:1: ( rule__Chemin__Group__6__Impl rule__Chemin__Group__7 )
            // InternalGame.g:3930:2: rule__Chemin__Group__6__Impl rule__Chemin__Group__7
            {
            pushFollow(FOLLOW_12);
            rule__Chemin__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__6"


    // $ANTLR start "rule__Chemin__Group__6__Impl"
    // InternalGame.g:3937:1: rule__Chemin__Group__6__Impl : ( ':' ) ;
    public final void rule__Chemin__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3941:1: ( ( ':' ) )
            // InternalGame.g:3942:1: ( ':' )
            {
            // InternalGame.g:3942:1: ( ':' )
            // InternalGame.g:3943:2: ':'
            {
             before(grammarAccess.getCheminAccess().getColonKeyword_6()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getColonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__6__Impl"


    // $ANTLR start "rule__Chemin__Group__7"
    // InternalGame.g:3952:1: rule__Chemin__Group__7 : rule__Chemin__Group__7__Impl rule__Chemin__Group__8 ;
    public final void rule__Chemin__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3956:1: ( rule__Chemin__Group__7__Impl rule__Chemin__Group__8 )
            // InternalGame.g:3957:2: rule__Chemin__Group__7__Impl rule__Chemin__Group__8
            {
            pushFollow(FOLLOW_33);
            rule__Chemin__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__7"


    // $ANTLR start "rule__Chemin__Group__7__Impl"
    // InternalGame.g:3964:1: rule__Chemin__Group__7__Impl : ( ( rule__Chemin__LieuOutAssignment_7 ) ) ;
    public final void rule__Chemin__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3968:1: ( ( ( rule__Chemin__LieuOutAssignment_7 ) ) )
            // InternalGame.g:3969:1: ( ( rule__Chemin__LieuOutAssignment_7 ) )
            {
            // InternalGame.g:3969:1: ( ( rule__Chemin__LieuOutAssignment_7 ) )
            // InternalGame.g:3970:2: ( rule__Chemin__LieuOutAssignment_7 )
            {
             before(grammarAccess.getCheminAccess().getLieuOutAssignment_7()); 
            // InternalGame.g:3971:2: ( rule__Chemin__LieuOutAssignment_7 )
            // InternalGame.g:3971:3: rule__Chemin__LieuOutAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__LieuOutAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getCheminAccess().getLieuOutAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__7__Impl"


    // $ANTLR start "rule__Chemin__Group__8"
    // InternalGame.g:3979:1: rule__Chemin__Group__8 : rule__Chemin__Group__8__Impl rule__Chemin__Group__9 ;
    public final void rule__Chemin__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3983:1: ( rule__Chemin__Group__8__Impl rule__Chemin__Group__9 )
            // InternalGame.g:3984:2: rule__Chemin__Group__8__Impl rule__Chemin__Group__9
            {
            pushFollow(FOLLOW_3);
            rule__Chemin__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__8"


    // $ANTLR start "rule__Chemin__Group__8__Impl"
    // InternalGame.g:3991:1: rule__Chemin__Group__8__Impl : ( 'ouvert' ) ;
    public final void rule__Chemin__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:3995:1: ( ( 'ouvert' ) )
            // InternalGame.g:3996:1: ( 'ouvert' )
            {
            // InternalGame.g:3996:1: ( 'ouvert' )
            // InternalGame.g:3997:2: 'ouvert'
            {
             before(grammarAccess.getCheminAccess().getOuvertKeyword_8()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getOuvertKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__8__Impl"


    // $ANTLR start "rule__Chemin__Group__9"
    // InternalGame.g:4006:1: rule__Chemin__Group__9 : rule__Chemin__Group__9__Impl rule__Chemin__Group__10 ;
    public final void rule__Chemin__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4010:1: ( rule__Chemin__Group__9__Impl rule__Chemin__Group__10 )
            // InternalGame.g:4011:2: rule__Chemin__Group__9__Impl rule__Chemin__Group__10
            {
            pushFollow(FOLLOW_15);
            rule__Chemin__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__9"


    // $ANTLR start "rule__Chemin__Group__9__Impl"
    // InternalGame.g:4018:1: rule__Chemin__Group__9__Impl : ( ':' ) ;
    public final void rule__Chemin__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4022:1: ( ( ':' ) )
            // InternalGame.g:4023:1: ( ':' )
            {
            // InternalGame.g:4023:1: ( ':' )
            // InternalGame.g:4024:2: ':'
            {
             before(grammarAccess.getCheminAccess().getColonKeyword_9()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getColonKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__9__Impl"


    // $ANTLR start "rule__Chemin__Group__10"
    // InternalGame.g:4033:1: rule__Chemin__Group__10 : rule__Chemin__Group__10__Impl rule__Chemin__Group__11 ;
    public final void rule__Chemin__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4037:1: ( rule__Chemin__Group__10__Impl rule__Chemin__Group__11 )
            // InternalGame.g:4038:2: rule__Chemin__Group__10__Impl rule__Chemin__Group__11
            {
            pushFollow(FOLLOW_14);
            rule__Chemin__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__10"


    // $ANTLR start "rule__Chemin__Group__10__Impl"
    // InternalGame.g:4045:1: rule__Chemin__Group__10__Impl : ( ( rule__Chemin__OuvertAssignment_10 ) ) ;
    public final void rule__Chemin__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4049:1: ( ( ( rule__Chemin__OuvertAssignment_10 ) ) )
            // InternalGame.g:4050:1: ( ( rule__Chemin__OuvertAssignment_10 ) )
            {
            // InternalGame.g:4050:1: ( ( rule__Chemin__OuvertAssignment_10 ) )
            // InternalGame.g:4051:2: ( rule__Chemin__OuvertAssignment_10 )
            {
             before(grammarAccess.getCheminAccess().getOuvertAssignment_10()); 
            // InternalGame.g:4052:2: ( rule__Chemin__OuvertAssignment_10 )
            // InternalGame.g:4052:3: rule__Chemin__OuvertAssignment_10
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__OuvertAssignment_10();

            state._fsp--;


            }

             after(grammarAccess.getCheminAccess().getOuvertAssignment_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__10__Impl"


    // $ANTLR start "rule__Chemin__Group__11"
    // InternalGame.g:4060:1: rule__Chemin__Group__11 : rule__Chemin__Group__11__Impl rule__Chemin__Group__12 ;
    public final void rule__Chemin__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4064:1: ( rule__Chemin__Group__11__Impl rule__Chemin__Group__12 )
            // InternalGame.g:4065:2: rule__Chemin__Group__11__Impl rule__Chemin__Group__12
            {
            pushFollow(FOLLOW_3);
            rule__Chemin__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__11"


    // $ANTLR start "rule__Chemin__Group__11__Impl"
    // InternalGame.g:4072:1: rule__Chemin__Group__11__Impl : ( 'visible' ) ;
    public final void rule__Chemin__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4076:1: ( ( 'visible' ) )
            // InternalGame.g:4077:1: ( 'visible' )
            {
            // InternalGame.g:4077:1: ( 'visible' )
            // InternalGame.g:4078:2: 'visible'
            {
             before(grammarAccess.getCheminAccess().getVisibleKeyword_11()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getVisibleKeyword_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__11__Impl"


    // $ANTLR start "rule__Chemin__Group__12"
    // InternalGame.g:4087:1: rule__Chemin__Group__12 : rule__Chemin__Group__12__Impl rule__Chemin__Group__13 ;
    public final void rule__Chemin__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4091:1: ( rule__Chemin__Group__12__Impl rule__Chemin__Group__13 )
            // InternalGame.g:4092:2: rule__Chemin__Group__12__Impl rule__Chemin__Group__13
            {
            pushFollow(FOLLOW_15);
            rule__Chemin__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__12"


    // $ANTLR start "rule__Chemin__Group__12__Impl"
    // InternalGame.g:4099:1: rule__Chemin__Group__12__Impl : ( ':' ) ;
    public final void rule__Chemin__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4103:1: ( ( ':' ) )
            // InternalGame.g:4104:1: ( ':' )
            {
            // InternalGame.g:4104:1: ( ':' )
            // InternalGame.g:4105:2: ':'
            {
             before(grammarAccess.getCheminAccess().getColonKeyword_12()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getColonKeyword_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__12__Impl"


    // $ANTLR start "rule__Chemin__Group__13"
    // InternalGame.g:4114:1: rule__Chemin__Group__13 : rule__Chemin__Group__13__Impl rule__Chemin__Group__14 ;
    public final void rule__Chemin__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4118:1: ( rule__Chemin__Group__13__Impl rule__Chemin__Group__14 )
            // InternalGame.g:4119:2: rule__Chemin__Group__13__Impl rule__Chemin__Group__14
            {
            pushFollow(FOLLOW_34);
            rule__Chemin__Group__13__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__13"


    // $ANTLR start "rule__Chemin__Group__13__Impl"
    // InternalGame.g:4126:1: rule__Chemin__Group__13__Impl : ( ( rule__Chemin__VisibleAssignment_13 ) ) ;
    public final void rule__Chemin__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4130:1: ( ( ( rule__Chemin__VisibleAssignment_13 ) ) )
            // InternalGame.g:4131:1: ( ( rule__Chemin__VisibleAssignment_13 ) )
            {
            // InternalGame.g:4131:1: ( ( rule__Chemin__VisibleAssignment_13 ) )
            // InternalGame.g:4132:2: ( rule__Chemin__VisibleAssignment_13 )
            {
             before(grammarAccess.getCheminAccess().getVisibleAssignment_13()); 
            // InternalGame.g:4133:2: ( rule__Chemin__VisibleAssignment_13 )
            // InternalGame.g:4133:3: rule__Chemin__VisibleAssignment_13
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__VisibleAssignment_13();

            state._fsp--;


            }

             after(grammarAccess.getCheminAccess().getVisibleAssignment_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__13__Impl"


    // $ANTLR start "rule__Chemin__Group__14"
    // InternalGame.g:4141:1: rule__Chemin__Group__14 : rule__Chemin__Group__14__Impl rule__Chemin__Group__15 ;
    public final void rule__Chemin__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4145:1: ( rule__Chemin__Group__14__Impl rule__Chemin__Group__15 )
            // InternalGame.g:4146:2: rule__Chemin__Group__14__Impl rule__Chemin__Group__15
            {
            pushFollow(FOLLOW_3);
            rule__Chemin__Group__14__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__14"


    // $ANTLR start "rule__Chemin__Group__14__Impl"
    // InternalGame.g:4153:1: rule__Chemin__Group__14__Impl : ( 'obligatoire' ) ;
    public final void rule__Chemin__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4157:1: ( ( 'obligatoire' ) )
            // InternalGame.g:4158:1: ( 'obligatoire' )
            {
            // InternalGame.g:4158:1: ( 'obligatoire' )
            // InternalGame.g:4159:2: 'obligatoire'
            {
             before(grammarAccess.getCheminAccess().getObligatoireKeyword_14()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getObligatoireKeyword_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__14__Impl"


    // $ANTLR start "rule__Chemin__Group__15"
    // InternalGame.g:4168:1: rule__Chemin__Group__15 : rule__Chemin__Group__15__Impl rule__Chemin__Group__16 ;
    public final void rule__Chemin__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4172:1: ( rule__Chemin__Group__15__Impl rule__Chemin__Group__16 )
            // InternalGame.g:4173:2: rule__Chemin__Group__15__Impl rule__Chemin__Group__16
            {
            pushFollow(FOLLOW_15);
            rule__Chemin__Group__15__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__16();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__15"


    // $ANTLR start "rule__Chemin__Group__15__Impl"
    // InternalGame.g:4180:1: rule__Chemin__Group__15__Impl : ( ':' ) ;
    public final void rule__Chemin__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4184:1: ( ( ':' ) )
            // InternalGame.g:4185:1: ( ':' )
            {
            // InternalGame.g:4185:1: ( ':' )
            // InternalGame.g:4186:2: ':'
            {
             before(grammarAccess.getCheminAccess().getColonKeyword_15()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getColonKeyword_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__15__Impl"


    // $ANTLR start "rule__Chemin__Group__16"
    // InternalGame.g:4195:1: rule__Chemin__Group__16 : rule__Chemin__Group__16__Impl rule__Chemin__Group__17 ;
    public final void rule__Chemin__Group__16() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4199:1: ( rule__Chemin__Group__16__Impl rule__Chemin__Group__17 )
            // InternalGame.g:4200:2: rule__Chemin__Group__16__Impl rule__Chemin__Group__17
            {
            pushFollow(FOLLOW_21);
            rule__Chemin__Group__16__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__17();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__16"


    // $ANTLR start "rule__Chemin__Group__16__Impl"
    // InternalGame.g:4207:1: rule__Chemin__Group__16__Impl : ( ( rule__Chemin__ObligatoireAssignment_16 ) ) ;
    public final void rule__Chemin__Group__16__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4211:1: ( ( ( rule__Chemin__ObligatoireAssignment_16 ) ) )
            // InternalGame.g:4212:1: ( ( rule__Chemin__ObligatoireAssignment_16 ) )
            {
            // InternalGame.g:4212:1: ( ( rule__Chemin__ObligatoireAssignment_16 ) )
            // InternalGame.g:4213:2: ( rule__Chemin__ObligatoireAssignment_16 )
            {
             before(grammarAccess.getCheminAccess().getObligatoireAssignment_16()); 
            // InternalGame.g:4214:2: ( rule__Chemin__ObligatoireAssignment_16 )
            // InternalGame.g:4214:3: rule__Chemin__ObligatoireAssignment_16
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__ObligatoireAssignment_16();

            state._fsp--;


            }

             after(grammarAccess.getCheminAccess().getObligatoireAssignment_16()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__16__Impl"


    // $ANTLR start "rule__Chemin__Group__17"
    // InternalGame.g:4222:1: rule__Chemin__Group__17 : rule__Chemin__Group__17__Impl rule__Chemin__Group__18 ;
    public final void rule__Chemin__Group__17() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4226:1: ( rule__Chemin__Group__17__Impl rule__Chemin__Group__18 )
            // InternalGame.g:4227:2: rule__Chemin__Group__17__Impl rule__Chemin__Group__18
            {
            pushFollow(FOLLOW_3);
            rule__Chemin__Group__17__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__18();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__17"


    // $ANTLR start "rule__Chemin__Group__17__Impl"
    // InternalGame.g:4234:1: rule__Chemin__Group__17__Impl : ( 'connaissances' ) ;
    public final void rule__Chemin__Group__17__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4238:1: ( ( 'connaissances' ) )
            // InternalGame.g:4239:1: ( 'connaissances' )
            {
            // InternalGame.g:4239:1: ( 'connaissances' )
            // InternalGame.g:4240:2: 'connaissances'
            {
             before(grammarAccess.getCheminAccess().getConnaissancesKeyword_17()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getConnaissancesKeyword_17()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__17__Impl"


    // $ANTLR start "rule__Chemin__Group__18"
    // InternalGame.g:4249:1: rule__Chemin__Group__18 : rule__Chemin__Group__18__Impl rule__Chemin__Group__19 ;
    public final void rule__Chemin__Group__18() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4253:1: ( rule__Chemin__Group__18__Impl rule__Chemin__Group__19 )
            // InternalGame.g:4254:2: rule__Chemin__Group__18__Impl rule__Chemin__Group__19
            {
            pushFollow(FOLLOW_35);
            rule__Chemin__Group__18__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__19();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__18"


    // $ANTLR start "rule__Chemin__Group__18__Impl"
    // InternalGame.g:4261:1: rule__Chemin__Group__18__Impl : ( ':' ) ;
    public final void rule__Chemin__Group__18__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4265:1: ( ( ':' ) )
            // InternalGame.g:4266:1: ( ':' )
            {
            // InternalGame.g:4266:1: ( ':' )
            // InternalGame.g:4267:2: ':'
            {
             before(grammarAccess.getCheminAccess().getColonKeyword_18()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getColonKeyword_18()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__18__Impl"


    // $ANTLR start "rule__Chemin__Group__19"
    // InternalGame.g:4276:1: rule__Chemin__Group__19 : rule__Chemin__Group__19__Impl rule__Chemin__Group__20 ;
    public final void rule__Chemin__Group__19() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4280:1: ( rule__Chemin__Group__19__Impl rule__Chemin__Group__20 )
            // InternalGame.g:4281:2: rule__Chemin__Group__19__Impl rule__Chemin__Group__20
            {
            pushFollow(FOLLOW_35);
            rule__Chemin__Group__19__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__20();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__19"


    // $ANTLR start "rule__Chemin__Group__19__Impl"
    // InternalGame.g:4288:1: rule__Chemin__Group__19__Impl : ( ( rule__Chemin__Group_19__0 )* ) ;
    public final void rule__Chemin__Group__19__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4292:1: ( ( ( rule__Chemin__Group_19__0 )* ) )
            // InternalGame.g:4293:1: ( ( rule__Chemin__Group_19__0 )* )
            {
            // InternalGame.g:4293:1: ( ( rule__Chemin__Group_19__0 )* )
            // InternalGame.g:4294:2: ( rule__Chemin__Group_19__0 )*
            {
             before(grammarAccess.getCheminAccess().getGroup_19()); 
            // InternalGame.g:4295:2: ( rule__Chemin__Group_19__0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==20) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalGame.g:4295:3: rule__Chemin__Group_19__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Chemin__Group_19__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getCheminAccess().getGroup_19()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__19__Impl"


    // $ANTLR start "rule__Chemin__Group__20"
    // InternalGame.g:4303:1: rule__Chemin__Group__20 : rule__Chemin__Group__20__Impl rule__Chemin__Group__21 ;
    public final void rule__Chemin__Group__20() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4307:1: ( rule__Chemin__Group__20__Impl rule__Chemin__Group__21 )
            // InternalGame.g:4308:2: rule__Chemin__Group__20__Impl rule__Chemin__Group__21
            {
            pushFollow(FOLLOW_3);
            rule__Chemin__Group__20__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__21();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__20"


    // $ANTLR start "rule__Chemin__Group__20__Impl"
    // InternalGame.g:4315:1: rule__Chemin__Group__20__Impl : ( 'objets_recus' ) ;
    public final void rule__Chemin__Group__20__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4319:1: ( ( 'objets_recus' ) )
            // InternalGame.g:4320:1: ( 'objets_recus' )
            {
            // InternalGame.g:4320:1: ( 'objets_recus' )
            // InternalGame.g:4321:2: 'objets_recus'
            {
             before(grammarAccess.getCheminAccess().getObjets_recusKeyword_20()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getObjets_recusKeyword_20()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__20__Impl"


    // $ANTLR start "rule__Chemin__Group__21"
    // InternalGame.g:4330:1: rule__Chemin__Group__21 : rule__Chemin__Group__21__Impl rule__Chemin__Group__22 ;
    public final void rule__Chemin__Group__21() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4334:1: ( rule__Chemin__Group__21__Impl rule__Chemin__Group__22 )
            // InternalGame.g:4335:2: rule__Chemin__Group__21__Impl rule__Chemin__Group__22
            {
            pushFollow(FOLLOW_36);
            rule__Chemin__Group__21__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__22();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__21"


    // $ANTLR start "rule__Chemin__Group__21__Impl"
    // InternalGame.g:4342:1: rule__Chemin__Group__21__Impl : ( ':' ) ;
    public final void rule__Chemin__Group__21__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4346:1: ( ( ':' ) )
            // InternalGame.g:4347:1: ( ':' )
            {
            // InternalGame.g:4347:1: ( ':' )
            // InternalGame.g:4348:2: ':'
            {
             before(grammarAccess.getCheminAccess().getColonKeyword_21()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getColonKeyword_21()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__21__Impl"


    // $ANTLR start "rule__Chemin__Group__22"
    // InternalGame.g:4357:1: rule__Chemin__Group__22 : rule__Chemin__Group__22__Impl rule__Chemin__Group__23 ;
    public final void rule__Chemin__Group__22() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4361:1: ( rule__Chemin__Group__22__Impl rule__Chemin__Group__23 )
            // InternalGame.g:4362:2: rule__Chemin__Group__22__Impl rule__Chemin__Group__23
            {
            pushFollow(FOLLOW_36);
            rule__Chemin__Group__22__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__23();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__22"


    // $ANTLR start "rule__Chemin__Group__22__Impl"
    // InternalGame.g:4369:1: rule__Chemin__Group__22__Impl : ( ( rule__Chemin__Group_22__0 )* ) ;
    public final void rule__Chemin__Group__22__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4373:1: ( ( ( rule__Chemin__Group_22__0 )* ) )
            // InternalGame.g:4374:1: ( ( rule__Chemin__Group_22__0 )* )
            {
            // InternalGame.g:4374:1: ( ( rule__Chemin__Group_22__0 )* )
            // InternalGame.g:4375:2: ( rule__Chemin__Group_22__0 )*
            {
             before(grammarAccess.getCheminAccess().getGroup_22()); 
            // InternalGame.g:4376:2: ( rule__Chemin__Group_22__0 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==20) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalGame.g:4376:3: rule__Chemin__Group_22__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Chemin__Group_22__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getCheminAccess().getGroup_22()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__22__Impl"


    // $ANTLR start "rule__Chemin__Group__23"
    // InternalGame.g:4384:1: rule__Chemin__Group__23 : rule__Chemin__Group__23__Impl rule__Chemin__Group__24 ;
    public final void rule__Chemin__Group__23() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4388:1: ( rule__Chemin__Group__23__Impl rule__Chemin__Group__24 )
            // InternalGame.g:4389:2: rule__Chemin__Group__23__Impl rule__Chemin__Group__24
            {
            pushFollow(FOLLOW_3);
            rule__Chemin__Group__23__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__24();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__23"


    // $ANTLR start "rule__Chemin__Group__23__Impl"
    // InternalGame.g:4396:1: rule__Chemin__Group__23__Impl : ( 'objets_conso' ) ;
    public final void rule__Chemin__Group__23__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4400:1: ( ( 'objets_conso' ) )
            // InternalGame.g:4401:1: ( 'objets_conso' )
            {
            // InternalGame.g:4401:1: ( 'objets_conso' )
            // InternalGame.g:4402:2: 'objets_conso'
            {
             before(grammarAccess.getCheminAccess().getObjets_consoKeyword_23()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getObjets_consoKeyword_23()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__23__Impl"


    // $ANTLR start "rule__Chemin__Group__24"
    // InternalGame.g:4411:1: rule__Chemin__Group__24 : rule__Chemin__Group__24__Impl rule__Chemin__Group__25 ;
    public final void rule__Chemin__Group__24() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4415:1: ( rule__Chemin__Group__24__Impl rule__Chemin__Group__25 )
            // InternalGame.g:4416:2: rule__Chemin__Group__24__Impl rule__Chemin__Group__25
            {
            pushFollow(FOLLOW_28);
            rule__Chemin__Group__24__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__25();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__24"


    // $ANTLR start "rule__Chemin__Group__24__Impl"
    // InternalGame.g:4423:1: rule__Chemin__Group__24__Impl : ( ':' ) ;
    public final void rule__Chemin__Group__24__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4427:1: ( ( ':' ) )
            // InternalGame.g:4428:1: ( ':' )
            {
            // InternalGame.g:4428:1: ( ':' )
            // InternalGame.g:4429:2: ':'
            {
             before(grammarAccess.getCheminAccess().getColonKeyword_24()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getColonKeyword_24()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__24__Impl"


    // $ANTLR start "rule__Chemin__Group__25"
    // InternalGame.g:4438:1: rule__Chemin__Group__25 : rule__Chemin__Group__25__Impl rule__Chemin__Group__26 ;
    public final void rule__Chemin__Group__25() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4442:1: ( rule__Chemin__Group__25__Impl rule__Chemin__Group__26 )
            // InternalGame.g:4443:2: rule__Chemin__Group__25__Impl rule__Chemin__Group__26
            {
            pushFollow(FOLLOW_28);
            rule__Chemin__Group__25__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__26();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__25"


    // $ANTLR start "rule__Chemin__Group__25__Impl"
    // InternalGame.g:4450:1: rule__Chemin__Group__25__Impl : ( ( rule__Chemin__Group_25__0 )* ) ;
    public final void rule__Chemin__Group__25__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4454:1: ( ( ( rule__Chemin__Group_25__0 )* ) )
            // InternalGame.g:4455:1: ( ( rule__Chemin__Group_25__0 )* )
            {
            // InternalGame.g:4455:1: ( ( rule__Chemin__Group_25__0 )* )
            // InternalGame.g:4456:2: ( rule__Chemin__Group_25__0 )*
            {
             before(grammarAccess.getCheminAccess().getGroup_25()); 
            // InternalGame.g:4457:2: ( rule__Chemin__Group_25__0 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==20) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalGame.g:4457:3: rule__Chemin__Group_25__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Chemin__Group_25__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getCheminAccess().getGroup_25()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__25__Impl"


    // $ANTLR start "rule__Chemin__Group__26"
    // InternalGame.g:4465:1: rule__Chemin__Group__26 : rule__Chemin__Group__26__Impl rule__Chemin__Group__27 ;
    public final void rule__Chemin__Group__26() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4469:1: ( rule__Chemin__Group__26__Impl rule__Chemin__Group__27 )
            // InternalGame.g:4470:2: rule__Chemin__Group__26__Impl rule__Chemin__Group__27
            {
            pushFollow(FOLLOW_3);
            rule__Chemin__Group__26__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__27();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__26"


    // $ANTLR start "rule__Chemin__Group__26__Impl"
    // InternalGame.g:4477:1: rule__Chemin__Group__26__Impl : ( 'descriptions' ) ;
    public final void rule__Chemin__Group__26__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4481:1: ( ( 'descriptions' ) )
            // InternalGame.g:4482:1: ( 'descriptions' )
            {
            // InternalGame.g:4482:1: ( 'descriptions' )
            // InternalGame.g:4483:2: 'descriptions'
            {
             before(grammarAccess.getCheminAccess().getDescriptionsKeyword_26()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getDescriptionsKeyword_26()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__26__Impl"


    // $ANTLR start "rule__Chemin__Group__27"
    // InternalGame.g:4492:1: rule__Chemin__Group__27 : rule__Chemin__Group__27__Impl rule__Chemin__Group__28 ;
    public final void rule__Chemin__Group__27() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4496:1: ( rule__Chemin__Group__27__Impl rule__Chemin__Group__28 )
            // InternalGame.g:4497:2: rule__Chemin__Group__27__Impl rule__Chemin__Group__28
            {
            pushFollow(FOLLOW_17);
            rule__Chemin__Group__27__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group__28();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__27"


    // $ANTLR start "rule__Chemin__Group__27__Impl"
    // InternalGame.g:4504:1: rule__Chemin__Group__27__Impl : ( ':' ) ;
    public final void rule__Chemin__Group__27__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4508:1: ( ( ':' ) )
            // InternalGame.g:4509:1: ( ':' )
            {
            // InternalGame.g:4509:1: ( ':' )
            // InternalGame.g:4510:2: ':'
            {
             before(grammarAccess.getCheminAccess().getColonKeyword_27()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getColonKeyword_27()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__27__Impl"


    // $ANTLR start "rule__Chemin__Group__28"
    // InternalGame.g:4519:1: rule__Chemin__Group__28 : rule__Chemin__Group__28__Impl ;
    public final void rule__Chemin__Group__28() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4523:1: ( rule__Chemin__Group__28__Impl )
            // InternalGame.g:4524:2: rule__Chemin__Group__28__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__Group__28__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__28"


    // $ANTLR start "rule__Chemin__Group__28__Impl"
    // InternalGame.g:4530:1: rule__Chemin__Group__28__Impl : ( ( ( rule__Chemin__Group_28__0 ) ) ( ( rule__Chemin__Group_28__0 )* ) ) ;
    public final void rule__Chemin__Group__28__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4534:1: ( ( ( ( rule__Chemin__Group_28__0 ) ) ( ( rule__Chemin__Group_28__0 )* ) ) )
            // InternalGame.g:4535:1: ( ( ( rule__Chemin__Group_28__0 ) ) ( ( rule__Chemin__Group_28__0 )* ) )
            {
            // InternalGame.g:4535:1: ( ( ( rule__Chemin__Group_28__0 ) ) ( ( rule__Chemin__Group_28__0 )* ) )
            // InternalGame.g:4536:2: ( ( rule__Chemin__Group_28__0 ) ) ( ( rule__Chemin__Group_28__0 )* )
            {
            // InternalGame.g:4536:2: ( ( rule__Chemin__Group_28__0 ) )
            // InternalGame.g:4537:3: ( rule__Chemin__Group_28__0 )
            {
             before(grammarAccess.getCheminAccess().getGroup_28()); 
            // InternalGame.g:4538:3: ( rule__Chemin__Group_28__0 )
            // InternalGame.g:4538:4: rule__Chemin__Group_28__0
            {
            pushFollow(FOLLOW_5);
            rule__Chemin__Group_28__0();

            state._fsp--;


            }

             after(grammarAccess.getCheminAccess().getGroup_28()); 

            }

            // InternalGame.g:4541:2: ( ( rule__Chemin__Group_28__0 )* )
            // InternalGame.g:4542:3: ( rule__Chemin__Group_28__0 )*
            {
             before(grammarAccess.getCheminAccess().getGroup_28()); 
            // InternalGame.g:4543:3: ( rule__Chemin__Group_28__0 )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==20) ) {
                    int LA21_2 = input.LA(2);

                    if ( (LA21_2==RULE_ID) ) {
                        int LA21_3 = input.LA(3);

                        if ( (LA21_3==14) ) {
                            int LA21_4 = input.LA(4);

                            if ( (LA21_4==44) ) {
                                alt21=1;
                            }


                        }


                    }


                }


                switch (alt21) {
            	case 1 :
            	    // InternalGame.g:4543:4: rule__Chemin__Group_28__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Chemin__Group_28__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

             after(grammarAccess.getCheminAccess().getGroup_28()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group__28__Impl"


    // $ANTLR start "rule__Chemin__Group_19__0"
    // InternalGame.g:4553:1: rule__Chemin__Group_19__0 : rule__Chemin__Group_19__0__Impl rule__Chemin__Group_19__1 ;
    public final void rule__Chemin__Group_19__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4557:1: ( rule__Chemin__Group_19__0__Impl rule__Chemin__Group_19__1 )
            // InternalGame.g:4558:2: rule__Chemin__Group_19__0__Impl rule__Chemin__Group_19__1
            {
            pushFollow(FOLLOW_12);
            rule__Chemin__Group_19__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group_19__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_19__0"


    // $ANTLR start "rule__Chemin__Group_19__0__Impl"
    // InternalGame.g:4565:1: rule__Chemin__Group_19__0__Impl : ( '-' ) ;
    public final void rule__Chemin__Group_19__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4569:1: ( ( '-' ) )
            // InternalGame.g:4570:1: ( '-' )
            {
            // InternalGame.g:4570:1: ( '-' )
            // InternalGame.g:4571:2: '-'
            {
             before(grammarAccess.getCheminAccess().getHyphenMinusKeyword_19_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getHyphenMinusKeyword_19_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_19__0__Impl"


    // $ANTLR start "rule__Chemin__Group_19__1"
    // InternalGame.g:4580:1: rule__Chemin__Group_19__1 : rule__Chemin__Group_19__1__Impl ;
    public final void rule__Chemin__Group_19__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4584:1: ( rule__Chemin__Group_19__1__Impl )
            // InternalGame.g:4585:2: rule__Chemin__Group_19__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__Group_19__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_19__1"


    // $ANTLR start "rule__Chemin__Group_19__1__Impl"
    // InternalGame.g:4591:1: rule__Chemin__Group_19__1__Impl : ( ( rule__Chemin__ConnaissancesAssignment_19_1 ) ) ;
    public final void rule__Chemin__Group_19__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4595:1: ( ( ( rule__Chemin__ConnaissancesAssignment_19_1 ) ) )
            // InternalGame.g:4596:1: ( ( rule__Chemin__ConnaissancesAssignment_19_1 ) )
            {
            // InternalGame.g:4596:1: ( ( rule__Chemin__ConnaissancesAssignment_19_1 ) )
            // InternalGame.g:4597:2: ( rule__Chemin__ConnaissancesAssignment_19_1 )
            {
             before(grammarAccess.getCheminAccess().getConnaissancesAssignment_19_1()); 
            // InternalGame.g:4598:2: ( rule__Chemin__ConnaissancesAssignment_19_1 )
            // InternalGame.g:4598:3: rule__Chemin__ConnaissancesAssignment_19_1
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__ConnaissancesAssignment_19_1();

            state._fsp--;


            }

             after(grammarAccess.getCheminAccess().getConnaissancesAssignment_19_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_19__1__Impl"


    // $ANTLR start "rule__Chemin__Group_22__0"
    // InternalGame.g:4607:1: rule__Chemin__Group_22__0 : rule__Chemin__Group_22__0__Impl rule__Chemin__Group_22__1 ;
    public final void rule__Chemin__Group_22__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4611:1: ( rule__Chemin__Group_22__0__Impl rule__Chemin__Group_22__1 )
            // InternalGame.g:4612:2: rule__Chemin__Group_22__0__Impl rule__Chemin__Group_22__1
            {
            pushFollow(FOLLOW_12);
            rule__Chemin__Group_22__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group_22__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_22__0"


    // $ANTLR start "rule__Chemin__Group_22__0__Impl"
    // InternalGame.g:4619:1: rule__Chemin__Group_22__0__Impl : ( '-' ) ;
    public final void rule__Chemin__Group_22__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4623:1: ( ( '-' ) )
            // InternalGame.g:4624:1: ( '-' )
            {
            // InternalGame.g:4624:1: ( '-' )
            // InternalGame.g:4625:2: '-'
            {
             before(grammarAccess.getCheminAccess().getHyphenMinusKeyword_22_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getHyphenMinusKeyword_22_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_22__0__Impl"


    // $ANTLR start "rule__Chemin__Group_22__1"
    // InternalGame.g:4634:1: rule__Chemin__Group_22__1 : rule__Chemin__Group_22__1__Impl ;
    public final void rule__Chemin__Group_22__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4638:1: ( rule__Chemin__Group_22__1__Impl )
            // InternalGame.g:4639:2: rule__Chemin__Group_22__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__Group_22__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_22__1"


    // $ANTLR start "rule__Chemin__Group_22__1__Impl"
    // InternalGame.g:4645:1: rule__Chemin__Group_22__1__Impl : ( ( rule__Chemin__ObjetsRecusAssignment_22_1 ) ) ;
    public final void rule__Chemin__Group_22__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4649:1: ( ( ( rule__Chemin__ObjetsRecusAssignment_22_1 ) ) )
            // InternalGame.g:4650:1: ( ( rule__Chemin__ObjetsRecusAssignment_22_1 ) )
            {
            // InternalGame.g:4650:1: ( ( rule__Chemin__ObjetsRecusAssignment_22_1 ) )
            // InternalGame.g:4651:2: ( rule__Chemin__ObjetsRecusAssignment_22_1 )
            {
             before(grammarAccess.getCheminAccess().getObjetsRecusAssignment_22_1()); 
            // InternalGame.g:4652:2: ( rule__Chemin__ObjetsRecusAssignment_22_1 )
            // InternalGame.g:4652:3: rule__Chemin__ObjetsRecusAssignment_22_1
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__ObjetsRecusAssignment_22_1();

            state._fsp--;


            }

             after(grammarAccess.getCheminAccess().getObjetsRecusAssignment_22_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_22__1__Impl"


    // $ANTLR start "rule__Chemin__Group_25__0"
    // InternalGame.g:4661:1: rule__Chemin__Group_25__0 : rule__Chemin__Group_25__0__Impl rule__Chemin__Group_25__1 ;
    public final void rule__Chemin__Group_25__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4665:1: ( rule__Chemin__Group_25__0__Impl rule__Chemin__Group_25__1 )
            // InternalGame.g:4666:2: rule__Chemin__Group_25__0__Impl rule__Chemin__Group_25__1
            {
            pushFollow(FOLLOW_12);
            rule__Chemin__Group_25__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group_25__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_25__0"


    // $ANTLR start "rule__Chemin__Group_25__0__Impl"
    // InternalGame.g:4673:1: rule__Chemin__Group_25__0__Impl : ( '-' ) ;
    public final void rule__Chemin__Group_25__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4677:1: ( ( '-' ) )
            // InternalGame.g:4678:1: ( '-' )
            {
            // InternalGame.g:4678:1: ( '-' )
            // InternalGame.g:4679:2: '-'
            {
             before(grammarAccess.getCheminAccess().getHyphenMinusKeyword_25_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getHyphenMinusKeyword_25_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_25__0__Impl"


    // $ANTLR start "rule__Chemin__Group_25__1"
    // InternalGame.g:4688:1: rule__Chemin__Group_25__1 : rule__Chemin__Group_25__1__Impl ;
    public final void rule__Chemin__Group_25__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4692:1: ( rule__Chemin__Group_25__1__Impl )
            // InternalGame.g:4693:2: rule__Chemin__Group_25__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__Group_25__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_25__1"


    // $ANTLR start "rule__Chemin__Group_25__1__Impl"
    // InternalGame.g:4699:1: rule__Chemin__Group_25__1__Impl : ( ( rule__Chemin__ObjetsConsoAssignment_25_1 ) ) ;
    public final void rule__Chemin__Group_25__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4703:1: ( ( ( rule__Chemin__ObjetsConsoAssignment_25_1 ) ) )
            // InternalGame.g:4704:1: ( ( rule__Chemin__ObjetsConsoAssignment_25_1 ) )
            {
            // InternalGame.g:4704:1: ( ( rule__Chemin__ObjetsConsoAssignment_25_1 ) )
            // InternalGame.g:4705:2: ( rule__Chemin__ObjetsConsoAssignment_25_1 )
            {
             before(grammarAccess.getCheminAccess().getObjetsConsoAssignment_25_1()); 
            // InternalGame.g:4706:2: ( rule__Chemin__ObjetsConsoAssignment_25_1 )
            // InternalGame.g:4706:3: rule__Chemin__ObjetsConsoAssignment_25_1
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__ObjetsConsoAssignment_25_1();

            state._fsp--;


            }

             after(grammarAccess.getCheminAccess().getObjetsConsoAssignment_25_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_25__1__Impl"


    // $ANTLR start "rule__Chemin__Group_28__0"
    // InternalGame.g:4715:1: rule__Chemin__Group_28__0 : rule__Chemin__Group_28__0__Impl rule__Chemin__Group_28__1 ;
    public final void rule__Chemin__Group_28__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4719:1: ( rule__Chemin__Group_28__0__Impl rule__Chemin__Group_28__1 )
            // InternalGame.g:4720:2: rule__Chemin__Group_28__0__Impl rule__Chemin__Group_28__1
            {
            pushFollow(FOLLOW_12);
            rule__Chemin__Group_28__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Chemin__Group_28__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_28__0"


    // $ANTLR start "rule__Chemin__Group_28__0__Impl"
    // InternalGame.g:4727:1: rule__Chemin__Group_28__0__Impl : ( '-' ) ;
    public final void rule__Chemin__Group_28__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4731:1: ( ( '-' ) )
            // InternalGame.g:4732:1: ( '-' )
            {
            // InternalGame.g:4732:1: ( '-' )
            // InternalGame.g:4733:2: '-'
            {
             before(grammarAccess.getCheminAccess().getHyphenMinusKeyword_28_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getHyphenMinusKeyword_28_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_28__0__Impl"


    // $ANTLR start "rule__Chemin__Group_28__1"
    // InternalGame.g:4742:1: rule__Chemin__Group_28__1 : rule__Chemin__Group_28__1__Impl ;
    public final void rule__Chemin__Group_28__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4746:1: ( rule__Chemin__Group_28__1__Impl )
            // InternalGame.g:4747:2: rule__Chemin__Group_28__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__Group_28__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_28__1"


    // $ANTLR start "rule__Chemin__Group_28__1__Impl"
    // InternalGame.g:4753:1: rule__Chemin__Group_28__1__Impl : ( ( rule__Chemin__DescriptionsAssignment_28_1 ) ) ;
    public final void rule__Chemin__Group_28__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4757:1: ( ( ( rule__Chemin__DescriptionsAssignment_28_1 ) ) )
            // InternalGame.g:4758:1: ( ( rule__Chemin__DescriptionsAssignment_28_1 ) )
            {
            // InternalGame.g:4758:1: ( ( rule__Chemin__DescriptionsAssignment_28_1 ) )
            // InternalGame.g:4759:2: ( rule__Chemin__DescriptionsAssignment_28_1 )
            {
             before(grammarAccess.getCheminAccess().getDescriptionsAssignment_28_1()); 
            // InternalGame.g:4760:2: ( rule__Chemin__DescriptionsAssignment_28_1 )
            // InternalGame.g:4760:3: rule__Chemin__DescriptionsAssignment_28_1
            {
            pushFollow(FOLLOW_2);
            rule__Chemin__DescriptionsAssignment_28_1();

            state._fsp--;


            }

             after(grammarAccess.getCheminAccess().getDescriptionsAssignment_28_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__Group_28__1__Impl"


    // $ANTLR start "rule__Personne__Group__0"
    // InternalGame.g:4769:1: rule__Personne__Group__0 : rule__Personne__Group__0__Impl rule__Personne__Group__1 ;
    public final void rule__Personne__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4773:1: ( rule__Personne__Group__0__Impl rule__Personne__Group__1 )
            // InternalGame.g:4774:2: rule__Personne__Group__0__Impl rule__Personne__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Personne__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Personne__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__0"


    // $ANTLR start "rule__Personne__Group__0__Impl"
    // InternalGame.g:4781:1: rule__Personne__Group__0__Impl : ( ( rule__Personne__NameAssignment_0 ) ) ;
    public final void rule__Personne__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4785:1: ( ( ( rule__Personne__NameAssignment_0 ) ) )
            // InternalGame.g:4786:1: ( ( rule__Personne__NameAssignment_0 ) )
            {
            // InternalGame.g:4786:1: ( ( rule__Personne__NameAssignment_0 ) )
            // InternalGame.g:4787:2: ( rule__Personne__NameAssignment_0 )
            {
             before(grammarAccess.getPersonneAccess().getNameAssignment_0()); 
            // InternalGame.g:4788:2: ( rule__Personne__NameAssignment_0 )
            // InternalGame.g:4788:3: rule__Personne__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Personne__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPersonneAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__0__Impl"


    // $ANTLR start "rule__Personne__Group__1"
    // InternalGame.g:4796:1: rule__Personne__Group__1 : rule__Personne__Group__1__Impl rule__Personne__Group__2 ;
    public final void rule__Personne__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4800:1: ( rule__Personne__Group__1__Impl rule__Personne__Group__2 )
            // InternalGame.g:4801:2: rule__Personne__Group__1__Impl rule__Personne__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Personne__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Personne__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__1"


    // $ANTLR start "rule__Personne__Group__1__Impl"
    // InternalGame.g:4808:1: rule__Personne__Group__1__Impl : ( ':' ) ;
    public final void rule__Personne__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4812:1: ( ( ':' ) )
            // InternalGame.g:4813:1: ( ':' )
            {
            // InternalGame.g:4813:1: ( ':' )
            // InternalGame.g:4814:2: ':'
            {
             before(grammarAccess.getPersonneAccess().getColonKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getPersonneAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__1__Impl"


    // $ANTLR start "rule__Personne__Group__2"
    // InternalGame.g:4823:1: rule__Personne__Group__2 : rule__Personne__Group__2__Impl rule__Personne__Group__3 ;
    public final void rule__Personne__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4827:1: ( rule__Personne__Group__2__Impl rule__Personne__Group__3 )
            // InternalGame.g:4828:2: rule__Personne__Group__2__Impl rule__Personne__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__Personne__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Personne__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__2"


    // $ANTLR start "rule__Personne__Group__2__Impl"
    // InternalGame.g:4835:1: rule__Personne__Group__2__Impl : ( 'visible' ) ;
    public final void rule__Personne__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4839:1: ( ( 'visible' ) )
            // InternalGame.g:4840:1: ( 'visible' )
            {
            // InternalGame.g:4840:1: ( 'visible' )
            // InternalGame.g:4841:2: 'visible'
            {
             before(grammarAccess.getPersonneAccess().getVisibleKeyword_2()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getPersonneAccess().getVisibleKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__2__Impl"


    // $ANTLR start "rule__Personne__Group__3"
    // InternalGame.g:4850:1: rule__Personne__Group__3 : rule__Personne__Group__3__Impl rule__Personne__Group__4 ;
    public final void rule__Personne__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4854:1: ( rule__Personne__Group__3__Impl rule__Personne__Group__4 )
            // InternalGame.g:4855:2: rule__Personne__Group__3__Impl rule__Personne__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__Personne__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Personne__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__3"


    // $ANTLR start "rule__Personne__Group__3__Impl"
    // InternalGame.g:4862:1: rule__Personne__Group__3__Impl : ( ':' ) ;
    public final void rule__Personne__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4866:1: ( ( ':' ) )
            // InternalGame.g:4867:1: ( ':' )
            {
            // InternalGame.g:4867:1: ( ':' )
            // InternalGame.g:4868:2: ':'
            {
             before(grammarAccess.getPersonneAccess().getColonKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getPersonneAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__3__Impl"


    // $ANTLR start "rule__Personne__Group__4"
    // InternalGame.g:4877:1: rule__Personne__Group__4 : rule__Personne__Group__4__Impl rule__Personne__Group__5 ;
    public final void rule__Personne__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4881:1: ( rule__Personne__Group__4__Impl rule__Personne__Group__5 )
            // InternalGame.g:4882:2: rule__Personne__Group__4__Impl rule__Personne__Group__5
            {
            pushFollow(FOLLOW_34);
            rule__Personne__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Personne__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__4"


    // $ANTLR start "rule__Personne__Group__4__Impl"
    // InternalGame.g:4889:1: rule__Personne__Group__4__Impl : ( ( rule__Personne__VisibleAssignment_4 ) ) ;
    public final void rule__Personne__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4893:1: ( ( ( rule__Personne__VisibleAssignment_4 ) ) )
            // InternalGame.g:4894:1: ( ( rule__Personne__VisibleAssignment_4 ) )
            {
            // InternalGame.g:4894:1: ( ( rule__Personne__VisibleAssignment_4 ) )
            // InternalGame.g:4895:2: ( rule__Personne__VisibleAssignment_4 )
            {
             before(grammarAccess.getPersonneAccess().getVisibleAssignment_4()); 
            // InternalGame.g:4896:2: ( rule__Personne__VisibleAssignment_4 )
            // InternalGame.g:4896:3: rule__Personne__VisibleAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Personne__VisibleAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getPersonneAccess().getVisibleAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__4__Impl"


    // $ANTLR start "rule__Personne__Group__5"
    // InternalGame.g:4904:1: rule__Personne__Group__5 : rule__Personne__Group__5__Impl rule__Personne__Group__6 ;
    public final void rule__Personne__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4908:1: ( rule__Personne__Group__5__Impl rule__Personne__Group__6 )
            // InternalGame.g:4909:2: rule__Personne__Group__5__Impl rule__Personne__Group__6
            {
            pushFollow(FOLLOW_3);
            rule__Personne__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Personne__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__5"


    // $ANTLR start "rule__Personne__Group__5__Impl"
    // InternalGame.g:4916:1: rule__Personne__Group__5__Impl : ( 'obligatoire' ) ;
    public final void rule__Personne__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4920:1: ( ( 'obligatoire' ) )
            // InternalGame.g:4921:1: ( 'obligatoire' )
            {
            // InternalGame.g:4921:1: ( 'obligatoire' )
            // InternalGame.g:4922:2: 'obligatoire'
            {
             before(grammarAccess.getPersonneAccess().getObligatoireKeyword_5()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getPersonneAccess().getObligatoireKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__5__Impl"


    // $ANTLR start "rule__Personne__Group__6"
    // InternalGame.g:4931:1: rule__Personne__Group__6 : rule__Personne__Group__6__Impl rule__Personne__Group__7 ;
    public final void rule__Personne__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4935:1: ( rule__Personne__Group__6__Impl rule__Personne__Group__7 )
            // InternalGame.g:4936:2: rule__Personne__Group__6__Impl rule__Personne__Group__7
            {
            pushFollow(FOLLOW_15);
            rule__Personne__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Personne__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__6"


    // $ANTLR start "rule__Personne__Group__6__Impl"
    // InternalGame.g:4943:1: rule__Personne__Group__6__Impl : ( ':' ) ;
    public final void rule__Personne__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4947:1: ( ( ':' ) )
            // InternalGame.g:4948:1: ( ':' )
            {
            // InternalGame.g:4948:1: ( ':' )
            // InternalGame.g:4949:2: ':'
            {
             before(grammarAccess.getPersonneAccess().getColonKeyword_6()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getPersonneAccess().getColonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__6__Impl"


    // $ANTLR start "rule__Personne__Group__7"
    // InternalGame.g:4958:1: rule__Personne__Group__7 : rule__Personne__Group__7__Impl rule__Personne__Group__8 ;
    public final void rule__Personne__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4962:1: ( rule__Personne__Group__7__Impl rule__Personne__Group__8 )
            // InternalGame.g:4963:2: rule__Personne__Group__7__Impl rule__Personne__Group__8
            {
            pushFollow(FOLLOW_37);
            rule__Personne__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Personne__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__7"


    // $ANTLR start "rule__Personne__Group__7__Impl"
    // InternalGame.g:4970:1: rule__Personne__Group__7__Impl : ( ( rule__Personne__ObligatoireAssignment_7 ) ) ;
    public final void rule__Personne__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4974:1: ( ( ( rule__Personne__ObligatoireAssignment_7 ) ) )
            // InternalGame.g:4975:1: ( ( rule__Personne__ObligatoireAssignment_7 ) )
            {
            // InternalGame.g:4975:1: ( ( rule__Personne__ObligatoireAssignment_7 ) )
            // InternalGame.g:4976:2: ( rule__Personne__ObligatoireAssignment_7 )
            {
             before(grammarAccess.getPersonneAccess().getObligatoireAssignment_7()); 
            // InternalGame.g:4977:2: ( rule__Personne__ObligatoireAssignment_7 )
            // InternalGame.g:4977:3: rule__Personne__ObligatoireAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__Personne__ObligatoireAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getPersonneAccess().getObligatoireAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__7__Impl"


    // $ANTLR start "rule__Personne__Group__8"
    // InternalGame.g:4985:1: rule__Personne__Group__8 : rule__Personne__Group__8__Impl rule__Personne__Group__9 ;
    public final void rule__Personne__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:4989:1: ( rule__Personne__Group__8__Impl rule__Personne__Group__9 )
            // InternalGame.g:4990:2: rule__Personne__Group__8__Impl rule__Personne__Group__9
            {
            pushFollow(FOLLOW_3);
            rule__Personne__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Personne__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__8"


    // $ANTLR start "rule__Personne__Group__8__Impl"
    // InternalGame.g:4997:1: rule__Personne__Group__8__Impl : ( 'interactions' ) ;
    public final void rule__Personne__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5001:1: ( ( 'interactions' ) )
            // InternalGame.g:5002:1: ( 'interactions' )
            {
            // InternalGame.g:5002:1: ( 'interactions' )
            // InternalGame.g:5003:2: 'interactions'
            {
             before(grammarAccess.getPersonneAccess().getInteractionsKeyword_8()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getPersonneAccess().getInteractionsKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__8__Impl"


    // $ANTLR start "rule__Personne__Group__9"
    // InternalGame.g:5012:1: rule__Personne__Group__9 : rule__Personne__Group__9__Impl rule__Personne__Group__10 ;
    public final void rule__Personne__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5016:1: ( rule__Personne__Group__9__Impl rule__Personne__Group__10 )
            // InternalGame.g:5017:2: rule__Personne__Group__9__Impl rule__Personne__Group__10
            {
            pushFollow(FOLLOW_17);
            rule__Personne__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Personne__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__9"


    // $ANTLR start "rule__Personne__Group__9__Impl"
    // InternalGame.g:5024:1: rule__Personne__Group__9__Impl : ( ':' ) ;
    public final void rule__Personne__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5028:1: ( ( ':' ) )
            // InternalGame.g:5029:1: ( ':' )
            {
            // InternalGame.g:5029:1: ( ':' )
            // InternalGame.g:5030:2: ':'
            {
             before(grammarAccess.getPersonneAccess().getColonKeyword_9()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getPersonneAccess().getColonKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__9__Impl"


    // $ANTLR start "rule__Personne__Group__10"
    // InternalGame.g:5039:1: rule__Personne__Group__10 : rule__Personne__Group__10__Impl ;
    public final void rule__Personne__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5043:1: ( rule__Personne__Group__10__Impl )
            // InternalGame.g:5044:2: rule__Personne__Group__10__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Personne__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__10"


    // $ANTLR start "rule__Personne__Group__10__Impl"
    // InternalGame.g:5050:1: rule__Personne__Group__10__Impl : ( ( ( rule__Personne__Group_10__0 ) ) ( ( rule__Personne__Group_10__0 )* ) ) ;
    public final void rule__Personne__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5054:1: ( ( ( ( rule__Personne__Group_10__0 ) ) ( ( rule__Personne__Group_10__0 )* ) ) )
            // InternalGame.g:5055:1: ( ( ( rule__Personne__Group_10__0 ) ) ( ( rule__Personne__Group_10__0 )* ) )
            {
            // InternalGame.g:5055:1: ( ( ( rule__Personne__Group_10__0 ) ) ( ( rule__Personne__Group_10__0 )* ) )
            // InternalGame.g:5056:2: ( ( rule__Personne__Group_10__0 ) ) ( ( rule__Personne__Group_10__0 )* )
            {
            // InternalGame.g:5056:2: ( ( rule__Personne__Group_10__0 ) )
            // InternalGame.g:5057:3: ( rule__Personne__Group_10__0 )
            {
             before(grammarAccess.getPersonneAccess().getGroup_10()); 
            // InternalGame.g:5058:3: ( rule__Personne__Group_10__0 )
            // InternalGame.g:5058:4: rule__Personne__Group_10__0
            {
            pushFollow(FOLLOW_5);
            rule__Personne__Group_10__0();

            state._fsp--;


            }

             after(grammarAccess.getPersonneAccess().getGroup_10()); 

            }

            // InternalGame.g:5061:2: ( ( rule__Personne__Group_10__0 )* )
            // InternalGame.g:5062:3: ( rule__Personne__Group_10__0 )*
            {
             before(grammarAccess.getPersonneAccess().getGroup_10()); 
            // InternalGame.g:5063:3: ( rule__Personne__Group_10__0 )*
            loop22:
            do {
                int alt22=2;
                alt22 = dfa22.predict(input);
                switch (alt22) {
            	case 1 :
            	    // InternalGame.g:5063:4: rule__Personne__Group_10__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Personne__Group_10__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getPersonneAccess().getGroup_10()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group__10__Impl"


    // $ANTLR start "rule__Personne__Group_10__0"
    // InternalGame.g:5073:1: rule__Personne__Group_10__0 : rule__Personne__Group_10__0__Impl rule__Personne__Group_10__1 ;
    public final void rule__Personne__Group_10__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5077:1: ( rule__Personne__Group_10__0__Impl rule__Personne__Group_10__1 )
            // InternalGame.g:5078:2: rule__Personne__Group_10__0__Impl rule__Personne__Group_10__1
            {
            pushFollow(FOLLOW_12);
            rule__Personne__Group_10__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Personne__Group_10__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group_10__0"


    // $ANTLR start "rule__Personne__Group_10__0__Impl"
    // InternalGame.g:5085:1: rule__Personne__Group_10__0__Impl : ( '-' ) ;
    public final void rule__Personne__Group_10__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5089:1: ( ( '-' ) )
            // InternalGame.g:5090:1: ( '-' )
            {
            // InternalGame.g:5090:1: ( '-' )
            // InternalGame.g:5091:2: '-'
            {
             before(grammarAccess.getPersonneAccess().getHyphenMinusKeyword_10_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getPersonneAccess().getHyphenMinusKeyword_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group_10__0__Impl"


    // $ANTLR start "rule__Personne__Group_10__1"
    // InternalGame.g:5100:1: rule__Personne__Group_10__1 : rule__Personne__Group_10__1__Impl ;
    public final void rule__Personne__Group_10__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5104:1: ( rule__Personne__Group_10__1__Impl )
            // InternalGame.g:5105:2: rule__Personne__Group_10__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Personne__Group_10__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group_10__1"


    // $ANTLR start "rule__Personne__Group_10__1__Impl"
    // InternalGame.g:5111:1: rule__Personne__Group_10__1__Impl : ( ( rule__Personne__InteractionsAssignment_10_1 ) ) ;
    public final void rule__Personne__Group_10__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5115:1: ( ( ( rule__Personne__InteractionsAssignment_10_1 ) ) )
            // InternalGame.g:5116:1: ( ( rule__Personne__InteractionsAssignment_10_1 ) )
            {
            // InternalGame.g:5116:1: ( ( rule__Personne__InteractionsAssignment_10_1 ) )
            // InternalGame.g:5117:2: ( rule__Personne__InteractionsAssignment_10_1 )
            {
             before(grammarAccess.getPersonneAccess().getInteractionsAssignment_10_1()); 
            // InternalGame.g:5118:2: ( rule__Personne__InteractionsAssignment_10_1 )
            // InternalGame.g:5118:3: rule__Personne__InteractionsAssignment_10_1
            {
            pushFollow(FOLLOW_2);
            rule__Personne__InteractionsAssignment_10_1();

            state._fsp--;


            }

             after(grammarAccess.getPersonneAccess().getInteractionsAssignment_10_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__Group_10__1__Impl"


    // $ANTLR start "rule__Interaction__Group__0"
    // InternalGame.g:5127:1: rule__Interaction__Group__0 : rule__Interaction__Group__0__Impl rule__Interaction__Group__1 ;
    public final void rule__Interaction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5131:1: ( rule__Interaction__Group__0__Impl rule__Interaction__Group__1 )
            // InternalGame.g:5132:2: rule__Interaction__Group__0__Impl rule__Interaction__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Interaction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__0"


    // $ANTLR start "rule__Interaction__Group__0__Impl"
    // InternalGame.g:5139:1: rule__Interaction__Group__0__Impl : ( ( rule__Interaction__NameAssignment_0 ) ) ;
    public final void rule__Interaction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5143:1: ( ( ( rule__Interaction__NameAssignment_0 ) ) )
            // InternalGame.g:5144:1: ( ( rule__Interaction__NameAssignment_0 ) )
            {
            // InternalGame.g:5144:1: ( ( rule__Interaction__NameAssignment_0 ) )
            // InternalGame.g:5145:2: ( rule__Interaction__NameAssignment_0 )
            {
             before(grammarAccess.getInteractionAccess().getNameAssignment_0()); 
            // InternalGame.g:5146:2: ( rule__Interaction__NameAssignment_0 )
            // InternalGame.g:5146:3: rule__Interaction__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Interaction__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getInteractionAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__0__Impl"


    // $ANTLR start "rule__Interaction__Group__1"
    // InternalGame.g:5154:1: rule__Interaction__Group__1 : rule__Interaction__Group__1__Impl rule__Interaction__Group__2 ;
    public final void rule__Interaction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5158:1: ( rule__Interaction__Group__1__Impl rule__Interaction__Group__2 )
            // InternalGame.g:5159:2: rule__Interaction__Group__1__Impl rule__Interaction__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Interaction__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__1"


    // $ANTLR start "rule__Interaction__Group__1__Impl"
    // InternalGame.g:5166:1: rule__Interaction__Group__1__Impl : ( ':' ) ;
    public final void rule__Interaction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5170:1: ( ( ':' ) )
            // InternalGame.g:5171:1: ( ':' )
            {
            // InternalGame.g:5171:1: ( ':' )
            // InternalGame.g:5172:2: ':'
            {
             before(grammarAccess.getInteractionAccess().getColonKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__1__Impl"


    // $ANTLR start "rule__Interaction__Group__2"
    // InternalGame.g:5181:1: rule__Interaction__Group__2 : rule__Interaction__Group__2__Impl rule__Interaction__Group__3 ;
    public final void rule__Interaction__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5185:1: ( rule__Interaction__Group__2__Impl rule__Interaction__Group__3 )
            // InternalGame.g:5186:2: rule__Interaction__Group__2__Impl rule__Interaction__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__Interaction__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__2"


    // $ANTLR start "rule__Interaction__Group__2__Impl"
    // InternalGame.g:5193:1: rule__Interaction__Group__2__Impl : ( 'visible' ) ;
    public final void rule__Interaction__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5197:1: ( ( 'visible' ) )
            // InternalGame.g:5198:1: ( 'visible' )
            {
            // InternalGame.g:5198:1: ( 'visible' )
            // InternalGame.g:5199:2: 'visible'
            {
             before(grammarAccess.getInteractionAccess().getVisibleKeyword_2()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getVisibleKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__2__Impl"


    // $ANTLR start "rule__Interaction__Group__3"
    // InternalGame.g:5208:1: rule__Interaction__Group__3 : rule__Interaction__Group__3__Impl rule__Interaction__Group__4 ;
    public final void rule__Interaction__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5212:1: ( rule__Interaction__Group__3__Impl rule__Interaction__Group__4 )
            // InternalGame.g:5213:2: rule__Interaction__Group__3__Impl rule__Interaction__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__Interaction__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__3"


    // $ANTLR start "rule__Interaction__Group__3__Impl"
    // InternalGame.g:5220:1: rule__Interaction__Group__3__Impl : ( ':' ) ;
    public final void rule__Interaction__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5224:1: ( ( ':' ) )
            // InternalGame.g:5225:1: ( ':' )
            {
            // InternalGame.g:5225:1: ( ':' )
            // InternalGame.g:5226:2: ':'
            {
             before(grammarAccess.getInteractionAccess().getColonKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__3__Impl"


    // $ANTLR start "rule__Interaction__Group__4"
    // InternalGame.g:5235:1: rule__Interaction__Group__4 : rule__Interaction__Group__4__Impl rule__Interaction__Group__5 ;
    public final void rule__Interaction__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5239:1: ( rule__Interaction__Group__4__Impl rule__Interaction__Group__5 )
            // InternalGame.g:5240:2: rule__Interaction__Group__4__Impl rule__Interaction__Group__5
            {
            pushFollow(FOLLOW_21);
            rule__Interaction__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__4"


    // $ANTLR start "rule__Interaction__Group__4__Impl"
    // InternalGame.g:5247:1: rule__Interaction__Group__4__Impl : ( ( rule__Interaction__VisibleAssignment_4 ) ) ;
    public final void rule__Interaction__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5251:1: ( ( ( rule__Interaction__VisibleAssignment_4 ) ) )
            // InternalGame.g:5252:1: ( ( rule__Interaction__VisibleAssignment_4 ) )
            {
            // InternalGame.g:5252:1: ( ( rule__Interaction__VisibleAssignment_4 ) )
            // InternalGame.g:5253:2: ( rule__Interaction__VisibleAssignment_4 )
            {
             before(grammarAccess.getInteractionAccess().getVisibleAssignment_4()); 
            // InternalGame.g:5254:2: ( rule__Interaction__VisibleAssignment_4 )
            // InternalGame.g:5254:3: rule__Interaction__VisibleAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Interaction__VisibleAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getInteractionAccess().getVisibleAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__4__Impl"


    // $ANTLR start "rule__Interaction__Group__5"
    // InternalGame.g:5262:1: rule__Interaction__Group__5 : rule__Interaction__Group__5__Impl rule__Interaction__Group__6 ;
    public final void rule__Interaction__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5266:1: ( rule__Interaction__Group__5__Impl rule__Interaction__Group__6 )
            // InternalGame.g:5267:2: rule__Interaction__Group__5__Impl rule__Interaction__Group__6
            {
            pushFollow(FOLLOW_3);
            rule__Interaction__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__5"


    // $ANTLR start "rule__Interaction__Group__5__Impl"
    // InternalGame.g:5274:1: rule__Interaction__Group__5__Impl : ( 'connaissances' ) ;
    public final void rule__Interaction__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5278:1: ( ( 'connaissances' ) )
            // InternalGame.g:5279:1: ( 'connaissances' )
            {
            // InternalGame.g:5279:1: ( 'connaissances' )
            // InternalGame.g:5280:2: 'connaissances'
            {
             before(grammarAccess.getInteractionAccess().getConnaissancesKeyword_5()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getConnaissancesKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__5__Impl"


    // $ANTLR start "rule__Interaction__Group__6"
    // InternalGame.g:5289:1: rule__Interaction__Group__6 : rule__Interaction__Group__6__Impl rule__Interaction__Group__7 ;
    public final void rule__Interaction__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5293:1: ( rule__Interaction__Group__6__Impl rule__Interaction__Group__7 )
            // InternalGame.g:5294:2: rule__Interaction__Group__6__Impl rule__Interaction__Group__7
            {
            pushFollow(FOLLOW_35);
            rule__Interaction__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__6"


    // $ANTLR start "rule__Interaction__Group__6__Impl"
    // InternalGame.g:5301:1: rule__Interaction__Group__6__Impl : ( ':' ) ;
    public final void rule__Interaction__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5305:1: ( ( ':' ) )
            // InternalGame.g:5306:1: ( ':' )
            {
            // InternalGame.g:5306:1: ( ':' )
            // InternalGame.g:5307:2: ':'
            {
             before(grammarAccess.getInteractionAccess().getColonKeyword_6()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getColonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__6__Impl"


    // $ANTLR start "rule__Interaction__Group__7"
    // InternalGame.g:5316:1: rule__Interaction__Group__7 : rule__Interaction__Group__7__Impl rule__Interaction__Group__8 ;
    public final void rule__Interaction__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5320:1: ( rule__Interaction__Group__7__Impl rule__Interaction__Group__8 )
            // InternalGame.g:5321:2: rule__Interaction__Group__7__Impl rule__Interaction__Group__8
            {
            pushFollow(FOLLOW_35);
            rule__Interaction__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__7"


    // $ANTLR start "rule__Interaction__Group__7__Impl"
    // InternalGame.g:5328:1: rule__Interaction__Group__7__Impl : ( ( rule__Interaction__Group_7__0 )* ) ;
    public final void rule__Interaction__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5332:1: ( ( ( rule__Interaction__Group_7__0 )* ) )
            // InternalGame.g:5333:1: ( ( rule__Interaction__Group_7__0 )* )
            {
            // InternalGame.g:5333:1: ( ( rule__Interaction__Group_7__0 )* )
            // InternalGame.g:5334:2: ( rule__Interaction__Group_7__0 )*
            {
             before(grammarAccess.getInteractionAccess().getGroup_7()); 
            // InternalGame.g:5335:2: ( rule__Interaction__Group_7__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==20) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalGame.g:5335:3: rule__Interaction__Group_7__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Interaction__Group_7__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getInteractionAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__7__Impl"


    // $ANTLR start "rule__Interaction__Group__8"
    // InternalGame.g:5343:1: rule__Interaction__Group__8 : rule__Interaction__Group__8__Impl rule__Interaction__Group__9 ;
    public final void rule__Interaction__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5347:1: ( rule__Interaction__Group__8__Impl rule__Interaction__Group__9 )
            // InternalGame.g:5348:2: rule__Interaction__Group__8__Impl rule__Interaction__Group__9
            {
            pushFollow(FOLLOW_3);
            rule__Interaction__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__8"


    // $ANTLR start "rule__Interaction__Group__8__Impl"
    // InternalGame.g:5355:1: rule__Interaction__Group__8__Impl : ( 'objets_recus' ) ;
    public final void rule__Interaction__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5359:1: ( ( 'objets_recus' ) )
            // InternalGame.g:5360:1: ( 'objets_recus' )
            {
            // InternalGame.g:5360:1: ( 'objets_recus' )
            // InternalGame.g:5361:2: 'objets_recus'
            {
             before(grammarAccess.getInteractionAccess().getObjets_recusKeyword_8()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getObjets_recusKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__8__Impl"


    // $ANTLR start "rule__Interaction__Group__9"
    // InternalGame.g:5370:1: rule__Interaction__Group__9 : rule__Interaction__Group__9__Impl rule__Interaction__Group__10 ;
    public final void rule__Interaction__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5374:1: ( rule__Interaction__Group__9__Impl rule__Interaction__Group__10 )
            // InternalGame.g:5375:2: rule__Interaction__Group__9__Impl rule__Interaction__Group__10
            {
            pushFollow(FOLLOW_36);
            rule__Interaction__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__9"


    // $ANTLR start "rule__Interaction__Group__9__Impl"
    // InternalGame.g:5382:1: rule__Interaction__Group__9__Impl : ( ':' ) ;
    public final void rule__Interaction__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5386:1: ( ( ':' ) )
            // InternalGame.g:5387:1: ( ':' )
            {
            // InternalGame.g:5387:1: ( ':' )
            // InternalGame.g:5388:2: ':'
            {
             before(grammarAccess.getInteractionAccess().getColonKeyword_9()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getColonKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__9__Impl"


    // $ANTLR start "rule__Interaction__Group__10"
    // InternalGame.g:5397:1: rule__Interaction__Group__10 : rule__Interaction__Group__10__Impl rule__Interaction__Group__11 ;
    public final void rule__Interaction__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5401:1: ( rule__Interaction__Group__10__Impl rule__Interaction__Group__11 )
            // InternalGame.g:5402:2: rule__Interaction__Group__10__Impl rule__Interaction__Group__11
            {
            pushFollow(FOLLOW_36);
            rule__Interaction__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__10"


    // $ANTLR start "rule__Interaction__Group__10__Impl"
    // InternalGame.g:5409:1: rule__Interaction__Group__10__Impl : ( ( rule__Interaction__Group_10__0 )* ) ;
    public final void rule__Interaction__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5413:1: ( ( ( rule__Interaction__Group_10__0 )* ) )
            // InternalGame.g:5414:1: ( ( rule__Interaction__Group_10__0 )* )
            {
            // InternalGame.g:5414:1: ( ( rule__Interaction__Group_10__0 )* )
            // InternalGame.g:5415:2: ( rule__Interaction__Group_10__0 )*
            {
             before(grammarAccess.getInteractionAccess().getGroup_10()); 
            // InternalGame.g:5416:2: ( rule__Interaction__Group_10__0 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==20) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalGame.g:5416:3: rule__Interaction__Group_10__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Interaction__Group_10__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

             after(grammarAccess.getInteractionAccess().getGroup_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__10__Impl"


    // $ANTLR start "rule__Interaction__Group__11"
    // InternalGame.g:5424:1: rule__Interaction__Group__11 : rule__Interaction__Group__11__Impl rule__Interaction__Group__12 ;
    public final void rule__Interaction__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5428:1: ( rule__Interaction__Group__11__Impl rule__Interaction__Group__12 )
            // InternalGame.g:5429:2: rule__Interaction__Group__11__Impl rule__Interaction__Group__12
            {
            pushFollow(FOLLOW_3);
            rule__Interaction__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__11"


    // $ANTLR start "rule__Interaction__Group__11__Impl"
    // InternalGame.g:5436:1: rule__Interaction__Group__11__Impl : ( 'objets_conso' ) ;
    public final void rule__Interaction__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5440:1: ( ( 'objets_conso' ) )
            // InternalGame.g:5441:1: ( 'objets_conso' )
            {
            // InternalGame.g:5441:1: ( 'objets_conso' )
            // InternalGame.g:5442:2: 'objets_conso'
            {
             before(grammarAccess.getInteractionAccess().getObjets_consoKeyword_11()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getObjets_consoKeyword_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__11__Impl"


    // $ANTLR start "rule__Interaction__Group__12"
    // InternalGame.g:5451:1: rule__Interaction__Group__12 : rule__Interaction__Group__12__Impl rule__Interaction__Group__13 ;
    public final void rule__Interaction__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5455:1: ( rule__Interaction__Group__12__Impl rule__Interaction__Group__13 )
            // InternalGame.g:5456:2: rule__Interaction__Group__12__Impl rule__Interaction__Group__13
            {
            pushFollow(FOLLOW_38);
            rule__Interaction__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__12"


    // $ANTLR start "rule__Interaction__Group__12__Impl"
    // InternalGame.g:5463:1: rule__Interaction__Group__12__Impl : ( ':' ) ;
    public final void rule__Interaction__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5467:1: ( ( ':' ) )
            // InternalGame.g:5468:1: ( ':' )
            {
            // InternalGame.g:5468:1: ( ':' )
            // InternalGame.g:5469:2: ':'
            {
             before(grammarAccess.getInteractionAccess().getColonKeyword_12()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getColonKeyword_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__12__Impl"


    // $ANTLR start "rule__Interaction__Group__13"
    // InternalGame.g:5478:1: rule__Interaction__Group__13 : rule__Interaction__Group__13__Impl rule__Interaction__Group__14 ;
    public final void rule__Interaction__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5482:1: ( rule__Interaction__Group__13__Impl rule__Interaction__Group__14 )
            // InternalGame.g:5483:2: rule__Interaction__Group__13__Impl rule__Interaction__Group__14
            {
            pushFollow(FOLLOW_38);
            rule__Interaction__Group__13__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__13"


    // $ANTLR start "rule__Interaction__Group__13__Impl"
    // InternalGame.g:5490:1: rule__Interaction__Group__13__Impl : ( ( rule__Interaction__Group_13__0 )* ) ;
    public final void rule__Interaction__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5494:1: ( ( ( rule__Interaction__Group_13__0 )* ) )
            // InternalGame.g:5495:1: ( ( rule__Interaction__Group_13__0 )* )
            {
            // InternalGame.g:5495:1: ( ( rule__Interaction__Group_13__0 )* )
            // InternalGame.g:5496:2: ( rule__Interaction__Group_13__0 )*
            {
             before(grammarAccess.getInteractionAccess().getGroup_13()); 
            // InternalGame.g:5497:2: ( rule__Interaction__Group_13__0 )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==20) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalGame.g:5497:3: rule__Interaction__Group_13__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Interaction__Group_13__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

             after(grammarAccess.getInteractionAccess().getGroup_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__13__Impl"


    // $ANTLR start "rule__Interaction__Group__14"
    // InternalGame.g:5505:1: rule__Interaction__Group__14 : rule__Interaction__Group__14__Impl rule__Interaction__Group__15 ;
    public final void rule__Interaction__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5509:1: ( rule__Interaction__Group__14__Impl rule__Interaction__Group__15 )
            // InternalGame.g:5510:2: rule__Interaction__Group__14__Impl rule__Interaction__Group__15
            {
            pushFollow(FOLLOW_3);
            rule__Interaction__Group__14__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__14"


    // $ANTLR start "rule__Interaction__Group__14__Impl"
    // InternalGame.g:5517:1: rule__Interaction__Group__14__Impl : ( 'actions' ) ;
    public final void rule__Interaction__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5521:1: ( ( 'actions' ) )
            // InternalGame.g:5522:1: ( 'actions' )
            {
            // InternalGame.g:5522:1: ( 'actions' )
            // InternalGame.g:5523:2: 'actions'
            {
             before(grammarAccess.getInteractionAccess().getActionsKeyword_14()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getActionsKeyword_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__14__Impl"


    // $ANTLR start "rule__Interaction__Group__15"
    // InternalGame.g:5532:1: rule__Interaction__Group__15 : rule__Interaction__Group__15__Impl rule__Interaction__Group__16 ;
    public final void rule__Interaction__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5536:1: ( rule__Interaction__Group__15__Impl rule__Interaction__Group__16 )
            // InternalGame.g:5537:2: rule__Interaction__Group__15__Impl rule__Interaction__Group__16
            {
            pushFollow(FOLLOW_17);
            rule__Interaction__Group__15__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group__16();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__15"


    // $ANTLR start "rule__Interaction__Group__15__Impl"
    // InternalGame.g:5544:1: rule__Interaction__Group__15__Impl : ( ':' ) ;
    public final void rule__Interaction__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5548:1: ( ( ':' ) )
            // InternalGame.g:5549:1: ( ':' )
            {
            // InternalGame.g:5549:1: ( ':' )
            // InternalGame.g:5550:2: ':'
            {
             before(grammarAccess.getInteractionAccess().getColonKeyword_15()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getColonKeyword_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__15__Impl"


    // $ANTLR start "rule__Interaction__Group__16"
    // InternalGame.g:5559:1: rule__Interaction__Group__16 : rule__Interaction__Group__16__Impl ;
    public final void rule__Interaction__Group__16() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5563:1: ( rule__Interaction__Group__16__Impl )
            // InternalGame.g:5564:2: rule__Interaction__Group__16__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Interaction__Group__16__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__16"


    // $ANTLR start "rule__Interaction__Group__16__Impl"
    // InternalGame.g:5570:1: rule__Interaction__Group__16__Impl : ( ( ( rule__Interaction__Group_16__0 ) ) ( ( rule__Interaction__Group_16__0 )* ) ) ;
    public final void rule__Interaction__Group__16__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5574:1: ( ( ( ( rule__Interaction__Group_16__0 ) ) ( ( rule__Interaction__Group_16__0 )* ) ) )
            // InternalGame.g:5575:1: ( ( ( rule__Interaction__Group_16__0 ) ) ( ( rule__Interaction__Group_16__0 )* ) )
            {
            // InternalGame.g:5575:1: ( ( ( rule__Interaction__Group_16__0 ) ) ( ( rule__Interaction__Group_16__0 )* ) )
            // InternalGame.g:5576:2: ( ( rule__Interaction__Group_16__0 ) ) ( ( rule__Interaction__Group_16__0 )* )
            {
            // InternalGame.g:5576:2: ( ( rule__Interaction__Group_16__0 ) )
            // InternalGame.g:5577:3: ( rule__Interaction__Group_16__0 )
            {
             before(grammarAccess.getInteractionAccess().getGroup_16()); 
            // InternalGame.g:5578:3: ( rule__Interaction__Group_16__0 )
            // InternalGame.g:5578:4: rule__Interaction__Group_16__0
            {
            pushFollow(FOLLOW_5);
            rule__Interaction__Group_16__0();

            state._fsp--;


            }

             after(grammarAccess.getInteractionAccess().getGroup_16()); 

            }

            // InternalGame.g:5581:2: ( ( rule__Interaction__Group_16__0 )* )
            // InternalGame.g:5582:3: ( rule__Interaction__Group_16__0 )*
            {
             before(grammarAccess.getInteractionAccess().getGroup_16()); 
            // InternalGame.g:5583:3: ( rule__Interaction__Group_16__0 )*
            loop26:
            do {
                int alt26=2;
                alt26 = dfa26.predict(input);
                switch (alt26) {
            	case 1 :
            	    // InternalGame.g:5583:4: rule__Interaction__Group_16__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Interaction__Group_16__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

             after(grammarAccess.getInteractionAccess().getGroup_16()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__16__Impl"


    // $ANTLR start "rule__Interaction__Group_7__0"
    // InternalGame.g:5593:1: rule__Interaction__Group_7__0 : rule__Interaction__Group_7__0__Impl rule__Interaction__Group_7__1 ;
    public final void rule__Interaction__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5597:1: ( rule__Interaction__Group_7__0__Impl rule__Interaction__Group_7__1 )
            // InternalGame.g:5598:2: rule__Interaction__Group_7__0__Impl rule__Interaction__Group_7__1
            {
            pushFollow(FOLLOW_12);
            rule__Interaction__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_7__0"


    // $ANTLR start "rule__Interaction__Group_7__0__Impl"
    // InternalGame.g:5605:1: rule__Interaction__Group_7__0__Impl : ( '-' ) ;
    public final void rule__Interaction__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5609:1: ( ( '-' ) )
            // InternalGame.g:5610:1: ( '-' )
            {
            // InternalGame.g:5610:1: ( '-' )
            // InternalGame.g:5611:2: '-'
            {
             before(grammarAccess.getInteractionAccess().getHyphenMinusKeyword_7_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getHyphenMinusKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_7__0__Impl"


    // $ANTLR start "rule__Interaction__Group_7__1"
    // InternalGame.g:5620:1: rule__Interaction__Group_7__1 : rule__Interaction__Group_7__1__Impl ;
    public final void rule__Interaction__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5624:1: ( rule__Interaction__Group_7__1__Impl )
            // InternalGame.g:5625:2: rule__Interaction__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Interaction__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_7__1"


    // $ANTLR start "rule__Interaction__Group_7__1__Impl"
    // InternalGame.g:5631:1: rule__Interaction__Group_7__1__Impl : ( ( rule__Interaction__ConnaissancesAssignment_7_1 ) ) ;
    public final void rule__Interaction__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5635:1: ( ( ( rule__Interaction__ConnaissancesAssignment_7_1 ) ) )
            // InternalGame.g:5636:1: ( ( rule__Interaction__ConnaissancesAssignment_7_1 ) )
            {
            // InternalGame.g:5636:1: ( ( rule__Interaction__ConnaissancesAssignment_7_1 ) )
            // InternalGame.g:5637:2: ( rule__Interaction__ConnaissancesAssignment_7_1 )
            {
             before(grammarAccess.getInteractionAccess().getConnaissancesAssignment_7_1()); 
            // InternalGame.g:5638:2: ( rule__Interaction__ConnaissancesAssignment_7_1 )
            // InternalGame.g:5638:3: rule__Interaction__ConnaissancesAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__Interaction__ConnaissancesAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getInteractionAccess().getConnaissancesAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_7__1__Impl"


    // $ANTLR start "rule__Interaction__Group_10__0"
    // InternalGame.g:5647:1: rule__Interaction__Group_10__0 : rule__Interaction__Group_10__0__Impl rule__Interaction__Group_10__1 ;
    public final void rule__Interaction__Group_10__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5651:1: ( rule__Interaction__Group_10__0__Impl rule__Interaction__Group_10__1 )
            // InternalGame.g:5652:2: rule__Interaction__Group_10__0__Impl rule__Interaction__Group_10__1
            {
            pushFollow(FOLLOW_12);
            rule__Interaction__Group_10__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group_10__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_10__0"


    // $ANTLR start "rule__Interaction__Group_10__0__Impl"
    // InternalGame.g:5659:1: rule__Interaction__Group_10__0__Impl : ( '-' ) ;
    public final void rule__Interaction__Group_10__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5663:1: ( ( '-' ) )
            // InternalGame.g:5664:1: ( '-' )
            {
            // InternalGame.g:5664:1: ( '-' )
            // InternalGame.g:5665:2: '-'
            {
             before(grammarAccess.getInteractionAccess().getHyphenMinusKeyword_10_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getHyphenMinusKeyword_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_10__0__Impl"


    // $ANTLR start "rule__Interaction__Group_10__1"
    // InternalGame.g:5674:1: rule__Interaction__Group_10__1 : rule__Interaction__Group_10__1__Impl ;
    public final void rule__Interaction__Group_10__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5678:1: ( rule__Interaction__Group_10__1__Impl )
            // InternalGame.g:5679:2: rule__Interaction__Group_10__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Interaction__Group_10__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_10__1"


    // $ANTLR start "rule__Interaction__Group_10__1__Impl"
    // InternalGame.g:5685:1: rule__Interaction__Group_10__1__Impl : ( ( rule__Interaction__ObjetsRecusAssignment_10_1 ) ) ;
    public final void rule__Interaction__Group_10__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5689:1: ( ( ( rule__Interaction__ObjetsRecusAssignment_10_1 ) ) )
            // InternalGame.g:5690:1: ( ( rule__Interaction__ObjetsRecusAssignment_10_1 ) )
            {
            // InternalGame.g:5690:1: ( ( rule__Interaction__ObjetsRecusAssignment_10_1 ) )
            // InternalGame.g:5691:2: ( rule__Interaction__ObjetsRecusAssignment_10_1 )
            {
             before(grammarAccess.getInteractionAccess().getObjetsRecusAssignment_10_1()); 
            // InternalGame.g:5692:2: ( rule__Interaction__ObjetsRecusAssignment_10_1 )
            // InternalGame.g:5692:3: rule__Interaction__ObjetsRecusAssignment_10_1
            {
            pushFollow(FOLLOW_2);
            rule__Interaction__ObjetsRecusAssignment_10_1();

            state._fsp--;


            }

             after(grammarAccess.getInteractionAccess().getObjetsRecusAssignment_10_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_10__1__Impl"


    // $ANTLR start "rule__Interaction__Group_13__0"
    // InternalGame.g:5701:1: rule__Interaction__Group_13__0 : rule__Interaction__Group_13__0__Impl rule__Interaction__Group_13__1 ;
    public final void rule__Interaction__Group_13__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5705:1: ( rule__Interaction__Group_13__0__Impl rule__Interaction__Group_13__1 )
            // InternalGame.g:5706:2: rule__Interaction__Group_13__0__Impl rule__Interaction__Group_13__1
            {
            pushFollow(FOLLOW_12);
            rule__Interaction__Group_13__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group_13__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_13__0"


    // $ANTLR start "rule__Interaction__Group_13__0__Impl"
    // InternalGame.g:5713:1: rule__Interaction__Group_13__0__Impl : ( '-' ) ;
    public final void rule__Interaction__Group_13__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5717:1: ( ( '-' ) )
            // InternalGame.g:5718:1: ( '-' )
            {
            // InternalGame.g:5718:1: ( '-' )
            // InternalGame.g:5719:2: '-'
            {
             before(grammarAccess.getInteractionAccess().getHyphenMinusKeyword_13_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getHyphenMinusKeyword_13_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_13__0__Impl"


    // $ANTLR start "rule__Interaction__Group_13__1"
    // InternalGame.g:5728:1: rule__Interaction__Group_13__1 : rule__Interaction__Group_13__1__Impl ;
    public final void rule__Interaction__Group_13__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5732:1: ( rule__Interaction__Group_13__1__Impl )
            // InternalGame.g:5733:2: rule__Interaction__Group_13__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Interaction__Group_13__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_13__1"


    // $ANTLR start "rule__Interaction__Group_13__1__Impl"
    // InternalGame.g:5739:1: rule__Interaction__Group_13__1__Impl : ( ( rule__Interaction__ObjetsConsoAssignment_13_1 ) ) ;
    public final void rule__Interaction__Group_13__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5743:1: ( ( ( rule__Interaction__ObjetsConsoAssignment_13_1 ) ) )
            // InternalGame.g:5744:1: ( ( rule__Interaction__ObjetsConsoAssignment_13_1 ) )
            {
            // InternalGame.g:5744:1: ( ( rule__Interaction__ObjetsConsoAssignment_13_1 ) )
            // InternalGame.g:5745:2: ( rule__Interaction__ObjetsConsoAssignment_13_1 )
            {
             before(grammarAccess.getInteractionAccess().getObjetsConsoAssignment_13_1()); 
            // InternalGame.g:5746:2: ( rule__Interaction__ObjetsConsoAssignment_13_1 )
            // InternalGame.g:5746:3: rule__Interaction__ObjetsConsoAssignment_13_1
            {
            pushFollow(FOLLOW_2);
            rule__Interaction__ObjetsConsoAssignment_13_1();

            state._fsp--;


            }

             after(grammarAccess.getInteractionAccess().getObjetsConsoAssignment_13_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_13__1__Impl"


    // $ANTLR start "rule__Interaction__Group_16__0"
    // InternalGame.g:5755:1: rule__Interaction__Group_16__0 : rule__Interaction__Group_16__0__Impl rule__Interaction__Group_16__1 ;
    public final void rule__Interaction__Group_16__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5759:1: ( rule__Interaction__Group_16__0__Impl rule__Interaction__Group_16__1 )
            // InternalGame.g:5760:2: rule__Interaction__Group_16__0__Impl rule__Interaction__Group_16__1
            {
            pushFollow(FOLLOW_12);
            rule__Interaction__Group_16__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interaction__Group_16__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_16__0"


    // $ANTLR start "rule__Interaction__Group_16__0__Impl"
    // InternalGame.g:5767:1: rule__Interaction__Group_16__0__Impl : ( '-' ) ;
    public final void rule__Interaction__Group_16__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5771:1: ( ( '-' ) )
            // InternalGame.g:5772:1: ( '-' )
            {
            // InternalGame.g:5772:1: ( '-' )
            // InternalGame.g:5773:2: '-'
            {
             before(grammarAccess.getInteractionAccess().getHyphenMinusKeyword_16_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getHyphenMinusKeyword_16_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_16__0__Impl"


    // $ANTLR start "rule__Interaction__Group_16__1"
    // InternalGame.g:5782:1: rule__Interaction__Group_16__1 : rule__Interaction__Group_16__1__Impl ;
    public final void rule__Interaction__Group_16__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5786:1: ( rule__Interaction__Group_16__1__Impl )
            // InternalGame.g:5787:2: rule__Interaction__Group_16__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Interaction__Group_16__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_16__1"


    // $ANTLR start "rule__Interaction__Group_16__1__Impl"
    // InternalGame.g:5793:1: rule__Interaction__Group_16__1__Impl : ( ( rule__Interaction__ActionsAssignment_16_1 ) ) ;
    public final void rule__Interaction__Group_16__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5797:1: ( ( ( rule__Interaction__ActionsAssignment_16_1 ) ) )
            // InternalGame.g:5798:1: ( ( rule__Interaction__ActionsAssignment_16_1 ) )
            {
            // InternalGame.g:5798:1: ( ( rule__Interaction__ActionsAssignment_16_1 ) )
            // InternalGame.g:5799:2: ( rule__Interaction__ActionsAssignment_16_1 )
            {
             before(grammarAccess.getInteractionAccess().getActionsAssignment_16_1()); 
            // InternalGame.g:5800:2: ( rule__Interaction__ActionsAssignment_16_1 )
            // InternalGame.g:5800:3: rule__Interaction__ActionsAssignment_16_1
            {
            pushFollow(FOLLOW_2);
            rule__Interaction__ActionsAssignment_16_1();

            state._fsp--;


            }

             after(grammarAccess.getInteractionAccess().getActionsAssignment_16_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group_16__1__Impl"


    // $ANTLR start "rule__Action__Group__0"
    // InternalGame.g:5809:1: rule__Action__Group__0 : rule__Action__Group__0__Impl rule__Action__Group__1 ;
    public final void rule__Action__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5813:1: ( rule__Action__Group__0__Impl rule__Action__Group__1 )
            // InternalGame.g:5814:2: rule__Action__Group__0__Impl rule__Action__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Action__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__0"


    // $ANTLR start "rule__Action__Group__0__Impl"
    // InternalGame.g:5821:1: rule__Action__Group__0__Impl : ( ( rule__Action__NameAssignment_0 ) ) ;
    public final void rule__Action__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5825:1: ( ( ( rule__Action__NameAssignment_0 ) ) )
            // InternalGame.g:5826:1: ( ( rule__Action__NameAssignment_0 ) )
            {
            // InternalGame.g:5826:1: ( ( rule__Action__NameAssignment_0 ) )
            // InternalGame.g:5827:2: ( rule__Action__NameAssignment_0 )
            {
             before(grammarAccess.getActionAccess().getNameAssignment_0()); 
            // InternalGame.g:5828:2: ( rule__Action__NameAssignment_0 )
            // InternalGame.g:5828:3: rule__Action__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Action__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__0__Impl"


    // $ANTLR start "rule__Action__Group__1"
    // InternalGame.g:5836:1: rule__Action__Group__1 : rule__Action__Group__1__Impl rule__Action__Group__2 ;
    public final void rule__Action__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5840:1: ( rule__Action__Group__1__Impl rule__Action__Group__2 )
            // InternalGame.g:5841:2: rule__Action__Group__1__Impl rule__Action__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Action__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__1"


    // $ANTLR start "rule__Action__Group__1__Impl"
    // InternalGame.g:5848:1: rule__Action__Group__1__Impl : ( ':' ) ;
    public final void rule__Action__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5852:1: ( ( ':' ) )
            // InternalGame.g:5853:1: ( ':' )
            {
            // InternalGame.g:5853:1: ( ':' )
            // InternalGame.g:5854:2: ':'
            {
             before(grammarAccess.getActionAccess().getColonKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__1__Impl"


    // $ANTLR start "rule__Action__Group__2"
    // InternalGame.g:5863:1: rule__Action__Group__2 : rule__Action__Group__2__Impl rule__Action__Group__3 ;
    public final void rule__Action__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5867:1: ( rule__Action__Group__2__Impl rule__Action__Group__3 )
            // InternalGame.g:5868:2: rule__Action__Group__2__Impl rule__Action__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__Action__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__2"


    // $ANTLR start "rule__Action__Group__2__Impl"
    // InternalGame.g:5875:1: rule__Action__Group__2__Impl : ( 'visible' ) ;
    public final void rule__Action__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5879:1: ( ( 'visible' ) )
            // InternalGame.g:5880:1: ( 'visible' )
            {
            // InternalGame.g:5880:1: ( 'visible' )
            // InternalGame.g:5881:2: 'visible'
            {
             before(grammarAccess.getActionAccess().getVisibleKeyword_2()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getVisibleKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__2__Impl"


    // $ANTLR start "rule__Action__Group__3"
    // InternalGame.g:5890:1: rule__Action__Group__3 : rule__Action__Group__3__Impl rule__Action__Group__4 ;
    public final void rule__Action__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5894:1: ( rule__Action__Group__3__Impl rule__Action__Group__4 )
            // InternalGame.g:5895:2: rule__Action__Group__3__Impl rule__Action__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__Action__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__3"


    // $ANTLR start "rule__Action__Group__3__Impl"
    // InternalGame.g:5902:1: rule__Action__Group__3__Impl : ( ':' ) ;
    public final void rule__Action__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5906:1: ( ( ':' ) )
            // InternalGame.g:5907:1: ( ':' )
            {
            // InternalGame.g:5907:1: ( ':' )
            // InternalGame.g:5908:2: ':'
            {
             before(grammarAccess.getActionAccess().getColonKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__3__Impl"


    // $ANTLR start "rule__Action__Group__4"
    // InternalGame.g:5917:1: rule__Action__Group__4 : rule__Action__Group__4__Impl rule__Action__Group__5 ;
    public final void rule__Action__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5921:1: ( rule__Action__Group__4__Impl rule__Action__Group__5 )
            // InternalGame.g:5922:2: rule__Action__Group__4__Impl rule__Action__Group__5
            {
            pushFollow(FOLLOW_39);
            rule__Action__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__4"


    // $ANTLR start "rule__Action__Group__4__Impl"
    // InternalGame.g:5929:1: rule__Action__Group__4__Impl : ( ( rule__Action__VisibleAssignment_4 ) ) ;
    public final void rule__Action__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5933:1: ( ( ( rule__Action__VisibleAssignment_4 ) ) )
            // InternalGame.g:5934:1: ( ( rule__Action__VisibleAssignment_4 ) )
            {
            // InternalGame.g:5934:1: ( ( rule__Action__VisibleAssignment_4 ) )
            // InternalGame.g:5935:2: ( rule__Action__VisibleAssignment_4 )
            {
             before(grammarAccess.getActionAccess().getVisibleAssignment_4()); 
            // InternalGame.g:5936:2: ( rule__Action__VisibleAssignment_4 )
            // InternalGame.g:5936:3: rule__Action__VisibleAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Action__VisibleAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getVisibleAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__4__Impl"


    // $ANTLR start "rule__Action__Group__5"
    // InternalGame.g:5944:1: rule__Action__Group__5 : rule__Action__Group__5__Impl rule__Action__Group__6 ;
    public final void rule__Action__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5948:1: ( rule__Action__Group__5__Impl rule__Action__Group__6 )
            // InternalGame.g:5949:2: rule__Action__Group__5__Impl rule__Action__Group__6
            {
            pushFollow(FOLLOW_3);
            rule__Action__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__5"


    // $ANTLR start "rule__Action__Group__5__Impl"
    // InternalGame.g:5956:1: rule__Action__Group__5__Impl : ( 'fin_interaction' ) ;
    public final void rule__Action__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5960:1: ( ( 'fin_interaction' ) )
            // InternalGame.g:5961:1: ( 'fin_interaction' )
            {
            // InternalGame.g:5961:1: ( 'fin_interaction' )
            // InternalGame.g:5962:2: 'fin_interaction'
            {
             before(grammarAccess.getActionAccess().getFin_interactionKeyword_5()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getFin_interactionKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__5__Impl"


    // $ANTLR start "rule__Action__Group__6"
    // InternalGame.g:5971:1: rule__Action__Group__6 : rule__Action__Group__6__Impl rule__Action__Group__7 ;
    public final void rule__Action__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5975:1: ( rule__Action__Group__6__Impl rule__Action__Group__7 )
            // InternalGame.g:5976:2: rule__Action__Group__6__Impl rule__Action__Group__7
            {
            pushFollow(FOLLOW_15);
            rule__Action__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__6"


    // $ANTLR start "rule__Action__Group__6__Impl"
    // InternalGame.g:5983:1: rule__Action__Group__6__Impl : ( ':' ) ;
    public final void rule__Action__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:5987:1: ( ( ':' ) )
            // InternalGame.g:5988:1: ( ':' )
            {
            // InternalGame.g:5988:1: ( ':' )
            // InternalGame.g:5989:2: ':'
            {
             before(grammarAccess.getActionAccess().getColonKeyword_6()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getColonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__6__Impl"


    // $ANTLR start "rule__Action__Group__7"
    // InternalGame.g:5998:1: rule__Action__Group__7 : rule__Action__Group__7__Impl rule__Action__Group__8 ;
    public final void rule__Action__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6002:1: ( rule__Action__Group__7__Impl rule__Action__Group__8 )
            // InternalGame.g:6003:2: rule__Action__Group__7__Impl rule__Action__Group__8
            {
            pushFollow(FOLLOW_21);
            rule__Action__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__7"


    // $ANTLR start "rule__Action__Group__7__Impl"
    // InternalGame.g:6010:1: rule__Action__Group__7__Impl : ( ( rule__Action__FinInteractionAssignment_7 ) ) ;
    public final void rule__Action__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6014:1: ( ( ( rule__Action__FinInteractionAssignment_7 ) ) )
            // InternalGame.g:6015:1: ( ( rule__Action__FinInteractionAssignment_7 ) )
            {
            // InternalGame.g:6015:1: ( ( rule__Action__FinInteractionAssignment_7 ) )
            // InternalGame.g:6016:2: ( rule__Action__FinInteractionAssignment_7 )
            {
             before(grammarAccess.getActionAccess().getFinInteractionAssignment_7()); 
            // InternalGame.g:6017:2: ( rule__Action__FinInteractionAssignment_7 )
            // InternalGame.g:6017:3: rule__Action__FinInteractionAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__Action__FinInteractionAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getFinInteractionAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__7__Impl"


    // $ANTLR start "rule__Action__Group__8"
    // InternalGame.g:6025:1: rule__Action__Group__8 : rule__Action__Group__8__Impl rule__Action__Group__9 ;
    public final void rule__Action__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6029:1: ( rule__Action__Group__8__Impl rule__Action__Group__9 )
            // InternalGame.g:6030:2: rule__Action__Group__8__Impl rule__Action__Group__9
            {
            pushFollow(FOLLOW_3);
            rule__Action__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__8"


    // $ANTLR start "rule__Action__Group__8__Impl"
    // InternalGame.g:6037:1: rule__Action__Group__8__Impl : ( 'connaissances' ) ;
    public final void rule__Action__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6041:1: ( ( 'connaissances' ) )
            // InternalGame.g:6042:1: ( 'connaissances' )
            {
            // InternalGame.g:6042:1: ( 'connaissances' )
            // InternalGame.g:6043:2: 'connaissances'
            {
             before(grammarAccess.getActionAccess().getConnaissancesKeyword_8()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getConnaissancesKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__8__Impl"


    // $ANTLR start "rule__Action__Group__9"
    // InternalGame.g:6052:1: rule__Action__Group__9 : rule__Action__Group__9__Impl rule__Action__Group__10 ;
    public final void rule__Action__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6056:1: ( rule__Action__Group__9__Impl rule__Action__Group__10 )
            // InternalGame.g:6057:2: rule__Action__Group__9__Impl rule__Action__Group__10
            {
            pushFollow(FOLLOW_35);
            rule__Action__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__9"


    // $ANTLR start "rule__Action__Group__9__Impl"
    // InternalGame.g:6064:1: rule__Action__Group__9__Impl : ( ':' ) ;
    public final void rule__Action__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6068:1: ( ( ':' ) )
            // InternalGame.g:6069:1: ( ':' )
            {
            // InternalGame.g:6069:1: ( ':' )
            // InternalGame.g:6070:2: ':'
            {
             before(grammarAccess.getActionAccess().getColonKeyword_9()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getColonKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__9__Impl"


    // $ANTLR start "rule__Action__Group__10"
    // InternalGame.g:6079:1: rule__Action__Group__10 : rule__Action__Group__10__Impl rule__Action__Group__11 ;
    public final void rule__Action__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6083:1: ( rule__Action__Group__10__Impl rule__Action__Group__11 )
            // InternalGame.g:6084:2: rule__Action__Group__10__Impl rule__Action__Group__11
            {
            pushFollow(FOLLOW_35);
            rule__Action__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__10"


    // $ANTLR start "rule__Action__Group__10__Impl"
    // InternalGame.g:6091:1: rule__Action__Group__10__Impl : ( ( rule__Action__Group_10__0 )* ) ;
    public final void rule__Action__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6095:1: ( ( ( rule__Action__Group_10__0 )* ) )
            // InternalGame.g:6096:1: ( ( rule__Action__Group_10__0 )* )
            {
            // InternalGame.g:6096:1: ( ( rule__Action__Group_10__0 )* )
            // InternalGame.g:6097:2: ( rule__Action__Group_10__0 )*
            {
             before(grammarAccess.getActionAccess().getGroup_10()); 
            // InternalGame.g:6098:2: ( rule__Action__Group_10__0 )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==20) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalGame.g:6098:3: rule__Action__Group_10__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Action__Group_10__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

             after(grammarAccess.getActionAccess().getGroup_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__10__Impl"


    // $ANTLR start "rule__Action__Group__11"
    // InternalGame.g:6106:1: rule__Action__Group__11 : rule__Action__Group__11__Impl rule__Action__Group__12 ;
    public final void rule__Action__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6110:1: ( rule__Action__Group__11__Impl rule__Action__Group__12 )
            // InternalGame.g:6111:2: rule__Action__Group__11__Impl rule__Action__Group__12
            {
            pushFollow(FOLLOW_3);
            rule__Action__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__11"


    // $ANTLR start "rule__Action__Group__11__Impl"
    // InternalGame.g:6118:1: rule__Action__Group__11__Impl : ( 'objets_recus' ) ;
    public final void rule__Action__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6122:1: ( ( 'objets_recus' ) )
            // InternalGame.g:6123:1: ( 'objets_recus' )
            {
            // InternalGame.g:6123:1: ( 'objets_recus' )
            // InternalGame.g:6124:2: 'objets_recus'
            {
             before(grammarAccess.getActionAccess().getObjets_recusKeyword_11()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getObjets_recusKeyword_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__11__Impl"


    // $ANTLR start "rule__Action__Group__12"
    // InternalGame.g:6133:1: rule__Action__Group__12 : rule__Action__Group__12__Impl rule__Action__Group__13 ;
    public final void rule__Action__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6137:1: ( rule__Action__Group__12__Impl rule__Action__Group__13 )
            // InternalGame.g:6138:2: rule__Action__Group__12__Impl rule__Action__Group__13
            {
            pushFollow(FOLLOW_36);
            rule__Action__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__12"


    // $ANTLR start "rule__Action__Group__12__Impl"
    // InternalGame.g:6145:1: rule__Action__Group__12__Impl : ( ':' ) ;
    public final void rule__Action__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6149:1: ( ( ':' ) )
            // InternalGame.g:6150:1: ( ':' )
            {
            // InternalGame.g:6150:1: ( ':' )
            // InternalGame.g:6151:2: ':'
            {
             before(grammarAccess.getActionAccess().getColonKeyword_12()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getColonKeyword_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__12__Impl"


    // $ANTLR start "rule__Action__Group__13"
    // InternalGame.g:6160:1: rule__Action__Group__13 : rule__Action__Group__13__Impl rule__Action__Group__14 ;
    public final void rule__Action__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6164:1: ( rule__Action__Group__13__Impl rule__Action__Group__14 )
            // InternalGame.g:6165:2: rule__Action__Group__13__Impl rule__Action__Group__14
            {
            pushFollow(FOLLOW_36);
            rule__Action__Group__13__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__13"


    // $ANTLR start "rule__Action__Group__13__Impl"
    // InternalGame.g:6172:1: rule__Action__Group__13__Impl : ( ( rule__Action__Group_13__0 )* ) ;
    public final void rule__Action__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6176:1: ( ( ( rule__Action__Group_13__0 )* ) )
            // InternalGame.g:6177:1: ( ( rule__Action__Group_13__0 )* )
            {
            // InternalGame.g:6177:1: ( ( rule__Action__Group_13__0 )* )
            // InternalGame.g:6178:2: ( rule__Action__Group_13__0 )*
            {
             before(grammarAccess.getActionAccess().getGroup_13()); 
            // InternalGame.g:6179:2: ( rule__Action__Group_13__0 )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==20) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalGame.g:6179:3: rule__Action__Group_13__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Action__Group_13__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

             after(grammarAccess.getActionAccess().getGroup_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__13__Impl"


    // $ANTLR start "rule__Action__Group__14"
    // InternalGame.g:6187:1: rule__Action__Group__14 : rule__Action__Group__14__Impl rule__Action__Group__15 ;
    public final void rule__Action__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6191:1: ( rule__Action__Group__14__Impl rule__Action__Group__15 )
            // InternalGame.g:6192:2: rule__Action__Group__14__Impl rule__Action__Group__15
            {
            pushFollow(FOLLOW_3);
            rule__Action__Group__14__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__14"


    // $ANTLR start "rule__Action__Group__14__Impl"
    // InternalGame.g:6199:1: rule__Action__Group__14__Impl : ( 'objets_conso' ) ;
    public final void rule__Action__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6203:1: ( ( 'objets_conso' ) )
            // InternalGame.g:6204:1: ( 'objets_conso' )
            {
            // InternalGame.g:6204:1: ( 'objets_conso' )
            // InternalGame.g:6205:2: 'objets_conso'
            {
             before(grammarAccess.getActionAccess().getObjets_consoKeyword_14()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getObjets_consoKeyword_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__14__Impl"


    // $ANTLR start "rule__Action__Group__15"
    // InternalGame.g:6214:1: rule__Action__Group__15 : rule__Action__Group__15__Impl rule__Action__Group__16 ;
    public final void rule__Action__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6218:1: ( rule__Action__Group__15__Impl rule__Action__Group__16 )
            // InternalGame.g:6219:2: rule__Action__Group__15__Impl rule__Action__Group__16
            {
            pushFollow(FOLLOW_28);
            rule__Action__Group__15__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__16();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__15"


    // $ANTLR start "rule__Action__Group__15__Impl"
    // InternalGame.g:6226:1: rule__Action__Group__15__Impl : ( ':' ) ;
    public final void rule__Action__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6230:1: ( ( ':' ) )
            // InternalGame.g:6231:1: ( ':' )
            {
            // InternalGame.g:6231:1: ( ':' )
            // InternalGame.g:6232:2: ':'
            {
             before(grammarAccess.getActionAccess().getColonKeyword_15()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getColonKeyword_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__15__Impl"


    // $ANTLR start "rule__Action__Group__16"
    // InternalGame.g:6241:1: rule__Action__Group__16 : rule__Action__Group__16__Impl rule__Action__Group__17 ;
    public final void rule__Action__Group__16() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6245:1: ( rule__Action__Group__16__Impl rule__Action__Group__17 )
            // InternalGame.g:6246:2: rule__Action__Group__16__Impl rule__Action__Group__17
            {
            pushFollow(FOLLOW_28);
            rule__Action__Group__16__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__17();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__16"


    // $ANTLR start "rule__Action__Group__16__Impl"
    // InternalGame.g:6253:1: rule__Action__Group__16__Impl : ( ( rule__Action__Group_16__0 )* ) ;
    public final void rule__Action__Group__16__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6257:1: ( ( ( rule__Action__Group_16__0 )* ) )
            // InternalGame.g:6258:1: ( ( rule__Action__Group_16__0 )* )
            {
            // InternalGame.g:6258:1: ( ( rule__Action__Group_16__0 )* )
            // InternalGame.g:6259:2: ( rule__Action__Group_16__0 )*
            {
             before(grammarAccess.getActionAccess().getGroup_16()); 
            // InternalGame.g:6260:2: ( rule__Action__Group_16__0 )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==20) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalGame.g:6260:3: rule__Action__Group_16__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Action__Group_16__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);

             after(grammarAccess.getActionAccess().getGroup_16()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__16__Impl"


    // $ANTLR start "rule__Action__Group__17"
    // InternalGame.g:6268:1: rule__Action__Group__17 : rule__Action__Group__17__Impl rule__Action__Group__18 ;
    public final void rule__Action__Group__17() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6272:1: ( rule__Action__Group__17__Impl rule__Action__Group__18 )
            // InternalGame.g:6273:2: rule__Action__Group__17__Impl rule__Action__Group__18
            {
            pushFollow(FOLLOW_3);
            rule__Action__Group__17__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__18();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__17"


    // $ANTLR start "rule__Action__Group__17__Impl"
    // InternalGame.g:6280:1: rule__Action__Group__17__Impl : ( 'descriptions' ) ;
    public final void rule__Action__Group__17__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6284:1: ( ( 'descriptions' ) )
            // InternalGame.g:6285:1: ( 'descriptions' )
            {
            // InternalGame.g:6285:1: ( 'descriptions' )
            // InternalGame.g:6286:2: 'descriptions'
            {
             before(grammarAccess.getActionAccess().getDescriptionsKeyword_17()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getDescriptionsKeyword_17()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__17__Impl"


    // $ANTLR start "rule__Action__Group__18"
    // InternalGame.g:6295:1: rule__Action__Group__18 : rule__Action__Group__18__Impl rule__Action__Group__19 ;
    public final void rule__Action__Group__18() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6299:1: ( rule__Action__Group__18__Impl rule__Action__Group__19 )
            // InternalGame.g:6300:2: rule__Action__Group__18__Impl rule__Action__Group__19
            {
            pushFollow(FOLLOW_17);
            rule__Action__Group__18__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__19();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__18"


    // $ANTLR start "rule__Action__Group__18__Impl"
    // InternalGame.g:6307:1: rule__Action__Group__18__Impl : ( ':' ) ;
    public final void rule__Action__Group__18__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6311:1: ( ( ':' ) )
            // InternalGame.g:6312:1: ( ':' )
            {
            // InternalGame.g:6312:1: ( ':' )
            // InternalGame.g:6313:2: ':'
            {
             before(grammarAccess.getActionAccess().getColonKeyword_18()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getColonKeyword_18()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__18__Impl"


    // $ANTLR start "rule__Action__Group__19"
    // InternalGame.g:6322:1: rule__Action__Group__19 : rule__Action__Group__19__Impl ;
    public final void rule__Action__Group__19() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6326:1: ( rule__Action__Group__19__Impl )
            // InternalGame.g:6327:2: rule__Action__Group__19__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Action__Group__19__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__19"


    // $ANTLR start "rule__Action__Group__19__Impl"
    // InternalGame.g:6333:1: rule__Action__Group__19__Impl : ( ( ( rule__Action__Group_19__0 ) ) ( ( rule__Action__Group_19__0 )* ) ) ;
    public final void rule__Action__Group__19__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6337:1: ( ( ( ( rule__Action__Group_19__0 ) ) ( ( rule__Action__Group_19__0 )* ) ) )
            // InternalGame.g:6338:1: ( ( ( rule__Action__Group_19__0 ) ) ( ( rule__Action__Group_19__0 )* ) )
            {
            // InternalGame.g:6338:1: ( ( ( rule__Action__Group_19__0 ) ) ( ( rule__Action__Group_19__0 )* ) )
            // InternalGame.g:6339:2: ( ( rule__Action__Group_19__0 ) ) ( ( rule__Action__Group_19__0 )* )
            {
            // InternalGame.g:6339:2: ( ( rule__Action__Group_19__0 ) )
            // InternalGame.g:6340:3: ( rule__Action__Group_19__0 )
            {
             before(grammarAccess.getActionAccess().getGroup_19()); 
            // InternalGame.g:6341:3: ( rule__Action__Group_19__0 )
            // InternalGame.g:6341:4: rule__Action__Group_19__0
            {
            pushFollow(FOLLOW_5);
            rule__Action__Group_19__0();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getGroup_19()); 

            }

            // InternalGame.g:6344:2: ( ( rule__Action__Group_19__0 )* )
            // InternalGame.g:6345:3: ( rule__Action__Group_19__0 )*
            {
             before(grammarAccess.getActionAccess().getGroup_19()); 
            // InternalGame.g:6346:3: ( rule__Action__Group_19__0 )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==20) ) {
                    int LA30_2 = input.LA(2);

                    if ( (LA30_2==RULE_ID) ) {
                        int LA30_3 = input.LA(3);

                        if ( (LA30_3==14) ) {
                            int LA30_4 = input.LA(4);

                            if ( (LA30_4==44) ) {
                                alt30=1;
                            }


                        }


                    }


                }


                switch (alt30) {
            	case 1 :
            	    // InternalGame.g:6346:4: rule__Action__Group_19__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Action__Group_19__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

             after(grammarAccess.getActionAccess().getGroup_19()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__19__Impl"


    // $ANTLR start "rule__Action__Group_10__0"
    // InternalGame.g:6356:1: rule__Action__Group_10__0 : rule__Action__Group_10__0__Impl rule__Action__Group_10__1 ;
    public final void rule__Action__Group_10__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6360:1: ( rule__Action__Group_10__0__Impl rule__Action__Group_10__1 )
            // InternalGame.g:6361:2: rule__Action__Group_10__0__Impl rule__Action__Group_10__1
            {
            pushFollow(FOLLOW_12);
            rule__Action__Group_10__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group_10__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_10__0"


    // $ANTLR start "rule__Action__Group_10__0__Impl"
    // InternalGame.g:6368:1: rule__Action__Group_10__0__Impl : ( '-' ) ;
    public final void rule__Action__Group_10__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6372:1: ( ( '-' ) )
            // InternalGame.g:6373:1: ( '-' )
            {
            // InternalGame.g:6373:1: ( '-' )
            // InternalGame.g:6374:2: '-'
            {
             before(grammarAccess.getActionAccess().getHyphenMinusKeyword_10_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getHyphenMinusKeyword_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_10__0__Impl"


    // $ANTLR start "rule__Action__Group_10__1"
    // InternalGame.g:6383:1: rule__Action__Group_10__1 : rule__Action__Group_10__1__Impl ;
    public final void rule__Action__Group_10__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6387:1: ( rule__Action__Group_10__1__Impl )
            // InternalGame.g:6388:2: rule__Action__Group_10__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Action__Group_10__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_10__1"


    // $ANTLR start "rule__Action__Group_10__1__Impl"
    // InternalGame.g:6394:1: rule__Action__Group_10__1__Impl : ( ( rule__Action__ConnaissancesAssignment_10_1 ) ) ;
    public final void rule__Action__Group_10__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6398:1: ( ( ( rule__Action__ConnaissancesAssignment_10_1 ) ) )
            // InternalGame.g:6399:1: ( ( rule__Action__ConnaissancesAssignment_10_1 ) )
            {
            // InternalGame.g:6399:1: ( ( rule__Action__ConnaissancesAssignment_10_1 ) )
            // InternalGame.g:6400:2: ( rule__Action__ConnaissancesAssignment_10_1 )
            {
             before(grammarAccess.getActionAccess().getConnaissancesAssignment_10_1()); 
            // InternalGame.g:6401:2: ( rule__Action__ConnaissancesAssignment_10_1 )
            // InternalGame.g:6401:3: rule__Action__ConnaissancesAssignment_10_1
            {
            pushFollow(FOLLOW_2);
            rule__Action__ConnaissancesAssignment_10_1();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getConnaissancesAssignment_10_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_10__1__Impl"


    // $ANTLR start "rule__Action__Group_13__0"
    // InternalGame.g:6410:1: rule__Action__Group_13__0 : rule__Action__Group_13__0__Impl rule__Action__Group_13__1 ;
    public final void rule__Action__Group_13__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6414:1: ( rule__Action__Group_13__0__Impl rule__Action__Group_13__1 )
            // InternalGame.g:6415:2: rule__Action__Group_13__0__Impl rule__Action__Group_13__1
            {
            pushFollow(FOLLOW_12);
            rule__Action__Group_13__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group_13__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_13__0"


    // $ANTLR start "rule__Action__Group_13__0__Impl"
    // InternalGame.g:6422:1: rule__Action__Group_13__0__Impl : ( '-' ) ;
    public final void rule__Action__Group_13__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6426:1: ( ( '-' ) )
            // InternalGame.g:6427:1: ( '-' )
            {
            // InternalGame.g:6427:1: ( '-' )
            // InternalGame.g:6428:2: '-'
            {
             before(grammarAccess.getActionAccess().getHyphenMinusKeyword_13_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getHyphenMinusKeyword_13_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_13__0__Impl"


    // $ANTLR start "rule__Action__Group_13__1"
    // InternalGame.g:6437:1: rule__Action__Group_13__1 : rule__Action__Group_13__1__Impl ;
    public final void rule__Action__Group_13__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6441:1: ( rule__Action__Group_13__1__Impl )
            // InternalGame.g:6442:2: rule__Action__Group_13__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Action__Group_13__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_13__1"


    // $ANTLR start "rule__Action__Group_13__1__Impl"
    // InternalGame.g:6448:1: rule__Action__Group_13__1__Impl : ( ( rule__Action__ObjetsRecusAssignment_13_1 ) ) ;
    public final void rule__Action__Group_13__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6452:1: ( ( ( rule__Action__ObjetsRecusAssignment_13_1 ) ) )
            // InternalGame.g:6453:1: ( ( rule__Action__ObjetsRecusAssignment_13_1 ) )
            {
            // InternalGame.g:6453:1: ( ( rule__Action__ObjetsRecusAssignment_13_1 ) )
            // InternalGame.g:6454:2: ( rule__Action__ObjetsRecusAssignment_13_1 )
            {
             before(grammarAccess.getActionAccess().getObjetsRecusAssignment_13_1()); 
            // InternalGame.g:6455:2: ( rule__Action__ObjetsRecusAssignment_13_1 )
            // InternalGame.g:6455:3: rule__Action__ObjetsRecusAssignment_13_1
            {
            pushFollow(FOLLOW_2);
            rule__Action__ObjetsRecusAssignment_13_1();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getObjetsRecusAssignment_13_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_13__1__Impl"


    // $ANTLR start "rule__Action__Group_16__0"
    // InternalGame.g:6464:1: rule__Action__Group_16__0 : rule__Action__Group_16__0__Impl rule__Action__Group_16__1 ;
    public final void rule__Action__Group_16__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6468:1: ( rule__Action__Group_16__0__Impl rule__Action__Group_16__1 )
            // InternalGame.g:6469:2: rule__Action__Group_16__0__Impl rule__Action__Group_16__1
            {
            pushFollow(FOLLOW_12);
            rule__Action__Group_16__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group_16__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_16__0"


    // $ANTLR start "rule__Action__Group_16__0__Impl"
    // InternalGame.g:6476:1: rule__Action__Group_16__0__Impl : ( '-' ) ;
    public final void rule__Action__Group_16__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6480:1: ( ( '-' ) )
            // InternalGame.g:6481:1: ( '-' )
            {
            // InternalGame.g:6481:1: ( '-' )
            // InternalGame.g:6482:2: '-'
            {
             before(grammarAccess.getActionAccess().getHyphenMinusKeyword_16_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getHyphenMinusKeyword_16_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_16__0__Impl"


    // $ANTLR start "rule__Action__Group_16__1"
    // InternalGame.g:6491:1: rule__Action__Group_16__1 : rule__Action__Group_16__1__Impl ;
    public final void rule__Action__Group_16__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6495:1: ( rule__Action__Group_16__1__Impl )
            // InternalGame.g:6496:2: rule__Action__Group_16__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Action__Group_16__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_16__1"


    // $ANTLR start "rule__Action__Group_16__1__Impl"
    // InternalGame.g:6502:1: rule__Action__Group_16__1__Impl : ( ( rule__Action__ObjetsConsoAssignment_16_1 ) ) ;
    public final void rule__Action__Group_16__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6506:1: ( ( ( rule__Action__ObjetsConsoAssignment_16_1 ) ) )
            // InternalGame.g:6507:1: ( ( rule__Action__ObjetsConsoAssignment_16_1 ) )
            {
            // InternalGame.g:6507:1: ( ( rule__Action__ObjetsConsoAssignment_16_1 ) )
            // InternalGame.g:6508:2: ( rule__Action__ObjetsConsoAssignment_16_1 )
            {
             before(grammarAccess.getActionAccess().getObjetsConsoAssignment_16_1()); 
            // InternalGame.g:6509:2: ( rule__Action__ObjetsConsoAssignment_16_1 )
            // InternalGame.g:6509:3: rule__Action__ObjetsConsoAssignment_16_1
            {
            pushFollow(FOLLOW_2);
            rule__Action__ObjetsConsoAssignment_16_1();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getObjetsConsoAssignment_16_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_16__1__Impl"


    // $ANTLR start "rule__Action__Group_19__0"
    // InternalGame.g:6518:1: rule__Action__Group_19__0 : rule__Action__Group_19__0__Impl rule__Action__Group_19__1 ;
    public final void rule__Action__Group_19__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6522:1: ( rule__Action__Group_19__0__Impl rule__Action__Group_19__1 )
            // InternalGame.g:6523:2: rule__Action__Group_19__0__Impl rule__Action__Group_19__1
            {
            pushFollow(FOLLOW_12);
            rule__Action__Group_19__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group_19__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_19__0"


    // $ANTLR start "rule__Action__Group_19__0__Impl"
    // InternalGame.g:6530:1: rule__Action__Group_19__0__Impl : ( '-' ) ;
    public final void rule__Action__Group_19__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6534:1: ( ( '-' ) )
            // InternalGame.g:6535:1: ( '-' )
            {
            // InternalGame.g:6535:1: ( '-' )
            // InternalGame.g:6536:2: '-'
            {
             before(grammarAccess.getActionAccess().getHyphenMinusKeyword_19_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getHyphenMinusKeyword_19_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_19__0__Impl"


    // $ANTLR start "rule__Action__Group_19__1"
    // InternalGame.g:6545:1: rule__Action__Group_19__1 : rule__Action__Group_19__1__Impl ;
    public final void rule__Action__Group_19__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6549:1: ( rule__Action__Group_19__1__Impl )
            // InternalGame.g:6550:2: rule__Action__Group_19__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Action__Group_19__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_19__1"


    // $ANTLR start "rule__Action__Group_19__1__Impl"
    // InternalGame.g:6556:1: rule__Action__Group_19__1__Impl : ( ( rule__Action__DescriptionsAssignment_19_1 ) ) ;
    public final void rule__Action__Group_19__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6560:1: ( ( ( rule__Action__DescriptionsAssignment_19_1 ) ) )
            // InternalGame.g:6561:1: ( ( rule__Action__DescriptionsAssignment_19_1 ) )
            {
            // InternalGame.g:6561:1: ( ( rule__Action__DescriptionsAssignment_19_1 ) )
            // InternalGame.g:6562:2: ( rule__Action__DescriptionsAssignment_19_1 )
            {
             before(grammarAccess.getActionAccess().getDescriptionsAssignment_19_1()); 
            // InternalGame.g:6563:2: ( rule__Action__DescriptionsAssignment_19_1 )
            // InternalGame.g:6563:3: rule__Action__DescriptionsAssignment_19_1
            {
            pushFollow(FOLLOW_2);
            rule__Action__DescriptionsAssignment_19_1();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getDescriptionsAssignment_19_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group_19__1__Impl"


    // $ANTLR start "rule__Description__Group__0"
    // InternalGame.g:6572:1: rule__Description__Group__0 : rule__Description__Group__0__Impl rule__Description__Group__1 ;
    public final void rule__Description__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6576:1: ( rule__Description__Group__0__Impl rule__Description__Group__1 )
            // InternalGame.g:6577:2: rule__Description__Group__0__Impl rule__Description__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Description__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Description__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__0"


    // $ANTLR start "rule__Description__Group__0__Impl"
    // InternalGame.g:6584:1: rule__Description__Group__0__Impl : ( ( rule__Description__NameAssignment_0 ) ) ;
    public final void rule__Description__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6588:1: ( ( ( rule__Description__NameAssignment_0 ) ) )
            // InternalGame.g:6589:1: ( ( rule__Description__NameAssignment_0 ) )
            {
            // InternalGame.g:6589:1: ( ( rule__Description__NameAssignment_0 ) )
            // InternalGame.g:6590:2: ( rule__Description__NameAssignment_0 )
            {
             before(grammarAccess.getDescriptionAccess().getNameAssignment_0()); 
            // InternalGame.g:6591:2: ( rule__Description__NameAssignment_0 )
            // InternalGame.g:6591:3: rule__Description__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Description__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDescriptionAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__0__Impl"


    // $ANTLR start "rule__Description__Group__1"
    // InternalGame.g:6599:1: rule__Description__Group__1 : rule__Description__Group__1__Impl rule__Description__Group__2 ;
    public final void rule__Description__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6603:1: ( rule__Description__Group__1__Impl rule__Description__Group__2 )
            // InternalGame.g:6604:2: rule__Description__Group__1__Impl rule__Description__Group__2
            {
            pushFollow(FOLLOW_40);
            rule__Description__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Description__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__1"


    // $ANTLR start "rule__Description__Group__1__Impl"
    // InternalGame.g:6611:1: rule__Description__Group__1__Impl : ( ':' ) ;
    public final void rule__Description__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6615:1: ( ( ':' ) )
            // InternalGame.g:6616:1: ( ':' )
            {
            // InternalGame.g:6616:1: ( ':' )
            // InternalGame.g:6617:2: ':'
            {
             before(grammarAccess.getDescriptionAccess().getColonKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getDescriptionAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__1__Impl"


    // $ANTLR start "rule__Description__Group__2"
    // InternalGame.g:6626:1: rule__Description__Group__2 : rule__Description__Group__2__Impl rule__Description__Group__3 ;
    public final void rule__Description__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6630:1: ( rule__Description__Group__2__Impl rule__Description__Group__3 )
            // InternalGame.g:6631:2: rule__Description__Group__2__Impl rule__Description__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__Description__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Description__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__2"


    // $ANTLR start "rule__Description__Group__2__Impl"
    // InternalGame.g:6638:1: rule__Description__Group__2__Impl : ( 'texte' ) ;
    public final void rule__Description__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6642:1: ( ( 'texte' ) )
            // InternalGame.g:6643:1: ( 'texte' )
            {
            // InternalGame.g:6643:1: ( 'texte' )
            // InternalGame.g:6644:2: 'texte'
            {
             before(grammarAccess.getDescriptionAccess().getTexteKeyword_2()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getDescriptionAccess().getTexteKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__2__Impl"


    // $ANTLR start "rule__Description__Group__3"
    // InternalGame.g:6653:1: rule__Description__Group__3 : rule__Description__Group__3__Impl rule__Description__Group__4 ;
    public final void rule__Description__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6657:1: ( rule__Description__Group__3__Impl rule__Description__Group__4 )
            // InternalGame.g:6658:2: rule__Description__Group__3__Impl rule__Description__Group__4
            {
            pushFollow(FOLLOW_41);
            rule__Description__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Description__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__3"


    // $ANTLR start "rule__Description__Group__3__Impl"
    // InternalGame.g:6665:1: rule__Description__Group__3__Impl : ( ':' ) ;
    public final void rule__Description__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6669:1: ( ( ':' ) )
            // InternalGame.g:6670:1: ( ':' )
            {
            // InternalGame.g:6670:1: ( ':' )
            // InternalGame.g:6671:2: ':'
            {
             before(grammarAccess.getDescriptionAccess().getColonKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getDescriptionAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__3__Impl"


    // $ANTLR start "rule__Description__Group__4"
    // InternalGame.g:6680:1: rule__Description__Group__4 : rule__Description__Group__4__Impl rule__Description__Group__5 ;
    public final void rule__Description__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6684:1: ( rule__Description__Group__4__Impl rule__Description__Group__5 )
            // InternalGame.g:6685:2: rule__Description__Group__4__Impl rule__Description__Group__5
            {
            pushFollow(FOLLOW_18);
            rule__Description__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Description__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__4"


    // $ANTLR start "rule__Description__Group__4__Impl"
    // InternalGame.g:6692:1: rule__Description__Group__4__Impl : ( ( rule__Description__TexteAssignment_4 ) ) ;
    public final void rule__Description__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6696:1: ( ( ( rule__Description__TexteAssignment_4 ) ) )
            // InternalGame.g:6697:1: ( ( rule__Description__TexteAssignment_4 ) )
            {
            // InternalGame.g:6697:1: ( ( rule__Description__TexteAssignment_4 ) )
            // InternalGame.g:6698:2: ( rule__Description__TexteAssignment_4 )
            {
             before(grammarAccess.getDescriptionAccess().getTexteAssignment_4()); 
            // InternalGame.g:6699:2: ( rule__Description__TexteAssignment_4 )
            // InternalGame.g:6699:3: rule__Description__TexteAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Description__TexteAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getDescriptionAccess().getTexteAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__4__Impl"


    // $ANTLR start "rule__Description__Group__5"
    // InternalGame.g:6707:1: rule__Description__Group__5 : rule__Description__Group__5__Impl rule__Description__Group__6 ;
    public final void rule__Description__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6711:1: ( rule__Description__Group__5__Impl rule__Description__Group__6 )
            // InternalGame.g:6712:2: rule__Description__Group__5__Impl rule__Description__Group__6
            {
            pushFollow(FOLLOW_3);
            rule__Description__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Description__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__5"


    // $ANTLR start "rule__Description__Group__5__Impl"
    // InternalGame.g:6719:1: rule__Description__Group__5__Impl : ( 'condition' ) ;
    public final void rule__Description__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6723:1: ( ( 'condition' ) )
            // InternalGame.g:6724:1: ( 'condition' )
            {
            // InternalGame.g:6724:1: ( 'condition' )
            // InternalGame.g:6725:2: 'condition'
            {
             before(grammarAccess.getDescriptionAccess().getConditionKeyword_5()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getDescriptionAccess().getConditionKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__5__Impl"


    // $ANTLR start "rule__Description__Group__6"
    // InternalGame.g:6734:1: rule__Description__Group__6 : rule__Description__Group__6__Impl rule__Description__Group__7 ;
    public final void rule__Description__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6738:1: ( rule__Description__Group__6__Impl rule__Description__Group__7 )
            // InternalGame.g:6739:2: rule__Description__Group__6__Impl rule__Description__Group__7
            {
            pushFollow(FOLLOW_15);
            rule__Description__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Description__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__6"


    // $ANTLR start "rule__Description__Group__6__Impl"
    // InternalGame.g:6746:1: rule__Description__Group__6__Impl : ( ':' ) ;
    public final void rule__Description__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6750:1: ( ( ':' ) )
            // InternalGame.g:6751:1: ( ':' )
            {
            // InternalGame.g:6751:1: ( ':' )
            // InternalGame.g:6752:2: ':'
            {
             before(grammarAccess.getDescriptionAccess().getColonKeyword_6()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getDescriptionAccess().getColonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__6__Impl"


    // $ANTLR start "rule__Description__Group__7"
    // InternalGame.g:6761:1: rule__Description__Group__7 : rule__Description__Group__7__Impl ;
    public final void rule__Description__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6765:1: ( rule__Description__Group__7__Impl )
            // InternalGame.g:6766:2: rule__Description__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Description__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__7"


    // $ANTLR start "rule__Description__Group__7__Impl"
    // InternalGame.g:6772:1: rule__Description__Group__7__Impl : ( ( rule__Description__ConditionAssignment_7 ) ) ;
    public final void rule__Description__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6776:1: ( ( ( rule__Description__ConditionAssignment_7 ) ) )
            // InternalGame.g:6777:1: ( ( rule__Description__ConditionAssignment_7 ) )
            {
            // InternalGame.g:6777:1: ( ( rule__Description__ConditionAssignment_7 ) )
            // InternalGame.g:6778:2: ( rule__Description__ConditionAssignment_7 )
            {
             before(grammarAccess.getDescriptionAccess().getConditionAssignment_7()); 
            // InternalGame.g:6779:2: ( rule__Description__ConditionAssignment_7 )
            // InternalGame.g:6779:3: rule__Description__ConditionAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__Description__ConditionAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getDescriptionAccess().getConditionAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__7__Impl"


    // $ANTLR start "rule__Condition__Group__0"
    // InternalGame.g:6788:1: rule__Condition__Group__0 : rule__Condition__Group__0__Impl rule__Condition__Group__1 ;
    public final void rule__Condition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6792:1: ( rule__Condition__Group__0__Impl rule__Condition__Group__1 )
            // InternalGame.g:6793:2: rule__Condition__Group__0__Impl rule__Condition__Group__1
            {
            pushFollow(FOLLOW_42);
            rule__Condition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__0"


    // $ANTLR start "rule__Condition__Group__0__Impl"
    // InternalGame.g:6800:1: rule__Condition__Group__0__Impl : ( ( rule__Condition__ConditionAssignment_0 ) ) ;
    public final void rule__Condition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6804:1: ( ( ( rule__Condition__ConditionAssignment_0 ) ) )
            // InternalGame.g:6805:1: ( ( rule__Condition__ConditionAssignment_0 ) )
            {
            // InternalGame.g:6805:1: ( ( rule__Condition__ConditionAssignment_0 ) )
            // InternalGame.g:6806:2: ( rule__Condition__ConditionAssignment_0 )
            {
             before(grammarAccess.getConditionAccess().getConditionAssignment_0()); 
            // InternalGame.g:6807:2: ( rule__Condition__ConditionAssignment_0 )
            // InternalGame.g:6807:3: rule__Condition__ConditionAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Condition__ConditionAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getConditionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__0__Impl"


    // $ANTLR start "rule__Condition__Group__1"
    // InternalGame.g:6815:1: rule__Condition__Group__1 : rule__Condition__Group__1__Impl ;
    public final void rule__Condition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6819:1: ( rule__Condition__Group__1__Impl )
            // InternalGame.g:6820:2: rule__Condition__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Condition__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__1"


    // $ANTLR start "rule__Condition__Group__1__Impl"
    // InternalGame.g:6826:1: rule__Condition__Group__1__Impl : ( ( rule__Condition__Group_1__0 )* ) ;
    public final void rule__Condition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6830:1: ( ( ( rule__Condition__Group_1__0 )* ) )
            // InternalGame.g:6831:1: ( ( rule__Condition__Group_1__0 )* )
            {
            // InternalGame.g:6831:1: ( ( rule__Condition__Group_1__0 )* )
            // InternalGame.g:6832:2: ( rule__Condition__Group_1__0 )*
            {
             before(grammarAccess.getConditionAccess().getGroup_1()); 
            // InternalGame.g:6833:2: ( rule__Condition__Group_1__0 )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==45) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalGame.g:6833:3: rule__Condition__Group_1__0
            	    {
            	    pushFollow(FOLLOW_43);
            	    rule__Condition__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

             after(grammarAccess.getConditionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__1__Impl"


    // $ANTLR start "rule__Condition__Group_1__0"
    // InternalGame.g:6842:1: rule__Condition__Group_1__0 : rule__Condition__Group_1__0__Impl rule__Condition__Group_1__1 ;
    public final void rule__Condition__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6846:1: ( rule__Condition__Group_1__0__Impl rule__Condition__Group_1__1 )
            // InternalGame.g:6847:2: rule__Condition__Group_1__0__Impl rule__Condition__Group_1__1
            {
            pushFollow(FOLLOW_15);
            rule__Condition__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_1__0"


    // $ANTLR start "rule__Condition__Group_1__0__Impl"
    // InternalGame.g:6854:1: rule__Condition__Group_1__0__Impl : ( '||' ) ;
    public final void rule__Condition__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6858:1: ( ( '||' ) )
            // InternalGame.g:6859:1: ( '||' )
            {
            // InternalGame.g:6859:1: ( '||' )
            // InternalGame.g:6860:2: '||'
            {
             before(grammarAccess.getConditionAccess().getVerticalLineVerticalLineKeyword_1_0()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getVerticalLineVerticalLineKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_1__0__Impl"


    // $ANTLR start "rule__Condition__Group_1__1"
    // InternalGame.g:6869:1: rule__Condition__Group_1__1 : rule__Condition__Group_1__1__Impl ;
    public final void rule__Condition__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6873:1: ( rule__Condition__Group_1__1__Impl )
            // InternalGame.g:6874:2: rule__Condition__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Condition__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_1__1"


    // $ANTLR start "rule__Condition__Group_1__1__Impl"
    // InternalGame.g:6880:1: rule__Condition__Group_1__1__Impl : ( ( rule__Condition__ConditionAssignment_1_1 ) ) ;
    public final void rule__Condition__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6884:1: ( ( ( rule__Condition__ConditionAssignment_1_1 ) ) )
            // InternalGame.g:6885:1: ( ( rule__Condition__ConditionAssignment_1_1 ) )
            {
            // InternalGame.g:6885:1: ( ( rule__Condition__ConditionAssignment_1_1 ) )
            // InternalGame.g:6886:2: ( rule__Condition__ConditionAssignment_1_1 )
            {
             before(grammarAccess.getConditionAccess().getConditionAssignment_1_1()); 
            // InternalGame.g:6887:2: ( rule__Condition__ConditionAssignment_1_1 )
            // InternalGame.g:6887:3: rule__Condition__ConditionAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Condition__ConditionAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getConditionAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_1__1__Impl"


    // $ANTLR start "rule__ConditionEt__Group__0"
    // InternalGame.g:6896:1: rule__ConditionEt__Group__0 : rule__ConditionEt__Group__0__Impl rule__ConditionEt__Group__1 ;
    public final void rule__ConditionEt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6900:1: ( rule__ConditionEt__Group__0__Impl rule__ConditionEt__Group__1 )
            // InternalGame.g:6901:2: rule__ConditionEt__Group__0__Impl rule__ConditionEt__Group__1
            {
            pushFollow(FOLLOW_44);
            rule__ConditionEt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConditionEt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionEt__Group__0"


    // $ANTLR start "rule__ConditionEt__Group__0__Impl"
    // InternalGame.g:6908:1: rule__ConditionEt__Group__0__Impl : ( ( rule__ConditionEt__ConditionTestAssignment_0 ) ) ;
    public final void rule__ConditionEt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6912:1: ( ( ( rule__ConditionEt__ConditionTestAssignment_0 ) ) )
            // InternalGame.g:6913:1: ( ( rule__ConditionEt__ConditionTestAssignment_0 ) )
            {
            // InternalGame.g:6913:1: ( ( rule__ConditionEt__ConditionTestAssignment_0 ) )
            // InternalGame.g:6914:2: ( rule__ConditionEt__ConditionTestAssignment_0 )
            {
             before(grammarAccess.getConditionEtAccess().getConditionTestAssignment_0()); 
            // InternalGame.g:6915:2: ( rule__ConditionEt__ConditionTestAssignment_0 )
            // InternalGame.g:6915:3: rule__ConditionEt__ConditionTestAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__ConditionEt__ConditionTestAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getConditionEtAccess().getConditionTestAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionEt__Group__0__Impl"


    // $ANTLR start "rule__ConditionEt__Group__1"
    // InternalGame.g:6923:1: rule__ConditionEt__Group__1 : rule__ConditionEt__Group__1__Impl ;
    public final void rule__ConditionEt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6927:1: ( rule__ConditionEt__Group__1__Impl )
            // InternalGame.g:6928:2: rule__ConditionEt__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ConditionEt__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionEt__Group__1"


    // $ANTLR start "rule__ConditionEt__Group__1__Impl"
    // InternalGame.g:6934:1: rule__ConditionEt__Group__1__Impl : ( ( rule__ConditionEt__Group_1__0 )* ) ;
    public final void rule__ConditionEt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6938:1: ( ( ( rule__ConditionEt__Group_1__0 )* ) )
            // InternalGame.g:6939:1: ( ( rule__ConditionEt__Group_1__0 )* )
            {
            // InternalGame.g:6939:1: ( ( rule__ConditionEt__Group_1__0 )* )
            // InternalGame.g:6940:2: ( rule__ConditionEt__Group_1__0 )*
            {
             before(grammarAccess.getConditionEtAccess().getGroup_1()); 
            // InternalGame.g:6941:2: ( rule__ConditionEt__Group_1__0 )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==46) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // InternalGame.g:6941:3: rule__ConditionEt__Group_1__0
            	    {
            	    pushFollow(FOLLOW_45);
            	    rule__ConditionEt__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);

             after(grammarAccess.getConditionEtAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionEt__Group__1__Impl"


    // $ANTLR start "rule__ConditionEt__Group_1__0"
    // InternalGame.g:6950:1: rule__ConditionEt__Group_1__0 : rule__ConditionEt__Group_1__0__Impl rule__ConditionEt__Group_1__1 ;
    public final void rule__ConditionEt__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6954:1: ( rule__ConditionEt__Group_1__0__Impl rule__ConditionEt__Group_1__1 )
            // InternalGame.g:6955:2: rule__ConditionEt__Group_1__0__Impl rule__ConditionEt__Group_1__1
            {
            pushFollow(FOLLOW_15);
            rule__ConditionEt__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConditionEt__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionEt__Group_1__0"


    // $ANTLR start "rule__ConditionEt__Group_1__0__Impl"
    // InternalGame.g:6962:1: rule__ConditionEt__Group_1__0__Impl : ( '&&' ) ;
    public final void rule__ConditionEt__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6966:1: ( ( '&&' ) )
            // InternalGame.g:6967:1: ( '&&' )
            {
            // InternalGame.g:6967:1: ( '&&' )
            // InternalGame.g:6968:2: '&&'
            {
             before(grammarAccess.getConditionEtAccess().getAmpersandAmpersandKeyword_1_0()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getConditionEtAccess().getAmpersandAmpersandKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionEt__Group_1__0__Impl"


    // $ANTLR start "rule__ConditionEt__Group_1__1"
    // InternalGame.g:6977:1: rule__ConditionEt__Group_1__1 : rule__ConditionEt__Group_1__1__Impl ;
    public final void rule__ConditionEt__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6981:1: ( rule__ConditionEt__Group_1__1__Impl )
            // InternalGame.g:6982:2: rule__ConditionEt__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ConditionEt__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionEt__Group_1__1"


    // $ANTLR start "rule__ConditionEt__Group_1__1__Impl"
    // InternalGame.g:6988:1: rule__ConditionEt__Group_1__1__Impl : ( ( rule__ConditionEt__ConditionTestAssignment_1_1 ) ) ;
    public final void rule__ConditionEt__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:6992:1: ( ( ( rule__ConditionEt__ConditionTestAssignment_1_1 ) ) )
            // InternalGame.g:6993:1: ( ( rule__ConditionEt__ConditionTestAssignment_1_1 ) )
            {
            // InternalGame.g:6993:1: ( ( rule__ConditionEt__ConditionTestAssignment_1_1 ) )
            // InternalGame.g:6994:2: ( rule__ConditionEt__ConditionTestAssignment_1_1 )
            {
             before(grammarAccess.getConditionEtAccess().getConditionTestAssignment_1_1()); 
            // InternalGame.g:6995:2: ( rule__ConditionEt__ConditionTestAssignment_1_1 )
            // InternalGame.g:6995:3: rule__ConditionEt__ConditionTestAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__ConditionEt__ConditionTestAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getConditionEtAccess().getConditionTestAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionEt__Group_1__1__Impl"


    // $ANTLR start "rule__ConditionConnaissance__Group__0"
    // InternalGame.g:7004:1: rule__ConditionConnaissance__Group__0 : rule__ConditionConnaissance__Group__0__Impl rule__ConditionConnaissance__Group__1 ;
    public final void rule__ConditionConnaissance__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7008:1: ( rule__ConditionConnaissance__Group__0__Impl rule__ConditionConnaissance__Group__1 )
            // InternalGame.g:7009:2: rule__ConditionConnaissance__Group__0__Impl rule__ConditionConnaissance__Group__1
            {
            pushFollow(FOLLOW_46);
            rule__ConditionConnaissance__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConditionConnaissance__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionConnaissance__Group__0"


    // $ANTLR start "rule__ConditionConnaissance__Group__0__Impl"
    // InternalGame.g:7016:1: rule__ConditionConnaissance__Group__0__Impl : ( ( rule__ConditionConnaissance__NegationAssignment_0 )? ) ;
    public final void rule__ConditionConnaissance__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7020:1: ( ( ( rule__ConditionConnaissance__NegationAssignment_0 )? ) )
            // InternalGame.g:7021:1: ( ( rule__ConditionConnaissance__NegationAssignment_0 )? )
            {
            // InternalGame.g:7021:1: ( ( rule__ConditionConnaissance__NegationAssignment_0 )? )
            // InternalGame.g:7022:2: ( rule__ConditionConnaissance__NegationAssignment_0 )?
            {
             before(grammarAccess.getConditionConnaissanceAccess().getNegationAssignment_0()); 
            // InternalGame.g:7023:2: ( rule__ConditionConnaissance__NegationAssignment_0 )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==47) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalGame.g:7023:3: rule__ConditionConnaissance__NegationAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ConditionConnaissance__NegationAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConditionConnaissanceAccess().getNegationAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionConnaissance__Group__0__Impl"


    // $ANTLR start "rule__ConditionConnaissance__Group__1"
    // InternalGame.g:7031:1: rule__ConditionConnaissance__Group__1 : rule__ConditionConnaissance__Group__1__Impl ;
    public final void rule__ConditionConnaissance__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7035:1: ( rule__ConditionConnaissance__Group__1__Impl )
            // InternalGame.g:7036:2: rule__ConditionConnaissance__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ConditionConnaissance__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionConnaissance__Group__1"


    // $ANTLR start "rule__ConditionConnaissance__Group__1__Impl"
    // InternalGame.g:7042:1: rule__ConditionConnaissance__Group__1__Impl : ( ( rule__ConditionConnaissance__ConnaissanceAssignment_1 ) ) ;
    public final void rule__ConditionConnaissance__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7046:1: ( ( ( rule__ConditionConnaissance__ConnaissanceAssignment_1 ) ) )
            // InternalGame.g:7047:1: ( ( rule__ConditionConnaissance__ConnaissanceAssignment_1 ) )
            {
            // InternalGame.g:7047:1: ( ( rule__ConditionConnaissance__ConnaissanceAssignment_1 ) )
            // InternalGame.g:7048:2: ( rule__ConditionConnaissance__ConnaissanceAssignment_1 )
            {
             before(grammarAccess.getConditionConnaissanceAccess().getConnaissanceAssignment_1()); 
            // InternalGame.g:7049:2: ( rule__ConditionConnaissance__ConnaissanceAssignment_1 )
            // InternalGame.g:7049:3: rule__ConditionConnaissance__ConnaissanceAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ConditionConnaissance__ConnaissanceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getConditionConnaissanceAccess().getConnaissanceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionConnaissance__Group__1__Impl"


    // $ANTLR start "rule__ConditionObjet__Group__0"
    // InternalGame.g:7058:1: rule__ConditionObjet__Group__0 : rule__ConditionObjet__Group__0__Impl rule__ConditionObjet__Group__1 ;
    public final void rule__ConditionObjet__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7062:1: ( rule__ConditionObjet__Group__0__Impl rule__ConditionObjet__Group__1 )
            // InternalGame.g:7063:2: rule__ConditionObjet__Group__0__Impl rule__ConditionObjet__Group__1
            {
            pushFollow(FOLLOW_47);
            rule__ConditionObjet__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConditionObjet__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionObjet__Group__0"


    // $ANTLR start "rule__ConditionObjet__Group__0__Impl"
    // InternalGame.g:7070:1: rule__ConditionObjet__Group__0__Impl : ( ( rule__ConditionObjet__ObjetAssignment_0 ) ) ;
    public final void rule__ConditionObjet__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7074:1: ( ( ( rule__ConditionObjet__ObjetAssignment_0 ) ) )
            // InternalGame.g:7075:1: ( ( rule__ConditionObjet__ObjetAssignment_0 ) )
            {
            // InternalGame.g:7075:1: ( ( rule__ConditionObjet__ObjetAssignment_0 ) )
            // InternalGame.g:7076:2: ( rule__ConditionObjet__ObjetAssignment_0 )
            {
             before(grammarAccess.getConditionObjetAccess().getObjetAssignment_0()); 
            // InternalGame.g:7077:2: ( rule__ConditionObjet__ObjetAssignment_0 )
            // InternalGame.g:7077:3: rule__ConditionObjet__ObjetAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__ConditionObjet__ObjetAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getConditionObjetAccess().getObjetAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionObjet__Group__0__Impl"


    // $ANTLR start "rule__ConditionObjet__Group__1"
    // InternalGame.g:7085:1: rule__ConditionObjet__Group__1 : rule__ConditionObjet__Group__1__Impl rule__ConditionObjet__Group__2 ;
    public final void rule__ConditionObjet__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7089:1: ( rule__ConditionObjet__Group__1__Impl rule__ConditionObjet__Group__2 )
            // InternalGame.g:7090:2: rule__ConditionObjet__Group__1__Impl rule__ConditionObjet__Group__2
            {
            pushFollow(FOLLOW_13);
            rule__ConditionObjet__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConditionObjet__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionObjet__Group__1"


    // $ANTLR start "rule__ConditionObjet__Group__1__Impl"
    // InternalGame.g:7097:1: rule__ConditionObjet__Group__1__Impl : ( ( rule__ConditionObjet__ComparateurAssignment_1 ) ) ;
    public final void rule__ConditionObjet__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7101:1: ( ( ( rule__ConditionObjet__ComparateurAssignment_1 ) ) )
            // InternalGame.g:7102:1: ( ( rule__ConditionObjet__ComparateurAssignment_1 ) )
            {
            // InternalGame.g:7102:1: ( ( rule__ConditionObjet__ComparateurAssignment_1 ) )
            // InternalGame.g:7103:2: ( rule__ConditionObjet__ComparateurAssignment_1 )
            {
             before(grammarAccess.getConditionObjetAccess().getComparateurAssignment_1()); 
            // InternalGame.g:7104:2: ( rule__ConditionObjet__ComparateurAssignment_1 )
            // InternalGame.g:7104:3: rule__ConditionObjet__ComparateurAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ConditionObjet__ComparateurAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getConditionObjetAccess().getComparateurAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionObjet__Group__1__Impl"


    // $ANTLR start "rule__ConditionObjet__Group__2"
    // InternalGame.g:7112:1: rule__ConditionObjet__Group__2 : rule__ConditionObjet__Group__2__Impl ;
    public final void rule__ConditionObjet__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7116:1: ( rule__ConditionObjet__Group__2__Impl )
            // InternalGame.g:7117:2: rule__ConditionObjet__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ConditionObjet__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionObjet__Group__2"


    // $ANTLR start "rule__ConditionObjet__Group__2__Impl"
    // InternalGame.g:7123:1: rule__ConditionObjet__Group__2__Impl : ( ( rule__ConditionObjet__NombreAssignment_2 ) ) ;
    public final void rule__ConditionObjet__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7127:1: ( ( ( rule__ConditionObjet__NombreAssignment_2 ) ) )
            // InternalGame.g:7128:1: ( ( rule__ConditionObjet__NombreAssignment_2 ) )
            {
            // InternalGame.g:7128:1: ( ( rule__ConditionObjet__NombreAssignment_2 ) )
            // InternalGame.g:7129:2: ( rule__ConditionObjet__NombreAssignment_2 )
            {
             before(grammarAccess.getConditionObjetAccess().getNombreAssignment_2()); 
            // InternalGame.g:7130:2: ( rule__ConditionObjet__NombreAssignment_2 )
            // InternalGame.g:7130:3: rule__ConditionObjet__NombreAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ConditionObjet__NombreAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getConditionObjetAccess().getNombreAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionObjet__Group__2__Impl"


    // $ANTLR start "rule__Jeu__ObjetsAssignment_2_1"
    // InternalGame.g:7139:1: rule__Jeu__ObjetsAssignment_2_1 : ( ruleObjet ) ;
    public final void rule__Jeu__ObjetsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7143:1: ( ( ruleObjet ) )
            // InternalGame.g:7144:2: ( ruleObjet )
            {
            // InternalGame.g:7144:2: ( ruleObjet )
            // InternalGame.g:7145:3: ruleObjet
            {
             before(grammarAccess.getJeuAccess().getObjetsObjetParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleObjet();

            state._fsp--;

             after(grammarAccess.getJeuAccess().getObjetsObjetParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__ObjetsAssignment_2_1"


    // $ANTLR start "rule__Jeu__TransformationsAssignment_5_1"
    // InternalGame.g:7154:1: rule__Jeu__TransformationsAssignment_5_1 : ( ruleTransformation ) ;
    public final void rule__Jeu__TransformationsAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7158:1: ( ( ruleTransformation ) )
            // InternalGame.g:7159:2: ( ruleTransformation )
            {
            // InternalGame.g:7159:2: ( ruleTransformation )
            // InternalGame.g:7160:3: ruleTransformation
            {
             before(grammarAccess.getJeuAccess().getTransformationsTransformationParserRuleCall_5_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTransformation();

            state._fsp--;

             after(grammarAccess.getJeuAccess().getTransformationsTransformationParserRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__TransformationsAssignment_5_1"


    // $ANTLR start "rule__Jeu__ConnaissancesAssignment_8_1"
    // InternalGame.g:7169:1: rule__Jeu__ConnaissancesAssignment_8_1 : ( ruleConnaissance ) ;
    public final void rule__Jeu__ConnaissancesAssignment_8_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7173:1: ( ( ruleConnaissance ) )
            // InternalGame.g:7174:2: ( ruleConnaissance )
            {
            // InternalGame.g:7174:2: ( ruleConnaissance )
            // InternalGame.g:7175:3: ruleConnaissance
            {
             before(grammarAccess.getJeuAccess().getConnaissancesConnaissanceParserRuleCall_8_1_0()); 
            pushFollow(FOLLOW_2);
            ruleConnaissance();

            state._fsp--;

             after(grammarAccess.getJeuAccess().getConnaissancesConnaissanceParserRuleCall_8_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__ConnaissancesAssignment_8_1"


    // $ANTLR start "rule__Jeu__ExplorateurAssignment_11"
    // InternalGame.g:7184:1: rule__Jeu__ExplorateurAssignment_11 : ( ruleExplorateur ) ;
    public final void rule__Jeu__ExplorateurAssignment_11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7188:1: ( ( ruleExplorateur ) )
            // InternalGame.g:7189:2: ( ruleExplorateur )
            {
            // InternalGame.g:7189:2: ( ruleExplorateur )
            // InternalGame.g:7190:3: ruleExplorateur
            {
             before(grammarAccess.getJeuAccess().getExplorateurExplorateurParserRuleCall_11_0()); 
            pushFollow(FOLLOW_2);
            ruleExplorateur();

            state._fsp--;

             after(grammarAccess.getJeuAccess().getExplorateurExplorateurParserRuleCall_11_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__ExplorateurAssignment_11"


    // $ANTLR start "rule__Jeu__PersonnesAssignment_14_1"
    // InternalGame.g:7199:1: rule__Jeu__PersonnesAssignment_14_1 : ( rulePersonne ) ;
    public final void rule__Jeu__PersonnesAssignment_14_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7203:1: ( ( rulePersonne ) )
            // InternalGame.g:7204:2: ( rulePersonne )
            {
            // InternalGame.g:7204:2: ( rulePersonne )
            // InternalGame.g:7205:3: rulePersonne
            {
             before(grammarAccess.getJeuAccess().getPersonnesPersonneParserRuleCall_14_1_0()); 
            pushFollow(FOLLOW_2);
            rulePersonne();

            state._fsp--;

             after(grammarAccess.getJeuAccess().getPersonnesPersonneParserRuleCall_14_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__PersonnesAssignment_14_1"


    // $ANTLR start "rule__Jeu__TerritoireAssignment_17"
    // InternalGame.g:7214:1: rule__Jeu__TerritoireAssignment_17 : ( ruleTerritoire ) ;
    public final void rule__Jeu__TerritoireAssignment_17() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7218:1: ( ( ruleTerritoire ) )
            // InternalGame.g:7219:2: ( ruleTerritoire )
            {
            // InternalGame.g:7219:2: ( ruleTerritoire )
            // InternalGame.g:7220:3: ruleTerritoire
            {
             before(grammarAccess.getJeuAccess().getTerritoireTerritoireParserRuleCall_17_0()); 
            pushFollow(FOLLOW_2);
            ruleTerritoire();

            state._fsp--;

             after(grammarAccess.getJeuAccess().getTerritoireTerritoireParserRuleCall_17_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Jeu__TerritoireAssignment_17"


    // $ANTLR start "rule__Objet__NameAssignment_0"
    // InternalGame.g:7229:1: rule__Objet__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Objet__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7233:1: ( ( RULE_ID ) )
            // InternalGame.g:7234:2: ( RULE_ID )
            {
            // InternalGame.g:7234:2: ( RULE_ID )
            // InternalGame.g:7235:3: RULE_ID
            {
             before(grammarAccess.getObjetAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getObjetAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__NameAssignment_0"


    // $ANTLR start "rule__Objet__TailleAssignment_4"
    // InternalGame.g:7244:1: rule__Objet__TailleAssignment_4 : ( RULE_INT ) ;
    public final void rule__Objet__TailleAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7248:1: ( ( RULE_INT ) )
            // InternalGame.g:7249:2: ( RULE_INT )
            {
            // InternalGame.g:7249:2: ( RULE_INT )
            // InternalGame.g:7250:3: RULE_INT
            {
             before(grammarAccess.getObjetAccess().getTailleINTTerminalRuleCall_4_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getObjetAccess().getTailleINTTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__TailleAssignment_4"


    // $ANTLR start "rule__Objet__VisibleAssignment_7"
    // InternalGame.g:7259:1: rule__Objet__VisibleAssignment_7 : ( ruleCondition ) ;
    public final void rule__Objet__VisibleAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7263:1: ( ( ruleCondition ) )
            // InternalGame.g:7264:2: ( ruleCondition )
            {
            // InternalGame.g:7264:2: ( ruleCondition )
            // InternalGame.g:7265:3: ruleCondition
            {
             before(grammarAccess.getObjetAccess().getVisibleConditionParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getObjetAccess().getVisibleConditionParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__VisibleAssignment_7"


    // $ANTLR start "rule__Objet__DescriptionsAssignment_10_1"
    // InternalGame.g:7274:1: rule__Objet__DescriptionsAssignment_10_1 : ( ruleDescription ) ;
    public final void rule__Objet__DescriptionsAssignment_10_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7278:1: ( ( ruleDescription ) )
            // InternalGame.g:7279:2: ( ruleDescription )
            {
            // InternalGame.g:7279:2: ( ruleDescription )
            // InternalGame.g:7280:3: ruleDescription
            {
             before(grammarAccess.getObjetAccess().getDescriptionsDescriptionParserRuleCall_10_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getObjetAccess().getDescriptionsDescriptionParserRuleCall_10_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Objet__DescriptionsAssignment_10_1"


    // $ANTLR start "rule__Transformation__NameAssignment_0"
    // InternalGame.g:7289:1: rule__Transformation__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Transformation__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7293:1: ( ( RULE_ID ) )
            // InternalGame.g:7294:2: ( RULE_ID )
            {
            // InternalGame.g:7294:2: ( RULE_ID )
            // InternalGame.g:7295:3: RULE_ID
            {
             before(grammarAccess.getTransformationAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTransformationAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__NameAssignment_0"


    // $ANTLR start "rule__Transformation__ConditionAssignment_4"
    // InternalGame.g:7304:1: rule__Transformation__ConditionAssignment_4 : ( ruleCondition ) ;
    public final void rule__Transformation__ConditionAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7308:1: ( ( ruleCondition ) )
            // InternalGame.g:7309:2: ( ruleCondition )
            {
            // InternalGame.g:7309:2: ( ruleCondition )
            // InternalGame.g:7310:3: ruleCondition
            {
             before(grammarAccess.getTransformationAccess().getConditionConditionParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getTransformationAccess().getConditionConditionParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__ConditionAssignment_4"


    // $ANTLR start "rule__Transformation__ObjetsInAssignment_7_1"
    // InternalGame.g:7319:1: rule__Transformation__ObjetsInAssignment_7_1 : ( ( RULE_ID ) ) ;
    public final void rule__Transformation__ObjetsInAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7323:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7324:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7324:2: ( ( RULE_ID ) )
            // InternalGame.g:7325:3: ( RULE_ID )
            {
             before(grammarAccess.getTransformationAccess().getObjetsInObjetCrossReference_7_1_0()); 
            // InternalGame.g:7326:3: ( RULE_ID )
            // InternalGame.g:7327:4: RULE_ID
            {
             before(grammarAccess.getTransformationAccess().getObjetsInObjetIDTerminalRuleCall_7_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTransformationAccess().getObjetsInObjetIDTerminalRuleCall_7_1_0_1()); 

            }

             after(grammarAccess.getTransformationAccess().getObjetsInObjetCrossReference_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__ObjetsInAssignment_7_1"


    // $ANTLR start "rule__Transformation__ObjetsOutAssignment_10_1"
    // InternalGame.g:7338:1: rule__Transformation__ObjetsOutAssignment_10_1 : ( ( RULE_ID ) ) ;
    public final void rule__Transformation__ObjetsOutAssignment_10_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7342:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7343:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7343:2: ( ( RULE_ID ) )
            // InternalGame.g:7344:3: ( RULE_ID )
            {
             before(grammarAccess.getTransformationAccess().getObjetsOutObjetCrossReference_10_1_0()); 
            // InternalGame.g:7345:3: ( RULE_ID )
            // InternalGame.g:7346:4: RULE_ID
            {
             before(grammarAccess.getTransformationAccess().getObjetsOutObjetIDTerminalRuleCall_10_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTransformationAccess().getObjetsOutObjetIDTerminalRuleCall_10_1_0_1()); 

            }

             after(grammarAccess.getTransformationAccess().getObjetsOutObjetCrossReference_10_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformation__ObjetsOutAssignment_10_1"


    // $ANTLR start "rule__Connaissance__NameAssignment_0"
    // InternalGame.g:7357:1: rule__Connaissance__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Connaissance__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7361:1: ( ( RULE_ID ) )
            // InternalGame.g:7362:2: ( RULE_ID )
            {
            // InternalGame.g:7362:2: ( RULE_ID )
            // InternalGame.g:7363:3: RULE_ID
            {
             before(grammarAccess.getConnaissanceAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getConnaissanceAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__NameAssignment_0"


    // $ANTLR start "rule__Connaissance__VisibleAssignment_4"
    // InternalGame.g:7372:1: rule__Connaissance__VisibleAssignment_4 : ( ruleCondition ) ;
    public final void rule__Connaissance__VisibleAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7376:1: ( ( ruleCondition ) )
            // InternalGame.g:7377:2: ( ruleCondition )
            {
            // InternalGame.g:7377:2: ( ruleCondition )
            // InternalGame.g:7378:3: ruleCondition
            {
             before(grammarAccess.getConnaissanceAccess().getVisibleConditionParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getConnaissanceAccess().getVisibleConditionParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__VisibleAssignment_4"


    // $ANTLR start "rule__Connaissance__DescriptionsAssignment_7_1"
    // InternalGame.g:7387:1: rule__Connaissance__DescriptionsAssignment_7_1 : ( ruleDescription ) ;
    public final void rule__Connaissance__DescriptionsAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7391:1: ( ( ruleDescription ) )
            // InternalGame.g:7392:2: ( ruleDescription )
            {
            // InternalGame.g:7392:2: ( ruleDescription )
            // InternalGame.g:7393:3: ruleDescription
            {
             before(grammarAccess.getConnaissanceAccess().getDescriptionsDescriptionParserRuleCall_7_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getConnaissanceAccess().getDescriptionsDescriptionParserRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connaissance__DescriptionsAssignment_7_1"


    // $ANTLR start "rule__Explorateur__TailleInventaireAssignment_2"
    // InternalGame.g:7402:1: rule__Explorateur__TailleInventaireAssignment_2 : ( RULE_INT ) ;
    public final void rule__Explorateur__TailleInventaireAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7406:1: ( ( RULE_INT ) )
            // InternalGame.g:7407:2: ( RULE_INT )
            {
            // InternalGame.g:7407:2: ( RULE_INT )
            // InternalGame.g:7408:3: RULE_INT
            {
             before(grammarAccess.getExplorateurAccess().getTailleInventaireINTTerminalRuleCall_2_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getExplorateurAccess().getTailleInventaireINTTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__TailleInventaireAssignment_2"


    // $ANTLR start "rule__Explorateur__ConnaissancesAssignment_5_1"
    // InternalGame.g:7417:1: rule__Explorateur__ConnaissancesAssignment_5_1 : ( ( RULE_ID ) ) ;
    public final void rule__Explorateur__ConnaissancesAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7421:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7422:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7422:2: ( ( RULE_ID ) )
            // InternalGame.g:7423:3: ( RULE_ID )
            {
             before(grammarAccess.getExplorateurAccess().getConnaissancesConnaissanceCrossReference_5_1_0()); 
            // InternalGame.g:7424:3: ( RULE_ID )
            // InternalGame.g:7425:4: RULE_ID
            {
             before(grammarAccess.getExplorateurAccess().getConnaissancesConnaissanceIDTerminalRuleCall_5_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getExplorateurAccess().getConnaissancesConnaissanceIDTerminalRuleCall_5_1_0_1()); 

            }

             after(grammarAccess.getExplorateurAccess().getConnaissancesConnaissanceCrossReference_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__ConnaissancesAssignment_5_1"


    // $ANTLR start "rule__Explorateur__ObjetsAssignment_8_1"
    // InternalGame.g:7436:1: rule__Explorateur__ObjetsAssignment_8_1 : ( ( RULE_ID ) ) ;
    public final void rule__Explorateur__ObjetsAssignment_8_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7440:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7441:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7441:2: ( ( RULE_ID ) )
            // InternalGame.g:7442:3: ( RULE_ID )
            {
             before(grammarAccess.getExplorateurAccess().getObjetsObjetCrossReference_8_1_0()); 
            // InternalGame.g:7443:3: ( RULE_ID )
            // InternalGame.g:7444:4: RULE_ID
            {
             before(grammarAccess.getExplorateurAccess().getObjetsObjetIDTerminalRuleCall_8_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getExplorateurAccess().getObjetsObjetIDTerminalRuleCall_8_1_0_1()); 

            }

             after(grammarAccess.getExplorateurAccess().getObjetsObjetCrossReference_8_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Explorateur__ObjetsAssignment_8_1"


    // $ANTLR start "rule__Territoire__LieuxAssignment_3_1"
    // InternalGame.g:7455:1: rule__Territoire__LieuxAssignment_3_1 : ( ruleLieu ) ;
    public final void rule__Territoire__LieuxAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7459:1: ( ( ruleLieu ) )
            // InternalGame.g:7460:2: ( ruleLieu )
            {
            // InternalGame.g:7460:2: ( ruleLieu )
            // InternalGame.g:7461:3: ruleLieu
            {
             before(grammarAccess.getTerritoireAccess().getLieuxLieuParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLieu();

            state._fsp--;

             after(grammarAccess.getTerritoireAccess().getLieuxLieuParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__LieuxAssignment_3_1"


    // $ANTLR start "rule__Territoire__CheminsAssignment_6_1"
    // InternalGame.g:7470:1: rule__Territoire__CheminsAssignment_6_1 : ( ruleChemin ) ;
    public final void rule__Territoire__CheminsAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7474:1: ( ( ruleChemin ) )
            // InternalGame.g:7475:2: ( ruleChemin )
            {
            // InternalGame.g:7475:2: ( ruleChemin )
            // InternalGame.g:7476:3: ruleChemin
            {
             before(grammarAccess.getTerritoireAccess().getCheminsCheminParserRuleCall_6_1_0()); 
            pushFollow(FOLLOW_2);
            ruleChemin();

            state._fsp--;

             after(grammarAccess.getTerritoireAccess().getCheminsCheminParserRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Territoire__CheminsAssignment_6_1"


    // $ANTLR start "rule__Lieu__NameAssignment_0"
    // InternalGame.g:7485:1: rule__Lieu__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Lieu__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7489:1: ( ( RULE_ID ) )
            // InternalGame.g:7490:2: ( RULE_ID )
            {
            // InternalGame.g:7490:2: ( RULE_ID )
            // InternalGame.g:7491:3: RULE_ID
            {
             before(grammarAccess.getLieuAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__NameAssignment_0"


    // $ANTLR start "rule__Lieu__DeposableAssignment_4"
    // InternalGame.g:7500:1: rule__Lieu__DeposableAssignment_4 : ( ruleCondition ) ;
    public final void rule__Lieu__DeposableAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7504:1: ( ( ruleCondition ) )
            // InternalGame.g:7505:2: ( ruleCondition )
            {
            // InternalGame.g:7505:2: ( ruleCondition )
            // InternalGame.g:7506:3: ruleCondition
            {
             before(grammarAccess.getLieuAccess().getDeposableConditionParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getLieuAccess().getDeposableConditionParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__DeposableAssignment_4"


    // $ANTLR start "rule__Lieu__DepartAssignment_7"
    // InternalGame.g:7515:1: rule__Lieu__DepartAssignment_7 : ( ruleCondition ) ;
    public final void rule__Lieu__DepartAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7519:1: ( ( ruleCondition ) )
            // InternalGame.g:7520:2: ( ruleCondition )
            {
            // InternalGame.g:7520:2: ( ruleCondition )
            // InternalGame.g:7521:3: ruleCondition
            {
             before(grammarAccess.getLieuAccess().getDepartConditionParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getLieuAccess().getDepartConditionParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__DepartAssignment_7"


    // $ANTLR start "rule__Lieu__FinAssignment_10"
    // InternalGame.g:7530:1: rule__Lieu__FinAssignment_10 : ( ruleCondition ) ;
    public final void rule__Lieu__FinAssignment_10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7534:1: ( ( ruleCondition ) )
            // InternalGame.g:7535:2: ( ruleCondition )
            {
            // InternalGame.g:7535:2: ( ruleCondition )
            // InternalGame.g:7536:3: ruleCondition
            {
             before(grammarAccess.getLieuAccess().getFinConditionParserRuleCall_10_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getLieuAccess().getFinConditionParserRuleCall_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__FinAssignment_10"


    // $ANTLR start "rule__Lieu__PersonnesAssignment_13_1"
    // InternalGame.g:7545:1: rule__Lieu__PersonnesAssignment_13_1 : ( ( RULE_ID ) ) ;
    public final void rule__Lieu__PersonnesAssignment_13_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7549:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7550:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7550:2: ( ( RULE_ID ) )
            // InternalGame.g:7551:3: ( RULE_ID )
            {
             before(grammarAccess.getLieuAccess().getPersonnesPersonneCrossReference_13_1_0()); 
            // InternalGame.g:7552:3: ( RULE_ID )
            // InternalGame.g:7553:4: RULE_ID
            {
             before(grammarAccess.getLieuAccess().getPersonnesPersonneIDTerminalRuleCall_13_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getPersonnesPersonneIDTerminalRuleCall_13_1_0_1()); 

            }

             after(grammarAccess.getLieuAccess().getPersonnesPersonneCrossReference_13_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__PersonnesAssignment_13_1"


    // $ANTLR start "rule__Lieu__DescriptionsAssignment_16_1"
    // InternalGame.g:7564:1: rule__Lieu__DescriptionsAssignment_16_1 : ( ruleDescription ) ;
    public final void rule__Lieu__DescriptionsAssignment_16_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7568:1: ( ( ruleDescription ) )
            // InternalGame.g:7569:2: ( ruleDescription )
            {
            // InternalGame.g:7569:2: ( ruleDescription )
            // InternalGame.g:7570:3: ruleDescription
            {
             before(grammarAccess.getLieuAccess().getDescriptionsDescriptionParserRuleCall_16_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getLieuAccess().getDescriptionsDescriptionParserRuleCall_16_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__DescriptionsAssignment_16_1"


    // $ANTLR start "rule__Lieu__ObjetsAssignment_19_1"
    // InternalGame.g:7579:1: rule__Lieu__ObjetsAssignment_19_1 : ( ( RULE_ID ) ) ;
    public final void rule__Lieu__ObjetsAssignment_19_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7583:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7584:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7584:2: ( ( RULE_ID ) )
            // InternalGame.g:7585:3: ( RULE_ID )
            {
             before(grammarAccess.getLieuAccess().getObjetsObjetCrossReference_19_1_0()); 
            // InternalGame.g:7586:3: ( RULE_ID )
            // InternalGame.g:7587:4: RULE_ID
            {
             before(grammarAccess.getLieuAccess().getObjetsObjetIDTerminalRuleCall_19_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getObjetsObjetIDTerminalRuleCall_19_1_0_1()); 

            }

             after(grammarAccess.getLieuAccess().getObjetsObjetCrossReference_19_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__ObjetsAssignment_19_1"


    // $ANTLR start "rule__Lieu__ConnaissancesAssignment_22_1"
    // InternalGame.g:7598:1: rule__Lieu__ConnaissancesAssignment_22_1 : ( ( RULE_ID ) ) ;
    public final void rule__Lieu__ConnaissancesAssignment_22_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7602:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7603:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7603:2: ( ( RULE_ID ) )
            // InternalGame.g:7604:3: ( RULE_ID )
            {
             before(grammarAccess.getLieuAccess().getConnaissancesConnaissanceCrossReference_22_1_0()); 
            // InternalGame.g:7605:3: ( RULE_ID )
            // InternalGame.g:7606:4: RULE_ID
            {
             before(grammarAccess.getLieuAccess().getConnaissancesConnaissanceIDTerminalRuleCall_22_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLieuAccess().getConnaissancesConnaissanceIDTerminalRuleCall_22_1_0_1()); 

            }

             after(grammarAccess.getLieuAccess().getConnaissancesConnaissanceCrossReference_22_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lieu__ConnaissancesAssignment_22_1"


    // $ANTLR start "rule__Chemin__NameAssignment_0"
    // InternalGame.g:7617:1: rule__Chemin__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Chemin__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7621:1: ( ( RULE_ID ) )
            // InternalGame.g:7622:2: ( RULE_ID )
            {
            // InternalGame.g:7622:2: ( RULE_ID )
            // InternalGame.g:7623:3: RULE_ID
            {
             before(grammarAccess.getCheminAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__NameAssignment_0"


    // $ANTLR start "rule__Chemin__LieuInAssignment_4"
    // InternalGame.g:7632:1: rule__Chemin__LieuInAssignment_4 : ( ( RULE_ID ) ) ;
    public final void rule__Chemin__LieuInAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7636:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7637:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7637:2: ( ( RULE_ID ) )
            // InternalGame.g:7638:3: ( RULE_ID )
            {
             before(grammarAccess.getCheminAccess().getLieuInLieuCrossReference_4_0()); 
            // InternalGame.g:7639:3: ( RULE_ID )
            // InternalGame.g:7640:4: RULE_ID
            {
             before(grammarAccess.getCheminAccess().getLieuInLieuIDTerminalRuleCall_4_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getLieuInLieuIDTerminalRuleCall_4_0_1()); 

            }

             after(grammarAccess.getCheminAccess().getLieuInLieuCrossReference_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__LieuInAssignment_4"


    // $ANTLR start "rule__Chemin__LieuOutAssignment_7"
    // InternalGame.g:7651:1: rule__Chemin__LieuOutAssignment_7 : ( ( RULE_ID ) ) ;
    public final void rule__Chemin__LieuOutAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7655:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7656:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7656:2: ( ( RULE_ID ) )
            // InternalGame.g:7657:3: ( RULE_ID )
            {
             before(grammarAccess.getCheminAccess().getLieuOutLieuCrossReference_7_0()); 
            // InternalGame.g:7658:3: ( RULE_ID )
            // InternalGame.g:7659:4: RULE_ID
            {
             before(grammarAccess.getCheminAccess().getLieuOutLieuIDTerminalRuleCall_7_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getLieuOutLieuIDTerminalRuleCall_7_0_1()); 

            }

             after(grammarAccess.getCheminAccess().getLieuOutLieuCrossReference_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__LieuOutAssignment_7"


    // $ANTLR start "rule__Chemin__OuvertAssignment_10"
    // InternalGame.g:7670:1: rule__Chemin__OuvertAssignment_10 : ( ruleCondition ) ;
    public final void rule__Chemin__OuvertAssignment_10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7674:1: ( ( ruleCondition ) )
            // InternalGame.g:7675:2: ( ruleCondition )
            {
            // InternalGame.g:7675:2: ( ruleCondition )
            // InternalGame.g:7676:3: ruleCondition
            {
             before(grammarAccess.getCheminAccess().getOuvertConditionParserRuleCall_10_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getCheminAccess().getOuvertConditionParserRuleCall_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__OuvertAssignment_10"


    // $ANTLR start "rule__Chemin__VisibleAssignment_13"
    // InternalGame.g:7685:1: rule__Chemin__VisibleAssignment_13 : ( ruleCondition ) ;
    public final void rule__Chemin__VisibleAssignment_13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7689:1: ( ( ruleCondition ) )
            // InternalGame.g:7690:2: ( ruleCondition )
            {
            // InternalGame.g:7690:2: ( ruleCondition )
            // InternalGame.g:7691:3: ruleCondition
            {
             before(grammarAccess.getCheminAccess().getVisibleConditionParserRuleCall_13_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getCheminAccess().getVisibleConditionParserRuleCall_13_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__VisibleAssignment_13"


    // $ANTLR start "rule__Chemin__ObligatoireAssignment_16"
    // InternalGame.g:7700:1: rule__Chemin__ObligatoireAssignment_16 : ( ruleCondition ) ;
    public final void rule__Chemin__ObligatoireAssignment_16() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7704:1: ( ( ruleCondition ) )
            // InternalGame.g:7705:2: ( ruleCondition )
            {
            // InternalGame.g:7705:2: ( ruleCondition )
            // InternalGame.g:7706:3: ruleCondition
            {
             before(grammarAccess.getCheminAccess().getObligatoireConditionParserRuleCall_16_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getCheminAccess().getObligatoireConditionParserRuleCall_16_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__ObligatoireAssignment_16"


    // $ANTLR start "rule__Chemin__ConnaissancesAssignment_19_1"
    // InternalGame.g:7715:1: rule__Chemin__ConnaissancesAssignment_19_1 : ( ( RULE_ID ) ) ;
    public final void rule__Chemin__ConnaissancesAssignment_19_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7719:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7720:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7720:2: ( ( RULE_ID ) )
            // InternalGame.g:7721:3: ( RULE_ID )
            {
             before(grammarAccess.getCheminAccess().getConnaissancesConnaissanceCrossReference_19_1_0()); 
            // InternalGame.g:7722:3: ( RULE_ID )
            // InternalGame.g:7723:4: RULE_ID
            {
             before(grammarAccess.getCheminAccess().getConnaissancesConnaissanceIDTerminalRuleCall_19_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getConnaissancesConnaissanceIDTerminalRuleCall_19_1_0_1()); 

            }

             after(grammarAccess.getCheminAccess().getConnaissancesConnaissanceCrossReference_19_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__ConnaissancesAssignment_19_1"


    // $ANTLR start "rule__Chemin__ObjetsRecusAssignment_22_1"
    // InternalGame.g:7734:1: rule__Chemin__ObjetsRecusAssignment_22_1 : ( ( RULE_ID ) ) ;
    public final void rule__Chemin__ObjetsRecusAssignment_22_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7738:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7739:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7739:2: ( ( RULE_ID ) )
            // InternalGame.g:7740:3: ( RULE_ID )
            {
             before(grammarAccess.getCheminAccess().getObjetsRecusObjetCrossReference_22_1_0()); 
            // InternalGame.g:7741:3: ( RULE_ID )
            // InternalGame.g:7742:4: RULE_ID
            {
             before(grammarAccess.getCheminAccess().getObjetsRecusObjetIDTerminalRuleCall_22_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getObjetsRecusObjetIDTerminalRuleCall_22_1_0_1()); 

            }

             after(grammarAccess.getCheminAccess().getObjetsRecusObjetCrossReference_22_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__ObjetsRecusAssignment_22_1"


    // $ANTLR start "rule__Chemin__ObjetsConsoAssignment_25_1"
    // InternalGame.g:7753:1: rule__Chemin__ObjetsConsoAssignment_25_1 : ( ( RULE_ID ) ) ;
    public final void rule__Chemin__ObjetsConsoAssignment_25_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7757:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7758:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7758:2: ( ( RULE_ID ) )
            // InternalGame.g:7759:3: ( RULE_ID )
            {
             before(grammarAccess.getCheminAccess().getObjetsConsoObjetCrossReference_25_1_0()); 
            // InternalGame.g:7760:3: ( RULE_ID )
            // InternalGame.g:7761:4: RULE_ID
            {
             before(grammarAccess.getCheminAccess().getObjetsConsoObjetIDTerminalRuleCall_25_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getCheminAccess().getObjetsConsoObjetIDTerminalRuleCall_25_1_0_1()); 

            }

             after(grammarAccess.getCheminAccess().getObjetsConsoObjetCrossReference_25_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__ObjetsConsoAssignment_25_1"


    // $ANTLR start "rule__Chemin__DescriptionsAssignment_28_1"
    // InternalGame.g:7772:1: rule__Chemin__DescriptionsAssignment_28_1 : ( ruleDescription ) ;
    public final void rule__Chemin__DescriptionsAssignment_28_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7776:1: ( ( ruleDescription ) )
            // InternalGame.g:7777:2: ( ruleDescription )
            {
            // InternalGame.g:7777:2: ( ruleDescription )
            // InternalGame.g:7778:3: ruleDescription
            {
             before(grammarAccess.getCheminAccess().getDescriptionsDescriptionParserRuleCall_28_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getCheminAccess().getDescriptionsDescriptionParserRuleCall_28_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Chemin__DescriptionsAssignment_28_1"


    // $ANTLR start "rule__Personne__NameAssignment_0"
    // InternalGame.g:7787:1: rule__Personne__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Personne__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7791:1: ( ( RULE_ID ) )
            // InternalGame.g:7792:2: ( RULE_ID )
            {
            // InternalGame.g:7792:2: ( RULE_ID )
            // InternalGame.g:7793:3: RULE_ID
            {
             before(grammarAccess.getPersonneAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPersonneAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__NameAssignment_0"


    // $ANTLR start "rule__Personne__VisibleAssignment_4"
    // InternalGame.g:7802:1: rule__Personne__VisibleAssignment_4 : ( ruleCondition ) ;
    public final void rule__Personne__VisibleAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7806:1: ( ( ruleCondition ) )
            // InternalGame.g:7807:2: ( ruleCondition )
            {
            // InternalGame.g:7807:2: ( ruleCondition )
            // InternalGame.g:7808:3: ruleCondition
            {
             before(grammarAccess.getPersonneAccess().getVisibleConditionParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getPersonneAccess().getVisibleConditionParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__VisibleAssignment_4"


    // $ANTLR start "rule__Personne__ObligatoireAssignment_7"
    // InternalGame.g:7817:1: rule__Personne__ObligatoireAssignment_7 : ( ruleCondition ) ;
    public final void rule__Personne__ObligatoireAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7821:1: ( ( ruleCondition ) )
            // InternalGame.g:7822:2: ( ruleCondition )
            {
            // InternalGame.g:7822:2: ( ruleCondition )
            // InternalGame.g:7823:3: ruleCondition
            {
             before(grammarAccess.getPersonneAccess().getObligatoireConditionParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getPersonneAccess().getObligatoireConditionParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__ObligatoireAssignment_7"


    // $ANTLR start "rule__Personne__InteractionsAssignment_10_1"
    // InternalGame.g:7832:1: rule__Personne__InteractionsAssignment_10_1 : ( ruleInteraction ) ;
    public final void rule__Personne__InteractionsAssignment_10_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7836:1: ( ( ruleInteraction ) )
            // InternalGame.g:7837:2: ( ruleInteraction )
            {
            // InternalGame.g:7837:2: ( ruleInteraction )
            // InternalGame.g:7838:3: ruleInteraction
            {
             before(grammarAccess.getPersonneAccess().getInteractionsInteractionParserRuleCall_10_1_0()); 
            pushFollow(FOLLOW_2);
            ruleInteraction();

            state._fsp--;

             after(grammarAccess.getPersonneAccess().getInteractionsInteractionParserRuleCall_10_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Personne__InteractionsAssignment_10_1"


    // $ANTLR start "rule__Interaction__NameAssignment_0"
    // InternalGame.g:7847:1: rule__Interaction__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Interaction__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7851:1: ( ( RULE_ID ) )
            // InternalGame.g:7852:2: ( RULE_ID )
            {
            // InternalGame.g:7852:2: ( RULE_ID )
            // InternalGame.g:7853:3: RULE_ID
            {
             before(grammarAccess.getInteractionAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__NameAssignment_0"


    // $ANTLR start "rule__Interaction__VisibleAssignment_4"
    // InternalGame.g:7862:1: rule__Interaction__VisibleAssignment_4 : ( ruleCondition ) ;
    public final void rule__Interaction__VisibleAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7866:1: ( ( ruleCondition ) )
            // InternalGame.g:7867:2: ( ruleCondition )
            {
            // InternalGame.g:7867:2: ( ruleCondition )
            // InternalGame.g:7868:3: ruleCondition
            {
             before(grammarAccess.getInteractionAccess().getVisibleConditionParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getInteractionAccess().getVisibleConditionParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__VisibleAssignment_4"


    // $ANTLR start "rule__Interaction__ConnaissancesAssignment_7_1"
    // InternalGame.g:7877:1: rule__Interaction__ConnaissancesAssignment_7_1 : ( ( RULE_ID ) ) ;
    public final void rule__Interaction__ConnaissancesAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7881:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7882:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7882:2: ( ( RULE_ID ) )
            // InternalGame.g:7883:3: ( RULE_ID )
            {
             before(grammarAccess.getInteractionAccess().getConnaissancesConnaissanceCrossReference_7_1_0()); 
            // InternalGame.g:7884:3: ( RULE_ID )
            // InternalGame.g:7885:4: RULE_ID
            {
             before(grammarAccess.getInteractionAccess().getConnaissancesConnaissanceIDTerminalRuleCall_7_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getConnaissancesConnaissanceIDTerminalRuleCall_7_1_0_1()); 

            }

             after(grammarAccess.getInteractionAccess().getConnaissancesConnaissanceCrossReference_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__ConnaissancesAssignment_7_1"


    // $ANTLR start "rule__Interaction__ObjetsRecusAssignment_10_1"
    // InternalGame.g:7896:1: rule__Interaction__ObjetsRecusAssignment_10_1 : ( ( RULE_ID ) ) ;
    public final void rule__Interaction__ObjetsRecusAssignment_10_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7900:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7901:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7901:2: ( ( RULE_ID ) )
            // InternalGame.g:7902:3: ( RULE_ID )
            {
             before(grammarAccess.getInteractionAccess().getObjetsRecusObjetCrossReference_10_1_0()); 
            // InternalGame.g:7903:3: ( RULE_ID )
            // InternalGame.g:7904:4: RULE_ID
            {
             before(grammarAccess.getInteractionAccess().getObjetsRecusObjetIDTerminalRuleCall_10_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getObjetsRecusObjetIDTerminalRuleCall_10_1_0_1()); 

            }

             after(grammarAccess.getInteractionAccess().getObjetsRecusObjetCrossReference_10_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__ObjetsRecusAssignment_10_1"


    // $ANTLR start "rule__Interaction__ObjetsConsoAssignment_13_1"
    // InternalGame.g:7915:1: rule__Interaction__ObjetsConsoAssignment_13_1 : ( ( RULE_ID ) ) ;
    public final void rule__Interaction__ObjetsConsoAssignment_13_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7919:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7920:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7920:2: ( ( RULE_ID ) )
            // InternalGame.g:7921:3: ( RULE_ID )
            {
             before(grammarAccess.getInteractionAccess().getObjetsConsoObjetCrossReference_13_1_0()); 
            // InternalGame.g:7922:3: ( RULE_ID )
            // InternalGame.g:7923:4: RULE_ID
            {
             before(grammarAccess.getInteractionAccess().getObjetsConsoObjetIDTerminalRuleCall_13_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getInteractionAccess().getObjetsConsoObjetIDTerminalRuleCall_13_1_0_1()); 

            }

             after(grammarAccess.getInteractionAccess().getObjetsConsoObjetCrossReference_13_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__ObjetsConsoAssignment_13_1"


    // $ANTLR start "rule__Interaction__ActionsAssignment_16_1"
    // InternalGame.g:7934:1: rule__Interaction__ActionsAssignment_16_1 : ( ruleAction ) ;
    public final void rule__Interaction__ActionsAssignment_16_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7938:1: ( ( ruleAction ) )
            // InternalGame.g:7939:2: ( ruleAction )
            {
            // InternalGame.g:7939:2: ( ruleAction )
            // InternalGame.g:7940:3: ruleAction
            {
             before(grammarAccess.getInteractionAccess().getActionsActionParserRuleCall_16_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAction();

            state._fsp--;

             after(grammarAccess.getInteractionAccess().getActionsActionParserRuleCall_16_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__ActionsAssignment_16_1"


    // $ANTLR start "rule__Action__NameAssignment_0"
    // InternalGame.g:7949:1: rule__Action__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Action__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7953:1: ( ( RULE_ID ) )
            // InternalGame.g:7954:2: ( RULE_ID )
            {
            // InternalGame.g:7954:2: ( RULE_ID )
            // InternalGame.g:7955:3: RULE_ID
            {
             before(grammarAccess.getActionAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__NameAssignment_0"


    // $ANTLR start "rule__Action__VisibleAssignment_4"
    // InternalGame.g:7964:1: rule__Action__VisibleAssignment_4 : ( ruleCondition ) ;
    public final void rule__Action__VisibleAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7968:1: ( ( ruleCondition ) )
            // InternalGame.g:7969:2: ( ruleCondition )
            {
            // InternalGame.g:7969:2: ( ruleCondition )
            // InternalGame.g:7970:3: ruleCondition
            {
             before(grammarAccess.getActionAccess().getVisibleConditionParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getActionAccess().getVisibleConditionParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__VisibleAssignment_4"


    // $ANTLR start "rule__Action__FinInteractionAssignment_7"
    // InternalGame.g:7979:1: rule__Action__FinInteractionAssignment_7 : ( ruleCondition ) ;
    public final void rule__Action__FinInteractionAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7983:1: ( ( ruleCondition ) )
            // InternalGame.g:7984:2: ( ruleCondition )
            {
            // InternalGame.g:7984:2: ( ruleCondition )
            // InternalGame.g:7985:3: ruleCondition
            {
             before(grammarAccess.getActionAccess().getFinInteractionConditionParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getActionAccess().getFinInteractionConditionParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__FinInteractionAssignment_7"


    // $ANTLR start "rule__Action__ConnaissancesAssignment_10_1"
    // InternalGame.g:7994:1: rule__Action__ConnaissancesAssignment_10_1 : ( ( RULE_ID ) ) ;
    public final void rule__Action__ConnaissancesAssignment_10_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:7998:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:7999:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:7999:2: ( ( RULE_ID ) )
            // InternalGame.g:8000:3: ( RULE_ID )
            {
             before(grammarAccess.getActionAccess().getConnaissancesConnaissanceCrossReference_10_1_0()); 
            // InternalGame.g:8001:3: ( RULE_ID )
            // InternalGame.g:8002:4: RULE_ID
            {
             before(grammarAccess.getActionAccess().getConnaissancesConnaissanceIDTerminalRuleCall_10_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getConnaissancesConnaissanceIDTerminalRuleCall_10_1_0_1()); 

            }

             after(grammarAccess.getActionAccess().getConnaissancesConnaissanceCrossReference_10_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__ConnaissancesAssignment_10_1"


    // $ANTLR start "rule__Action__ObjetsRecusAssignment_13_1"
    // InternalGame.g:8013:1: rule__Action__ObjetsRecusAssignment_13_1 : ( ( RULE_ID ) ) ;
    public final void rule__Action__ObjetsRecusAssignment_13_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8017:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:8018:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:8018:2: ( ( RULE_ID ) )
            // InternalGame.g:8019:3: ( RULE_ID )
            {
             before(grammarAccess.getActionAccess().getObjetsRecusObjetCrossReference_13_1_0()); 
            // InternalGame.g:8020:3: ( RULE_ID )
            // InternalGame.g:8021:4: RULE_ID
            {
             before(grammarAccess.getActionAccess().getObjetsRecusObjetIDTerminalRuleCall_13_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getObjetsRecusObjetIDTerminalRuleCall_13_1_0_1()); 

            }

             after(grammarAccess.getActionAccess().getObjetsRecusObjetCrossReference_13_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__ObjetsRecusAssignment_13_1"


    // $ANTLR start "rule__Action__ObjetsConsoAssignment_16_1"
    // InternalGame.g:8032:1: rule__Action__ObjetsConsoAssignment_16_1 : ( ( RULE_ID ) ) ;
    public final void rule__Action__ObjetsConsoAssignment_16_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8036:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:8037:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:8037:2: ( ( RULE_ID ) )
            // InternalGame.g:8038:3: ( RULE_ID )
            {
             before(grammarAccess.getActionAccess().getObjetsConsoObjetCrossReference_16_1_0()); 
            // InternalGame.g:8039:3: ( RULE_ID )
            // InternalGame.g:8040:4: RULE_ID
            {
             before(grammarAccess.getActionAccess().getObjetsConsoObjetIDTerminalRuleCall_16_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getObjetsConsoObjetIDTerminalRuleCall_16_1_0_1()); 

            }

             after(grammarAccess.getActionAccess().getObjetsConsoObjetCrossReference_16_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__ObjetsConsoAssignment_16_1"


    // $ANTLR start "rule__Action__DescriptionsAssignment_19_1"
    // InternalGame.g:8051:1: rule__Action__DescriptionsAssignment_19_1 : ( ruleDescription ) ;
    public final void rule__Action__DescriptionsAssignment_19_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8055:1: ( ( ruleDescription ) )
            // InternalGame.g:8056:2: ( ruleDescription )
            {
            // InternalGame.g:8056:2: ( ruleDescription )
            // InternalGame.g:8057:3: ruleDescription
            {
             before(grammarAccess.getActionAccess().getDescriptionsDescriptionParserRuleCall_19_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getActionAccess().getDescriptionsDescriptionParserRuleCall_19_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__DescriptionsAssignment_19_1"


    // $ANTLR start "rule__Description__NameAssignment_0"
    // InternalGame.g:8066:1: rule__Description__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Description__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8070:1: ( ( RULE_ID ) )
            // InternalGame.g:8071:2: ( RULE_ID )
            {
            // InternalGame.g:8071:2: ( RULE_ID )
            // InternalGame.g:8072:3: RULE_ID
            {
             before(grammarAccess.getDescriptionAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getDescriptionAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__NameAssignment_0"


    // $ANTLR start "rule__Description__TexteAssignment_4"
    // InternalGame.g:8081:1: rule__Description__TexteAssignment_4 : ( RULE_STRING ) ;
    public final void rule__Description__TexteAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8085:1: ( ( RULE_STRING ) )
            // InternalGame.g:8086:2: ( RULE_STRING )
            {
            // InternalGame.g:8086:2: ( RULE_STRING )
            // InternalGame.g:8087:3: RULE_STRING
            {
             before(grammarAccess.getDescriptionAccess().getTexteSTRINGTerminalRuleCall_4_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getDescriptionAccess().getTexteSTRINGTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__TexteAssignment_4"


    // $ANTLR start "rule__Description__ConditionAssignment_7"
    // InternalGame.g:8096:1: rule__Description__ConditionAssignment_7 : ( ruleCondition ) ;
    public final void rule__Description__ConditionAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8100:1: ( ( ruleCondition ) )
            // InternalGame.g:8101:2: ( ruleCondition )
            {
            // InternalGame.g:8101:2: ( ruleCondition )
            // InternalGame.g:8102:3: ruleCondition
            {
             before(grammarAccess.getDescriptionAccess().getConditionConditionParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getDescriptionAccess().getConditionConditionParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__ConditionAssignment_7"


    // $ANTLR start "rule__Condition__ConditionAssignment_0"
    // InternalGame.g:8111:1: rule__Condition__ConditionAssignment_0 : ( ruleConditionEt ) ;
    public final void rule__Condition__ConditionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8115:1: ( ( ruleConditionEt ) )
            // InternalGame.g:8116:2: ( ruleConditionEt )
            {
            // InternalGame.g:8116:2: ( ruleConditionEt )
            // InternalGame.g:8117:3: ruleConditionEt
            {
             before(grammarAccess.getConditionAccess().getConditionConditionEtParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleConditionEt();

            state._fsp--;

             after(grammarAccess.getConditionAccess().getConditionConditionEtParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__ConditionAssignment_0"


    // $ANTLR start "rule__Condition__ConditionAssignment_1_1"
    // InternalGame.g:8126:1: rule__Condition__ConditionAssignment_1_1 : ( ruleConditionEt ) ;
    public final void rule__Condition__ConditionAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8130:1: ( ( ruleConditionEt ) )
            // InternalGame.g:8131:2: ( ruleConditionEt )
            {
            // InternalGame.g:8131:2: ( ruleConditionEt )
            // InternalGame.g:8132:3: ruleConditionEt
            {
             before(grammarAccess.getConditionAccess().getConditionConditionEtParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleConditionEt();

            state._fsp--;

             after(grammarAccess.getConditionAccess().getConditionConditionEtParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__ConditionAssignment_1_1"


    // $ANTLR start "rule__ConditionEt__ConditionTestAssignment_0"
    // InternalGame.g:8141:1: rule__ConditionEt__ConditionTestAssignment_0 : ( ruleConditionTest ) ;
    public final void rule__ConditionEt__ConditionTestAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8145:1: ( ( ruleConditionTest ) )
            // InternalGame.g:8146:2: ( ruleConditionTest )
            {
            // InternalGame.g:8146:2: ( ruleConditionTest )
            // InternalGame.g:8147:3: ruleConditionTest
            {
             before(grammarAccess.getConditionEtAccess().getConditionTestConditionTestParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleConditionTest();

            state._fsp--;

             after(grammarAccess.getConditionEtAccess().getConditionTestConditionTestParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionEt__ConditionTestAssignment_0"


    // $ANTLR start "rule__ConditionEt__ConditionTestAssignment_1_1"
    // InternalGame.g:8156:1: rule__ConditionEt__ConditionTestAssignment_1_1 : ( ruleConditionTest ) ;
    public final void rule__ConditionEt__ConditionTestAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8160:1: ( ( ruleConditionTest ) )
            // InternalGame.g:8161:2: ( ruleConditionTest )
            {
            // InternalGame.g:8161:2: ( ruleConditionTest )
            // InternalGame.g:8162:3: ruleConditionTest
            {
             before(grammarAccess.getConditionEtAccess().getConditionTestConditionTestParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleConditionTest();

            state._fsp--;

             after(grammarAccess.getConditionEtAccess().getConditionTestConditionTestParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionEt__ConditionTestAssignment_1_1"


    // $ANTLR start "rule__ConditionBoolean__ValeurAssignment"
    // InternalGame.g:8171:1: rule__ConditionBoolean__ValeurAssignment : ( RULE_BOOLEAN ) ;
    public final void rule__ConditionBoolean__ValeurAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8175:1: ( ( RULE_BOOLEAN ) )
            // InternalGame.g:8176:2: ( RULE_BOOLEAN )
            {
            // InternalGame.g:8176:2: ( RULE_BOOLEAN )
            // InternalGame.g:8177:3: RULE_BOOLEAN
            {
             before(grammarAccess.getConditionBooleanAccess().getValeurBOOLEANTerminalRuleCall_0()); 
            match(input,RULE_BOOLEAN,FOLLOW_2); 
             after(grammarAccess.getConditionBooleanAccess().getValeurBOOLEANTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionBoolean__ValeurAssignment"


    // $ANTLR start "rule__ConditionConnaissance__NegationAssignment_0"
    // InternalGame.g:8186:1: rule__ConditionConnaissance__NegationAssignment_0 : ( ( '!' ) ) ;
    public final void rule__ConditionConnaissance__NegationAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8190:1: ( ( ( '!' ) ) )
            // InternalGame.g:8191:2: ( ( '!' ) )
            {
            // InternalGame.g:8191:2: ( ( '!' ) )
            // InternalGame.g:8192:3: ( '!' )
            {
             before(grammarAccess.getConditionConnaissanceAccess().getNegationExclamationMarkKeyword_0_0()); 
            // InternalGame.g:8193:3: ( '!' )
            // InternalGame.g:8194:4: '!'
            {
             before(grammarAccess.getConditionConnaissanceAccess().getNegationExclamationMarkKeyword_0_0()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getConditionConnaissanceAccess().getNegationExclamationMarkKeyword_0_0()); 

            }

             after(grammarAccess.getConditionConnaissanceAccess().getNegationExclamationMarkKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionConnaissance__NegationAssignment_0"


    // $ANTLR start "rule__ConditionConnaissance__ConnaissanceAssignment_1"
    // InternalGame.g:8205:1: rule__ConditionConnaissance__ConnaissanceAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__ConditionConnaissance__ConnaissanceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8209:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:8210:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:8210:2: ( ( RULE_ID ) )
            // InternalGame.g:8211:3: ( RULE_ID )
            {
             before(grammarAccess.getConditionConnaissanceAccess().getConnaissanceConnaissanceCrossReference_1_0()); 
            // InternalGame.g:8212:3: ( RULE_ID )
            // InternalGame.g:8213:4: RULE_ID
            {
             before(grammarAccess.getConditionConnaissanceAccess().getConnaissanceConnaissanceIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getConditionConnaissanceAccess().getConnaissanceConnaissanceIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getConditionConnaissanceAccess().getConnaissanceConnaissanceCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionConnaissance__ConnaissanceAssignment_1"


    // $ANTLR start "rule__ConditionObjet__ObjetAssignment_0"
    // InternalGame.g:8224:1: rule__ConditionObjet__ObjetAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__ConditionObjet__ObjetAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8228:1: ( ( ( RULE_ID ) ) )
            // InternalGame.g:8229:2: ( ( RULE_ID ) )
            {
            // InternalGame.g:8229:2: ( ( RULE_ID ) )
            // InternalGame.g:8230:3: ( RULE_ID )
            {
             before(grammarAccess.getConditionObjetAccess().getObjetObjetCrossReference_0_0()); 
            // InternalGame.g:8231:3: ( RULE_ID )
            // InternalGame.g:8232:4: RULE_ID
            {
             before(grammarAccess.getConditionObjetAccess().getObjetObjetIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getConditionObjetAccess().getObjetObjetIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getConditionObjetAccess().getObjetObjetCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionObjet__ObjetAssignment_0"


    // $ANTLR start "rule__ConditionObjet__ComparateurAssignment_1"
    // InternalGame.g:8243:1: rule__ConditionObjet__ComparateurAssignment_1 : ( RULE_COMPARATEUR ) ;
    public final void rule__ConditionObjet__ComparateurAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8247:1: ( ( RULE_COMPARATEUR ) )
            // InternalGame.g:8248:2: ( RULE_COMPARATEUR )
            {
            // InternalGame.g:8248:2: ( RULE_COMPARATEUR )
            // InternalGame.g:8249:3: RULE_COMPARATEUR
            {
             before(grammarAccess.getConditionObjetAccess().getComparateurCOMPARATEURTerminalRuleCall_1_0()); 
            match(input,RULE_COMPARATEUR,FOLLOW_2); 
             after(grammarAccess.getConditionObjetAccess().getComparateurCOMPARATEURTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionObjet__ComparateurAssignment_1"


    // $ANTLR start "rule__ConditionObjet__NombreAssignment_2"
    // InternalGame.g:8258:1: rule__ConditionObjet__NombreAssignment_2 : ( RULE_INT ) ;
    public final void rule__ConditionObjet__NombreAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGame.g:8262:1: ( ( RULE_INT ) )
            // InternalGame.g:8263:2: ( RULE_INT )
            {
            // InternalGame.g:8263:2: ( RULE_INT )
            // InternalGame.g:8264:3: RULE_INT
            {
             before(grammarAccess.getConditionObjetAccess().getNombreINTTerminalRuleCall_2_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getConditionObjetAccess().getNombreINTTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionObjet__NombreAssignment_2"

    // Delegated rules


    protected DFA22 dfa22 = new DFA22(this);
    protected DFA26 dfa26 = new DFA26(this);
    static final String dfa_1s = "\43\uffff";
    static final String dfa_2s = "\1\1\42\uffff";
    static final String dfa_3s = "\1\23\1\uffff\1\4\1\16\1\26\1\16\1\4\1\33\1\4\1\10\2\4\1\uffff\1\33\1\5\1\33\1\4\1\10\1\33\1\4\1\10\2\33\1\5\1\4\1\33\1\5\2\33\1\4\1\10\2\33\1\5\1\33";
    static final String dfa_4s = "\1\24\1\uffff\1\4\1\16\1\26\1\16\1\57\1\56\1\4\1\56\2\57\1\uffff\1\56\1\5\1\56\1\4\2\56\1\4\3\56\1\5\1\57\1\56\1\5\2\56\1\4\3\56\1\5\1\56";
    static final String dfa_5s = "\1\uffff\1\2\12\uffff\1\1\26\uffff";
    static final String dfa_6s = "\43\uffff}>";
    static final String[] dfa_7s = {
            "\1\1\1\2",
            "",
            "\1\3",
            "\1\4",
            "\1\5",
            "\1\6",
            "\1\11\2\uffff\1\7\47\uffff\1\10",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\15",
            "\1\16\22\uffff\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\21\2\uffff\1\17\47\uffff\1\20",
            "\1\24\2\uffff\1\22\47\uffff\1\23",
            "",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\25",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\26",
            "\1\27\22\uffff\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\30",
            "\1\31",
            "\1\32\22\uffff\1\14\12\uffff\1\1\6\uffff\1\13\1\30",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\33",
            "\1\36\2\uffff\1\34\47\uffff\1\35",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\30",
            "\1\37",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\30",
            "\1\40",
            "\1\41\22\uffff\1\14\12\uffff\1\1\6\uffff\1\13\1\30",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\30",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\30",
            "\1\42",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\30"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA22 extends DFA {

        public DFA22(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 22;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "()* loopback of 5063:3: ( rule__Personne__Group_10__0 )*";
        }
    }
    static final String[] dfa_8s = {
            "\1\1\1\2",
            "",
            "\1\3",
            "\1\4",
            "\1\5",
            "\1\6",
            "\1\11\2\uffff\1\7\47\uffff\1\10",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\15",
            "\1\16\22\uffff\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\21\2\uffff\1\17\47\uffff\1\20",
            "\1\24\2\uffff\1\22\47\uffff\1\23",
            "",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\25",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\26",
            "\1\27\22\uffff\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30",
            "\1\31",
            "\1\32\22\uffff\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\33",
            "\1\36\2\uffff\1\34\47\uffff\1\35",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30",
            "\1\37",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30",
            "\1\40",
            "\1\41\22\uffff\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30",
            "\1\42",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30"
    };
    static final short[][] dfa_8 = unpackEncodedStringArray(dfa_8s);

    class DFA26 extends DFA {

        public DFA26(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 26;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_8;
        }
        public String getDescription() {
            return "()* loopback of 5583:3: ( rule__Interaction__Group_16__0 )*";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000108000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000110000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000120000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000800000000090L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000004100000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000010100000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000040100000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000900000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000008100000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000008000100000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000010000100000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000040000100000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000200000000002L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000400000000002L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000800000000010L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000000000100L});

}