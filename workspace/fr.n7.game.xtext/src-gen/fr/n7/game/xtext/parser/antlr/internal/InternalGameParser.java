package fr.n7.game.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.n7.game.xtext.services.GameGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalGameParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_BOOLEAN", "RULE_COMPARATEUR", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Objets'", "':'", "'-'", "'Transformations'", "'Connaissances'", "'Explorateur'", "'Personnes'", "'Territoire'", "'taille'", "'visible'", "'descriptions'", "'condition'", "'objets_in'", "'objets_out'", "'connaissances'", "'objets'", "'Lieux'", "'Chemins'", "'deposable'", "'depart'", "'fin'", "'personnes'", "'lieu_in'", "'lieu_out'", "'ouvert'", "'obligatoire'", "'objets_recus'", "'objets_conso'", "'interactions'", "'actions'", "'fin_interaction'", "'texte'", "'||'", "'&&'", "'!'"
    };
    public static final int RULE_BOOLEAN=7;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int RULE_COMPARATEUR=8;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalGameParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalGameParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalGameParser.tokenNames; }
    public String getGrammarFileName() { return "InternalGame.g"; }



     	private GameGrammarAccess grammarAccess;

        public InternalGameParser(TokenStream input, GameGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Jeu";
       	}

       	@Override
       	protected GameGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleJeu"
    // InternalGame.g:64:1: entryRuleJeu returns [EObject current=null] : iv_ruleJeu= ruleJeu EOF ;
    public final EObject entryRuleJeu() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJeu = null;


        try {
            // InternalGame.g:64:44: (iv_ruleJeu= ruleJeu EOF )
            // InternalGame.g:65:2: iv_ruleJeu= ruleJeu EOF
            {
             newCompositeNode(grammarAccess.getJeuRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleJeu=ruleJeu();

            state._fsp--;

             current =iv_ruleJeu; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJeu"


    // $ANTLR start "ruleJeu"
    // InternalGame.g:71:1: ruleJeu returns [EObject current=null] : (otherlv_0= 'Objets' otherlv_1= ':' (otherlv_2= '-' ( (lv_objets_3_0= ruleObjet ) ) )* otherlv_4= 'Transformations' otherlv_5= ':' (otherlv_6= '-' ( (lv_transformations_7_0= ruleTransformation ) ) )* otherlv_8= 'Connaissances' otherlv_9= ':' (otherlv_10= '-' ( (lv_connaissances_11_0= ruleConnaissance ) ) )* otherlv_12= 'Explorateur' otherlv_13= ':' ( (lv_explorateur_14_0= ruleExplorateur ) ) otherlv_15= 'Personnes' otherlv_16= ':' (otherlv_17= '-' ( (lv_personnes_18_0= rulePersonne ) ) )* otherlv_19= 'Territoire' otherlv_20= ':' ( (lv_territoire_21_0= ruleTerritoire ) ) ) ;
    public final EObject ruleJeu() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        EObject lv_objets_3_0 = null;

        EObject lv_transformations_7_0 = null;

        EObject lv_connaissances_11_0 = null;

        EObject lv_explorateur_14_0 = null;

        EObject lv_personnes_18_0 = null;

        EObject lv_territoire_21_0 = null;



        	enterRule();

        try {
            // InternalGame.g:77:2: ( (otherlv_0= 'Objets' otherlv_1= ':' (otherlv_2= '-' ( (lv_objets_3_0= ruleObjet ) ) )* otherlv_4= 'Transformations' otherlv_5= ':' (otherlv_6= '-' ( (lv_transformations_7_0= ruleTransformation ) ) )* otherlv_8= 'Connaissances' otherlv_9= ':' (otherlv_10= '-' ( (lv_connaissances_11_0= ruleConnaissance ) ) )* otherlv_12= 'Explorateur' otherlv_13= ':' ( (lv_explorateur_14_0= ruleExplorateur ) ) otherlv_15= 'Personnes' otherlv_16= ':' (otherlv_17= '-' ( (lv_personnes_18_0= rulePersonne ) ) )* otherlv_19= 'Territoire' otherlv_20= ':' ( (lv_territoire_21_0= ruleTerritoire ) ) ) )
            // InternalGame.g:78:2: (otherlv_0= 'Objets' otherlv_1= ':' (otherlv_2= '-' ( (lv_objets_3_0= ruleObjet ) ) )* otherlv_4= 'Transformations' otherlv_5= ':' (otherlv_6= '-' ( (lv_transformations_7_0= ruleTransformation ) ) )* otherlv_8= 'Connaissances' otherlv_9= ':' (otherlv_10= '-' ( (lv_connaissances_11_0= ruleConnaissance ) ) )* otherlv_12= 'Explorateur' otherlv_13= ':' ( (lv_explorateur_14_0= ruleExplorateur ) ) otherlv_15= 'Personnes' otherlv_16= ':' (otherlv_17= '-' ( (lv_personnes_18_0= rulePersonne ) ) )* otherlv_19= 'Territoire' otherlv_20= ':' ( (lv_territoire_21_0= ruleTerritoire ) ) )
            {
            // InternalGame.g:78:2: (otherlv_0= 'Objets' otherlv_1= ':' (otherlv_2= '-' ( (lv_objets_3_0= ruleObjet ) ) )* otherlv_4= 'Transformations' otherlv_5= ':' (otherlv_6= '-' ( (lv_transformations_7_0= ruleTransformation ) ) )* otherlv_8= 'Connaissances' otherlv_9= ':' (otherlv_10= '-' ( (lv_connaissances_11_0= ruleConnaissance ) ) )* otherlv_12= 'Explorateur' otherlv_13= ':' ( (lv_explorateur_14_0= ruleExplorateur ) ) otherlv_15= 'Personnes' otherlv_16= ':' (otherlv_17= '-' ( (lv_personnes_18_0= rulePersonne ) ) )* otherlv_19= 'Territoire' otherlv_20= ':' ( (lv_territoire_21_0= ruleTerritoire ) ) )
            // InternalGame.g:79:3: otherlv_0= 'Objets' otherlv_1= ':' (otherlv_2= '-' ( (lv_objets_3_0= ruleObjet ) ) )* otherlv_4= 'Transformations' otherlv_5= ':' (otherlv_6= '-' ( (lv_transformations_7_0= ruleTransformation ) ) )* otherlv_8= 'Connaissances' otherlv_9= ':' (otherlv_10= '-' ( (lv_connaissances_11_0= ruleConnaissance ) ) )* otherlv_12= 'Explorateur' otherlv_13= ':' ( (lv_explorateur_14_0= ruleExplorateur ) ) otherlv_15= 'Personnes' otherlv_16= ':' (otherlv_17= '-' ( (lv_personnes_18_0= rulePersonne ) ) )* otherlv_19= 'Territoire' otherlv_20= ':' ( (lv_territoire_21_0= ruleTerritoire ) )
            {
            otherlv_0=(Token)match(input,13,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getJeuAccess().getObjetsKeyword_0());
            		
            otherlv_1=(Token)match(input,14,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getJeuAccess().getColonKeyword_1());
            		
            // InternalGame.g:87:3: (otherlv_2= '-' ( (lv_objets_3_0= ruleObjet ) ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==15) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalGame.g:88:4: otherlv_2= '-' ( (lv_objets_3_0= ruleObjet ) )
            	    {
            	    otherlv_2=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_2, grammarAccess.getJeuAccess().getHyphenMinusKeyword_2_0());
            	    			
            	    // InternalGame.g:92:4: ( (lv_objets_3_0= ruleObjet ) )
            	    // InternalGame.g:93:5: (lv_objets_3_0= ruleObjet )
            	    {
            	    // InternalGame.g:93:5: (lv_objets_3_0= ruleObjet )
            	    // InternalGame.g:94:6: lv_objets_3_0= ruleObjet
            	    {

            	    						newCompositeNode(grammarAccess.getJeuAccess().getObjetsObjetParserRuleCall_2_1_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_objets_3_0=ruleObjet();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getJeuRule());
            	    						}
            	    						add(
            	    							current,
            	    							"objets",
            	    							lv_objets_3_0,
            	    							"fr.n7.game.xtext.Game.Objet");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_4=(Token)match(input,16,FOLLOW_3); 

            			newLeafNode(otherlv_4, grammarAccess.getJeuAccess().getTransformationsKeyword_3());
            		
            otherlv_5=(Token)match(input,14,FOLLOW_6); 

            			newLeafNode(otherlv_5, grammarAccess.getJeuAccess().getColonKeyword_4());
            		
            // InternalGame.g:120:3: (otherlv_6= '-' ( (lv_transformations_7_0= ruleTransformation ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==15) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalGame.g:121:4: otherlv_6= '-' ( (lv_transformations_7_0= ruleTransformation ) )
            	    {
            	    otherlv_6=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_6, grammarAccess.getJeuAccess().getHyphenMinusKeyword_5_0());
            	    			
            	    // InternalGame.g:125:4: ( (lv_transformations_7_0= ruleTransformation ) )
            	    // InternalGame.g:126:5: (lv_transformations_7_0= ruleTransformation )
            	    {
            	    // InternalGame.g:126:5: (lv_transformations_7_0= ruleTransformation )
            	    // InternalGame.g:127:6: lv_transformations_7_0= ruleTransformation
            	    {

            	    						newCompositeNode(grammarAccess.getJeuAccess().getTransformationsTransformationParserRuleCall_5_1_0());
            	    					
            	    pushFollow(FOLLOW_6);
            	    lv_transformations_7_0=ruleTransformation();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getJeuRule());
            	    						}
            	    						add(
            	    							current,
            	    							"transformations",
            	    							lv_transformations_7_0,
            	    							"fr.n7.game.xtext.Game.Transformation");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_8=(Token)match(input,17,FOLLOW_3); 

            			newLeafNode(otherlv_8, grammarAccess.getJeuAccess().getConnaissancesKeyword_6());
            		
            otherlv_9=(Token)match(input,14,FOLLOW_7); 

            			newLeafNode(otherlv_9, grammarAccess.getJeuAccess().getColonKeyword_7());
            		
            // InternalGame.g:153:3: (otherlv_10= '-' ( (lv_connaissances_11_0= ruleConnaissance ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==15) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalGame.g:154:4: otherlv_10= '-' ( (lv_connaissances_11_0= ruleConnaissance ) )
            	    {
            	    otherlv_10=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_10, grammarAccess.getJeuAccess().getHyphenMinusKeyword_8_0());
            	    			
            	    // InternalGame.g:158:4: ( (lv_connaissances_11_0= ruleConnaissance ) )
            	    // InternalGame.g:159:5: (lv_connaissances_11_0= ruleConnaissance )
            	    {
            	    // InternalGame.g:159:5: (lv_connaissances_11_0= ruleConnaissance )
            	    // InternalGame.g:160:6: lv_connaissances_11_0= ruleConnaissance
            	    {

            	    						newCompositeNode(grammarAccess.getJeuAccess().getConnaissancesConnaissanceParserRuleCall_8_1_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_connaissances_11_0=ruleConnaissance();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getJeuRule());
            	    						}
            	    						add(
            	    							current,
            	    							"connaissances",
            	    							lv_connaissances_11_0,
            	    							"fr.n7.game.xtext.Game.Connaissance");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_12=(Token)match(input,18,FOLLOW_3); 

            			newLeafNode(otherlv_12, grammarAccess.getJeuAccess().getExplorateurKeyword_9());
            		
            otherlv_13=(Token)match(input,14,FOLLOW_8); 

            			newLeafNode(otherlv_13, grammarAccess.getJeuAccess().getColonKeyword_10());
            		
            // InternalGame.g:186:3: ( (lv_explorateur_14_0= ruleExplorateur ) )
            // InternalGame.g:187:4: (lv_explorateur_14_0= ruleExplorateur )
            {
            // InternalGame.g:187:4: (lv_explorateur_14_0= ruleExplorateur )
            // InternalGame.g:188:5: lv_explorateur_14_0= ruleExplorateur
            {

            					newCompositeNode(grammarAccess.getJeuAccess().getExplorateurExplorateurParserRuleCall_11_0());
            				
            pushFollow(FOLLOW_9);
            lv_explorateur_14_0=ruleExplorateur();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getJeuRule());
            					}
            					set(
            						current,
            						"explorateur",
            						lv_explorateur_14_0,
            						"fr.n7.game.xtext.Game.Explorateur");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_15=(Token)match(input,19,FOLLOW_3); 

            			newLeafNode(otherlv_15, grammarAccess.getJeuAccess().getPersonnesKeyword_12());
            		
            otherlv_16=(Token)match(input,14,FOLLOW_10); 

            			newLeafNode(otherlv_16, grammarAccess.getJeuAccess().getColonKeyword_13());
            		
            // InternalGame.g:213:3: (otherlv_17= '-' ( (lv_personnes_18_0= rulePersonne ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==15) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalGame.g:214:4: otherlv_17= '-' ( (lv_personnes_18_0= rulePersonne ) )
            	    {
            	    otherlv_17=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_17, grammarAccess.getJeuAccess().getHyphenMinusKeyword_14_0());
            	    			
            	    // InternalGame.g:218:4: ( (lv_personnes_18_0= rulePersonne ) )
            	    // InternalGame.g:219:5: (lv_personnes_18_0= rulePersonne )
            	    {
            	    // InternalGame.g:219:5: (lv_personnes_18_0= rulePersonne )
            	    // InternalGame.g:220:6: lv_personnes_18_0= rulePersonne
            	    {

            	    						newCompositeNode(grammarAccess.getJeuAccess().getPersonnesPersonneParserRuleCall_14_1_0());
            	    					
            	    pushFollow(FOLLOW_10);
            	    lv_personnes_18_0=rulePersonne();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getJeuRule());
            	    						}
            	    						add(
            	    							current,
            	    							"personnes",
            	    							lv_personnes_18_0,
            	    							"fr.n7.game.xtext.Game.Personne");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_19=(Token)match(input,20,FOLLOW_3); 

            			newLeafNode(otherlv_19, grammarAccess.getJeuAccess().getTerritoireKeyword_15());
            		
            otherlv_20=(Token)match(input,14,FOLLOW_11); 

            			newLeafNode(otherlv_20, grammarAccess.getJeuAccess().getColonKeyword_16());
            		
            // InternalGame.g:246:3: ( (lv_territoire_21_0= ruleTerritoire ) )
            // InternalGame.g:247:4: (lv_territoire_21_0= ruleTerritoire )
            {
            // InternalGame.g:247:4: (lv_territoire_21_0= ruleTerritoire )
            // InternalGame.g:248:5: lv_territoire_21_0= ruleTerritoire
            {

            					newCompositeNode(grammarAccess.getJeuAccess().getTerritoireTerritoireParserRuleCall_17_0());
            				
            pushFollow(FOLLOW_2);
            lv_territoire_21_0=ruleTerritoire();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getJeuRule());
            					}
            					set(
            						current,
            						"territoire",
            						lv_territoire_21_0,
            						"fr.n7.game.xtext.Game.Territoire");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJeu"


    // $ANTLR start "entryRuleObjet"
    // InternalGame.g:269:1: entryRuleObjet returns [EObject current=null] : iv_ruleObjet= ruleObjet EOF ;
    public final EObject entryRuleObjet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjet = null;


        try {
            // InternalGame.g:269:46: (iv_ruleObjet= ruleObjet EOF )
            // InternalGame.g:270:2: iv_ruleObjet= ruleObjet EOF
            {
             newCompositeNode(grammarAccess.getObjetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleObjet=ruleObjet();

            state._fsp--;

             current =iv_ruleObjet; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjet"


    // $ANTLR start "ruleObjet"
    // InternalGame.g:276:1: ruleObjet returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'taille' otherlv_3= ':' ( (lv_taille_4_0= RULE_INT ) ) otherlv_5= 'visible' otherlv_6= ':' ( (lv_visible_7_0= ruleCondition ) ) otherlv_8= 'descriptions' otherlv_9= ':' (otherlv_10= '-' ( (lv_descriptions_11_0= ruleDescription ) ) )+ ) ;
    public final EObject ruleObjet() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_taille_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        EObject lv_visible_7_0 = null;

        EObject lv_descriptions_11_0 = null;



        	enterRule();

        try {
            // InternalGame.g:282:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'taille' otherlv_3= ':' ( (lv_taille_4_0= RULE_INT ) ) otherlv_5= 'visible' otherlv_6= ':' ( (lv_visible_7_0= ruleCondition ) ) otherlv_8= 'descriptions' otherlv_9= ':' (otherlv_10= '-' ( (lv_descriptions_11_0= ruleDescription ) ) )+ ) )
            // InternalGame.g:283:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'taille' otherlv_3= ':' ( (lv_taille_4_0= RULE_INT ) ) otherlv_5= 'visible' otherlv_6= ':' ( (lv_visible_7_0= ruleCondition ) ) otherlv_8= 'descriptions' otherlv_9= ':' (otherlv_10= '-' ( (lv_descriptions_11_0= ruleDescription ) ) )+ )
            {
            // InternalGame.g:283:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'taille' otherlv_3= ':' ( (lv_taille_4_0= RULE_INT ) ) otherlv_5= 'visible' otherlv_6= ':' ( (lv_visible_7_0= ruleCondition ) ) otherlv_8= 'descriptions' otherlv_9= ':' (otherlv_10= '-' ( (lv_descriptions_11_0= ruleDescription ) ) )+ )
            // InternalGame.g:284:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'taille' otherlv_3= ':' ( (lv_taille_4_0= RULE_INT ) ) otherlv_5= 'visible' otherlv_6= ':' ( (lv_visible_7_0= ruleCondition ) ) otherlv_8= 'descriptions' otherlv_9= ':' (otherlv_10= '-' ( (lv_descriptions_11_0= ruleDescription ) ) )+
            {
            // InternalGame.g:284:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGame.g:285:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGame.g:285:4: (lv_name_0_0= RULE_ID )
            // InternalGame.g:286:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_name_0_0, grammarAccess.getObjetAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getObjetRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_8); 

            			newLeafNode(otherlv_1, grammarAccess.getObjetAccess().getColonKeyword_1());
            		
            otherlv_2=(Token)match(input,21,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getObjetAccess().getTailleKeyword_2());
            		
            otherlv_3=(Token)match(input,14,FOLLOW_12); 

            			newLeafNode(otherlv_3, grammarAccess.getObjetAccess().getColonKeyword_3());
            		
            // InternalGame.g:314:3: ( (lv_taille_4_0= RULE_INT ) )
            // InternalGame.g:315:4: (lv_taille_4_0= RULE_INT )
            {
            // InternalGame.g:315:4: (lv_taille_4_0= RULE_INT )
            // InternalGame.g:316:5: lv_taille_4_0= RULE_INT
            {
            lv_taille_4_0=(Token)match(input,RULE_INT,FOLLOW_13); 

            					newLeafNode(lv_taille_4_0, grammarAccess.getObjetAccess().getTailleINTTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getObjetRule());
            					}
            					setWithLastConsumed(
            						current,
            						"taille",
            						lv_taille_4_0,
            						"org.eclipse.xtext.common.Terminals.INT");
            				

            }


            }

            otherlv_5=(Token)match(input,22,FOLLOW_3); 

            			newLeafNode(otherlv_5, grammarAccess.getObjetAccess().getVisibleKeyword_5());
            		
            otherlv_6=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_6, grammarAccess.getObjetAccess().getColonKeyword_6());
            		
            // InternalGame.g:340:3: ( (lv_visible_7_0= ruleCondition ) )
            // InternalGame.g:341:4: (lv_visible_7_0= ruleCondition )
            {
            // InternalGame.g:341:4: (lv_visible_7_0= ruleCondition )
            // InternalGame.g:342:5: lv_visible_7_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getObjetAccess().getVisibleConditionParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_15);
            lv_visible_7_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getObjetRule());
            					}
            					set(
            						current,
            						"visible",
            						lv_visible_7_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_8=(Token)match(input,23,FOLLOW_3); 

            			newLeafNode(otherlv_8, grammarAccess.getObjetAccess().getDescriptionsKeyword_8());
            		
            otherlv_9=(Token)match(input,14,FOLLOW_16); 

            			newLeafNode(otherlv_9, grammarAccess.getObjetAccess().getColonKeyword_9());
            		
            // InternalGame.g:367:3: (otherlv_10= '-' ( (lv_descriptions_11_0= ruleDescription ) ) )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==15) ) {
                    int LA5_2 = input.LA(2);

                    if ( (LA5_2==RULE_ID) ) {
                        int LA5_3 = input.LA(3);

                        if ( (LA5_3==14) ) {
                            int LA5_4 = input.LA(4);

                            if ( (LA5_4==44) ) {
                                alt5=1;
                            }


                        }


                    }


                }


                switch (alt5) {
            	case 1 :
            	    // InternalGame.g:368:4: otherlv_10= '-' ( (lv_descriptions_11_0= ruleDescription ) )
            	    {
            	    otherlv_10=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_10, grammarAccess.getObjetAccess().getHyphenMinusKeyword_10_0());
            	    			
            	    // InternalGame.g:372:4: ( (lv_descriptions_11_0= ruleDescription ) )
            	    // InternalGame.g:373:5: (lv_descriptions_11_0= ruleDescription )
            	    {
            	    // InternalGame.g:373:5: (lv_descriptions_11_0= ruleDescription )
            	    // InternalGame.g:374:6: lv_descriptions_11_0= ruleDescription
            	    {

            	    						newCompositeNode(grammarAccess.getObjetAccess().getDescriptionsDescriptionParserRuleCall_10_1_0());
            	    					
            	    pushFollow(FOLLOW_17);
            	    lv_descriptions_11_0=ruleDescription();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getObjetRule());
            	    						}
            	    						add(
            	    							current,
            	    							"descriptions",
            	    							lv_descriptions_11_0,
            	    							"fr.n7.game.xtext.Game.Description");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjet"


    // $ANTLR start "entryRuleTransformation"
    // InternalGame.g:396:1: entryRuleTransformation returns [EObject current=null] : iv_ruleTransformation= ruleTransformation EOF ;
    public final EObject entryRuleTransformation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransformation = null;


        try {
            // InternalGame.g:396:55: (iv_ruleTransformation= ruleTransformation EOF )
            // InternalGame.g:397:2: iv_ruleTransformation= ruleTransformation EOF
            {
             newCompositeNode(grammarAccess.getTransformationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransformation=ruleTransformation();

            state._fsp--;

             current =iv_ruleTransformation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransformation"


    // $ANTLR start "ruleTransformation"
    // InternalGame.g:403:1: ruleTransformation returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'condition' otherlv_3= ':' ( (lv_condition_4_0= ruleCondition ) ) otherlv_5= 'objets_in' otherlv_6= ':' (otherlv_7= '-' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'objets_out' otherlv_10= ':' (otherlv_11= '-' ( (otherlv_12= RULE_ID ) ) )* ) ;
    public final EObject ruleTransformation() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        EObject lv_condition_4_0 = null;



        	enterRule();

        try {
            // InternalGame.g:409:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'condition' otherlv_3= ':' ( (lv_condition_4_0= ruleCondition ) ) otherlv_5= 'objets_in' otherlv_6= ':' (otherlv_7= '-' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'objets_out' otherlv_10= ':' (otherlv_11= '-' ( (otherlv_12= RULE_ID ) ) )* ) )
            // InternalGame.g:410:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'condition' otherlv_3= ':' ( (lv_condition_4_0= ruleCondition ) ) otherlv_5= 'objets_in' otherlv_6= ':' (otherlv_7= '-' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'objets_out' otherlv_10= ':' (otherlv_11= '-' ( (otherlv_12= RULE_ID ) ) )* )
            {
            // InternalGame.g:410:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'condition' otherlv_3= ':' ( (lv_condition_4_0= ruleCondition ) ) otherlv_5= 'objets_in' otherlv_6= ':' (otherlv_7= '-' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'objets_out' otherlv_10= ':' (otherlv_11= '-' ( (otherlv_12= RULE_ID ) ) )* )
            // InternalGame.g:411:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'condition' otherlv_3= ':' ( (lv_condition_4_0= ruleCondition ) ) otherlv_5= 'objets_in' otherlv_6= ':' (otherlv_7= '-' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'objets_out' otherlv_10= ':' (otherlv_11= '-' ( (otherlv_12= RULE_ID ) ) )*
            {
            // InternalGame.g:411:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGame.g:412:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGame.g:412:4: (lv_name_0_0= RULE_ID )
            // InternalGame.g:413:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_name_0_0, grammarAccess.getTransformationAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransformationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_18); 

            			newLeafNode(otherlv_1, grammarAccess.getTransformationAccess().getColonKeyword_1());
            		
            otherlv_2=(Token)match(input,24,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getTransformationAccess().getConditionKeyword_2());
            		
            otherlv_3=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_3, grammarAccess.getTransformationAccess().getColonKeyword_3());
            		
            // InternalGame.g:441:3: ( (lv_condition_4_0= ruleCondition ) )
            // InternalGame.g:442:4: (lv_condition_4_0= ruleCondition )
            {
            // InternalGame.g:442:4: (lv_condition_4_0= ruleCondition )
            // InternalGame.g:443:5: lv_condition_4_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getTransformationAccess().getConditionConditionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_19);
            lv_condition_4_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTransformationRule());
            					}
            					set(
            						current,
            						"condition",
            						lv_condition_4_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,25,FOLLOW_3); 

            			newLeafNode(otherlv_5, grammarAccess.getTransformationAccess().getObjets_inKeyword_5());
            		
            otherlv_6=(Token)match(input,14,FOLLOW_20); 

            			newLeafNode(otherlv_6, grammarAccess.getTransformationAccess().getColonKeyword_6());
            		
            // InternalGame.g:468:3: (otherlv_7= '-' ( (otherlv_8= RULE_ID ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==15) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalGame.g:469:4: otherlv_7= '-' ( (otherlv_8= RULE_ID ) )
            	    {
            	    otherlv_7=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_7, grammarAccess.getTransformationAccess().getHyphenMinusKeyword_7_0());
            	    			
            	    // InternalGame.g:473:4: ( (otherlv_8= RULE_ID ) )
            	    // InternalGame.g:474:5: (otherlv_8= RULE_ID )
            	    {
            	    // InternalGame.g:474:5: (otherlv_8= RULE_ID )
            	    // InternalGame.g:475:6: otherlv_8= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getTransformationRule());
            	    						}
            	    					
            	    otherlv_8=(Token)match(input,RULE_ID,FOLLOW_20); 

            	    						newLeafNode(otherlv_8, grammarAccess.getTransformationAccess().getObjetsInObjetCrossReference_7_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            otherlv_9=(Token)match(input,26,FOLLOW_3); 

            			newLeafNode(otherlv_9, grammarAccess.getTransformationAccess().getObjets_outKeyword_8());
            		
            otherlv_10=(Token)match(input,14,FOLLOW_17); 

            			newLeafNode(otherlv_10, grammarAccess.getTransformationAccess().getColonKeyword_9());
            		
            // InternalGame.g:495:3: (otherlv_11= '-' ( (otherlv_12= RULE_ID ) ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==15) ) {
                    int LA7_2 = input.LA(2);

                    if ( (LA7_2==RULE_ID) ) {
                        int LA7_3 = input.LA(3);

                        if ( (LA7_3==EOF||LA7_3==15||LA7_3==17) ) {
                            alt7=1;
                        }


                    }


                }


                switch (alt7) {
            	case 1 :
            	    // InternalGame.g:496:4: otherlv_11= '-' ( (otherlv_12= RULE_ID ) )
            	    {
            	    otherlv_11=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_11, grammarAccess.getTransformationAccess().getHyphenMinusKeyword_10_0());
            	    			
            	    // InternalGame.g:500:4: ( (otherlv_12= RULE_ID ) )
            	    // InternalGame.g:501:5: (otherlv_12= RULE_ID )
            	    {
            	    // InternalGame.g:501:5: (otherlv_12= RULE_ID )
            	    // InternalGame.g:502:6: otherlv_12= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getTransformationRule());
            	    						}
            	    					
            	    otherlv_12=(Token)match(input,RULE_ID,FOLLOW_17); 

            	    						newLeafNode(otherlv_12, grammarAccess.getTransformationAccess().getObjetsOutObjetCrossReference_10_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransformation"


    // $ANTLR start "entryRuleConnaissance"
    // InternalGame.g:518:1: entryRuleConnaissance returns [EObject current=null] : iv_ruleConnaissance= ruleConnaissance EOF ;
    public final EObject entryRuleConnaissance() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConnaissance = null;


        try {
            // InternalGame.g:518:53: (iv_ruleConnaissance= ruleConnaissance EOF )
            // InternalGame.g:519:2: iv_ruleConnaissance= ruleConnaissance EOF
            {
             newCompositeNode(grammarAccess.getConnaissanceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConnaissance=ruleConnaissance();

            state._fsp--;

             current =iv_ruleConnaissance; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConnaissance"


    // $ANTLR start "ruleConnaissance"
    // InternalGame.g:525:1: ruleConnaissance returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'descriptions' otherlv_6= ':' (otherlv_7= '-' ( (lv_descriptions_8_0= ruleDescription ) ) )+ ) ;
    public final EObject ruleConnaissance() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_visible_4_0 = null;

        EObject lv_descriptions_8_0 = null;



        	enterRule();

        try {
            // InternalGame.g:531:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'descriptions' otherlv_6= ':' (otherlv_7= '-' ( (lv_descriptions_8_0= ruleDescription ) ) )+ ) )
            // InternalGame.g:532:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'descriptions' otherlv_6= ':' (otherlv_7= '-' ( (lv_descriptions_8_0= ruleDescription ) ) )+ )
            {
            // InternalGame.g:532:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'descriptions' otherlv_6= ':' (otherlv_7= '-' ( (lv_descriptions_8_0= ruleDescription ) ) )+ )
            // InternalGame.g:533:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'descriptions' otherlv_6= ':' (otherlv_7= '-' ( (lv_descriptions_8_0= ruleDescription ) ) )+
            {
            // InternalGame.g:533:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGame.g:534:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGame.g:534:4: (lv_name_0_0= RULE_ID )
            // InternalGame.g:535:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_name_0_0, grammarAccess.getConnaissanceAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConnaissanceRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getConnaissanceAccess().getColonKeyword_1());
            		
            otherlv_2=(Token)match(input,22,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getConnaissanceAccess().getVisibleKeyword_2());
            		
            otherlv_3=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_3, grammarAccess.getConnaissanceAccess().getColonKeyword_3());
            		
            // InternalGame.g:563:3: ( (lv_visible_4_0= ruleCondition ) )
            // InternalGame.g:564:4: (lv_visible_4_0= ruleCondition )
            {
            // InternalGame.g:564:4: (lv_visible_4_0= ruleCondition )
            // InternalGame.g:565:5: lv_visible_4_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getConnaissanceAccess().getVisibleConditionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_15);
            lv_visible_4_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConnaissanceRule());
            					}
            					set(
            						current,
            						"visible",
            						lv_visible_4_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,23,FOLLOW_3); 

            			newLeafNode(otherlv_5, grammarAccess.getConnaissanceAccess().getDescriptionsKeyword_5());
            		
            otherlv_6=(Token)match(input,14,FOLLOW_16); 

            			newLeafNode(otherlv_6, grammarAccess.getConnaissanceAccess().getColonKeyword_6());
            		
            // InternalGame.g:590:3: (otherlv_7= '-' ( (lv_descriptions_8_0= ruleDescription ) ) )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==15) ) {
                    int LA8_2 = input.LA(2);

                    if ( (LA8_2==RULE_ID) ) {
                        int LA8_3 = input.LA(3);

                        if ( (LA8_3==14) ) {
                            int LA8_4 = input.LA(4);

                            if ( (LA8_4==44) ) {
                                alt8=1;
                            }


                        }


                    }


                }


                switch (alt8) {
            	case 1 :
            	    // InternalGame.g:591:4: otherlv_7= '-' ( (lv_descriptions_8_0= ruleDescription ) )
            	    {
            	    otherlv_7=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_7, grammarAccess.getConnaissanceAccess().getHyphenMinusKeyword_7_0());
            	    			
            	    // InternalGame.g:595:4: ( (lv_descriptions_8_0= ruleDescription ) )
            	    // InternalGame.g:596:5: (lv_descriptions_8_0= ruleDescription )
            	    {
            	    // InternalGame.g:596:5: (lv_descriptions_8_0= ruleDescription )
            	    // InternalGame.g:597:6: lv_descriptions_8_0= ruleDescription
            	    {

            	    						newCompositeNode(grammarAccess.getConnaissanceAccess().getDescriptionsDescriptionParserRuleCall_7_1_0());
            	    					
            	    pushFollow(FOLLOW_17);
            	    lv_descriptions_8_0=ruleDescription();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getConnaissanceRule());
            	    						}
            	    						add(
            	    							current,
            	    							"descriptions",
            	    							lv_descriptions_8_0,
            	    							"fr.n7.game.xtext.Game.Description");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConnaissance"


    // $ANTLR start "entryRuleExplorateur"
    // InternalGame.g:619:1: entryRuleExplorateur returns [EObject current=null] : iv_ruleExplorateur= ruleExplorateur EOF ;
    public final EObject entryRuleExplorateur() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExplorateur = null;


        try {
            // InternalGame.g:619:52: (iv_ruleExplorateur= ruleExplorateur EOF )
            // InternalGame.g:620:2: iv_ruleExplorateur= ruleExplorateur EOF
            {
             newCompositeNode(grammarAccess.getExplorateurRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExplorateur=ruleExplorateur();

            state._fsp--;

             current =iv_ruleExplorateur; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExplorateur"


    // $ANTLR start "ruleExplorateur"
    // InternalGame.g:626:1: ruleExplorateur returns [EObject current=null] : (otherlv_0= 'taille' otherlv_1= ':' ( (lv_tailleInventaire_2_0= RULE_INT ) ) otherlv_3= 'connaissances' otherlv_4= ':' (otherlv_5= '-' ( (otherlv_6= RULE_ID ) ) )* otherlv_7= 'objets' otherlv_8= ':' (otherlv_9= '-' ( (otherlv_10= RULE_ID ) ) )* ) ;
    public final EObject ruleExplorateur() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_tailleInventaire_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;


        	enterRule();

        try {
            // InternalGame.g:632:2: ( (otherlv_0= 'taille' otherlv_1= ':' ( (lv_tailleInventaire_2_0= RULE_INT ) ) otherlv_3= 'connaissances' otherlv_4= ':' (otherlv_5= '-' ( (otherlv_6= RULE_ID ) ) )* otherlv_7= 'objets' otherlv_8= ':' (otherlv_9= '-' ( (otherlv_10= RULE_ID ) ) )* ) )
            // InternalGame.g:633:2: (otherlv_0= 'taille' otherlv_1= ':' ( (lv_tailleInventaire_2_0= RULE_INT ) ) otherlv_3= 'connaissances' otherlv_4= ':' (otherlv_5= '-' ( (otherlv_6= RULE_ID ) ) )* otherlv_7= 'objets' otherlv_8= ':' (otherlv_9= '-' ( (otherlv_10= RULE_ID ) ) )* )
            {
            // InternalGame.g:633:2: (otherlv_0= 'taille' otherlv_1= ':' ( (lv_tailleInventaire_2_0= RULE_INT ) ) otherlv_3= 'connaissances' otherlv_4= ':' (otherlv_5= '-' ( (otherlv_6= RULE_ID ) ) )* otherlv_7= 'objets' otherlv_8= ':' (otherlv_9= '-' ( (otherlv_10= RULE_ID ) ) )* )
            // InternalGame.g:634:3: otherlv_0= 'taille' otherlv_1= ':' ( (lv_tailleInventaire_2_0= RULE_INT ) ) otherlv_3= 'connaissances' otherlv_4= ':' (otherlv_5= '-' ( (otherlv_6= RULE_ID ) ) )* otherlv_7= 'objets' otherlv_8= ':' (otherlv_9= '-' ( (otherlv_10= RULE_ID ) ) )*
            {
            otherlv_0=(Token)match(input,21,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getExplorateurAccess().getTailleKeyword_0());
            		
            otherlv_1=(Token)match(input,14,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getExplorateurAccess().getColonKeyword_1());
            		
            // InternalGame.g:642:3: ( (lv_tailleInventaire_2_0= RULE_INT ) )
            // InternalGame.g:643:4: (lv_tailleInventaire_2_0= RULE_INT )
            {
            // InternalGame.g:643:4: (lv_tailleInventaire_2_0= RULE_INT )
            // InternalGame.g:644:5: lv_tailleInventaire_2_0= RULE_INT
            {
            lv_tailleInventaire_2_0=(Token)match(input,RULE_INT,FOLLOW_21); 

            					newLeafNode(lv_tailleInventaire_2_0, grammarAccess.getExplorateurAccess().getTailleInventaireINTTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getExplorateurRule());
            					}
            					setWithLastConsumed(
            						current,
            						"tailleInventaire",
            						lv_tailleInventaire_2_0,
            						"org.eclipse.xtext.common.Terminals.INT");
            				

            }


            }

            otherlv_3=(Token)match(input,27,FOLLOW_3); 

            			newLeafNode(otherlv_3, grammarAccess.getExplorateurAccess().getConnaissancesKeyword_3());
            		
            otherlv_4=(Token)match(input,14,FOLLOW_22); 

            			newLeafNode(otherlv_4, grammarAccess.getExplorateurAccess().getColonKeyword_4());
            		
            // InternalGame.g:668:3: (otherlv_5= '-' ( (otherlv_6= RULE_ID ) ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==15) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalGame.g:669:4: otherlv_5= '-' ( (otherlv_6= RULE_ID ) )
            	    {
            	    otherlv_5=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_5, grammarAccess.getExplorateurAccess().getHyphenMinusKeyword_5_0());
            	    			
            	    // InternalGame.g:673:4: ( (otherlv_6= RULE_ID ) )
            	    // InternalGame.g:674:5: (otherlv_6= RULE_ID )
            	    {
            	    // InternalGame.g:674:5: (otherlv_6= RULE_ID )
            	    // InternalGame.g:675:6: otherlv_6= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getExplorateurRule());
            	    						}
            	    					
            	    otherlv_6=(Token)match(input,RULE_ID,FOLLOW_22); 

            	    						newLeafNode(otherlv_6, grammarAccess.getExplorateurAccess().getConnaissancesConnaissanceCrossReference_5_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            otherlv_7=(Token)match(input,28,FOLLOW_3); 

            			newLeafNode(otherlv_7, grammarAccess.getExplorateurAccess().getObjetsKeyword_6());
            		
            otherlv_8=(Token)match(input,14,FOLLOW_17); 

            			newLeafNode(otherlv_8, grammarAccess.getExplorateurAccess().getColonKeyword_7());
            		
            // InternalGame.g:695:3: (otherlv_9= '-' ( (otherlv_10= RULE_ID ) ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==15) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalGame.g:696:4: otherlv_9= '-' ( (otherlv_10= RULE_ID ) )
            	    {
            	    otherlv_9=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_9, grammarAccess.getExplorateurAccess().getHyphenMinusKeyword_8_0());
            	    			
            	    // InternalGame.g:700:4: ( (otherlv_10= RULE_ID ) )
            	    // InternalGame.g:701:5: (otherlv_10= RULE_ID )
            	    {
            	    // InternalGame.g:701:5: (otherlv_10= RULE_ID )
            	    // InternalGame.g:702:6: otherlv_10= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getExplorateurRule());
            	    						}
            	    					
            	    otherlv_10=(Token)match(input,RULE_ID,FOLLOW_17); 

            	    						newLeafNode(otherlv_10, grammarAccess.getExplorateurAccess().getObjetsObjetCrossReference_8_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExplorateur"


    // $ANTLR start "entryRuleTerritoire"
    // InternalGame.g:718:1: entryRuleTerritoire returns [EObject current=null] : iv_ruleTerritoire= ruleTerritoire EOF ;
    public final EObject entryRuleTerritoire() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTerritoire = null;


        try {
            // InternalGame.g:718:51: (iv_ruleTerritoire= ruleTerritoire EOF )
            // InternalGame.g:719:2: iv_ruleTerritoire= ruleTerritoire EOF
            {
             newCompositeNode(grammarAccess.getTerritoireRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTerritoire=ruleTerritoire();

            state._fsp--;

             current =iv_ruleTerritoire; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTerritoire"


    // $ANTLR start "ruleTerritoire"
    // InternalGame.g:725:1: ruleTerritoire returns [EObject current=null] : ( () otherlv_1= 'Lieux' otherlv_2= ':' (otherlv_3= '-' ( (lv_lieux_4_0= ruleLieu ) ) )* otherlv_5= 'Chemins' otherlv_6= ':' (otherlv_7= '-' ( (lv_chemins_8_0= ruleChemin ) ) )* ) ;
    public final EObject ruleTerritoire() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_lieux_4_0 = null;

        EObject lv_chemins_8_0 = null;



        	enterRule();

        try {
            // InternalGame.g:731:2: ( ( () otherlv_1= 'Lieux' otherlv_2= ':' (otherlv_3= '-' ( (lv_lieux_4_0= ruleLieu ) ) )* otherlv_5= 'Chemins' otherlv_6= ':' (otherlv_7= '-' ( (lv_chemins_8_0= ruleChemin ) ) )* ) )
            // InternalGame.g:732:2: ( () otherlv_1= 'Lieux' otherlv_2= ':' (otherlv_3= '-' ( (lv_lieux_4_0= ruleLieu ) ) )* otherlv_5= 'Chemins' otherlv_6= ':' (otherlv_7= '-' ( (lv_chemins_8_0= ruleChemin ) ) )* )
            {
            // InternalGame.g:732:2: ( () otherlv_1= 'Lieux' otherlv_2= ':' (otherlv_3= '-' ( (lv_lieux_4_0= ruleLieu ) ) )* otherlv_5= 'Chemins' otherlv_6= ':' (otherlv_7= '-' ( (lv_chemins_8_0= ruleChemin ) ) )* )
            // InternalGame.g:733:3: () otherlv_1= 'Lieux' otherlv_2= ':' (otherlv_3= '-' ( (lv_lieux_4_0= ruleLieu ) ) )* otherlv_5= 'Chemins' otherlv_6= ':' (otherlv_7= '-' ( (lv_chemins_8_0= ruleChemin ) ) )*
            {
            // InternalGame.g:733:3: ()
            // InternalGame.g:734:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTerritoireAccess().getTerritoireAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,29,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getTerritoireAccess().getLieuxKeyword_1());
            		
            otherlv_2=(Token)match(input,14,FOLLOW_23); 

            			newLeafNode(otherlv_2, grammarAccess.getTerritoireAccess().getColonKeyword_2());
            		
            // InternalGame.g:748:3: (otherlv_3= '-' ( (lv_lieux_4_0= ruleLieu ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==15) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalGame.g:749:4: otherlv_3= '-' ( (lv_lieux_4_0= ruleLieu ) )
            	    {
            	    otherlv_3=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_3, grammarAccess.getTerritoireAccess().getHyphenMinusKeyword_3_0());
            	    			
            	    // InternalGame.g:753:4: ( (lv_lieux_4_0= ruleLieu ) )
            	    // InternalGame.g:754:5: (lv_lieux_4_0= ruleLieu )
            	    {
            	    // InternalGame.g:754:5: (lv_lieux_4_0= ruleLieu )
            	    // InternalGame.g:755:6: lv_lieux_4_0= ruleLieu
            	    {

            	    						newCompositeNode(grammarAccess.getTerritoireAccess().getLieuxLieuParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_23);
            	    lv_lieux_4_0=ruleLieu();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getTerritoireRule());
            	    						}
            	    						add(
            	    							current,
            	    							"lieux",
            	    							lv_lieux_4_0,
            	    							"fr.n7.game.xtext.Game.Lieu");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            otherlv_5=(Token)match(input,30,FOLLOW_3); 

            			newLeafNode(otherlv_5, grammarAccess.getTerritoireAccess().getCheminsKeyword_4());
            		
            otherlv_6=(Token)match(input,14,FOLLOW_17); 

            			newLeafNode(otherlv_6, grammarAccess.getTerritoireAccess().getColonKeyword_5());
            		
            // InternalGame.g:781:3: (otherlv_7= '-' ( (lv_chemins_8_0= ruleChemin ) ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==15) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalGame.g:782:4: otherlv_7= '-' ( (lv_chemins_8_0= ruleChemin ) )
            	    {
            	    otherlv_7=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_7, grammarAccess.getTerritoireAccess().getHyphenMinusKeyword_6_0());
            	    			
            	    // InternalGame.g:786:4: ( (lv_chemins_8_0= ruleChemin ) )
            	    // InternalGame.g:787:5: (lv_chemins_8_0= ruleChemin )
            	    {
            	    // InternalGame.g:787:5: (lv_chemins_8_0= ruleChemin )
            	    // InternalGame.g:788:6: lv_chemins_8_0= ruleChemin
            	    {

            	    						newCompositeNode(grammarAccess.getTerritoireAccess().getCheminsCheminParserRuleCall_6_1_0());
            	    					
            	    pushFollow(FOLLOW_17);
            	    lv_chemins_8_0=ruleChemin();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getTerritoireRule());
            	    						}
            	    						add(
            	    							current,
            	    							"chemins",
            	    							lv_chemins_8_0,
            	    							"fr.n7.game.xtext.Game.Chemin");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTerritoire"


    // $ANTLR start "entryRuleLieu"
    // InternalGame.g:810:1: entryRuleLieu returns [EObject current=null] : iv_ruleLieu= ruleLieu EOF ;
    public final EObject entryRuleLieu() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLieu = null;


        try {
            // InternalGame.g:810:45: (iv_ruleLieu= ruleLieu EOF )
            // InternalGame.g:811:2: iv_ruleLieu= ruleLieu EOF
            {
             newCompositeNode(grammarAccess.getLieuRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLieu=ruleLieu();

            state._fsp--;

             current =iv_ruleLieu; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLieu"


    // $ANTLR start "ruleLieu"
    // InternalGame.g:817:1: ruleLieu returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'deposable' otherlv_3= ':' ( (lv_deposable_4_0= ruleCondition ) ) otherlv_5= 'depart' otherlv_6= ':' ( (lv_depart_7_0= ruleCondition ) ) otherlv_8= 'fin' otherlv_9= ':' ( (lv_fin_10_0= ruleCondition ) ) otherlv_11= 'personnes' otherlv_12= ':' (otherlv_13= '-' ( (otherlv_14= RULE_ID ) ) )* otherlv_15= 'descriptions' otherlv_16= ':' (otherlv_17= '-' ( (lv_descriptions_18_0= ruleDescription ) ) )+ otherlv_19= 'objets' otherlv_20= ':' (otherlv_21= '-' ( (otherlv_22= RULE_ID ) ) )* otherlv_23= 'connaissances' otherlv_24= ':' (otherlv_25= '-' ( (otherlv_26= RULE_ID ) ) )* ) ;
    public final EObject ruleLieu() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        Token otherlv_26=null;
        EObject lv_deposable_4_0 = null;

        EObject lv_depart_7_0 = null;

        EObject lv_fin_10_0 = null;

        EObject lv_descriptions_18_0 = null;



        	enterRule();

        try {
            // InternalGame.g:823:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'deposable' otherlv_3= ':' ( (lv_deposable_4_0= ruleCondition ) ) otherlv_5= 'depart' otherlv_6= ':' ( (lv_depart_7_0= ruleCondition ) ) otherlv_8= 'fin' otherlv_9= ':' ( (lv_fin_10_0= ruleCondition ) ) otherlv_11= 'personnes' otherlv_12= ':' (otherlv_13= '-' ( (otherlv_14= RULE_ID ) ) )* otherlv_15= 'descriptions' otherlv_16= ':' (otherlv_17= '-' ( (lv_descriptions_18_0= ruleDescription ) ) )+ otherlv_19= 'objets' otherlv_20= ':' (otherlv_21= '-' ( (otherlv_22= RULE_ID ) ) )* otherlv_23= 'connaissances' otherlv_24= ':' (otherlv_25= '-' ( (otherlv_26= RULE_ID ) ) )* ) )
            // InternalGame.g:824:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'deposable' otherlv_3= ':' ( (lv_deposable_4_0= ruleCondition ) ) otherlv_5= 'depart' otherlv_6= ':' ( (lv_depart_7_0= ruleCondition ) ) otherlv_8= 'fin' otherlv_9= ':' ( (lv_fin_10_0= ruleCondition ) ) otherlv_11= 'personnes' otherlv_12= ':' (otherlv_13= '-' ( (otherlv_14= RULE_ID ) ) )* otherlv_15= 'descriptions' otherlv_16= ':' (otherlv_17= '-' ( (lv_descriptions_18_0= ruleDescription ) ) )+ otherlv_19= 'objets' otherlv_20= ':' (otherlv_21= '-' ( (otherlv_22= RULE_ID ) ) )* otherlv_23= 'connaissances' otherlv_24= ':' (otherlv_25= '-' ( (otherlv_26= RULE_ID ) ) )* )
            {
            // InternalGame.g:824:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'deposable' otherlv_3= ':' ( (lv_deposable_4_0= ruleCondition ) ) otherlv_5= 'depart' otherlv_6= ':' ( (lv_depart_7_0= ruleCondition ) ) otherlv_8= 'fin' otherlv_9= ':' ( (lv_fin_10_0= ruleCondition ) ) otherlv_11= 'personnes' otherlv_12= ':' (otherlv_13= '-' ( (otherlv_14= RULE_ID ) ) )* otherlv_15= 'descriptions' otherlv_16= ':' (otherlv_17= '-' ( (lv_descriptions_18_0= ruleDescription ) ) )+ otherlv_19= 'objets' otherlv_20= ':' (otherlv_21= '-' ( (otherlv_22= RULE_ID ) ) )* otherlv_23= 'connaissances' otherlv_24= ':' (otherlv_25= '-' ( (otherlv_26= RULE_ID ) ) )* )
            // InternalGame.g:825:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'deposable' otherlv_3= ':' ( (lv_deposable_4_0= ruleCondition ) ) otherlv_5= 'depart' otherlv_6= ':' ( (lv_depart_7_0= ruleCondition ) ) otherlv_8= 'fin' otherlv_9= ':' ( (lv_fin_10_0= ruleCondition ) ) otherlv_11= 'personnes' otherlv_12= ':' (otherlv_13= '-' ( (otherlv_14= RULE_ID ) ) )* otherlv_15= 'descriptions' otherlv_16= ':' (otherlv_17= '-' ( (lv_descriptions_18_0= ruleDescription ) ) )+ otherlv_19= 'objets' otherlv_20= ':' (otherlv_21= '-' ( (otherlv_22= RULE_ID ) ) )* otherlv_23= 'connaissances' otherlv_24= ':' (otherlv_25= '-' ( (otherlv_26= RULE_ID ) ) )*
            {
            // InternalGame.g:825:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGame.g:826:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGame.g:826:4: (lv_name_0_0= RULE_ID )
            // InternalGame.g:827:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_name_0_0, grammarAccess.getLieuAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getLieuRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_24); 

            			newLeafNode(otherlv_1, grammarAccess.getLieuAccess().getColonKeyword_1());
            		
            otherlv_2=(Token)match(input,31,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getLieuAccess().getDeposableKeyword_2());
            		
            otherlv_3=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_3, grammarAccess.getLieuAccess().getColonKeyword_3());
            		
            // InternalGame.g:855:3: ( (lv_deposable_4_0= ruleCondition ) )
            // InternalGame.g:856:4: (lv_deposable_4_0= ruleCondition )
            {
            // InternalGame.g:856:4: (lv_deposable_4_0= ruleCondition )
            // InternalGame.g:857:5: lv_deposable_4_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getLieuAccess().getDeposableConditionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_25);
            lv_deposable_4_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLieuRule());
            					}
            					set(
            						current,
            						"deposable",
            						lv_deposable_4_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,32,FOLLOW_3); 

            			newLeafNode(otherlv_5, grammarAccess.getLieuAccess().getDepartKeyword_5());
            		
            otherlv_6=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_6, grammarAccess.getLieuAccess().getColonKeyword_6());
            		
            // InternalGame.g:882:3: ( (lv_depart_7_0= ruleCondition ) )
            // InternalGame.g:883:4: (lv_depart_7_0= ruleCondition )
            {
            // InternalGame.g:883:4: (lv_depart_7_0= ruleCondition )
            // InternalGame.g:884:5: lv_depart_7_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getLieuAccess().getDepartConditionParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_26);
            lv_depart_7_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLieuRule());
            					}
            					set(
            						current,
            						"depart",
            						lv_depart_7_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_8=(Token)match(input,33,FOLLOW_3); 

            			newLeafNode(otherlv_8, grammarAccess.getLieuAccess().getFinKeyword_8());
            		
            otherlv_9=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_9, grammarAccess.getLieuAccess().getColonKeyword_9());
            		
            // InternalGame.g:909:3: ( (lv_fin_10_0= ruleCondition ) )
            // InternalGame.g:910:4: (lv_fin_10_0= ruleCondition )
            {
            // InternalGame.g:910:4: (lv_fin_10_0= ruleCondition )
            // InternalGame.g:911:5: lv_fin_10_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getLieuAccess().getFinConditionParserRuleCall_10_0());
            				
            pushFollow(FOLLOW_27);
            lv_fin_10_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLieuRule());
            					}
            					set(
            						current,
            						"fin",
            						lv_fin_10_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_11=(Token)match(input,34,FOLLOW_3); 

            			newLeafNode(otherlv_11, grammarAccess.getLieuAccess().getPersonnesKeyword_11());
            		
            otherlv_12=(Token)match(input,14,FOLLOW_28); 

            			newLeafNode(otherlv_12, grammarAccess.getLieuAccess().getColonKeyword_12());
            		
            // InternalGame.g:936:3: (otherlv_13= '-' ( (otherlv_14= RULE_ID ) ) )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==15) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalGame.g:937:4: otherlv_13= '-' ( (otherlv_14= RULE_ID ) )
            	    {
            	    otherlv_13=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_13, grammarAccess.getLieuAccess().getHyphenMinusKeyword_13_0());
            	    			
            	    // InternalGame.g:941:4: ( (otherlv_14= RULE_ID ) )
            	    // InternalGame.g:942:5: (otherlv_14= RULE_ID )
            	    {
            	    // InternalGame.g:942:5: (otherlv_14= RULE_ID )
            	    // InternalGame.g:943:6: otherlv_14= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getLieuRule());
            	    						}
            	    					
            	    otherlv_14=(Token)match(input,RULE_ID,FOLLOW_28); 

            	    						newLeafNode(otherlv_14, grammarAccess.getLieuAccess().getPersonnesPersonneCrossReference_13_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            otherlv_15=(Token)match(input,23,FOLLOW_3); 

            			newLeafNode(otherlv_15, grammarAccess.getLieuAccess().getDescriptionsKeyword_14());
            		
            otherlv_16=(Token)match(input,14,FOLLOW_16); 

            			newLeafNode(otherlv_16, grammarAccess.getLieuAccess().getColonKeyword_15());
            		
            // InternalGame.g:963:3: (otherlv_17= '-' ( (lv_descriptions_18_0= ruleDescription ) ) )+
            int cnt14=0;
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==15) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalGame.g:964:4: otherlv_17= '-' ( (lv_descriptions_18_0= ruleDescription ) )
            	    {
            	    otherlv_17=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_17, grammarAccess.getLieuAccess().getHyphenMinusKeyword_16_0());
            	    			
            	    // InternalGame.g:968:4: ( (lv_descriptions_18_0= ruleDescription ) )
            	    // InternalGame.g:969:5: (lv_descriptions_18_0= ruleDescription )
            	    {
            	    // InternalGame.g:969:5: (lv_descriptions_18_0= ruleDescription )
            	    // InternalGame.g:970:6: lv_descriptions_18_0= ruleDescription
            	    {

            	    						newCompositeNode(grammarAccess.getLieuAccess().getDescriptionsDescriptionParserRuleCall_16_1_0());
            	    					
            	    pushFollow(FOLLOW_22);
            	    lv_descriptions_18_0=ruleDescription();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getLieuRule());
            	    						}
            	    						add(
            	    							current,
            	    							"descriptions",
            	    							lv_descriptions_18_0,
            	    							"fr.n7.game.xtext.Game.Description");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt14 >= 1 ) break loop14;
                        EarlyExitException eee =
                            new EarlyExitException(14, input);
                        throw eee;
                }
                cnt14++;
            } while (true);

            otherlv_19=(Token)match(input,28,FOLLOW_3); 

            			newLeafNode(otherlv_19, grammarAccess.getLieuAccess().getObjetsKeyword_17());
            		
            otherlv_20=(Token)match(input,14,FOLLOW_29); 

            			newLeafNode(otherlv_20, grammarAccess.getLieuAccess().getColonKeyword_18());
            		
            // InternalGame.g:996:3: (otherlv_21= '-' ( (otherlv_22= RULE_ID ) ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==15) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalGame.g:997:4: otherlv_21= '-' ( (otherlv_22= RULE_ID ) )
            	    {
            	    otherlv_21=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_21, grammarAccess.getLieuAccess().getHyphenMinusKeyword_19_0());
            	    			
            	    // InternalGame.g:1001:4: ( (otherlv_22= RULE_ID ) )
            	    // InternalGame.g:1002:5: (otherlv_22= RULE_ID )
            	    {
            	    // InternalGame.g:1002:5: (otherlv_22= RULE_ID )
            	    // InternalGame.g:1003:6: otherlv_22= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getLieuRule());
            	    						}
            	    					
            	    otherlv_22=(Token)match(input,RULE_ID,FOLLOW_29); 

            	    						newLeafNode(otherlv_22, grammarAccess.getLieuAccess().getObjetsObjetCrossReference_19_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            otherlv_23=(Token)match(input,27,FOLLOW_3); 

            			newLeafNode(otherlv_23, grammarAccess.getLieuAccess().getConnaissancesKeyword_20());
            		
            otherlv_24=(Token)match(input,14,FOLLOW_17); 

            			newLeafNode(otherlv_24, grammarAccess.getLieuAccess().getColonKeyword_21());
            		
            // InternalGame.g:1023:3: (otherlv_25= '-' ( (otherlv_26= RULE_ID ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==15) ) {
                    int LA16_2 = input.LA(2);

                    if ( (LA16_2==RULE_ID) ) {
                        int LA16_3 = input.LA(3);

                        if ( (LA16_3==EOF||LA16_3==15||LA16_3==30) ) {
                            alt16=1;
                        }


                    }


                }


                switch (alt16) {
            	case 1 :
            	    // InternalGame.g:1024:4: otherlv_25= '-' ( (otherlv_26= RULE_ID ) )
            	    {
            	    otherlv_25=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_25, grammarAccess.getLieuAccess().getHyphenMinusKeyword_22_0());
            	    			
            	    // InternalGame.g:1028:4: ( (otherlv_26= RULE_ID ) )
            	    // InternalGame.g:1029:5: (otherlv_26= RULE_ID )
            	    {
            	    // InternalGame.g:1029:5: (otherlv_26= RULE_ID )
            	    // InternalGame.g:1030:6: otherlv_26= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getLieuRule());
            	    						}
            	    					
            	    otherlv_26=(Token)match(input,RULE_ID,FOLLOW_17); 

            	    						newLeafNode(otherlv_26, grammarAccess.getLieuAccess().getConnaissancesConnaissanceCrossReference_22_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLieu"


    // $ANTLR start "entryRuleChemin"
    // InternalGame.g:1046:1: entryRuleChemin returns [EObject current=null] : iv_ruleChemin= ruleChemin EOF ;
    public final EObject entryRuleChemin() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleChemin = null;


        try {
            // InternalGame.g:1046:47: (iv_ruleChemin= ruleChemin EOF )
            // InternalGame.g:1047:2: iv_ruleChemin= ruleChemin EOF
            {
             newCompositeNode(grammarAccess.getCheminRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleChemin=ruleChemin();

            state._fsp--;

             current =iv_ruleChemin; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleChemin"


    // $ANTLR start "ruleChemin"
    // InternalGame.g:1053:1: ruleChemin returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'lieu_in' otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'lieu_out' otherlv_6= ':' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'ouvert' otherlv_9= ':' ( (lv_ouvert_10_0= ruleCondition ) ) otherlv_11= 'visible' otherlv_12= ':' ( (lv_visible_13_0= ruleCondition ) ) otherlv_14= 'obligatoire' otherlv_15= ':' ( (lv_obligatoire_16_0= ruleCondition ) ) otherlv_17= 'connaissances' otherlv_18= ':' (otherlv_19= '-' ( (otherlv_20= RULE_ID ) ) )* otherlv_21= 'objets_recus' otherlv_22= ':' (otherlv_23= '-' ( (otherlv_24= RULE_ID ) ) )* otherlv_25= 'objets_conso' otherlv_26= ':' (otherlv_27= '-' ( (otherlv_28= RULE_ID ) ) )* otherlv_29= 'descriptions' otherlv_30= ':' (otherlv_31= '-' ( (lv_descriptions_32_0= ruleDescription ) ) )+ ) ;
    public final EObject ruleChemin() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        Token otherlv_26=null;
        Token otherlv_27=null;
        Token otherlv_28=null;
        Token otherlv_29=null;
        Token otherlv_30=null;
        Token otherlv_31=null;
        EObject lv_ouvert_10_0 = null;

        EObject lv_visible_13_0 = null;

        EObject lv_obligatoire_16_0 = null;

        EObject lv_descriptions_32_0 = null;



        	enterRule();

        try {
            // InternalGame.g:1059:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'lieu_in' otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'lieu_out' otherlv_6= ':' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'ouvert' otherlv_9= ':' ( (lv_ouvert_10_0= ruleCondition ) ) otherlv_11= 'visible' otherlv_12= ':' ( (lv_visible_13_0= ruleCondition ) ) otherlv_14= 'obligatoire' otherlv_15= ':' ( (lv_obligatoire_16_0= ruleCondition ) ) otherlv_17= 'connaissances' otherlv_18= ':' (otherlv_19= '-' ( (otherlv_20= RULE_ID ) ) )* otherlv_21= 'objets_recus' otherlv_22= ':' (otherlv_23= '-' ( (otherlv_24= RULE_ID ) ) )* otherlv_25= 'objets_conso' otherlv_26= ':' (otherlv_27= '-' ( (otherlv_28= RULE_ID ) ) )* otherlv_29= 'descriptions' otherlv_30= ':' (otherlv_31= '-' ( (lv_descriptions_32_0= ruleDescription ) ) )+ ) )
            // InternalGame.g:1060:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'lieu_in' otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'lieu_out' otherlv_6= ':' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'ouvert' otherlv_9= ':' ( (lv_ouvert_10_0= ruleCondition ) ) otherlv_11= 'visible' otherlv_12= ':' ( (lv_visible_13_0= ruleCondition ) ) otherlv_14= 'obligatoire' otherlv_15= ':' ( (lv_obligatoire_16_0= ruleCondition ) ) otherlv_17= 'connaissances' otherlv_18= ':' (otherlv_19= '-' ( (otherlv_20= RULE_ID ) ) )* otherlv_21= 'objets_recus' otherlv_22= ':' (otherlv_23= '-' ( (otherlv_24= RULE_ID ) ) )* otherlv_25= 'objets_conso' otherlv_26= ':' (otherlv_27= '-' ( (otherlv_28= RULE_ID ) ) )* otherlv_29= 'descriptions' otherlv_30= ':' (otherlv_31= '-' ( (lv_descriptions_32_0= ruleDescription ) ) )+ )
            {
            // InternalGame.g:1060:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'lieu_in' otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'lieu_out' otherlv_6= ':' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'ouvert' otherlv_9= ':' ( (lv_ouvert_10_0= ruleCondition ) ) otherlv_11= 'visible' otherlv_12= ':' ( (lv_visible_13_0= ruleCondition ) ) otherlv_14= 'obligatoire' otherlv_15= ':' ( (lv_obligatoire_16_0= ruleCondition ) ) otherlv_17= 'connaissances' otherlv_18= ':' (otherlv_19= '-' ( (otherlv_20= RULE_ID ) ) )* otherlv_21= 'objets_recus' otherlv_22= ':' (otherlv_23= '-' ( (otherlv_24= RULE_ID ) ) )* otherlv_25= 'objets_conso' otherlv_26= ':' (otherlv_27= '-' ( (otherlv_28= RULE_ID ) ) )* otherlv_29= 'descriptions' otherlv_30= ':' (otherlv_31= '-' ( (lv_descriptions_32_0= ruleDescription ) ) )+ )
            // InternalGame.g:1061:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'lieu_in' otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= 'lieu_out' otherlv_6= ':' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'ouvert' otherlv_9= ':' ( (lv_ouvert_10_0= ruleCondition ) ) otherlv_11= 'visible' otherlv_12= ':' ( (lv_visible_13_0= ruleCondition ) ) otherlv_14= 'obligatoire' otherlv_15= ':' ( (lv_obligatoire_16_0= ruleCondition ) ) otherlv_17= 'connaissances' otherlv_18= ':' (otherlv_19= '-' ( (otherlv_20= RULE_ID ) ) )* otherlv_21= 'objets_recus' otherlv_22= ':' (otherlv_23= '-' ( (otherlv_24= RULE_ID ) ) )* otherlv_25= 'objets_conso' otherlv_26= ':' (otherlv_27= '-' ( (otherlv_28= RULE_ID ) ) )* otherlv_29= 'descriptions' otherlv_30= ':' (otherlv_31= '-' ( (lv_descriptions_32_0= ruleDescription ) ) )+
            {
            // InternalGame.g:1061:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGame.g:1062:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGame.g:1062:4: (lv_name_0_0= RULE_ID )
            // InternalGame.g:1063:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_name_0_0, grammarAccess.getCheminAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCheminRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getCheminAccess().getColonKeyword_1());
            		
            otherlv_2=(Token)match(input,35,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getCheminAccess().getLieu_inKeyword_2());
            		
            otherlv_3=(Token)match(input,14,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getCheminAccess().getColonKeyword_3());
            		
            // InternalGame.g:1091:3: ( (otherlv_4= RULE_ID ) )
            // InternalGame.g:1092:4: (otherlv_4= RULE_ID )
            {
            // InternalGame.g:1092:4: (otherlv_4= RULE_ID )
            // InternalGame.g:1093:5: otherlv_4= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCheminRule());
            					}
            				
            otherlv_4=(Token)match(input,RULE_ID,FOLLOW_31); 

            					newLeafNode(otherlv_4, grammarAccess.getCheminAccess().getLieuInLieuCrossReference_4_0());
            				

            }


            }

            otherlv_5=(Token)match(input,36,FOLLOW_3); 

            			newLeafNode(otherlv_5, grammarAccess.getCheminAccess().getLieu_outKeyword_5());
            		
            otherlv_6=(Token)match(input,14,FOLLOW_5); 

            			newLeafNode(otherlv_6, grammarAccess.getCheminAccess().getColonKeyword_6());
            		
            // InternalGame.g:1112:3: ( (otherlv_7= RULE_ID ) )
            // InternalGame.g:1113:4: (otherlv_7= RULE_ID )
            {
            // InternalGame.g:1113:4: (otherlv_7= RULE_ID )
            // InternalGame.g:1114:5: otherlv_7= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCheminRule());
            					}
            				
            otherlv_7=(Token)match(input,RULE_ID,FOLLOW_32); 

            					newLeafNode(otherlv_7, grammarAccess.getCheminAccess().getLieuOutLieuCrossReference_7_0());
            				

            }


            }

            otherlv_8=(Token)match(input,37,FOLLOW_3); 

            			newLeafNode(otherlv_8, grammarAccess.getCheminAccess().getOuvertKeyword_8());
            		
            otherlv_9=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_9, grammarAccess.getCheminAccess().getColonKeyword_9());
            		
            // InternalGame.g:1133:3: ( (lv_ouvert_10_0= ruleCondition ) )
            // InternalGame.g:1134:4: (lv_ouvert_10_0= ruleCondition )
            {
            // InternalGame.g:1134:4: (lv_ouvert_10_0= ruleCondition )
            // InternalGame.g:1135:5: lv_ouvert_10_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getCheminAccess().getOuvertConditionParserRuleCall_10_0());
            				
            pushFollow(FOLLOW_13);
            lv_ouvert_10_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCheminRule());
            					}
            					set(
            						current,
            						"ouvert",
            						lv_ouvert_10_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_11=(Token)match(input,22,FOLLOW_3); 

            			newLeafNode(otherlv_11, grammarAccess.getCheminAccess().getVisibleKeyword_11());
            		
            otherlv_12=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_12, grammarAccess.getCheminAccess().getColonKeyword_12());
            		
            // InternalGame.g:1160:3: ( (lv_visible_13_0= ruleCondition ) )
            // InternalGame.g:1161:4: (lv_visible_13_0= ruleCondition )
            {
            // InternalGame.g:1161:4: (lv_visible_13_0= ruleCondition )
            // InternalGame.g:1162:5: lv_visible_13_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getCheminAccess().getVisibleConditionParserRuleCall_13_0());
            				
            pushFollow(FOLLOW_33);
            lv_visible_13_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCheminRule());
            					}
            					set(
            						current,
            						"visible",
            						lv_visible_13_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_14=(Token)match(input,38,FOLLOW_3); 

            			newLeafNode(otherlv_14, grammarAccess.getCheminAccess().getObligatoireKeyword_14());
            		
            otherlv_15=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_15, grammarAccess.getCheminAccess().getColonKeyword_15());
            		
            // InternalGame.g:1187:3: ( (lv_obligatoire_16_0= ruleCondition ) )
            // InternalGame.g:1188:4: (lv_obligatoire_16_0= ruleCondition )
            {
            // InternalGame.g:1188:4: (lv_obligatoire_16_0= ruleCondition )
            // InternalGame.g:1189:5: lv_obligatoire_16_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getCheminAccess().getObligatoireConditionParserRuleCall_16_0());
            				
            pushFollow(FOLLOW_21);
            lv_obligatoire_16_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCheminRule());
            					}
            					set(
            						current,
            						"obligatoire",
            						lv_obligatoire_16_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_17=(Token)match(input,27,FOLLOW_3); 

            			newLeafNode(otherlv_17, grammarAccess.getCheminAccess().getConnaissancesKeyword_17());
            		
            otherlv_18=(Token)match(input,14,FOLLOW_34); 

            			newLeafNode(otherlv_18, grammarAccess.getCheminAccess().getColonKeyword_18());
            		
            // InternalGame.g:1214:3: (otherlv_19= '-' ( (otherlv_20= RULE_ID ) ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==15) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalGame.g:1215:4: otherlv_19= '-' ( (otherlv_20= RULE_ID ) )
            	    {
            	    otherlv_19=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_19, grammarAccess.getCheminAccess().getHyphenMinusKeyword_19_0());
            	    			
            	    // InternalGame.g:1219:4: ( (otherlv_20= RULE_ID ) )
            	    // InternalGame.g:1220:5: (otherlv_20= RULE_ID )
            	    {
            	    // InternalGame.g:1220:5: (otherlv_20= RULE_ID )
            	    // InternalGame.g:1221:6: otherlv_20= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getCheminRule());
            	    						}
            	    					
            	    otherlv_20=(Token)match(input,RULE_ID,FOLLOW_34); 

            	    						newLeafNode(otherlv_20, grammarAccess.getCheminAccess().getConnaissancesConnaissanceCrossReference_19_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            otherlv_21=(Token)match(input,39,FOLLOW_3); 

            			newLeafNode(otherlv_21, grammarAccess.getCheminAccess().getObjets_recusKeyword_20());
            		
            otherlv_22=(Token)match(input,14,FOLLOW_35); 

            			newLeafNode(otherlv_22, grammarAccess.getCheminAccess().getColonKeyword_21());
            		
            // InternalGame.g:1241:3: (otherlv_23= '-' ( (otherlv_24= RULE_ID ) ) )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==15) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalGame.g:1242:4: otherlv_23= '-' ( (otherlv_24= RULE_ID ) )
            	    {
            	    otherlv_23=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_23, grammarAccess.getCheminAccess().getHyphenMinusKeyword_22_0());
            	    			
            	    // InternalGame.g:1246:4: ( (otherlv_24= RULE_ID ) )
            	    // InternalGame.g:1247:5: (otherlv_24= RULE_ID )
            	    {
            	    // InternalGame.g:1247:5: (otherlv_24= RULE_ID )
            	    // InternalGame.g:1248:6: otherlv_24= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getCheminRule());
            	    						}
            	    					
            	    otherlv_24=(Token)match(input,RULE_ID,FOLLOW_35); 

            	    						newLeafNode(otherlv_24, grammarAccess.getCheminAccess().getObjetsRecusObjetCrossReference_22_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

            otherlv_25=(Token)match(input,40,FOLLOW_3); 

            			newLeafNode(otherlv_25, grammarAccess.getCheminAccess().getObjets_consoKeyword_23());
            		
            otherlv_26=(Token)match(input,14,FOLLOW_28); 

            			newLeafNode(otherlv_26, grammarAccess.getCheminAccess().getColonKeyword_24());
            		
            // InternalGame.g:1268:3: (otherlv_27= '-' ( (otherlv_28= RULE_ID ) ) )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==15) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalGame.g:1269:4: otherlv_27= '-' ( (otherlv_28= RULE_ID ) )
            	    {
            	    otherlv_27=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_27, grammarAccess.getCheminAccess().getHyphenMinusKeyword_25_0());
            	    			
            	    // InternalGame.g:1273:4: ( (otherlv_28= RULE_ID ) )
            	    // InternalGame.g:1274:5: (otherlv_28= RULE_ID )
            	    {
            	    // InternalGame.g:1274:5: (otherlv_28= RULE_ID )
            	    // InternalGame.g:1275:6: otherlv_28= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getCheminRule());
            	    						}
            	    					
            	    otherlv_28=(Token)match(input,RULE_ID,FOLLOW_28); 

            	    						newLeafNode(otherlv_28, grammarAccess.getCheminAccess().getObjetsConsoObjetCrossReference_25_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

            otherlv_29=(Token)match(input,23,FOLLOW_3); 

            			newLeafNode(otherlv_29, grammarAccess.getCheminAccess().getDescriptionsKeyword_26());
            		
            otherlv_30=(Token)match(input,14,FOLLOW_16); 

            			newLeafNode(otherlv_30, grammarAccess.getCheminAccess().getColonKeyword_27());
            		
            // InternalGame.g:1295:3: (otherlv_31= '-' ( (lv_descriptions_32_0= ruleDescription ) ) )+
            int cnt20=0;
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==15) ) {
                    int LA20_2 = input.LA(2);

                    if ( (LA20_2==RULE_ID) ) {
                        int LA20_3 = input.LA(3);

                        if ( (LA20_3==14) ) {
                            int LA20_4 = input.LA(4);

                            if ( (LA20_4==44) ) {
                                alt20=1;
                            }


                        }


                    }


                }


                switch (alt20) {
            	case 1 :
            	    // InternalGame.g:1296:4: otherlv_31= '-' ( (lv_descriptions_32_0= ruleDescription ) )
            	    {
            	    otherlv_31=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_31, grammarAccess.getCheminAccess().getHyphenMinusKeyword_28_0());
            	    			
            	    // InternalGame.g:1300:4: ( (lv_descriptions_32_0= ruleDescription ) )
            	    // InternalGame.g:1301:5: (lv_descriptions_32_0= ruleDescription )
            	    {
            	    // InternalGame.g:1301:5: (lv_descriptions_32_0= ruleDescription )
            	    // InternalGame.g:1302:6: lv_descriptions_32_0= ruleDescription
            	    {

            	    						newCompositeNode(grammarAccess.getCheminAccess().getDescriptionsDescriptionParserRuleCall_28_1_0());
            	    					
            	    pushFollow(FOLLOW_17);
            	    lv_descriptions_32_0=ruleDescription();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getCheminRule());
            	    						}
            	    						add(
            	    							current,
            	    							"descriptions",
            	    							lv_descriptions_32_0,
            	    							"fr.n7.game.xtext.Game.Description");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt20 >= 1 ) break loop20;
                        EarlyExitException eee =
                            new EarlyExitException(20, input);
                        throw eee;
                }
                cnt20++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChemin"


    // $ANTLR start "entryRulePersonne"
    // InternalGame.g:1324:1: entryRulePersonne returns [EObject current=null] : iv_rulePersonne= rulePersonne EOF ;
    public final EObject entryRulePersonne() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePersonne = null;


        try {
            // InternalGame.g:1324:49: (iv_rulePersonne= rulePersonne EOF )
            // InternalGame.g:1325:2: iv_rulePersonne= rulePersonne EOF
            {
             newCompositeNode(grammarAccess.getPersonneRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePersonne=rulePersonne();

            state._fsp--;

             current =iv_rulePersonne; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePersonne"


    // $ANTLR start "rulePersonne"
    // InternalGame.g:1331:1: rulePersonne returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'obligatoire' otherlv_6= ':' ( (lv_obligatoire_7_0= ruleCondition ) ) otherlv_8= 'interactions' otherlv_9= ':' (otherlv_10= '-' ( (lv_interactions_11_0= ruleInteraction ) ) )+ ) ;
    public final EObject rulePersonne() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        EObject lv_visible_4_0 = null;

        EObject lv_obligatoire_7_0 = null;

        EObject lv_interactions_11_0 = null;



        	enterRule();

        try {
            // InternalGame.g:1337:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'obligatoire' otherlv_6= ':' ( (lv_obligatoire_7_0= ruleCondition ) ) otherlv_8= 'interactions' otherlv_9= ':' (otherlv_10= '-' ( (lv_interactions_11_0= ruleInteraction ) ) )+ ) )
            // InternalGame.g:1338:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'obligatoire' otherlv_6= ':' ( (lv_obligatoire_7_0= ruleCondition ) ) otherlv_8= 'interactions' otherlv_9= ':' (otherlv_10= '-' ( (lv_interactions_11_0= ruleInteraction ) ) )+ )
            {
            // InternalGame.g:1338:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'obligatoire' otherlv_6= ':' ( (lv_obligatoire_7_0= ruleCondition ) ) otherlv_8= 'interactions' otherlv_9= ':' (otherlv_10= '-' ( (lv_interactions_11_0= ruleInteraction ) ) )+ )
            // InternalGame.g:1339:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'obligatoire' otherlv_6= ':' ( (lv_obligatoire_7_0= ruleCondition ) ) otherlv_8= 'interactions' otherlv_9= ':' (otherlv_10= '-' ( (lv_interactions_11_0= ruleInteraction ) ) )+
            {
            // InternalGame.g:1339:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGame.g:1340:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGame.g:1340:4: (lv_name_0_0= RULE_ID )
            // InternalGame.g:1341:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_name_0_0, grammarAccess.getPersonneAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPersonneRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getPersonneAccess().getColonKeyword_1());
            		
            otherlv_2=(Token)match(input,22,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getPersonneAccess().getVisibleKeyword_2());
            		
            otherlv_3=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_3, grammarAccess.getPersonneAccess().getColonKeyword_3());
            		
            // InternalGame.g:1369:3: ( (lv_visible_4_0= ruleCondition ) )
            // InternalGame.g:1370:4: (lv_visible_4_0= ruleCondition )
            {
            // InternalGame.g:1370:4: (lv_visible_4_0= ruleCondition )
            // InternalGame.g:1371:5: lv_visible_4_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getPersonneAccess().getVisibleConditionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_33);
            lv_visible_4_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPersonneRule());
            					}
            					set(
            						current,
            						"visible",
            						lv_visible_4_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,38,FOLLOW_3); 

            			newLeafNode(otherlv_5, grammarAccess.getPersonneAccess().getObligatoireKeyword_5());
            		
            otherlv_6=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_6, grammarAccess.getPersonneAccess().getColonKeyword_6());
            		
            // InternalGame.g:1396:3: ( (lv_obligatoire_7_0= ruleCondition ) )
            // InternalGame.g:1397:4: (lv_obligatoire_7_0= ruleCondition )
            {
            // InternalGame.g:1397:4: (lv_obligatoire_7_0= ruleCondition )
            // InternalGame.g:1398:5: lv_obligatoire_7_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getPersonneAccess().getObligatoireConditionParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_36);
            lv_obligatoire_7_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPersonneRule());
            					}
            					set(
            						current,
            						"obligatoire",
            						lv_obligatoire_7_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_8=(Token)match(input,41,FOLLOW_3); 

            			newLeafNode(otherlv_8, grammarAccess.getPersonneAccess().getInteractionsKeyword_8());
            		
            otherlv_9=(Token)match(input,14,FOLLOW_16); 

            			newLeafNode(otherlv_9, grammarAccess.getPersonneAccess().getColonKeyword_9());
            		
            // InternalGame.g:1423:3: (otherlv_10= '-' ( (lv_interactions_11_0= ruleInteraction ) ) )+
            int cnt21=0;
            loop21:
            do {
                int alt21=2;
                alt21 = dfa21.predict(input);
                switch (alt21) {
            	case 1 :
            	    // InternalGame.g:1424:4: otherlv_10= '-' ( (lv_interactions_11_0= ruleInteraction ) )
            	    {
            	    otherlv_10=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_10, grammarAccess.getPersonneAccess().getHyphenMinusKeyword_10_0());
            	    			
            	    // InternalGame.g:1428:4: ( (lv_interactions_11_0= ruleInteraction ) )
            	    // InternalGame.g:1429:5: (lv_interactions_11_0= ruleInteraction )
            	    {
            	    // InternalGame.g:1429:5: (lv_interactions_11_0= ruleInteraction )
            	    // InternalGame.g:1430:6: lv_interactions_11_0= ruleInteraction
            	    {

            	    						newCompositeNode(grammarAccess.getPersonneAccess().getInteractionsInteractionParserRuleCall_10_1_0());
            	    					
            	    pushFollow(FOLLOW_17);
            	    lv_interactions_11_0=ruleInteraction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getPersonneRule());
            	    						}
            	    						add(
            	    							current,
            	    							"interactions",
            	    							lv_interactions_11_0,
            	    							"fr.n7.game.xtext.Game.Interaction");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt21 >= 1 ) break loop21;
                        EarlyExitException eee =
                            new EarlyExitException(21, input);
                        throw eee;
                }
                cnt21++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePersonne"


    // $ANTLR start "entryRuleInteraction"
    // InternalGame.g:1452:1: entryRuleInteraction returns [EObject current=null] : iv_ruleInteraction= ruleInteraction EOF ;
    public final EObject entryRuleInteraction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteraction = null;


        try {
            // InternalGame.g:1452:52: (iv_ruleInteraction= ruleInteraction EOF )
            // InternalGame.g:1453:2: iv_ruleInteraction= ruleInteraction EOF
            {
             newCompositeNode(grammarAccess.getInteractionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInteraction=ruleInteraction();

            state._fsp--;

             current =iv_ruleInteraction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteraction"


    // $ANTLR start "ruleInteraction"
    // InternalGame.g:1459:1: ruleInteraction returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'connaissances' otherlv_6= ':' (otherlv_7= '-' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'objets_recus' otherlv_10= ':' (otherlv_11= '-' ( (otherlv_12= RULE_ID ) ) )* otherlv_13= 'objets_conso' otherlv_14= ':' (otherlv_15= '-' ( (otherlv_16= RULE_ID ) ) )* otherlv_17= 'actions' otherlv_18= ':' (otherlv_19= '-' ( (lv_actions_20_0= ruleAction ) ) )+ ) ;
    public final EObject ruleInteraction() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        EObject lv_visible_4_0 = null;

        EObject lv_actions_20_0 = null;



        	enterRule();

        try {
            // InternalGame.g:1465:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'connaissances' otherlv_6= ':' (otherlv_7= '-' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'objets_recus' otherlv_10= ':' (otherlv_11= '-' ( (otherlv_12= RULE_ID ) ) )* otherlv_13= 'objets_conso' otherlv_14= ':' (otherlv_15= '-' ( (otherlv_16= RULE_ID ) ) )* otherlv_17= 'actions' otherlv_18= ':' (otherlv_19= '-' ( (lv_actions_20_0= ruleAction ) ) )+ ) )
            // InternalGame.g:1466:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'connaissances' otherlv_6= ':' (otherlv_7= '-' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'objets_recus' otherlv_10= ':' (otherlv_11= '-' ( (otherlv_12= RULE_ID ) ) )* otherlv_13= 'objets_conso' otherlv_14= ':' (otherlv_15= '-' ( (otherlv_16= RULE_ID ) ) )* otherlv_17= 'actions' otherlv_18= ':' (otherlv_19= '-' ( (lv_actions_20_0= ruleAction ) ) )+ )
            {
            // InternalGame.g:1466:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'connaissances' otherlv_6= ':' (otherlv_7= '-' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'objets_recus' otherlv_10= ':' (otherlv_11= '-' ( (otherlv_12= RULE_ID ) ) )* otherlv_13= 'objets_conso' otherlv_14= ':' (otherlv_15= '-' ( (otherlv_16= RULE_ID ) ) )* otherlv_17= 'actions' otherlv_18= ':' (otherlv_19= '-' ( (lv_actions_20_0= ruleAction ) ) )+ )
            // InternalGame.g:1467:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'connaissances' otherlv_6= ':' (otherlv_7= '-' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'objets_recus' otherlv_10= ':' (otherlv_11= '-' ( (otherlv_12= RULE_ID ) ) )* otherlv_13= 'objets_conso' otherlv_14= ':' (otherlv_15= '-' ( (otherlv_16= RULE_ID ) ) )* otherlv_17= 'actions' otherlv_18= ':' (otherlv_19= '-' ( (lv_actions_20_0= ruleAction ) ) )+
            {
            // InternalGame.g:1467:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGame.g:1468:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGame.g:1468:4: (lv_name_0_0= RULE_ID )
            // InternalGame.g:1469:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_name_0_0, grammarAccess.getInteractionAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getInteractionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getInteractionAccess().getColonKeyword_1());
            		
            otherlv_2=(Token)match(input,22,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getInteractionAccess().getVisibleKeyword_2());
            		
            otherlv_3=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_3, grammarAccess.getInteractionAccess().getColonKeyword_3());
            		
            // InternalGame.g:1497:3: ( (lv_visible_4_0= ruleCondition ) )
            // InternalGame.g:1498:4: (lv_visible_4_0= ruleCondition )
            {
            // InternalGame.g:1498:4: (lv_visible_4_0= ruleCondition )
            // InternalGame.g:1499:5: lv_visible_4_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getInteractionAccess().getVisibleConditionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_21);
            lv_visible_4_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getInteractionRule());
            					}
            					set(
            						current,
            						"visible",
            						lv_visible_4_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,27,FOLLOW_3); 

            			newLeafNode(otherlv_5, grammarAccess.getInteractionAccess().getConnaissancesKeyword_5());
            		
            otherlv_6=(Token)match(input,14,FOLLOW_34); 

            			newLeafNode(otherlv_6, grammarAccess.getInteractionAccess().getColonKeyword_6());
            		
            // InternalGame.g:1524:3: (otherlv_7= '-' ( (otherlv_8= RULE_ID ) ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==15) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalGame.g:1525:4: otherlv_7= '-' ( (otherlv_8= RULE_ID ) )
            	    {
            	    otherlv_7=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_7, grammarAccess.getInteractionAccess().getHyphenMinusKeyword_7_0());
            	    			
            	    // InternalGame.g:1529:4: ( (otherlv_8= RULE_ID ) )
            	    // InternalGame.g:1530:5: (otherlv_8= RULE_ID )
            	    {
            	    // InternalGame.g:1530:5: (otherlv_8= RULE_ID )
            	    // InternalGame.g:1531:6: otherlv_8= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getInteractionRule());
            	    						}
            	    					
            	    otherlv_8=(Token)match(input,RULE_ID,FOLLOW_34); 

            	    						newLeafNode(otherlv_8, grammarAccess.getInteractionAccess().getConnaissancesConnaissanceCrossReference_7_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

            otherlv_9=(Token)match(input,39,FOLLOW_3); 

            			newLeafNode(otherlv_9, grammarAccess.getInteractionAccess().getObjets_recusKeyword_8());
            		
            otherlv_10=(Token)match(input,14,FOLLOW_35); 

            			newLeafNode(otherlv_10, grammarAccess.getInteractionAccess().getColonKeyword_9());
            		
            // InternalGame.g:1551:3: (otherlv_11= '-' ( (otherlv_12= RULE_ID ) ) )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==15) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalGame.g:1552:4: otherlv_11= '-' ( (otherlv_12= RULE_ID ) )
            	    {
            	    otherlv_11=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_11, grammarAccess.getInteractionAccess().getHyphenMinusKeyword_10_0());
            	    			
            	    // InternalGame.g:1556:4: ( (otherlv_12= RULE_ID ) )
            	    // InternalGame.g:1557:5: (otherlv_12= RULE_ID )
            	    {
            	    // InternalGame.g:1557:5: (otherlv_12= RULE_ID )
            	    // InternalGame.g:1558:6: otherlv_12= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getInteractionRule());
            	    						}
            	    					
            	    otherlv_12=(Token)match(input,RULE_ID,FOLLOW_35); 

            	    						newLeafNode(otherlv_12, grammarAccess.getInteractionAccess().getObjetsRecusObjetCrossReference_10_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

            otherlv_13=(Token)match(input,40,FOLLOW_3); 

            			newLeafNode(otherlv_13, grammarAccess.getInteractionAccess().getObjets_consoKeyword_11());
            		
            otherlv_14=(Token)match(input,14,FOLLOW_37); 

            			newLeafNode(otherlv_14, grammarAccess.getInteractionAccess().getColonKeyword_12());
            		
            // InternalGame.g:1578:3: (otherlv_15= '-' ( (otherlv_16= RULE_ID ) ) )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==15) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalGame.g:1579:4: otherlv_15= '-' ( (otherlv_16= RULE_ID ) )
            	    {
            	    otherlv_15=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_15, grammarAccess.getInteractionAccess().getHyphenMinusKeyword_13_0());
            	    			
            	    // InternalGame.g:1583:4: ( (otherlv_16= RULE_ID ) )
            	    // InternalGame.g:1584:5: (otherlv_16= RULE_ID )
            	    {
            	    // InternalGame.g:1584:5: (otherlv_16= RULE_ID )
            	    // InternalGame.g:1585:6: otherlv_16= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getInteractionRule());
            	    						}
            	    					
            	    otherlv_16=(Token)match(input,RULE_ID,FOLLOW_37); 

            	    						newLeafNode(otherlv_16, grammarAccess.getInteractionAccess().getObjetsConsoObjetCrossReference_13_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

            otherlv_17=(Token)match(input,42,FOLLOW_3); 

            			newLeafNode(otherlv_17, grammarAccess.getInteractionAccess().getActionsKeyword_14());
            		
            otherlv_18=(Token)match(input,14,FOLLOW_16); 

            			newLeafNode(otherlv_18, grammarAccess.getInteractionAccess().getColonKeyword_15());
            		
            // InternalGame.g:1605:3: (otherlv_19= '-' ( (lv_actions_20_0= ruleAction ) ) )+
            int cnt25=0;
            loop25:
            do {
                int alt25=2;
                alt25 = dfa25.predict(input);
                switch (alt25) {
            	case 1 :
            	    // InternalGame.g:1606:4: otherlv_19= '-' ( (lv_actions_20_0= ruleAction ) )
            	    {
            	    otherlv_19=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_19, grammarAccess.getInteractionAccess().getHyphenMinusKeyword_16_0());
            	    			
            	    // InternalGame.g:1610:4: ( (lv_actions_20_0= ruleAction ) )
            	    // InternalGame.g:1611:5: (lv_actions_20_0= ruleAction )
            	    {
            	    // InternalGame.g:1611:5: (lv_actions_20_0= ruleAction )
            	    // InternalGame.g:1612:6: lv_actions_20_0= ruleAction
            	    {

            	    						newCompositeNode(grammarAccess.getInteractionAccess().getActionsActionParserRuleCall_16_1_0());
            	    					
            	    pushFollow(FOLLOW_17);
            	    lv_actions_20_0=ruleAction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getInteractionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"actions",
            	    							lv_actions_20_0,
            	    							"fr.n7.game.xtext.Game.Action");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt25 >= 1 ) break loop25;
                        EarlyExitException eee =
                            new EarlyExitException(25, input);
                        throw eee;
                }
                cnt25++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteraction"


    // $ANTLR start "entryRuleAction"
    // InternalGame.g:1634:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalGame.g:1634:47: (iv_ruleAction= ruleAction EOF )
            // InternalGame.g:1635:2: iv_ruleAction= ruleAction EOF
            {
             newCompositeNode(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;

             current =iv_ruleAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalGame.g:1641:1: ruleAction returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'fin_interaction' otherlv_6= ':' ( (lv_finInteraction_7_0= ruleCondition ) ) otherlv_8= 'connaissances' otherlv_9= ':' (otherlv_10= '-' ( (otherlv_11= RULE_ID ) ) )* otherlv_12= 'objets_recus' otherlv_13= ':' (otherlv_14= '-' ( (otherlv_15= RULE_ID ) ) )* otherlv_16= 'objets_conso' otherlv_17= ':' (otherlv_18= '-' ( (otherlv_19= RULE_ID ) ) )* otherlv_20= 'descriptions' otherlv_21= ':' (otherlv_22= '-' ( (lv_descriptions_23_0= ruleDescription ) ) )+ ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        EObject lv_visible_4_0 = null;

        EObject lv_finInteraction_7_0 = null;

        EObject lv_descriptions_23_0 = null;



        	enterRule();

        try {
            // InternalGame.g:1647:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'fin_interaction' otherlv_6= ':' ( (lv_finInteraction_7_0= ruleCondition ) ) otherlv_8= 'connaissances' otherlv_9= ':' (otherlv_10= '-' ( (otherlv_11= RULE_ID ) ) )* otherlv_12= 'objets_recus' otherlv_13= ':' (otherlv_14= '-' ( (otherlv_15= RULE_ID ) ) )* otherlv_16= 'objets_conso' otherlv_17= ':' (otherlv_18= '-' ( (otherlv_19= RULE_ID ) ) )* otherlv_20= 'descriptions' otherlv_21= ':' (otherlv_22= '-' ( (lv_descriptions_23_0= ruleDescription ) ) )+ ) )
            // InternalGame.g:1648:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'fin_interaction' otherlv_6= ':' ( (lv_finInteraction_7_0= ruleCondition ) ) otherlv_8= 'connaissances' otherlv_9= ':' (otherlv_10= '-' ( (otherlv_11= RULE_ID ) ) )* otherlv_12= 'objets_recus' otherlv_13= ':' (otherlv_14= '-' ( (otherlv_15= RULE_ID ) ) )* otherlv_16= 'objets_conso' otherlv_17= ':' (otherlv_18= '-' ( (otherlv_19= RULE_ID ) ) )* otherlv_20= 'descriptions' otherlv_21= ':' (otherlv_22= '-' ( (lv_descriptions_23_0= ruleDescription ) ) )+ )
            {
            // InternalGame.g:1648:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'fin_interaction' otherlv_6= ':' ( (lv_finInteraction_7_0= ruleCondition ) ) otherlv_8= 'connaissances' otherlv_9= ':' (otherlv_10= '-' ( (otherlv_11= RULE_ID ) ) )* otherlv_12= 'objets_recus' otherlv_13= ':' (otherlv_14= '-' ( (otherlv_15= RULE_ID ) ) )* otherlv_16= 'objets_conso' otherlv_17= ':' (otherlv_18= '-' ( (otherlv_19= RULE_ID ) ) )* otherlv_20= 'descriptions' otherlv_21= ':' (otherlv_22= '-' ( (lv_descriptions_23_0= ruleDescription ) ) )+ )
            // InternalGame.g:1649:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'visible' otherlv_3= ':' ( (lv_visible_4_0= ruleCondition ) ) otherlv_5= 'fin_interaction' otherlv_6= ':' ( (lv_finInteraction_7_0= ruleCondition ) ) otherlv_8= 'connaissances' otherlv_9= ':' (otherlv_10= '-' ( (otherlv_11= RULE_ID ) ) )* otherlv_12= 'objets_recus' otherlv_13= ':' (otherlv_14= '-' ( (otherlv_15= RULE_ID ) ) )* otherlv_16= 'objets_conso' otherlv_17= ':' (otherlv_18= '-' ( (otherlv_19= RULE_ID ) ) )* otherlv_20= 'descriptions' otherlv_21= ':' (otherlv_22= '-' ( (lv_descriptions_23_0= ruleDescription ) ) )+
            {
            // InternalGame.g:1649:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGame.g:1650:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGame.g:1650:4: (lv_name_0_0= RULE_ID )
            // InternalGame.g:1651:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_name_0_0, grammarAccess.getActionAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getActionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getActionAccess().getColonKeyword_1());
            		
            otherlv_2=(Token)match(input,22,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getActionAccess().getVisibleKeyword_2());
            		
            otherlv_3=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_3, grammarAccess.getActionAccess().getColonKeyword_3());
            		
            // InternalGame.g:1679:3: ( (lv_visible_4_0= ruleCondition ) )
            // InternalGame.g:1680:4: (lv_visible_4_0= ruleCondition )
            {
            // InternalGame.g:1680:4: (lv_visible_4_0= ruleCondition )
            // InternalGame.g:1681:5: lv_visible_4_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getActionAccess().getVisibleConditionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_38);
            lv_visible_4_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getActionRule());
            					}
            					set(
            						current,
            						"visible",
            						lv_visible_4_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,43,FOLLOW_3); 

            			newLeafNode(otherlv_5, grammarAccess.getActionAccess().getFin_interactionKeyword_5());
            		
            otherlv_6=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_6, grammarAccess.getActionAccess().getColonKeyword_6());
            		
            // InternalGame.g:1706:3: ( (lv_finInteraction_7_0= ruleCondition ) )
            // InternalGame.g:1707:4: (lv_finInteraction_7_0= ruleCondition )
            {
            // InternalGame.g:1707:4: (lv_finInteraction_7_0= ruleCondition )
            // InternalGame.g:1708:5: lv_finInteraction_7_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getActionAccess().getFinInteractionConditionParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_21);
            lv_finInteraction_7_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getActionRule());
            					}
            					set(
            						current,
            						"finInteraction",
            						lv_finInteraction_7_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_8=(Token)match(input,27,FOLLOW_3); 

            			newLeafNode(otherlv_8, grammarAccess.getActionAccess().getConnaissancesKeyword_8());
            		
            otherlv_9=(Token)match(input,14,FOLLOW_34); 

            			newLeafNode(otherlv_9, grammarAccess.getActionAccess().getColonKeyword_9());
            		
            // InternalGame.g:1733:3: (otherlv_10= '-' ( (otherlv_11= RULE_ID ) ) )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==15) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalGame.g:1734:4: otherlv_10= '-' ( (otherlv_11= RULE_ID ) )
            	    {
            	    otherlv_10=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_10, grammarAccess.getActionAccess().getHyphenMinusKeyword_10_0());
            	    			
            	    // InternalGame.g:1738:4: ( (otherlv_11= RULE_ID ) )
            	    // InternalGame.g:1739:5: (otherlv_11= RULE_ID )
            	    {
            	    // InternalGame.g:1739:5: (otherlv_11= RULE_ID )
            	    // InternalGame.g:1740:6: otherlv_11= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getActionRule());
            	    						}
            	    					
            	    otherlv_11=(Token)match(input,RULE_ID,FOLLOW_34); 

            	    						newLeafNode(otherlv_11, grammarAccess.getActionAccess().getConnaissancesConnaissanceCrossReference_10_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

            otherlv_12=(Token)match(input,39,FOLLOW_3); 

            			newLeafNode(otherlv_12, grammarAccess.getActionAccess().getObjets_recusKeyword_11());
            		
            otherlv_13=(Token)match(input,14,FOLLOW_35); 

            			newLeafNode(otherlv_13, grammarAccess.getActionAccess().getColonKeyword_12());
            		
            // InternalGame.g:1760:3: (otherlv_14= '-' ( (otherlv_15= RULE_ID ) ) )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==15) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalGame.g:1761:4: otherlv_14= '-' ( (otherlv_15= RULE_ID ) )
            	    {
            	    otherlv_14=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_14, grammarAccess.getActionAccess().getHyphenMinusKeyword_13_0());
            	    			
            	    // InternalGame.g:1765:4: ( (otherlv_15= RULE_ID ) )
            	    // InternalGame.g:1766:5: (otherlv_15= RULE_ID )
            	    {
            	    // InternalGame.g:1766:5: (otherlv_15= RULE_ID )
            	    // InternalGame.g:1767:6: otherlv_15= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getActionRule());
            	    						}
            	    					
            	    otherlv_15=(Token)match(input,RULE_ID,FOLLOW_35); 

            	    						newLeafNode(otherlv_15, grammarAccess.getActionAccess().getObjetsRecusObjetCrossReference_13_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

            otherlv_16=(Token)match(input,40,FOLLOW_3); 

            			newLeafNode(otherlv_16, grammarAccess.getActionAccess().getObjets_consoKeyword_14());
            		
            otherlv_17=(Token)match(input,14,FOLLOW_28); 

            			newLeafNode(otherlv_17, grammarAccess.getActionAccess().getColonKeyword_15());
            		
            // InternalGame.g:1787:3: (otherlv_18= '-' ( (otherlv_19= RULE_ID ) ) )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==15) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalGame.g:1788:4: otherlv_18= '-' ( (otherlv_19= RULE_ID ) )
            	    {
            	    otherlv_18=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_18, grammarAccess.getActionAccess().getHyphenMinusKeyword_16_0());
            	    			
            	    // InternalGame.g:1792:4: ( (otherlv_19= RULE_ID ) )
            	    // InternalGame.g:1793:5: (otherlv_19= RULE_ID )
            	    {
            	    // InternalGame.g:1793:5: (otherlv_19= RULE_ID )
            	    // InternalGame.g:1794:6: otherlv_19= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getActionRule());
            	    						}
            	    					
            	    otherlv_19=(Token)match(input,RULE_ID,FOLLOW_28); 

            	    						newLeafNode(otherlv_19, grammarAccess.getActionAccess().getObjetsConsoObjetCrossReference_16_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

            otherlv_20=(Token)match(input,23,FOLLOW_3); 

            			newLeafNode(otherlv_20, grammarAccess.getActionAccess().getDescriptionsKeyword_17());
            		
            otherlv_21=(Token)match(input,14,FOLLOW_16); 

            			newLeafNode(otherlv_21, grammarAccess.getActionAccess().getColonKeyword_18());
            		
            // InternalGame.g:1814:3: (otherlv_22= '-' ( (lv_descriptions_23_0= ruleDescription ) ) )+
            int cnt29=0;
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==15) ) {
                    int LA29_2 = input.LA(2);

                    if ( (LA29_2==RULE_ID) ) {
                        int LA29_3 = input.LA(3);

                        if ( (LA29_3==14) ) {
                            int LA29_4 = input.LA(4);

                            if ( (LA29_4==44) ) {
                                alt29=1;
                            }


                        }


                    }


                }


                switch (alt29) {
            	case 1 :
            	    // InternalGame.g:1815:4: otherlv_22= '-' ( (lv_descriptions_23_0= ruleDescription ) )
            	    {
            	    otherlv_22=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_22, grammarAccess.getActionAccess().getHyphenMinusKeyword_19_0());
            	    			
            	    // InternalGame.g:1819:4: ( (lv_descriptions_23_0= ruleDescription ) )
            	    // InternalGame.g:1820:5: (lv_descriptions_23_0= ruleDescription )
            	    {
            	    // InternalGame.g:1820:5: (lv_descriptions_23_0= ruleDescription )
            	    // InternalGame.g:1821:6: lv_descriptions_23_0= ruleDescription
            	    {

            	    						newCompositeNode(grammarAccess.getActionAccess().getDescriptionsDescriptionParserRuleCall_19_1_0());
            	    					
            	    pushFollow(FOLLOW_17);
            	    lv_descriptions_23_0=ruleDescription();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getActionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"descriptions",
            	    							lv_descriptions_23_0,
            	    							"fr.n7.game.xtext.Game.Description");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt29 >= 1 ) break loop29;
                        EarlyExitException eee =
                            new EarlyExitException(29, input);
                        throw eee;
                }
                cnt29++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleDescription"
    // InternalGame.g:1843:1: entryRuleDescription returns [EObject current=null] : iv_ruleDescription= ruleDescription EOF ;
    public final EObject entryRuleDescription() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDescription = null;


        try {
            // InternalGame.g:1843:52: (iv_ruleDescription= ruleDescription EOF )
            // InternalGame.g:1844:2: iv_ruleDescription= ruleDescription EOF
            {
             newCompositeNode(grammarAccess.getDescriptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDescription=ruleDescription();

            state._fsp--;

             current =iv_ruleDescription; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDescription"


    // $ANTLR start "ruleDescription"
    // InternalGame.g:1850:1: ruleDescription returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'texte' otherlv_3= ':' ( (lv_texte_4_0= RULE_STRING ) ) otherlv_5= 'condition' otherlv_6= ':' ( (lv_condition_7_0= ruleCondition ) ) ) ;
    public final EObject ruleDescription() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_texte_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        EObject lv_condition_7_0 = null;



        	enterRule();

        try {
            // InternalGame.g:1856:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'texte' otherlv_3= ':' ( (lv_texte_4_0= RULE_STRING ) ) otherlv_5= 'condition' otherlv_6= ':' ( (lv_condition_7_0= ruleCondition ) ) ) )
            // InternalGame.g:1857:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'texte' otherlv_3= ':' ( (lv_texte_4_0= RULE_STRING ) ) otherlv_5= 'condition' otherlv_6= ':' ( (lv_condition_7_0= ruleCondition ) ) )
            {
            // InternalGame.g:1857:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'texte' otherlv_3= ':' ( (lv_texte_4_0= RULE_STRING ) ) otherlv_5= 'condition' otherlv_6= ':' ( (lv_condition_7_0= ruleCondition ) ) )
            // InternalGame.g:1858:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' otherlv_2= 'texte' otherlv_3= ':' ( (lv_texte_4_0= RULE_STRING ) ) otherlv_5= 'condition' otherlv_6= ':' ( (lv_condition_7_0= ruleCondition ) )
            {
            // InternalGame.g:1858:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGame.g:1859:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGame.g:1859:4: (lv_name_0_0= RULE_ID )
            // InternalGame.g:1860:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_name_0_0, grammarAccess.getDescriptionAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDescriptionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_39); 

            			newLeafNode(otherlv_1, grammarAccess.getDescriptionAccess().getColonKeyword_1());
            		
            otherlv_2=(Token)match(input,44,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getDescriptionAccess().getTexteKeyword_2());
            		
            otherlv_3=(Token)match(input,14,FOLLOW_40); 

            			newLeafNode(otherlv_3, grammarAccess.getDescriptionAccess().getColonKeyword_3());
            		
            // InternalGame.g:1888:3: ( (lv_texte_4_0= RULE_STRING ) )
            // InternalGame.g:1889:4: (lv_texte_4_0= RULE_STRING )
            {
            // InternalGame.g:1889:4: (lv_texte_4_0= RULE_STRING )
            // InternalGame.g:1890:5: lv_texte_4_0= RULE_STRING
            {
            lv_texte_4_0=(Token)match(input,RULE_STRING,FOLLOW_18); 

            					newLeafNode(lv_texte_4_0, grammarAccess.getDescriptionAccess().getTexteSTRINGTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDescriptionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"texte",
            						lv_texte_4_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_5=(Token)match(input,24,FOLLOW_3); 

            			newLeafNode(otherlv_5, grammarAccess.getDescriptionAccess().getConditionKeyword_5());
            		
            otherlv_6=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_6, grammarAccess.getDescriptionAccess().getColonKeyword_6());
            		
            // InternalGame.g:1914:3: ( (lv_condition_7_0= ruleCondition ) )
            // InternalGame.g:1915:4: (lv_condition_7_0= ruleCondition )
            {
            // InternalGame.g:1915:4: (lv_condition_7_0= ruleCondition )
            // InternalGame.g:1916:5: lv_condition_7_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getDescriptionAccess().getConditionConditionParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_2);
            lv_condition_7_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDescriptionRule());
            					}
            					set(
            						current,
            						"condition",
            						lv_condition_7_0,
            						"fr.n7.game.xtext.Game.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDescription"


    // $ANTLR start "entryRuleCondition"
    // InternalGame.g:1937:1: entryRuleCondition returns [EObject current=null] : iv_ruleCondition= ruleCondition EOF ;
    public final EObject entryRuleCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCondition = null;


        try {
            // InternalGame.g:1937:50: (iv_ruleCondition= ruleCondition EOF )
            // InternalGame.g:1938:2: iv_ruleCondition= ruleCondition EOF
            {
             newCompositeNode(grammarAccess.getConditionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCondition=ruleCondition();

            state._fsp--;

             current =iv_ruleCondition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCondition"


    // $ANTLR start "ruleCondition"
    // InternalGame.g:1944:1: ruleCondition returns [EObject current=null] : ( ( (lv_condition_0_0= ruleConditionEt ) ) (otherlv_1= '||' ( (lv_condition_2_0= ruleConditionEt ) ) )* ) ;
    public final EObject ruleCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_condition_0_0 = null;

        EObject lv_condition_2_0 = null;



        	enterRule();

        try {
            // InternalGame.g:1950:2: ( ( ( (lv_condition_0_0= ruleConditionEt ) ) (otherlv_1= '||' ( (lv_condition_2_0= ruleConditionEt ) ) )* ) )
            // InternalGame.g:1951:2: ( ( (lv_condition_0_0= ruleConditionEt ) ) (otherlv_1= '||' ( (lv_condition_2_0= ruleConditionEt ) ) )* )
            {
            // InternalGame.g:1951:2: ( ( (lv_condition_0_0= ruleConditionEt ) ) (otherlv_1= '||' ( (lv_condition_2_0= ruleConditionEt ) ) )* )
            // InternalGame.g:1952:3: ( (lv_condition_0_0= ruleConditionEt ) ) (otherlv_1= '||' ( (lv_condition_2_0= ruleConditionEt ) ) )*
            {
            // InternalGame.g:1952:3: ( (lv_condition_0_0= ruleConditionEt ) )
            // InternalGame.g:1953:4: (lv_condition_0_0= ruleConditionEt )
            {
            // InternalGame.g:1953:4: (lv_condition_0_0= ruleConditionEt )
            // InternalGame.g:1954:5: lv_condition_0_0= ruleConditionEt
            {

            					newCompositeNode(grammarAccess.getConditionAccess().getConditionConditionEtParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_41);
            lv_condition_0_0=ruleConditionEt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConditionRule());
            					}
            					add(
            						current,
            						"condition",
            						lv_condition_0_0,
            						"fr.n7.game.xtext.Game.ConditionEt");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGame.g:1971:3: (otherlv_1= '||' ( (lv_condition_2_0= ruleConditionEt ) ) )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==45) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalGame.g:1972:4: otherlv_1= '||' ( (lv_condition_2_0= ruleConditionEt ) )
            	    {
            	    otherlv_1=(Token)match(input,45,FOLLOW_14); 

            	    				newLeafNode(otherlv_1, grammarAccess.getConditionAccess().getVerticalLineVerticalLineKeyword_1_0());
            	    			
            	    // InternalGame.g:1976:4: ( (lv_condition_2_0= ruleConditionEt ) )
            	    // InternalGame.g:1977:5: (lv_condition_2_0= ruleConditionEt )
            	    {
            	    // InternalGame.g:1977:5: (lv_condition_2_0= ruleConditionEt )
            	    // InternalGame.g:1978:6: lv_condition_2_0= ruleConditionEt
            	    {

            	    						newCompositeNode(grammarAccess.getConditionAccess().getConditionConditionEtParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_41);
            	    lv_condition_2_0=ruleConditionEt();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getConditionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"condition",
            	    							lv_condition_2_0,
            	    							"fr.n7.game.xtext.Game.ConditionEt");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCondition"


    // $ANTLR start "entryRuleConditionEt"
    // InternalGame.g:2000:1: entryRuleConditionEt returns [EObject current=null] : iv_ruleConditionEt= ruleConditionEt EOF ;
    public final EObject entryRuleConditionEt() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditionEt = null;


        try {
            // InternalGame.g:2000:52: (iv_ruleConditionEt= ruleConditionEt EOF )
            // InternalGame.g:2001:2: iv_ruleConditionEt= ruleConditionEt EOF
            {
             newCompositeNode(grammarAccess.getConditionEtRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConditionEt=ruleConditionEt();

            state._fsp--;

             current =iv_ruleConditionEt; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditionEt"


    // $ANTLR start "ruleConditionEt"
    // InternalGame.g:2007:1: ruleConditionEt returns [EObject current=null] : ( ( (lv_conditionTest_0_0= ruleConditionTest ) ) (otherlv_1= '&&' ( (lv_conditionTest_2_0= ruleConditionTest ) ) )* ) ;
    public final EObject ruleConditionEt() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_conditionTest_0_0 = null;

        EObject lv_conditionTest_2_0 = null;



        	enterRule();

        try {
            // InternalGame.g:2013:2: ( ( ( (lv_conditionTest_0_0= ruleConditionTest ) ) (otherlv_1= '&&' ( (lv_conditionTest_2_0= ruleConditionTest ) ) )* ) )
            // InternalGame.g:2014:2: ( ( (lv_conditionTest_0_0= ruleConditionTest ) ) (otherlv_1= '&&' ( (lv_conditionTest_2_0= ruleConditionTest ) ) )* )
            {
            // InternalGame.g:2014:2: ( ( (lv_conditionTest_0_0= ruleConditionTest ) ) (otherlv_1= '&&' ( (lv_conditionTest_2_0= ruleConditionTest ) ) )* )
            // InternalGame.g:2015:3: ( (lv_conditionTest_0_0= ruleConditionTest ) ) (otherlv_1= '&&' ( (lv_conditionTest_2_0= ruleConditionTest ) ) )*
            {
            // InternalGame.g:2015:3: ( (lv_conditionTest_0_0= ruleConditionTest ) )
            // InternalGame.g:2016:4: (lv_conditionTest_0_0= ruleConditionTest )
            {
            // InternalGame.g:2016:4: (lv_conditionTest_0_0= ruleConditionTest )
            // InternalGame.g:2017:5: lv_conditionTest_0_0= ruleConditionTest
            {

            					newCompositeNode(grammarAccess.getConditionEtAccess().getConditionTestConditionTestParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_42);
            lv_conditionTest_0_0=ruleConditionTest();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConditionEtRule());
            					}
            					add(
            						current,
            						"conditionTest",
            						lv_conditionTest_0_0,
            						"fr.n7.game.xtext.Game.ConditionTest");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGame.g:2034:3: (otherlv_1= '&&' ( (lv_conditionTest_2_0= ruleConditionTest ) ) )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==46) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalGame.g:2035:4: otherlv_1= '&&' ( (lv_conditionTest_2_0= ruleConditionTest ) )
            	    {
            	    otherlv_1=(Token)match(input,46,FOLLOW_14); 

            	    				newLeafNode(otherlv_1, grammarAccess.getConditionEtAccess().getAmpersandAmpersandKeyword_1_0());
            	    			
            	    // InternalGame.g:2039:4: ( (lv_conditionTest_2_0= ruleConditionTest ) )
            	    // InternalGame.g:2040:5: (lv_conditionTest_2_0= ruleConditionTest )
            	    {
            	    // InternalGame.g:2040:5: (lv_conditionTest_2_0= ruleConditionTest )
            	    // InternalGame.g:2041:6: lv_conditionTest_2_0= ruleConditionTest
            	    {

            	    						newCompositeNode(grammarAccess.getConditionEtAccess().getConditionTestConditionTestParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_42);
            	    lv_conditionTest_2_0=ruleConditionTest();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getConditionEtRule());
            	    						}
            	    						add(
            	    							current,
            	    							"conditionTest",
            	    							lv_conditionTest_2_0,
            	    							"fr.n7.game.xtext.Game.ConditionTest");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionEt"


    // $ANTLR start "entryRuleConditionTest"
    // InternalGame.g:2063:1: entryRuleConditionTest returns [EObject current=null] : iv_ruleConditionTest= ruleConditionTest EOF ;
    public final EObject entryRuleConditionTest() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditionTest = null;


        try {
            // InternalGame.g:2063:54: (iv_ruleConditionTest= ruleConditionTest EOF )
            // InternalGame.g:2064:2: iv_ruleConditionTest= ruleConditionTest EOF
            {
             newCompositeNode(grammarAccess.getConditionTestRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConditionTest=ruleConditionTest();

            state._fsp--;

             current =iv_ruleConditionTest; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditionTest"


    // $ANTLR start "ruleConditionTest"
    // InternalGame.g:2070:1: ruleConditionTest returns [EObject current=null] : (this_ConditionBoolean_0= ruleConditionBoolean | this_ConditionConnaissance_1= ruleConditionConnaissance | this_ConditionObjet_2= ruleConditionObjet ) ;
    public final EObject ruleConditionTest() throws RecognitionException {
        EObject current = null;

        EObject this_ConditionBoolean_0 = null;

        EObject this_ConditionConnaissance_1 = null;

        EObject this_ConditionObjet_2 = null;



        	enterRule();

        try {
            // InternalGame.g:2076:2: ( (this_ConditionBoolean_0= ruleConditionBoolean | this_ConditionConnaissance_1= ruleConditionConnaissance | this_ConditionObjet_2= ruleConditionObjet ) )
            // InternalGame.g:2077:2: (this_ConditionBoolean_0= ruleConditionBoolean | this_ConditionConnaissance_1= ruleConditionConnaissance | this_ConditionObjet_2= ruleConditionObjet )
            {
            // InternalGame.g:2077:2: (this_ConditionBoolean_0= ruleConditionBoolean | this_ConditionConnaissance_1= ruleConditionConnaissance | this_ConditionObjet_2= ruleConditionObjet )
            int alt32=3;
            switch ( input.LA(1) ) {
            case RULE_BOOLEAN:
                {
                alt32=1;
                }
                break;
            case 47:
                {
                alt32=2;
                }
                break;
            case RULE_ID:
                {
                int LA32_3 = input.LA(2);

                if ( (LA32_3==EOF||(LA32_3>=15 && LA32_3<=16)||LA32_3==18||LA32_3==20||(LA32_3>=22 && LA32_3<=23)||LA32_3==25||(LA32_3>=27 && LA32_3<=28)||(LA32_3>=32 && LA32_3<=34)||LA32_3==38||LA32_3==41||LA32_3==43||(LA32_3>=45 && LA32_3<=46)) ) {
                    alt32=2;
                }
                else if ( (LA32_3==RULE_COMPARATEUR) ) {
                    alt32=3;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 32, 3, input);

                    throw nvae;
                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }

            switch (alt32) {
                case 1 :
                    // InternalGame.g:2078:3: this_ConditionBoolean_0= ruleConditionBoolean
                    {

                    			newCompositeNode(grammarAccess.getConditionTestAccess().getConditionBooleanParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_ConditionBoolean_0=ruleConditionBoolean();

                    state._fsp--;


                    			current = this_ConditionBoolean_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalGame.g:2087:3: this_ConditionConnaissance_1= ruleConditionConnaissance
                    {

                    			newCompositeNode(grammarAccess.getConditionTestAccess().getConditionConnaissanceParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ConditionConnaissance_1=ruleConditionConnaissance();

                    state._fsp--;


                    			current = this_ConditionConnaissance_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalGame.g:2096:3: this_ConditionObjet_2= ruleConditionObjet
                    {

                    			newCompositeNode(grammarAccess.getConditionTestAccess().getConditionObjetParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_ConditionObjet_2=ruleConditionObjet();

                    state._fsp--;


                    			current = this_ConditionObjet_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionTest"


    // $ANTLR start "entryRuleConditionBoolean"
    // InternalGame.g:2108:1: entryRuleConditionBoolean returns [EObject current=null] : iv_ruleConditionBoolean= ruleConditionBoolean EOF ;
    public final EObject entryRuleConditionBoolean() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditionBoolean = null;


        try {
            // InternalGame.g:2108:57: (iv_ruleConditionBoolean= ruleConditionBoolean EOF )
            // InternalGame.g:2109:2: iv_ruleConditionBoolean= ruleConditionBoolean EOF
            {
             newCompositeNode(grammarAccess.getConditionBooleanRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConditionBoolean=ruleConditionBoolean();

            state._fsp--;

             current =iv_ruleConditionBoolean; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditionBoolean"


    // $ANTLR start "ruleConditionBoolean"
    // InternalGame.g:2115:1: ruleConditionBoolean returns [EObject current=null] : ( (lv_valeur_0_0= RULE_BOOLEAN ) ) ;
    public final EObject ruleConditionBoolean() throws RecognitionException {
        EObject current = null;

        Token lv_valeur_0_0=null;


        	enterRule();

        try {
            // InternalGame.g:2121:2: ( ( (lv_valeur_0_0= RULE_BOOLEAN ) ) )
            // InternalGame.g:2122:2: ( (lv_valeur_0_0= RULE_BOOLEAN ) )
            {
            // InternalGame.g:2122:2: ( (lv_valeur_0_0= RULE_BOOLEAN ) )
            // InternalGame.g:2123:3: (lv_valeur_0_0= RULE_BOOLEAN )
            {
            // InternalGame.g:2123:3: (lv_valeur_0_0= RULE_BOOLEAN )
            // InternalGame.g:2124:4: lv_valeur_0_0= RULE_BOOLEAN
            {
            lv_valeur_0_0=(Token)match(input,RULE_BOOLEAN,FOLLOW_2); 

            				newLeafNode(lv_valeur_0_0, grammarAccess.getConditionBooleanAccess().getValeurBOOLEANTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getConditionBooleanRule());
            				}
            				setWithLastConsumed(
            					current,
            					"valeur",
            					lv_valeur_0_0,
            					"fr.n7.game.xtext.Game.BOOLEAN");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionBoolean"


    // $ANTLR start "entryRuleConditionConnaissance"
    // InternalGame.g:2143:1: entryRuleConditionConnaissance returns [EObject current=null] : iv_ruleConditionConnaissance= ruleConditionConnaissance EOF ;
    public final EObject entryRuleConditionConnaissance() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditionConnaissance = null;


        try {
            // InternalGame.g:2143:62: (iv_ruleConditionConnaissance= ruleConditionConnaissance EOF )
            // InternalGame.g:2144:2: iv_ruleConditionConnaissance= ruleConditionConnaissance EOF
            {
             newCompositeNode(grammarAccess.getConditionConnaissanceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConditionConnaissance=ruleConditionConnaissance();

            state._fsp--;

             current =iv_ruleConditionConnaissance; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditionConnaissance"


    // $ANTLR start "ruleConditionConnaissance"
    // InternalGame.g:2150:1: ruleConditionConnaissance returns [EObject current=null] : ( ( (lv_negation_0_0= '!' ) )? ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleConditionConnaissance() throws RecognitionException {
        EObject current = null;

        Token lv_negation_0_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalGame.g:2156:2: ( ( ( (lv_negation_0_0= '!' ) )? ( (otherlv_1= RULE_ID ) ) ) )
            // InternalGame.g:2157:2: ( ( (lv_negation_0_0= '!' ) )? ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalGame.g:2157:2: ( ( (lv_negation_0_0= '!' ) )? ( (otherlv_1= RULE_ID ) ) )
            // InternalGame.g:2158:3: ( (lv_negation_0_0= '!' ) )? ( (otherlv_1= RULE_ID ) )
            {
            // InternalGame.g:2158:3: ( (lv_negation_0_0= '!' ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==47) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalGame.g:2159:4: (lv_negation_0_0= '!' )
                    {
                    // InternalGame.g:2159:4: (lv_negation_0_0= '!' )
                    // InternalGame.g:2160:5: lv_negation_0_0= '!'
                    {
                    lv_negation_0_0=(Token)match(input,47,FOLLOW_5); 

                    					newLeafNode(lv_negation_0_0, grammarAccess.getConditionConnaissanceAccess().getNegationExclamationMarkKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getConditionConnaissanceRule());
                    					}
                    					setWithLastConsumed(current, "negation", lv_negation_0_0, "!");
                    				

                    }


                    }
                    break;

            }

            // InternalGame.g:2172:3: ( (otherlv_1= RULE_ID ) )
            // InternalGame.g:2173:4: (otherlv_1= RULE_ID )
            {
            // InternalGame.g:2173:4: (otherlv_1= RULE_ID )
            // InternalGame.g:2174:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConditionConnaissanceRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_1, grammarAccess.getConditionConnaissanceAccess().getConnaissanceConnaissanceCrossReference_1_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionConnaissance"


    // $ANTLR start "entryRuleConditionObjet"
    // InternalGame.g:2189:1: entryRuleConditionObjet returns [EObject current=null] : iv_ruleConditionObjet= ruleConditionObjet EOF ;
    public final EObject entryRuleConditionObjet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditionObjet = null;


        try {
            // InternalGame.g:2189:55: (iv_ruleConditionObjet= ruleConditionObjet EOF )
            // InternalGame.g:2190:2: iv_ruleConditionObjet= ruleConditionObjet EOF
            {
             newCompositeNode(grammarAccess.getConditionObjetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConditionObjet=ruleConditionObjet();

            state._fsp--;

             current =iv_ruleConditionObjet; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditionObjet"


    // $ANTLR start "ruleConditionObjet"
    // InternalGame.g:2196:1: ruleConditionObjet returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) ( (lv_comparateur_1_0= RULE_COMPARATEUR ) ) ( (lv_nombre_2_0= RULE_INT ) ) ) ;
    public final EObject ruleConditionObjet() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_comparateur_1_0=null;
        Token lv_nombre_2_0=null;


        	enterRule();

        try {
            // InternalGame.g:2202:2: ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_comparateur_1_0= RULE_COMPARATEUR ) ) ( (lv_nombre_2_0= RULE_INT ) ) ) )
            // InternalGame.g:2203:2: ( ( (otherlv_0= RULE_ID ) ) ( (lv_comparateur_1_0= RULE_COMPARATEUR ) ) ( (lv_nombre_2_0= RULE_INT ) ) )
            {
            // InternalGame.g:2203:2: ( ( (otherlv_0= RULE_ID ) ) ( (lv_comparateur_1_0= RULE_COMPARATEUR ) ) ( (lv_nombre_2_0= RULE_INT ) ) )
            // InternalGame.g:2204:3: ( (otherlv_0= RULE_ID ) ) ( (lv_comparateur_1_0= RULE_COMPARATEUR ) ) ( (lv_nombre_2_0= RULE_INT ) )
            {
            // InternalGame.g:2204:3: ( (otherlv_0= RULE_ID ) )
            // InternalGame.g:2205:4: (otherlv_0= RULE_ID )
            {
            // InternalGame.g:2205:4: (otherlv_0= RULE_ID )
            // InternalGame.g:2206:5: otherlv_0= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConditionObjetRule());
            					}
            				
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_43); 

            					newLeafNode(otherlv_0, grammarAccess.getConditionObjetAccess().getObjetObjetCrossReference_0_0());
            				

            }


            }

            // InternalGame.g:2217:3: ( (lv_comparateur_1_0= RULE_COMPARATEUR ) )
            // InternalGame.g:2218:4: (lv_comparateur_1_0= RULE_COMPARATEUR )
            {
            // InternalGame.g:2218:4: (lv_comparateur_1_0= RULE_COMPARATEUR )
            // InternalGame.g:2219:5: lv_comparateur_1_0= RULE_COMPARATEUR
            {
            lv_comparateur_1_0=(Token)match(input,RULE_COMPARATEUR,FOLLOW_12); 

            					newLeafNode(lv_comparateur_1_0, grammarAccess.getConditionObjetAccess().getComparateurCOMPARATEURTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConditionObjetRule());
            					}
            					setWithLastConsumed(
            						current,
            						"comparateur",
            						lv_comparateur_1_0,
            						"fr.n7.game.xtext.Game.COMPARATEUR");
            				

            }


            }

            // InternalGame.g:2235:3: ( (lv_nombre_2_0= RULE_INT ) )
            // InternalGame.g:2236:4: (lv_nombre_2_0= RULE_INT )
            {
            // InternalGame.g:2236:4: (lv_nombre_2_0= RULE_INT )
            // InternalGame.g:2237:5: lv_nombre_2_0= RULE_INT
            {
            lv_nombre_2_0=(Token)match(input,RULE_INT,FOLLOW_2); 

            					newLeafNode(lv_nombre_2_0, grammarAccess.getConditionObjetAccess().getNombreINTTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConditionObjetRule());
            					}
            					setWithLastConsumed(
            						current,
            						"nombre",
            						lv_nombre_2_0,
            						"org.eclipse.xtext.common.Terminals.INT");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionObjet"

    // Delegated rules


    protected DFA21 dfa21 = new DFA21(this);
    protected DFA25 dfa25 = new DFA25(this);
    static final String dfa_1s = "\43\uffff";
    static final String dfa_2s = "\1\1\42\uffff";
    static final String dfa_3s = "\1\17\1\uffff\1\4\1\16\1\26\1\16\1\4\1\33\1\4\1\10\2\4\1\uffff\1\33\1\5\1\33\1\4\1\10\1\33\1\4\1\10\2\33\1\5\1\4\1\33\1\5\2\33\1\4\1\10\2\33\1\5\1\33";
    static final String dfa_4s = "\1\24\1\uffff\1\4\1\16\1\26\1\16\1\57\1\56\1\4\1\56\2\57\1\uffff\1\56\1\5\1\56\1\4\2\56\1\4\3\56\1\5\1\57\1\56\1\5\2\56\1\4\3\56\1\5\1\56";
    static final String dfa_5s = "\1\uffff\1\2\12\uffff\1\1\26\uffff";
    static final String dfa_6s = "\43\uffff}>";
    static final String[] dfa_7s = {
            "\1\2\4\uffff\1\1",
            "",
            "\1\3",
            "\1\4",
            "\1\5",
            "\1\6",
            "\1\11\2\uffff\1\7\47\uffff\1\10",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\15",
            "\1\16\22\uffff\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\21\2\uffff\1\17\47\uffff\1\20",
            "\1\24\2\uffff\1\22\47\uffff\1\23",
            "",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\25",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\26",
            "\1\27\22\uffff\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\30",
            "\1\31",
            "\1\32\22\uffff\1\14\12\uffff\1\1\6\uffff\1\13\1\30",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\33",
            "\1\36\2\uffff\1\34\47\uffff\1\35",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\30",
            "\1\37",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\12",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\30",
            "\1\40",
            "\1\41\22\uffff\1\14\12\uffff\1\1\6\uffff\1\13\1\30",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\30",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\30",
            "\1\42",
            "\1\14\12\uffff\1\1\6\uffff\1\13\1\30"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA21 extends DFA {

        public DFA21(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 21;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "()+ loopback of 1423:3: (otherlv_10= '-' ( (lv_interactions_11_0= ruleInteraction ) ) )+";
        }
    }
    static final String[] dfa_8s = {
            "\1\2\4\uffff\1\1",
            "",
            "\1\3",
            "\1\4",
            "\1\5",
            "\1\6",
            "\1\11\2\uffff\1\7\47\uffff\1\10",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\15",
            "\1\16\22\uffff\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\21\2\uffff\1\17\47\uffff\1\20",
            "\1\24\2\uffff\1\22\47\uffff\1\23",
            "",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\25",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\26",
            "\1\27\22\uffff\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30",
            "\1\31",
            "\1\32\22\uffff\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\33",
            "\1\36\2\uffff\1\34\47\uffff\1\35",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30",
            "\1\37",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\12",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30",
            "\1\40",
            "\1\41\22\uffff\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30",
            "\1\42",
            "\1\1\12\uffff\1\1\4\uffff\1\14\1\uffff\1\13\1\30"
    };
    static final short[][] dfa_8 = unpackEncodedStringArray(dfa_8s);

    class DFA25 extends DFA {

        public DFA25(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 25;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_8;
        }
        public String getDescription() {
            return "()+ loopback of 1605:3: (otherlv_19= '-' ( (lv_actions_20_0= ruleAction ) ) )+";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000028000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000048000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000108000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000800000000090L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000004008000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000010008000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000040008000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000808000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000008008000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000008000008000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000010000008000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000040000008000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000200000000002L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000400000000002L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000000000000100L});

}