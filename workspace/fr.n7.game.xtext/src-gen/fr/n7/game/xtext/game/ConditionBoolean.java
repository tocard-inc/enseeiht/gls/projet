/**
 * generated by Xtext 2.23.0
 */
package fr.n7.game.xtext.game;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Condition Boolean</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.n7.game.xtext.game.ConditionBoolean#getValeur <em>Valeur</em>}</li>
 * </ul>
 *
 * @see fr.n7.game.xtext.game.GamePackage#getConditionBoolean()
 * @model
 * @generated
 */
public interface ConditionBoolean extends ConditionTest
{
  /**
   * Returns the value of the '<em><b>Valeur</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Valeur</em>' attribute.
   * @see #setValeur(String)
   * @see fr.n7.game.xtext.game.GamePackage#getConditionBoolean_Valeur()
   * @model
   * @generated
   */
  String getValeur();

  /**
   * Sets the value of the '{@link fr.n7.game.xtext.game.ConditionBoolean#getValeur <em>Valeur</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Valeur</em>' attribute.
   * @see #getValeur()
   * @generated
   */
  void setValeur(String value);

} // ConditionBoolean
