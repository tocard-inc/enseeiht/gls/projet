/**
 * generated by Xtext 2.23.0
 */
package fr.n7.game.xtext.game.impl;

import fr.n7.game.xtext.game.ConditionConnaissance;
import fr.n7.game.xtext.game.Connaissance;
import fr.n7.game.xtext.game.GamePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Condition Connaissance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.n7.game.xtext.game.impl.ConditionConnaissanceImpl#getNegation <em>Negation</em>}</li>
 *   <li>{@link fr.n7.game.xtext.game.impl.ConditionConnaissanceImpl#getConnaissance <em>Connaissance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConditionConnaissanceImpl extends ConditionTestImpl implements ConditionConnaissance
{
  /**
   * The default value of the '{@link #getNegation() <em>Negation</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNegation()
   * @generated
   * @ordered
   */
  protected static final String NEGATION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getNegation() <em>Negation</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNegation()
   * @generated
   * @ordered
   */
  protected String negation = NEGATION_EDEFAULT;

  /**
   * The cached value of the '{@link #getConnaissance() <em>Connaissance</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConnaissance()
   * @generated
   * @ordered
   */
  protected Connaissance connaissance;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ConditionConnaissanceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return GamePackage.Literals.CONDITION_CONNAISSANCE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getNegation()
  {
    return negation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setNegation(String newNegation)
  {
    String oldNegation = negation;
    negation = newNegation;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, GamePackage.CONDITION_CONNAISSANCE__NEGATION, oldNegation, negation));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Connaissance getConnaissance()
  {
    if (connaissance != null && connaissance.eIsProxy())
    {
      InternalEObject oldConnaissance = (InternalEObject)connaissance;
      connaissance = (Connaissance)eResolveProxy(oldConnaissance);
      if (connaissance != oldConnaissance)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, GamePackage.CONDITION_CONNAISSANCE__CONNAISSANCE, oldConnaissance, connaissance));
      }
    }
    return connaissance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Connaissance basicGetConnaissance()
  {
    return connaissance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setConnaissance(Connaissance newConnaissance)
  {
    Connaissance oldConnaissance = connaissance;
    connaissance = newConnaissance;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, GamePackage.CONDITION_CONNAISSANCE__CONNAISSANCE, oldConnaissance, connaissance));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case GamePackage.CONDITION_CONNAISSANCE__NEGATION:
        return getNegation();
      case GamePackage.CONDITION_CONNAISSANCE__CONNAISSANCE:
        if (resolve) return getConnaissance();
        return basicGetConnaissance();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case GamePackage.CONDITION_CONNAISSANCE__NEGATION:
        setNegation((String)newValue);
        return;
      case GamePackage.CONDITION_CONNAISSANCE__CONNAISSANCE:
        setConnaissance((Connaissance)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case GamePackage.CONDITION_CONNAISSANCE__NEGATION:
        setNegation(NEGATION_EDEFAULT);
        return;
      case GamePackage.CONDITION_CONNAISSANCE__CONNAISSANCE:
        setConnaissance((Connaissance)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case GamePackage.CONDITION_CONNAISSANCE__NEGATION:
        return NEGATION_EDEFAULT == null ? negation != null : !NEGATION_EDEFAULT.equals(negation);
      case GamePackage.CONDITION_CONNAISSANCE__CONNAISSANCE:
        return connaissance != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (negation: ");
    result.append(negation);
    result.append(')');
    return result.toString();
  }

} //ConditionConnaissanceImpl
