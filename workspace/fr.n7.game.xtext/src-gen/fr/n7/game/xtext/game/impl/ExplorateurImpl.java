/**
 * generated by Xtext 2.23.0
 */
package fr.n7.game.xtext.game.impl;

import fr.n7.game.xtext.game.Connaissance;
import fr.n7.game.xtext.game.Explorateur;
import fr.n7.game.xtext.game.GamePackage;
import fr.n7.game.xtext.game.Objet;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Explorateur</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.n7.game.xtext.game.impl.ExplorateurImpl#getTailleInventaire <em>Taille Inventaire</em>}</li>
 *   <li>{@link fr.n7.game.xtext.game.impl.ExplorateurImpl#getConnaissances <em>Connaissances</em>}</li>
 *   <li>{@link fr.n7.game.xtext.game.impl.ExplorateurImpl#getObjets <em>Objets</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExplorateurImpl extends MinimalEObjectImpl.Container implements Explorateur
{
  /**
   * The default value of the '{@link #getTailleInventaire() <em>Taille Inventaire</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTailleInventaire()
   * @generated
   * @ordered
   */
  protected static final int TAILLE_INVENTAIRE_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getTailleInventaire() <em>Taille Inventaire</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTailleInventaire()
   * @generated
   * @ordered
   */
  protected int tailleInventaire = TAILLE_INVENTAIRE_EDEFAULT;

  /**
   * The cached value of the '{@link #getConnaissances() <em>Connaissances</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConnaissances()
   * @generated
   * @ordered
   */
  protected EList<Connaissance> connaissances;

  /**
   * The cached value of the '{@link #getObjets() <em>Objets</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getObjets()
   * @generated
   * @ordered
   */
  protected EList<Objet> objets;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ExplorateurImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return GamePackage.Literals.EXPLORATEUR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int getTailleInventaire()
  {
    return tailleInventaire;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setTailleInventaire(int newTailleInventaire)
  {
    int oldTailleInventaire = tailleInventaire;
    tailleInventaire = newTailleInventaire;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, GamePackage.EXPLORATEUR__TAILLE_INVENTAIRE, oldTailleInventaire, tailleInventaire));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<Connaissance> getConnaissances()
  {
    if (connaissances == null)
    {
      connaissances = new EObjectResolvingEList<Connaissance>(Connaissance.class, this, GamePackage.EXPLORATEUR__CONNAISSANCES);
    }
    return connaissances;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<Objet> getObjets()
  {
    if (objets == null)
    {
      objets = new EObjectResolvingEList<Objet>(Objet.class, this, GamePackage.EXPLORATEUR__OBJETS);
    }
    return objets;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case GamePackage.EXPLORATEUR__TAILLE_INVENTAIRE:
        return getTailleInventaire();
      case GamePackage.EXPLORATEUR__CONNAISSANCES:
        return getConnaissances();
      case GamePackage.EXPLORATEUR__OBJETS:
        return getObjets();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case GamePackage.EXPLORATEUR__TAILLE_INVENTAIRE:
        setTailleInventaire((Integer)newValue);
        return;
      case GamePackage.EXPLORATEUR__CONNAISSANCES:
        getConnaissances().clear();
        getConnaissances().addAll((Collection<? extends Connaissance>)newValue);
        return;
      case GamePackage.EXPLORATEUR__OBJETS:
        getObjets().clear();
        getObjets().addAll((Collection<? extends Objet>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case GamePackage.EXPLORATEUR__TAILLE_INVENTAIRE:
        setTailleInventaire(TAILLE_INVENTAIRE_EDEFAULT);
        return;
      case GamePackage.EXPLORATEUR__CONNAISSANCES:
        getConnaissances().clear();
        return;
      case GamePackage.EXPLORATEUR__OBJETS:
        getObjets().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case GamePackage.EXPLORATEUR__TAILLE_INVENTAIRE:
        return tailleInventaire != TAILLE_INVENTAIRE_EDEFAULT;
      case GamePackage.EXPLORATEUR__CONNAISSANCES:
        return connaissances != null && !connaissances.isEmpty();
      case GamePackage.EXPLORATEUR__OBJETS:
        return objets != null && !objets.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (tailleInventaire: ");
    result.append(tailleInventaire);
    result.append(')');
    return result.toString();
  }

} //ExplorateurImpl
