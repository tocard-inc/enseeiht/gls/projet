# Projet d'Ingénierie Dirigée par les Modèles (IDM)

## Built with

- [Java](https://www.java.com/en/)
- [Eclipse](https://www.eclipse.org/)
- [EGit](https://www.eclipse.org/egit/)
- [PlantUML](https://plantuml.com/)

## Contributing

Please use [conventional commits](https://www.conventionalcommits.org/).

## License

Distributed under the [MIT](https://choosealicense.com/licenses/mit/) license.
See [`LICENSE`](https://git.inpt.fr/fainsil/booplaysgba/-/blob/master/LICENSE) for more information.

## Contacts

Damien Guillotin \<[damguillotin@gmail.com](mailto:damguillotin@gmail.com)\> \
Laurent Fainsin \<[laurentfainsin@protonmail.com](mailto:laurentfainsin@protonmail.com)\> \
Philippe Negrel-Jerzy \<[philippe.negreljerzy@etu.inp-n7.fr](mailto:philippe.negreljerzy@etu.inp-n7.fr)\> \
Sébastien Pont \<[sebastien.pont@etu.inp-n7.fr](mailto:sebastien.pont@etu.inp-n7.fr)\>
