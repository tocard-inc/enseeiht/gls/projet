module Game2PetriNet;
create OUT: petrinet from  IN: game;




helper context String
def: getJeu() : game!Jeu =
	game!Jeu.allInstances()
		->asSequence()
		->first();

helper context String
def: getTaille() : Integer =
	game!Objet.allInstances()
		->asSequence()
		->select(o | o.name = self)
		->first().taille;

helper context game!Lieu
def : estDepart() : Integer =
	self.depart.condition
		->select(ce | ce.conditionTest
			->select(ct | ct.oclIsTypeOf(game!ConditionBoolean))
			->select(ct | ct.valeur = 'true')
			->size() > 0)
		->size();

-- Obtenir la taille de l'explorateur
helper context String
def: getTailleExp() : Integer =
	game!Explorateur.allInstances()
			->asSequence()
			->first().tailleInventaire;

helper context String
def: getTransition() : petrinet!Transition =
	petrinet!Transition.allInstances()
			->asSequence()
			->select(t | t.name = self)
			->first();

-- Nombre d'objets initiaux de l'explo
helper context game!Objet
def : getNombreInit() : Integer =
	game!Explorateur.allInstances()
			->asSequence()
			->first().objets
			->select(o | o.name = self.name)
			->size();

-- Nombre de connaissances initiaux de l'explo
helper context game!Connaissance
def : getNombreInit() : Integer =
	game!Explorateur.allInstances()
			->asSequence()
			->first().connaissances
			->select(c | c.name = self.name)
			->size();

-- Nombre de place libre itiale de l'explo
helper context game!Explorateur
def : getNombreInit() : Integer =
	game!Objet.allInstances()
			->asSequence()
			->iterate(o; res : Integer = self.tailleInventaire | res - o.getNombreInit() * o.taille);


helper context game!Lieu
def: getPlace(): petrinet!Place =
	petrinet!Place.allInstances()
			->select(p | p.name = 'lieu_' + self.name)
			->asSequence()
			->first();

helper context game!Interaction
def: getPersonne(): game!Personne =
	game!Personne.allInstances()
			->select(p | (p.interactions
							->select(i | i = self)
							->size()
						) > 0)
			->asSequence()
			->first();

helper context String
def : getObjet() : game!Objet =
	game!Objet.allInstances()
			->select(o | o.name = self)
			->asSequence()
			->first();

helper context game!Personne
def: getLieu(): game!Lieu =
	game!Lieu.allInstances()
			->select(l | l.personnes
						->select(p | p.name = self.name)
						->asSequence()
						->size() > 0)
			->asSequence()
			->first();

helper context game!Action
def : getInteraction() : game!Interaction =
	game!Interaction.allInstances()
			->select(i | i.actions
						->select(a | a.name = self.name)
						->asSequence()
						->size() > 0)
			->asSequence()
			->first();

helper context String
def : getPlace() : petrinet!Place =
	petrinet!Place.allInstances()
			->select(p | p.name = self)
			->asSequence()
			->first();


rule Game2PetriNet {
	from p: game!Jeu
	to pn: petrinet!Network (name <- 'jeu')
}

rule Explorateur2Petrinet {
	from e: game!Explorateur
	to
		p: petrinet!Place(
			name <- 'taille',
			tokens <- e.getNombreInit(),
			network <- ''.getJeu())
}

rule Objet2PetriNet {
	from o: game!Objet
	to
		p_obj: petrinet!Place(
			name <- 'objet_' + o.name,
			tokens <- o.getNombreInit(),
			network <- ''.getJeu()),
		
		p_objneg: petrinet!Place(
			name <- 'objet_' + o.name + '_neg',
			tokens <- ''.getTailleExp() div o.taille - o.getNombreInit(),
			network <- ''.getJeu())
}

rule Connaissance2PetriNet {
	from c: game!Connaissance
	to
		p_con: petrinet!Place(
			name <- 'connaissance_' + c.name,
			tokens <- c.getNombreInit(),
			network <- ''.getJeu()),
			
		p_conneg: petrinet!Place(
			name <- 'connaissance_' + c.name + '_neg',
			tokens <- 1 - c.getNombreInit(),
			network <- ''.getJeu())
}

rule Personne2PetriNet {
	from p: game!Personne
	to
		pers: petrinet!Place(
			name <- 'personne_' + p.name,
			tokens <- 0,
			network <- ''.getJeu())
}

rule Lieu2PetriNet {
	from l: game!Lieu
	to
		p: petrinet!Place(
				name <- 'lieu_' + l.name,
				tokens <- l.estDepart(),
				network <- ''.getJeu())
}

rule Chemin2PetriNet {
	from c: game!Chemin
	using {
		index : Integer = 0;
	}
	do {
		index <- 0;
		for (condVis in c.visible.condition) {
			for (condOuv in c.ouvert.condition) {
				thisModule.newChemin(c, index, condVis.conditionTest.union(condOuv.conditionTest));
				index <- index + 1;
			}
		}
	}
}

rule newChemin(c : game!Chemin, index : Integer, conditions : game!ConditionTest) {
	to
		t: petrinet!Transition(
				name <- 'chemin_' + c.name + '_' + index.toString(),
				network <- ''.getJeu()),
				
		arcIn: petrinet!Arc(
				place <- ('lieu_' + c.lieuIn.name).getPlace(),
				transition <- t,
				outgoing <- false,
				weight <- 1),
				
		arcOut: petrinet!Arc(
				place <- ('lieu_' + c.lieuOut.name).getPlace(),
				transition <- t,
				outgoing <- true,
				weight <- 1)
	do {
		for (o in c.objetsConso) {
			thisModule.consoObjet(t, o.name);
		}
		for (o in c.objetsRecus) {
			thisModule.recuObjet(t, o.name);
		}
		for (con in c.connaissances) {
			thisModule.recuConn(t, con.name);
		}
		thisModule.Condition2PetriNet(t, conditions);
	}
}

rule Interaction2PetriNet {
	from i: game!Interaction
	using {
		index : Integer = 0;
	}
	do {
		index <- 0;
		for (condVis in i.getPersonne().visible.condition) {
			thisModule.newInteraction(i, index, condVis.conditionTest);
			index <- index + 1;
		}
	}
}

rule newInteraction(i : game!Interaction, index : Integer, conditions : game!ConditionTest) {
	to
		t: petrinet!Transition(
				name <- 'interaction_' + i.name + '_' + i.getPersonne().name + '_' + index.toString(),
				network <- ''.getJeu()),
				
		arcIn: petrinet!Arc(
				place <- ('personne_' + i.getPersonne().name).getPlace(),
				transition <- t,
				outgoing <- true,
				weight <- 1),
				
		arcOut: petrinet!Arc(
				place <- ('lieu_' + i.getPersonne().getLieu().name).getPlace(),
				transition <- t,
				outgoing <- false,
				weight <- 1)
	do {
		for (o in i.objetsConso) {
			thisModule.consoObjet(t, o.name);
		}
		for (o in i.objetsRecus) {
			thisModule.recuObjet(t, o.name);
		}
		for (con in i.connaissances) {
			thisModule.recuConn(t, con.name);
		}
		thisModule.Condition2PetriNet(t, conditions);
	}
}


rule Condition2PetriNet(t : petrinet!Transition, c : game!Condition) {
	do {
		for (cond in c) {
			if (cond.oclIsKindOf(game!ConditionObjet)) {
				if (cond.comparateur = '==') {
					thisModule.readObjetEga(t, cond.objet.name, cond.nombre);
				} else if (cond.comparateur = '>') {
					thisModule.readObjetSup(t, cond.objet.name, cond.nombre);
				} else if (cond.comparateur = '>=') {
					thisModule.readObjetSup(t, cond.objet.name, cond.nombre - 1);
				} else if (cond.comparateur = '<') {
					thisModule.readObjetInf(t, cond.objet.name, cond.nombre);
				} else if (cond.comparateur = '<=') {
					thisModule.readObjetInf(t, cond.objet.name, cond.nombre + 1);
				}
			} else if (cond.oclIsKindOf(game!ConditionConnaissance)) {
				if (cond.negation = '!') {
					thisModule.readConnNeg(t, cond.connaissance.name);
				} else {
					thisModule.readConn(t, cond.connaissance.name);
				}
			} else {
			}
		}
	}
}

rule Action2PetriNet {
	from a: game!Action
	using {
		index : Integer = 0;
	}
	do {
		index <- 0;
		for (condVis in a.visible.condition) {
			thisModule.newAction(a, index, condVis.conditionTest);
			index <- index + 1;
		}
	}
}

rule newAction(a : game!Action, index : Integer, conditions : game!ConditionTest) {
	to
		t: petrinet!Transition(
				name <- 'action_' + a.name,
				network <- ''.getJeu()),
				
		arcIn: petrinet!Arc(
				place <- a.getInteraction().getPersonne(),
				transition <- t,
				outgoing <- false,
				weight <- 1),
		
		arcOut: petrinet!Arc(
				place <- a.getInteraction().getPersonne().getLieu().getPlace(),
				transition <- t,
				outgoing <- true,
				weight <- 1)
	do {
		for (o in a.objetsConso) {
			thisModule.consoObjet(t, o.name);
		}
		for (o in a.objetsRecus) {
			thisModule.recuObjet(t, o.name);
		}
		for (c in a.connaissances) {
			thisModule.recuConn(t, c.name);
		}
	}
}

rule consoObjet(t : game!Transition, s : String) {
	to
		arc : petrinet!Arc(
			place <- ('objet_' + s).getPlace(),
			transition <- t,
			outgoing <- false,
			weight <- 1),
		
		arcNeg : petrinet!Arc(
			place <- ('objet_' + s + '_neg').getPlace(),
			transition <- t,
			outgoing <- true,
			weight <- 1),
		
		arcTaille : petrinet!Arc(
			place <- 'taille'.getPlace(),
			transition <- t,
			outgoing <- true,
			weight <- s.getObjet().taille)
}

rule readObjetEga(t : game!Transition, s : String, n : Integer) {
	to
		arc : petrinet!Arc(
			place <- ('objet_' + s).getPlace(),
			transition <- t,
			outgoing <- false,
			weight <- n),
		
		arc2 : petrinet!Arc(
			place <- ('objet_' + s).getPlace(),
			transition <- t,
			outgoing <- true,
			weight <- n),
		
		arc_neg : petrinet!Arc(
			place <- ('objet_' + s + '_neg').getPlace(),
			transition <- t,
			outgoing <- false,
			weight <- (''.getTailleExp() div s.getTaille()) - n),
		
		arc2_neg : petrinet!Arc(
			place <- ('objet_' + s + '_neg').getPlace(),
			transition <- t,
			outgoing <- true,
			weight <- (''.getTailleExp() div s.getTaille()) - n)
}

rule readObjetSup(t : game!Transition, s : String, n : Integer) {
	to
		arc : petrinet!Arc(
			place <- ('objet_' + s).getPlace(),
			transition <- t,
			outgoing <- false,
			weight <- n + 1),
		
		arc2 : petrinet!Arc(
			place <- ('objet_' + s).getPlace(),
			transition <- t,
			outgoing <- true,
			weight <- n + 1)
}

rule readObjetInf(t : game!Transition, s : String, n : Integer) {
	to
		arc : petrinet!Arc(
			place <- ('objet_' + s + '_neg').getPlace(),
			transition <- t,
			outgoing <- false,
			weight <- (s.getTailleExp() div s.getTaille()) - n + 1),
		
		arc2 : petrinet!Arc(
			place <- ('objet_' + s + '_neg').getPlace(),
			transition <- t,
			outgoing <- true,
			weight <- (s.getTailleExp() div s.getTaille()) - n + 1)
}

rule readConn(t : game!Transition, s : String) {
	to
		arc : petrinet!Arc(
			place <- ('connaissance_' + s).getPlace(),
			transition <- t,
			outgoing <- false,
			weight <- 1),
		
		arc2 : petrinet!Arc(
			place <- ('connaissance_' + s).getPlace(),
			transition <- t,
			outgoing <- true,
			weight <- 1)
}

rule readConnNeg(t : game!Transition, s : String) {
	to
		arc : petrinet!Arc(
			place <- ('connaissance_' + s + '_neg').getPlace(),
			transition <- t,
			outgoing <- false,
			weight <- 1),
		
		arc2 : petrinet!Arc(
			place <- ('connaissance_' + s + '_neg').getPlace(),
			transition <- t,
			outgoing <- true,
			weight <- 1)
}

rule recuObjet(t : game!Transition, s : String) {
	to
		arc : petrinet!Arc(
			place <- ('objet_' + s).getPlace(),
			transition <- t,
			outgoing <- true,
			weight <- 1),
		
		arcNeg : petrinet!Arc(
			place <- ('objet_' + s + '_neg').getPlace(),
			transition <- t,
			outgoing <- false,
			weight <- 1),
		
		arcTaille : petrinet!Arc(
			place <- 'taille'.getPlace(),
			transition <- t,
			outgoing <- false,
			weight <- s.getObjet().taille)
}

rule recuConn(t : game!Transition, s : String) {
	to
		arc : petrinet!Arc(
			place <- ('connaissance_' + s).getPlace(),
			transition <- t,
			outgoing <- true,
			weight <- 1),
		
		arcNeg : petrinet!Arc(
			place <- ('connaissance_' + s + '_neg').getPlace(),
			transition <- t,
			outgoing <- false,
			weight <- 1)
}