import java.util.List;

public class Explorateur {
    int taille;
    List<Connaissance> connaissances;
    List<Objet> objets;

    public Explorateur(
            int taille,
            List<Connaissance> connaissances,
            List<Objet> objets) {
        this.taille = taille;
        this.connaissances = connaissances;
        this.objets = objets;
    }

    @Override
    public String toString() {
        return "\tObjets : " + this.objets + "\n\tConnaissances : " + this.connaissances;
    }

}
