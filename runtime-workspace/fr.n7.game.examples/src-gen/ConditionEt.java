import java.util.List;

public class ConditionEt {

    List<ConditionTest> conditionTests;

    public ConditionEt(List<ConditionTest> conditionTests) {
        this.conditionTests = conditionTests;
    }

    public Boolean evaluer() {
        for (ConditionTest cond : conditionTests) {
            if (!cond.evaluer()) {
                return false;
            }
        }
        return true;
    }

}
