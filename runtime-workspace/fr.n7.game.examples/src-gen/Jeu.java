import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Jeu {
    public static Explorateur explorateur;
    Territoire territoire;
    Map<String, Objet> objets;
    Map<String, Connaissance> connaissances;
    Map<String, Personne> personnes;
    Map<String, Transformation> transformations;

    static Lieu lieu = null;

    static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
        System.out.println("Lieu actuel: " + lieu + "\n");
        System.out.println("Explorateur:\n" + Jeu.explorateur + "\n");
        System.out.println("-".repeat(50));
    }

    public Jeu(
            Territoire territoire,
            Map<String, Objet> objets,
            Map<String, Connaissance> connaissances,
            Map<String, Personne> personnes,
            Map<String, Transformation> transformations) {
        this.territoire = territoire;
        this.objets = objets;
        this.connaissances = connaissances;
        this.personnes = personnes;
        this.transformations = transformations;
    }

    void jouer() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        // on cherche le (premier) lieu de départ
        for (Lieu l : territoire.lieux.values()) {
            if (l.depart.evaluer()) {
                lieu = l;
                break;
            }
        }

        // tant qu'on est pas sur un lieu de fin
        mainloop: while (!lieu.fin.evaluer()) {

            int selection = 0;
            List<Object> choix = new ArrayList<>();
            clearScreen();

            if (lieu.deposable.evaluer()) {
                System.out.println("[" + choix.size() + "] déposer/ramasser");
                choix.add(lieu);
            }

            for (Personne p : lieu.personnes) {
                if (p.visible.evaluer() && p.obligatoire.evaluer()) {
                    System.out.println(p + ":");
                    p.interragir(reader);
                    continue mainloop;
                } else if (lieu.personnes.contains(p) && p.visible.evaluer()) {
                    System.out.println("[" + choix.size() + "] " + p);
                    choix.add(p);
                }
            }

            for (Chemin c : territoire.chemins.values()) {
                if (c.lieuIn == lieu) {
                    if (c.visible.evaluer() && c.obligatoire.evaluer() && c.ouvert.evaluer()) {
                        lieu = c.lieuOut;
                        continue mainloop;
                    } else if (c.visible.evaluer()) {
                        if (c.ouvert.evaluer()) {
                            System.out.println("[" + choix.size() + "] " + c);
                            choix.add(c);
                        } else {
                            System.out.println("[X] " + c);
                        }
                    }
                }
            }

            for (Transformation t : transformations.values()) {
                if (t.possible()) {
                    System.out.println("[" + choix.size() + "] " + t);
                    choix.add(t);
                }
            }

            try {
                System.out.print("\nChoix : ");
                selection = Integer.parseInt(reader.readLine());
                Object choix_selection = choix.get(selection);

                if (choix_selection instanceof Lieu) {
                    ((Lieu) choix_selection).interragir(reader);
                } else if (choix_selection instanceof Chemin) {
                    lieu = ((Chemin) choix_selection).emprunter();
                } else if (choix_selection instanceof Personne) {
                    ((Personne) choix_selection).interragir(reader);
                } else if (choix_selection instanceof Transformation) {
                    ((Transformation) choix_selection).transformer(reader);
                } else {
                    throw new UnsupportedOperationException();
                }

            } catch (Exception e) {
                continue;
            }
        }

        System.out.println("FIN : " + lieu.nom);
    }
}
