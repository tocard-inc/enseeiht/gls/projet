import java.util.List;

public class Chemin {
    String nom;
    Lieu lieuIn;
    Lieu lieuOut;
    Condition ouvert;
    Condition visible;
    Condition obligatoire;
    List<Connaissance> connaissancesRecus;
    List<Objet> objetsRecus;
    List<Objet> objetsConso;
    List<Description> descriptions;

    public Chemin(
            String nom,
            Lieu lieuIn,
            Lieu lieuOut,
            Condition ouvert,
            Condition visible,
            Condition obligatoire,
            List<Connaissance> connaissancesRecus,
            List<Objet> objetsRecus,
            List<Objet> objetsConso,
            List<Description> descriptions) {
        this.nom = nom;
        this.lieuIn = lieuIn;
        this.lieuOut = lieuOut;
        this.ouvert = ouvert;
        this.visible = visible;
        this.obligatoire = obligatoire;
        this.connaissancesRecus = connaissancesRecus;
        this.objetsRecus = objetsRecus;
        this.objetsConso = objetsConso;
        this.descriptions = descriptions;

    }

    @Override
    public String toString() {
        for (Description d : descriptions) {
            if (d.condition.evaluer()) {
                return this.nom + " (" + d + ")";
            }
        }
        return "(no description)";
    }

    public Lieu emprunter() {
        for (Objet o : this.objetsRecus) {
            Jeu.explorateur.objets.add(o);
        }
        for (Objet o : this.objetsConso) {
            Jeu.explorateur.objets.remove(o);
        }
        for (Connaissance c : this.connaissancesRecus) {
            if (!Jeu.explorateur.connaissances.contains(c)) {
                Jeu.explorateur.connaissances.add(c);
            }
        }
        return this.lieuOut;
    }

}
