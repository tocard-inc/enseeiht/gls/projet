import java.util.List;

public class Objet {
    String nom;
    int taille;
    Condition visible;
    List<Description> descriptions;

    public Objet(
            String nom,
            int taille,
            Condition visible,
            List<Description> descriptions) {
        this.nom = nom;
        this.taille = taille;
        this.visible = visible;
        this.descriptions = descriptions;
    }

    @Override
    public String toString() {
        for (Description d : this.descriptions) {
            if (d.condition.evaluer()) {
                return this.nom + " (" + d + ")";
            }
        }
        return "(no description)";
    }

}
