import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import static java.util.Map.entry;

public class Prototype {
public static void main(String[] args) {

// Objets
Map<String, Objet> jeu_objets = Map.ofEntries(
	entry(
		"warpToken",
		new Objet(
			"warpToken",
			1,
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							true
						
						),
						new ConditionBoolean(
							true
						
						),
						new ConditionBoolean(
							true
						
						)
					),
					List.of(
						new ConditionBoolean(
							true
						
						),
						new ConditionBoolean(
							false
						
						)
					),
					List.of(
						new ConditionBoolean(
							false
						
						)
					)
				)
			),
			List.of(
				new Description(
					"Description1",
					"warpToken description",
					new Condition(
						List.of(
							List.of(
								new ConditionBoolean(
									true
								
								)
							)
						)
					)
				)
			)
		)
	),
	entry(
		"warpTicket",
		new Objet(
			"warpTicket",
			1,
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							true
						
						)
					)
				)
			),
			List.of(
				new Description(
					"Description1",
					"warpTicket description",
					new Condition(
						List.of(
							List.of(
								new ConditionBoolean(
									true
								
								)
							)
						)
					)
				)
			)
		)
	),
	entry(
		"warpReceipt",
		new Objet(
			"warpReceipt",
			1,
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							true
						
						)
					)
				)
			),
			List.of(
				new Description(
					"Description1",
					"warpReceipt description",
					new Condition(
						List.of(
							List.of(
								new ConditionBoolean(
									true
								
								)
							)
						)
					)
				)
			)
		)
	)
);

// Connaissances
Map<String, Connaissance> jeu_connaissances = Map.ofEntries(
);

// Transformations
Map<String, Transformation> jeu_transformations = Map.ofEntries(
	entry(
		"TestTransfo",
		new Transformation(
			"TestTransfo",
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							true
						
						)
					)
				)
			),
			List.of(
				jeu_objets.get("warpReceipt")
			),
			List.of(
				jeu_objets.get("warpTicket")
			)
		)
	)
);

// Explorateur
Jeu.explorateur = new Explorateur(
    5,
    new ArrayList<>(List.of(
	)),
    new ArrayList<>(List.of(
		jeu_objets.get("warpTicket")
	))
);

// Personnes
Map<String, Personne> jeu_personnes = Map.ofEntries(
	entry(
		"cashier",
		new Personne(
			"cashier",
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							true
						
						)
					)
				)
			),
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							false
						
						)
					)
				)
			),
			List.of(
				new Interaction(
					"Parler",
					new Condition(
						List.of(
							List.of(
								new ConditionBoolean(
									true
								
								)
							)
						)
					),
				    List.of(
					),
				    List.of(
					),
				    List.of(
					),
					List.of(
						new Action(
							"Acheter",
							new Condition(
								List.of(
									List.of(
										new ConditionObjet(
											jeu_objets.get("warpTicket"),
											"==",
											1
										)
									)
								)
							),
							new Condition(
								List.of(
									List.of(
										new ConditionBoolean(
											true
										
										)
									)
								)
							),
						    List.of(
							),
						    List.of(
								jeu_objets.get("warpToken"),
								jeu_objets.get("warpReceipt")
							),
						    List.of(
								jeu_objets.get("warpTicket")
							),
							List.of(
								new Description(
									"Description1",
									"Acheter un ticket",
									new Condition(
										List.of(
											List.of(
												new ConditionBoolean(
													true
												
												)
											)
										)
									)
								)
							)
						),
						new Action(
							"Quitter",
							new Condition(
								List.of(
									List.of(
										new ConditionBoolean(
											true
										
										)
									)
								)
							),
							new Condition(
								List.of(
									List.of(
										new ConditionBoolean(
											true
										
										)
									)
								)
							),
						    List.of(
							),
						    List.of(
							),
						    List.of(
							),
							List.of(
								new Description(
									"Description1",
									"Quitter",
									new Condition(
										List.of(
											List.of(
												new ConditionBoolean(
													true
												
												)
											)
										)
									)
								)
							)
						)
					)
				)
			)
		)
	)
);

// Lieux
Map<String, Lieu> territoire_lieux = Map.ofEntries(
	entry(
		"preWarp",
		new Lieu(
			"preWarp",
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							false
						
						)
					)
				)
			),
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							true
						
						)
					)
				)
			),
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							false
						
						)
					)
				)
			),
		    List.of(
				jeu_personnes.get("cashier")
			),
		   	List.of(
		   		new Description(
		   			"Description1",
		   			"preWarp description",
		   			new Condition(
		   				List.of(
		   					List.of(
		   						new ConditionBoolean(
		   							true
		   						
		   						)
		   					)
		   				)
		   			)
		   		)
		   	),
		    new ArrayList<>(List.of(
			)),
		    List.of(
			)
		)
	),
	entry(
		"postWarp",
		new Lieu(
			"postWarp",
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							true
						
						)
					)
				)
			),
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							false
						
						)
					)
				)
			),
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							false
						
						)
					)
				)
			),
		    List.of(
			),
		   	List.of(
		   		new Description(
		   			"Description1",
		   			"postWarp description",
		   			new Condition(
		   				List.of(
		   					List.of(
		   						new ConditionBoolean(
		   							true
		   						
		   						)
		   					)
		   				)
		   			)
		   		)
		   	),
		    new ArrayList<>(List.of(
			)),
		    List.of(
			)
		)
	),
	entry(
		"END",
		new Lieu(
			"END",
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							false
						
						)
					)
				)
			),
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							false
						
						)
					)
				)
			),
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							true
						
						)
					)
				)
			),
		    List.of(
			),
		   	List.of(
		   		new Description(
		   			"Description1",
		   			"END description",
		   			new Condition(
		   				List.of(
		   					List.of(
		   						new ConditionBoolean(
		   							true
		   						
		   						)
		   					)
		   				)
		   			)
		   		)
		   	),
		    new ArrayList<>(List.of(
			)),
		    List.of(
			)
		)
	)
);

// Chemins
Map<String, Chemin> territoire_chemins = Map.ofEntries(
	entry(
		"Warp",
		new Chemin(
			"Warp",
			territoire_lieux.get("preWarp"),
			territoire_lieux.get("postWarp"),
			new Condition(
				List.of(
					List.of(
						new ConditionObjet(
							jeu_objets.get("warpToken"),
							">",
							0
						)
					)
				)
			),
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							true
						
						)
					)
				)
			),
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							false
						
						)
					)
				)
			),
		    List.of(
			),
		    List.of(
			),
		    List.of(
				jeu_objets.get("warpToken")
			),
		   	List.of(
		   		new Description(
		   			"DescriptionToken",
		   			"Warp description (need token)",
		   			new Condition(
		   				List.of(
		   					List.of(
		   						new ConditionObjet(
		   							jeu_objets.get("warpToken"),
		   							"==",
		   							0
		   						)
		   					)
		   				)
		   			)
		   		),
		   		new Description(
		   			"DescriptionNoToken",
		   			"Warp description (token acquired)",
		   			new Condition(
		   				List.of(
		   					List.of(
		   						new ConditionObjet(
		   							jeu_objets.get("warpToken"),
		   							"==",
		   							1
		   						)
		   					)
		   				)
		   			)
		   		)
		   	)
		)
	),
	entry(
		"EndChemin",
		new Chemin(
			"EndChemin",
			territoire_lieux.get("postWarp"),
			territoire_lieux.get("END"),
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							true
						
						)
					)
				)
			),
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							true
						
						)
					)
				)
			),
			new Condition(
				List.of(
					List.of(
						new ConditionBoolean(
							false
						
						)
					)
				)
			),
		    List.of(
			),
		    List.of(
			),
		    List.of(
			),
		   	List.of(
		   		new Description(
		   			"DescriptionToken",
		   			"END description",
		   			new Condition(
		   				List.of(
		   					List.of(
		   						new ConditionBoolean(
		   							true
		   						
		   						)
		   					)
		   				)
		   			)
		   		)
		   	)
		)
	)
);

// Territoire
Territoire jeu_territoire = new Territoire(territoire_lieux, territoire_chemins);

// Jeu
Jeu ze_GAME = new Jeu(
	jeu_territoire,
	jeu_objets,
	jeu_connaissances,
	jeu_personnes,
	jeu_transformations
);

ze_GAME.jouer();

}
}
