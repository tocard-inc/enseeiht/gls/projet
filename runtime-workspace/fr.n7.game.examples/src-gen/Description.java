public class Description {
    String texte;
    Condition condition;
    String nom;

    public Description(
            String nom,
            String texte,
            Condition condition) {
        this.nom = nom;
        this.texte = texte;
        this.condition = condition;
    }

    @Override
    public String toString() {
        return this.texte;
    }

}
