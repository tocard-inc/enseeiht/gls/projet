import java.util.List;
import java.util.ArrayList;
import java.io.BufferedReader;

public class Interaction {
    String nom;
    Condition visible;
    List<Connaissance> connaissances;
    List<Objet> objetsRecus;
    List<Objet> objetsConso;
    List<Action> actions;

    public Interaction(
            String nom,
            Condition visible,
            List<Connaissance> connaissances,
            List<Objet> objetsRecus,
            List<Objet> objetsConso,
            List<Action> actions) {
        this.nom = nom;
        this.visible = visible;
        this.connaissances = connaissances;
        this.objetsRecus = objetsRecus;
        this.objetsConso = objetsConso;
        this.actions = actions;
    }

    void interragir(BufferedReader reader) {

        for (Objet o : this.objetsRecus) {
            Jeu.explorateur.objets.add(o);
        }
        for (Objet o : this.objetsConso) {
            Jeu.explorateur.objets.remove(o);
        }
        for (Connaissance c : this.connaissances) {
            if (!Jeu.explorateur.connaissances.contains(c)) {
                Jeu.explorateur.connaissances.add(c);
            }
        }

        while (true) {
            Jeu.clearScreen();
            List<Action> interaction_choix = new ArrayList<>();
            for (Action a : this.actions) {
                if (a.visible.evaluer()) {
                    System.out.println("[" + interaction_choix.size() + "] " + a);
                    interaction_choix.add(a);
                }
            }
            System.out.print("\nChoix : ");

            int choix = 0;
            Action a = null;
            try {
                choix = Integer.parseInt(reader.readLine());
                a = interaction_choix.get(choix);
            } catch (Exception e) {
                continue;
            }

            a.actionner();

            if (a.finInterraction.evaluer())
                break;
        }
    }

}
