import java.util.Map;

public class Territoire {
    Map<String, Lieu> lieux;
    Map<String, Chemin> chemins;

    public Territoire(
            Map<String, Lieu> lieux,
            Map<String, Chemin> chemins) {
        this.lieux = lieux;
        this.chemins = chemins;
    }
}
