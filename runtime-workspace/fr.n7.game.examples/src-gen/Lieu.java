import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

public class Lieu {
    String nom;
    Condition deposable;
    Condition depart;
    Condition fin;
    List<Personne> personnes;
    List<Description> descriptions;
    List<Objet> objets;
    List<Connaissance> connaissances;

    public Lieu(
            String nom,
            Condition deposable,
            Condition depart,
            Condition fin,
            List<Personne> personnes,
            List<Description> descriptions,
            List<Objet> objets,
            List<Connaissance> connaissances) {
        this.nom = nom;
        this.deposable = deposable;
        this.depart = depart;
        this.fin = fin;
        this.personnes = personnes;
        this.descriptions = descriptions;
        this.objets = objets;
        this.connaissances = connaissances;
    }

    @Override
    public String toString() {
        return nom;
    }

    public void interragir(BufferedReader reader) {
        while (true) {

            Jeu.clearScreen();
            List<Objet> actions_choix = new ArrayList<>();
            for (Objet o : this.objets) {
                System.out.println("[" + actions_choix.size() + "] Ramasser " + o);
                actions_choix.add(o);
            }
            for (Objet o : Jeu.explorateur.objets) {
                System.out.println("[" + actions_choix.size() + "] Déposer " + o);
                actions_choix.add(o);
            }
            System.out.print("\nChoix : ");

            int choix = 0;
            Objet obj = null;
            try {
                choix = Integer.parseInt(reader.readLine());
                obj = actions_choix.get(choix);
            } catch (Exception e) {
                continue;
            }

            if (choix < this.objets.size()) {
                Jeu.explorateur.objets.add(obj);
                this.objets.remove(obj);
                break;
            } else if (choix < actions_choix.size()) {
                this.objets.add(obj);
                Jeu.explorateur.objets.remove(obj);
                break;
            }
        }
    }
}
