import java.io.BufferedReader;
import java.util.List;

public class Transformation {
    String nom;
    Condition possible;
    List<Objet> objetsSources;
    List<Objet> objetsResultats;

    public Transformation(
            String nom,
            Condition possible,
            List<Objet> objetsSources,
            List<Objet> objetsResultats) {
        this.nom = nom;
        this.possible = possible;
        this.objetsSources = objetsSources;
        this.objetsResultats = objetsResultats;
    }

    public void transformer(BufferedReader reader) {
        Jeu.clearScreen();
        for (Objet objet : objetsResultats) {
            Jeu.explorateur.objets.add(objet);
        }
        for (Objet objet : objetsSources) {
            Jeu.explorateur.objets.remove(objet);
        }
    }

    @Override
    public String toString() {
        return "Transformation " + this.nom;
    }

    public boolean possible() {
        return this.possible.evaluer() && Jeu.explorateur.objets.containsAll(this.objetsSources);
    }

}
