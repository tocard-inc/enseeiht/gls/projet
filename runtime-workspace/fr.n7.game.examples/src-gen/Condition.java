import java.util.List;

public class Condition {

    List<List<ConditionTest>> conditionsNormaleDisjonctive;

    public Condition(List<List<ConditionTest>> conditions) {
        this.conditionsNormaleDisjonctive = conditions;
    }

    public Boolean evaluer() {

        return conditionsNormaleDisjonctive.stream()
                .anyMatch(
                        conditionOU -> conditionOU.stream()
                                .allMatch(c -> c.evaluer()));
    }

}
