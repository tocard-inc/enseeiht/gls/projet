import java.util.List;

public class Action {
    Condition visible;
    List<Connaissance> connaissances;
    List<Objet> objetsRecus;
    List<Objet> objetsConso;
    Condition finInterraction;
    List<Description> descriptions;

    public Action(
            Condition visible,
            List<Connaissance> connaissances,
            List<Objet> objetsRecus,
            List<Objet> objetsConso,
            Condition finInterraction,
            List<Description> descriptions) {
        this.visible = visible;
        this.connaissances = connaissances;
        this.objetsRecus = objetsRecus;
        this.objetsConso = objetsConso;
        this.finInterraction = finInterraction;
        this.descriptions = descriptions;
    }

    void actionner() {
        for (Connaissance c : this.connaissances) {
            if (!Jeu.explorateur.connaissances.contains(c)) {
                Jeu.explorateur.connaissances.add(c);
            }
        }
        for (Objet o : this.objetsRecus) {
            Jeu.explorateur.objets.add(o);
        }
        for (Objet o : this.objetsConso) {
            Jeu.explorateur.objets.remove(o);
        }
    }

    @Override
    public String toString() {
        for (Description d : this.descriptions) {
            if (d.condition.evaluer()) {
                return d.toString();
            }
        }
        return "No desc";
    }
}
