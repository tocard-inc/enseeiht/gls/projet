import java.util.List;
import java.util.ArrayList;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

public class Interaction {
    Condition visible;
    List<Connaissance> connaissances;
    List<Objet> objetsRecus;
    List<Objet> objetsConso;
    List<Action> actions;

    public Interaction(
            Condition visible,
            List<Connaissance> connaissances,
            List<Objet> objetsRecus,
            List<Objet> objetsConso,
            List<Action> actions) {
        this.visible = visible;
        this.connaissances = connaissances;
        this.objetsRecus = objetsRecus;
        this.objetsConso = objetsConso;
        this.actions = actions;
    }

    void interragir(BufferedReader reader, Lieu lieu) {
        boolean arreter_interraction = false;

        while (!arreter_interraction) {
            System.out.println(this);
            System.out.print("\nChoix : ");

            List<Action> actions_choix = new ArrayList<>();
            for (Action a : this.actions) {
                if (a.visible.evaluer()) {
                    actions_choix.add(a);
                }
            }
            int choix = 0;
            Action a = null;
            try {
                choix = Integer.parseInt(reader.readLine());
                a = actions_choix.get(choix);
            } catch (NumberFormatException e) {
                continue;
            } catch (IndexOutOfBoundsException e) {
                continue;
            } catch (IOException e) {
                e.printStackTrace();
            }
            a.actionner();

            arreter_interraction = a.finInterraction.evaluer();
        }
    }

    @Override
    public String toString() {
        String res = "";
        int k = 0;
        for (Action a : this.actions) {
            if (a.visible.evaluer()) {
                res += "[" + k + "] " + a + "\n";
                k++;
            }
        }
        return res;
    }
}
