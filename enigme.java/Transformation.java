import java.util.List;

public class Transformation {
    Condition possible;
    List<Objet> objetsSources;
    List<Objet> objetsResultats;

    public Transformation(
            Condition possible,
            List<Objet> objetsSources,
            List<Objet> objetsResultats) {
        this.possible = possible;
        this.objetsSources = objetsSources;
        this.objetsResultats = objetsResultats;
    }
}
