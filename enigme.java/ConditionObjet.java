public class ConditionObjet implements ConditionTest {
    Objet objet;
    String operateur;
    int nombre;

    public ConditionObjet(
            Objet objet,
            String operateur,
            int nombre) {
        this.objet = objet;
        this.operateur = operateur;
        this.nombre = nombre;
    }

    public Boolean evaluer() {
        int compteur = 0;
        for (Objet obj : Jeu.explorateur.objets) {
            if (obj.equals(this.objet)) {
                compteur++;
            }
        }

        if (this.operateur.equals("<")) {
            return compteur < nombre;
        } else if (this.operateur.equals(">")) {
            return compteur > nombre;
        } else if (this.operateur.equals("==")) {
            return compteur == nombre;
        } else if (this.operateur.equals("<=")) {
            return compteur <= nombre;
        } else if (this.operateur.equals(">=")) {
            return compteur >= nombre;
        } else if (this.operateur.equals("!=")) {
            return compteur != nombre;
        } else {
            throw new Error("dafuk");
        }
    }
}
