import java.util.List;

public class Lieu {
    String nom;
    Condition deposable;
    Condition depart;
    Condition fin;
    List<Personne> personnes;
    List<Description> descriptions;
    List<Objet> objets;
    List<Connaissance> connaissances;

    public Lieu(
            String nom,
            Condition deposable,
            Condition depart,
            Condition fin,
            List<Personne> personnes,
            List<Description> descriptions,
            List<Objet> objets,
            List<Connaissance> connaissances) {
        this.nom = nom;
        this.deposable = deposable;
        this.depart = depart;
        this.fin = fin;
        this.personnes = personnes;
        this.descriptions = descriptions;
        this.objets = objets;
        this.connaissances = connaissances;
    }

    @Override
    public String toString() {
        return nom;
    }
}
