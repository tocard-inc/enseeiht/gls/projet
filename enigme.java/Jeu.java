import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Jeu {
    public static Explorateur explorateur;
    Territoire territoire;
    List<Objet> objets;
    List<Connaissance> connaissances;
    List<Personne> personnes;
    List<Transformation> transformations;

    public Jeu(
            Territoire territoire,
            List<Objet> objets,
            List<Connaissance> connaissances,
            List<Personne> personnes,
            List<Transformation> transformations) {
        this.territoire = territoire;
        this.objets = objets;
        this.connaissances = connaissances;
        this.personnes = personnes;
        this.transformations = transformations;
    }

    void jouer() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Lieu lieu = null;
        for (Lieu l : territoire.lieux) {
            if (l.depart.evaluer()) {
                lieu = l;
                break;
            }
        }

        while (!lieu.fin.evaluer()) {
            boolean recommencer = false;

            System.out.println("\n\n\n\n\n\n\n\n\n\nLieu : " + lieu + "\n");
            System.out.println(Jeu.explorateur);

            for (Personne p : lieu.personnes) {
                if (p.visible.evaluer() && p.obligatoire.evaluer()) {
                    System.out.println(p + " :");

                    p.interragir(reader, lieu);
                    recommencer = true;
                    break;
                }
            }
            if (recommencer) {
                continue;
            }

            for (Chemin c : territoire.chemins) {
                if (c.lieuIn == lieu) {
                    if (c.visible.evaluer() && c.obligatoire.evaluer() && c.ouvert.evaluer()) {
                        lieu = c.lieuOut;
                        recommencer = true;
                        break;
                    }
                }
            }
            if (recommencer) {
                continue;
            }

            int k = 0;
            List<Chemin> chemins_choix = new ArrayList<>();
            for (Chemin c : territoire.chemins) {
                if (c.lieuIn == lieu) {
                    if (c.visible.evaluer() && c.ouvert.evaluer()) {
                        chemins_choix.add(c);
                        System.out.println("[" + k + "] " + c);
                        k++;
                    }
                }
            }

            List<Personne> personnes_choix = new ArrayList<>();
            for (Personne p : personnes) {
                if (lieu.personnes.contains(p)) {
                    if (p.visible.evaluer()) {
                        personnes_choix.add(p);
                        System.out.println("[" + k + "] " + p);
                        k++;
                    }
                }
            }

            int choix = 0;
            try {
                System.out.print("\nChoix : ");
                choix = Integer.parseInt(reader.readLine());
                if (choix < chemins_choix.size()) {
                    lieu = chemins_choix.get(choix).lieuOut;
                } else {
                    Personne p = personnes_choix.get(choix - chemins_choix.size());

                    System.out.println("\n\n\n\n\n\n\n\n\n\nLieu : " + lieu + "\n");
                    System.out.println(Jeu.explorateur);
                    System.out.println(p + " :");

                    p.interragir(reader, lieu);
                }
            } catch (NumberFormatException e) {
                continue;
            } catch (IndexOutOfBoundsException e) {
                continue;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println("FIN : " + lieu.nom);
    }

    public static void main(String[] args) {

        // raccourcis "true" "false"
        Condition faux = new Condition(new ConditionEt(new ConditionBoolean(false)));
        Condition vraie = new Condition(new ConditionEt(new ConditionBoolean(true)));

        // "Objets": [

        List<Objet> jeu_objets = new ArrayList<>();

        Objet tentative_objet = new Objet(
                "tentative",
                1,
                vraie,
                new ArrayList<>());
        jeu_objets.add(tentative_objet);

        // "Connaissances" : [

        List<Connaissance> jeu_connaissances = new ArrayList<>();

        Connaissance reussite_connaissance = new Connaissance(
                "Réussite",
                vraie,
                new ArrayList<>());
        jeu_connaissances.add(reussite_connaissance);

        // Conditions

        Condition condition0 = new Condition(new ConditionEt(
                new ConditionConnaissance(reussite_connaissance, true),
                new ConditionObjet(tentative_objet, ">", 0)));
        Condition condition1 = new Condition(new ConditionEt(
                new ConditionObjet(tentative_objet, ">", 0)));
        Condition condition2 = new Condition(new ConditionEt(
                new ConditionConnaissance(reussite_connaissance, true)));

        // "Transformations" : [

        List<Transformation> transformations = new ArrayList<>();

        // "Explorateur" : [

        int explorateur_tailleInventaire = 3;

        List<Objet> explorateur_inventaire = new ArrayList<>();
        explorateur_inventaire.add(tentative_objet);
        explorateur_inventaire.add(tentative_objet);
        explorateur_inventaire.add(tentative_objet);

        List<Connaissance> explorateur_connaissances = new ArrayList<>();

        Jeu.explorateur = new Explorateur(
                explorateur_tailleInventaire,
                explorateur_connaissances,
                explorateur_inventaire);

        // "Personnes" : [

        List<Personne> jeu_personnes = new ArrayList<>();

        List<Interaction> sphinx_interactions = new ArrayList<>();

        List<Action> sphinx_interaction_actions = new ArrayList<>();

        List<Connaissance> connaissances_action_reussite = new ArrayList<>();

        connaissances_action_reussite.add(reussite_connaissance);

        List<Description> action_reussite_descriptions = new ArrayList<>();

        action_reussite_descriptions.add(new Description(
                "Wsh jveux réussir, rwwwwwit *TOCC* NOICE",
                vraie));

        Action action_reussite = new Action(
                vraie,
                connaissances_action_reussite,
                new ArrayList<>(),
                new ArrayList<>(),
                vraie,
                action_reussite_descriptions);
        sphinx_interaction_actions.add(action_reussite);

        List<Objet> objets_action_echec = new ArrayList<>();

        objets_action_echec.add(tentative_objet);

        List<Description> action_echec_descriptions = new ArrayList<>();

        action_echec_descriptions.add(new Description(
                "Vive la défaite ! dommage (PS: JSON c B1 hein) :)",
                vraie));

        Action action_echec = new Action(
                vraie,
                new ArrayList<>(),
                new ArrayList<>(),
                objets_action_echec,
                vraie,
                action_echec_descriptions);
        sphinx_interaction_actions.add(action_echec);

        Interaction sphinx_interaction = new Interaction(
                vraie,
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                sphinx_interaction_actions);
        sphinx_interactions.add(sphinx_interaction);

        Personne sphinx_personne = new Personne(
                "Sphinx",
                condition0,
                vraie,
                sphinx_interactions);
        jeu_personnes.add(sphinx_personne);

        // "Territoire" : {

        List<Lieu> lieux = new ArrayList<>();

        List<Personne> debut_personnes = new ArrayList<>();
        debut_personnes.add(sphinx_personne);

        Lieu enigme = new Lieu(
                "Énigme",
                faux,
                vraie,
                faux,
                debut_personnes,
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>());
        lieux.add(enigme);

        Lieu succes = new Lieu(
                "Succès",
                faux,
                faux,
                vraie,
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>());
        lieux.add(succes);

        Lieu echec = new Lieu(
                "Échec",
                faux,
                faux,
                vraie,
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>());
        lieux.add(echec);

        List<Chemin> chemins = new ArrayList<>();

        List<Description> descriptions_chemin_enigme_succes = new ArrayList<>();

        descriptions_chemin_enigme_succes.add(new Description("Route du succes", vraie));

        Chemin enigme_succes = new Chemin(
                enigme,
                succes,
                vraie,
                condition1,
                vraie,
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                descriptions_chemin_enigme_succes);
        chemins.add(enigme_succes);

        List<Description> descriptions_chemin_enigme_echec = new ArrayList<>();

        descriptions_chemin_enigme_echec.add(new Description("Chemin de la mort", vraie));

        Chemin enigme_echec = new Chemin(
                enigme,
                echec,
                vraie,
                condition2,
                vraie,
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                descriptions_chemin_enigme_echec);
        chemins.add(enigme_echec);

        Territoire territoire = new Territoire(
                lieux,
                chemins);

        Jeu jeu = new Jeu(
                territoire,
                jeu_objets,
                jeu_connaissances,
                jeu_personnes,
                transformations);

        jeu.jouer();

    }

}
