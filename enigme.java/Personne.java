import java.util.List;
import java.io.InputStreamReader;
import java.io.BufferedReader;

public class Personne {
    String nom;
    Condition visible;
    Condition obligatoire;
    List<Interaction> interactions;

    public Personne(
            String nom,
            Condition visible,
            Condition obligatoire,
            List<Interaction> interactions) {
        this.nom = nom;
        this.visible = visible;
        this.obligatoire = obligatoire;
        this.interactions = interactions;
    }

    void interragir(BufferedReader reader, Lieu lieu) {
        for (Interaction i : this.interactions) {
            if (i.visible.evaluer()) {
                i.interragir(reader, lieu);
            }
            break;
        }
    }

    @Override
    public String toString() {
        return nom;
    }
}
