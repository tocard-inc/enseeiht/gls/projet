public class Description {
    String texte;
    Condition condition;

    public Description(
            String texte,
            Condition condition) {
        this.texte = texte;
        this.condition = condition;
    }

    @Override
    public String toString() {
        return this.texte;
    }
}
