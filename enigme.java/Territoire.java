import java.util.List;

public class Territoire {
    List<Lieu> lieux;
    List<Chemin> chemins;

    public Territoire(
            List<Lieu> lieux,
            List<Chemin> chemins) {
        this.lieux = lieux;
        this.chemins = chemins;
    }
}
