import java.util.List;

public class Connaissance {
    String nom;
    Condition visible;
    List<Description> descriptions;

    public Connaissance(
            String nom,
            Condition visible,
            List<Description> descriptions) {
        this.nom = nom;
        this.visible = visible;
        this.descriptions = descriptions;
    }

    @Override
    public String toString() {
        for (Description d : this.descriptions) {
            if (d.condition.evaluer()) {
                return "(" + this.nom + " : " + d + ")";
            }
        }
        return "(" + this.nom + ")";
    }
}
