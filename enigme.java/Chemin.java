import java.util.List;

public class Chemin {
    Lieu lieuIn;
    Lieu lieuOut;
    Condition ouvert;
    Condition visible;
    Condition obligatoire;
    List<Connaissance> connaissancesRecus;
    List<Objet> objetsRecus;
    List<Objet> objetsConso;
    List<Description> descriptions;

    public Chemin(
            Lieu lieuIn,
            Lieu lieuOut,
            Condition ouvert,
            Condition visible,
            Condition obligatoire,
            List<Connaissance> connaissancesRecus,
            List<Objet> objetsRecus,
            List<Objet> objetsConso,
            List<Description> descriptions) {
        this.lieuIn = lieuIn;
        this.lieuOut = lieuOut;
        this.ouvert = ouvert;
        this.visible = visible;
        this.obligatoire = obligatoire;
        this.connaissancesRecus = connaissancesRecus;
        this.objetsRecus = objetsRecus;
        this.objetsConso = objetsConso;
        this.descriptions = descriptions;

    }

    @Override
    public String toString() {
        for (Description d : descriptions) {
            if (d.condition.evaluer()) {
                return d.toString();
            }
        }
        return "No desc";
    }
}
