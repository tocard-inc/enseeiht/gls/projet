public class ConditionBoolean implements ConditionTest {
    Boolean bool;

    public ConditionBoolean(Boolean bool) {
        this.bool = bool;
    }

    public Boolean evaluer() {
        return this.bool;
    }
}
