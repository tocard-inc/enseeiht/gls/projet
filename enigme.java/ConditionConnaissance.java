public class ConditionConnaissance implements ConditionTest {

    Connaissance connaissance;
    Boolean negation;

    public ConditionConnaissance(
            Connaissance connaissance,
            Boolean negation) {
        this.connaissance = connaissance;
        this.negation = negation;
    }

    public Boolean evaluer() {
        if (Jeu.explorateur.connaissances.contains(this.connaissance)) {
            return !this.negation;
        } else {
            return this.negation;
        }
    }

}
