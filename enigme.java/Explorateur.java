import java.util.List;

public class Explorateur {
    int taille;
    List<Connaissance> connaissances;
    List<Objet> objets;

    public Explorateur(
            int taille,
            List<Connaissance> connaissances,
            List<Objet> objets) {
        this.taille = taille;
        this.connaissances = connaissances;
        this.objets = objets;
    }

    @Override
    public String toString() {
        String txt = "Objets :\n";
        for (Objet c : this.objets) {
            txt += c + " ";
        }
        txt += "\n\nConnaissances :\n";
        for (Connaissance c : this.connaissances) {
            txt += c + " ";
        }
        txt += "\n===================================================";
        return txt;
    }

}
