import java.util.List;
import java.util.ArrayList;

public class ConditionEt {

    List<ConditionTest> conditionTests;

    public ConditionEt(ConditionTest... conditionTests) {
        this.conditionTests = new ArrayList<>();
        for (ConditionTest c : conditionTests) {
            this.conditionTests.add(c);
        }
    }

    public Boolean evaluer() {
        for (ConditionTest cond : conditionTests) {
            if (!cond.evaluer()) {
                return false;
            }
        }
        return true;
    }

}
