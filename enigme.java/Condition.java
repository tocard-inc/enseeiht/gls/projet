import java.util.List;
import java.util.ArrayList;

public class Condition {

    List<ConditionEt> conditionEts;

    public Condition(ConditionEt... conditionEts) {
        this.conditionEts = new ArrayList<>();
        for (ConditionEt c : conditionEts) {
            this.conditionEts.add(c);
        }
    }

    public Boolean evaluer() {
        for (ConditionEt cond : conditionEts) {
            if (cond.evaluer()) {
                return true;
            }
        }
        return false;
    }

}
